import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'name'
})
export class NamePipe implements PipeTransform {

  array = [
    'PK',
    'Humpty Sharma',
    'Narendra Modi',
    'Kailash Satyarthi',
    'K. Radhakrishnan',
    'Amit Shah',
    'Arvind Kejriwal',
    'Kapil Sharma',
    'Shahrukh Khan',
    'Aamir Khan',
    'Baba Ramdev',
    'Sachin Tendulkar',
    'Honey Singh',
    'Chetan Bhagat',
    'Sundar Pichai',
    'Nandan Nilekani',
    'A.R. Rahman',
    'Virat Kohli',
    'Mary Kom',
    'Mukesh Ambani',
    'Rajat Sharma',
    'M.S. Dhoni',
    'Anil Ambani',
    'Amitabh Bachchan',
    'Satya Nadella',
    'Arnab Goswami',
    'Rahul Gandhi',
    'Saina Nehwal',
    'Jagat Janani',
    'Badrinath Bansal',
    'Steve Jobs',
    'Bill Gates',
    'Gabbar'
  ];

  transform(value: any, args?: any): any {
    const name = this.array[Math.floor(Math.random() * this.array.length)];
    return name;
  }
}
