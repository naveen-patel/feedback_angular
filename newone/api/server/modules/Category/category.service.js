import CategoryModel from "./category.model";
import ProductModel from "../Product/Product.model";
const fs = require('fs');
var _ = require("lodash");
var mongoose = require('mongoose');
import settings from '../../../settings';
let config = require('./../../../config/' + settings.environment + '.config').default;
const MEDIA = config.media;
const CategoryService = {
  getCategoryById: async id => {
    try {
    let category = await CategoryModel.findOne({_id: Object(id)});
      return category;
    } catch (error) {
      throw error;
    }
  },
  createCategory: async newCategory => {
    /////ok////
    try {
      var savedCategory = await new CategoryModel(newCategory).save();
      return savedCategory
    } catch (error) {
      throw error;
    }
  },
  updateCategory: async (data,id) => {
    try {
      const update = await CategoryModel.findOneAndUpdate({
        _id: id
      }, data, {
        strict: true,
        new: true
      });
      return update;
    } catch (error) {
      throw error;
    }
  },
  getAllCategory: async (searchkey,limit, skip) => {
    try {
      var json = {};
      
      const count = await CategoryModel.find({
        "category_name": {
            "$regex": searchkey,
            "$options": "i"
          }
      }).count();
      let cat;

      if(!limit){  
        cat = await CategoryModel.aggregate(
          [
          {
            '$lookup': {
              from: 'brand',
              localField: '_id',
              foreignField: 'categoryDetails.category',
              as: 'brands'
            }
          },{$match:{"category_name": {
            "$regex": searchkey,
            "$options": "i"
          }}},
          {$sort:{"priority":1,"createdAt": 1}},
          {
            $project: {
              '_id':1,
              'category_name':1,
              'logo':1,
              'priority':1,
              'brand_id':'$brands._id',
              'brand_name':'$brands.brand_name'
            }
          }]
          );
      }else{
        cat = await CategoryModel.find({
          "category_name": {
            "$regex": searchkey,
            "$options": "i"
          }
        }).sort({"priority":1,"createdAt": 1}).limit(limit).skip(skip).lean();
      }
      var categoryArray = []
      cat.forEach(element => {
        if (element.logo && element.logo !== "") {
          element.logo = config.api_end_point + element.logo;
        } else {
          element.logo = "";
        }
       
        categoryArray.push(element)
      });
      json.totalcount = count;
      json.category = categoryArray;
      return json;
    } catch (error) {
      throw error;
    }
  },
  searchProductCategory :async (searchkey,category_id,limit,skip)=>{
    try {
      let searchQuery = {'$and':[{category_id:Object(category_id)},{product_name:{
        "$regex": searchkey,
        "$options": "i"
      }}]};
     let count = await ProductModel.find(searchQuery).count();
     let product = await ProductModel.find(searchQuery).sort({
      "createdAt": -1
    }).limit(limit).skip(skip).lean();
    let json ={};
    json.totalcount = count;
    json.product = product
    return json;
    } catch (error) {
      throw error;
    }
  },
  getBrandNameById: async (id) => {
  try {
    const Product = await ProductModel.find({category_id:Object(id)},
    {product_name:0,product_code:0,code:0,product_type:0,stock_location:0,fmcg_availability:0,expiry_date:0,ru_per_case:0,ean_code_ru:0,
      ean_code_case:0,language:0,origin:0,quantity:0,sales_per_case_aed:0,sales_per_case_usd:0,cost_price_per_case:0,
      description:0,size:0,product_front_image:0,product_back_image:0,supplier_id:0,category_id:0,_id:0,updatedAt:0,createdAt:0});
      return Product;

    // const Product = await CategoryModel.aggregate([
    //   {"$lookup":{
    //     from:'product',
    //     localField:'id',
    //     foreignField:'category_id',
    //     as:'products'
    //   }},
    //   {
    //     $unwind:{'$products'}
    //   },{
    //     $project:{
    //       'category_id':1,
    //       'category_name':1,
    //       'product_name':'$products.product_name',
    //       'product_id':'$products._id'
    //     }
    //   }
    // ]);
    // return Product;
  } catch (error) {
    throw error;
  }
  },

  deleteCategoryByIds: async (ids) => {
    try {
      var data = await CategoryModel.remove({
        _id: {
          $in: ids
        }
      });;
      return data;
    } catch (error) {
      throw error;
    }
  }
};
export default CategoryService;