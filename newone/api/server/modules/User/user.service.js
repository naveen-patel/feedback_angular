import User from "../User/user.model";
import DeviceModel from './user.device.model'; 

import logger from "../../../config/logger.config";
import sha256 from "sha256";
import { platform } from "os";
var uuidv1 = require('uuid/v1');
var mongoose = require('mongoose');

const UserService = {};

UserService.getAllUsers = async (searchkey, limit, skip, type, status) => {
  try {
    var json = {};
    let count,users;
    if(limit ==null){
      count = await User.find({
        "type": type
      }).count();
      users = await User.find({
        "type": type
      },{password:0}).sort({
        "createdAt": -1
      }).lean();
    }else{
   count = await User.find({
      "first_name": {
        "$regex": searchkey,
        "$options": "i"
      },
      "type": type,
      "status": status
    }).count();
    users = await User.find({
      "first_name": {
        "$regex": searchkey,
        "$options": "i"
      },
      "type": type,
      "status": status
    },{password:0}).sort({
      "createdAt": -1
    }).limit(limit).skip(skip).lean();
  }
    json.totalcount = count;
    json.users = users
    return json;
  } catch (error) {
    throw error;
  }
}
UserService.changeStatus = async (id, status) => {
  try {
    const update = await User.findOneAndUpdate({
      _id: id
    }, {
      status: status
    }, {
      strict: true,
      new: true
    });
    return update;
  } catch (error) {
    throw error;
  }
};
UserService.findAndUpdateDevice = async (device)=>{
  try { 
    let is = await DeviceModel.update({
      user_id: device.user_id,'device.device_id':device.device[0].device_id}, 
      { $set: 
        { "device.$.token" : device.device[0].token,
        "device.$.device_id" : device.device[0].device_id,
        "device.$.device_palform" : device.device[0].device_palform,
        }} );
        if(is.nModified===0){
          is = await DeviceModel.update({ user_id: device.user_id},{$push:{device:{$each:device.device}}});
        }
        return is
  } catch (error) {
    throw error
  }
};
UserService.addDevice = async (device)=>{
try {
  return await new DeviceModel(device).save();
} catch (error) {
  throw error
}
}
UserService.getUserByPhoneNumber = async (number,id) => {
  try {
    var obj = {
        phone_number: number
    }
    if(id!==''){
      var arr = [];
        var objId = mongoose.Types.ObjectId(id);
        arr.push(objId);
      obj._id = {"$nin": arr}
    }
    const user = await User.findOne(obj);
    return user;
  } catch (error) {
    throw error
  }
}

UserService.getUserByMobileNumber = async (mobile,id) => {
  try {
    var obj = {
        mobile_number: mobile
    }
    if(id!==''){
      var arr = [];
        var objId = mongoose.Types.ObjectId(id);
        arr.push(objId);
      obj._id = {"$nin": arr}
    }
    const user = await User.findOne(obj);
    return user;
  } catch (error) {
    throw error
  }
}

UserService.addUser = async user => {
  try {
    user.password = await sha256(user.password);
    let userToAdd = new User(user);
    const savedUser = await userToAdd.save();
    return savedUser;
  } catch (error) {
    throw error;
  }
};

UserService.getUserByUserId = async user_id => {
  try {
    const user = await User.findOne({
      _id: Object(user_id)
    });
    return user;
  } catch (error) {
    throw error;
  }
};

UserService.getLoginEmailOrPhoneNumber = async EmailId => {
  try {
    const user = await User.findOne({
      $or: [{
        email: new RegExp("^" + EmailId + "$", "i")
      }, {
        phone_number: EmailId
      }],
      type: 'user'
    });
    return user;
  } catch (error) {
    throw error;
  }
};
UserService.getUserByEmailId = async (EmailId,id) => {
  try {
    var  obj={
      email: new RegExp("^" + EmailId + "$", "i"),
    }
    if(id!==''){
      var arr = [];
        var objId = mongoose.Types.ObjectId(id);
        arr.push(objId);
      obj._id = {"$nin": arr}
    }
    const user = await User.findOne(obj);
    return user;
  } catch (error) {
    throw error;
  }
};
UserService.getSuperAdmin = async EmailId => {
  try {
    const user = await User.findOne({
      email: new RegExp("^" + EmailId + "$", "i")
    });
    return user;
  } catch (error) {
    throw error;
  }
};


UserService.getSuperAdminByEmailId = async EmailId => {
  try {
    const user = await User.findOne({
      email: new RegExp("^" + EmailId + "$", "i"),
      //      type: 'super_admin'
    });
    return user;
  } catch (error) {
    throw error;
  }
};

UserService.getAllUserByType = async type => {
  try {
    const user = await User.find({
      type: type
    }).sort({
      "updatedAt": -1
    });
    return user;
  } catch (error) {
    throw error;
  }
};

UserService.updateBusinessAdminbyBussinessAdminId = async (user_id, data) => {
  try {
    const updatedUser = await User.findOneAndUpdate({
      _id: user_id
    }, data, {
      strict: true,
      new: true
    });
    return updatedUser;
  } catch (error) {
    throw error;
  }
};

UserService.updateUserById = async (user_id, data) => {
  try {
    const updatedUser = await User.findOneAndUpdate({
      _id: user_id
    }, data, {
      strict: true,
      new: true
    });
    return updatedUser;
  } catch (error) {
    throw error;
  }
};

UserService.getUserEmailAvailability = async email => {
  try {
    let user = await User.count({
        email: new RegExp("^" + email + "$", "i")
      },
      null,
      null
    );
    if (user) {
      return false;
    } else {
      return true;
    }
  } catch (error) {
    throw error;
  }
};

UserService.userChangePassword = async (user_id, data) => {
  try {
    data.password = await sha256(data.password);
    data.password_uuid = uuidv1();
    const updatedUser = await User.findOneAndUpdate({
      _id: user_id
    }, data, {
      strict: true,
      new: true
    });
    return updatedUser;
  } catch (error) {
    throw error;
  }
};

UserService.checkChangePassword = async (user_id, data) => {
  try {
    data.old_password = await sha256(data.old_password);
    const user = await User.count({
      _id: Object(user_id),
      "password": data.old_password
    });
    return ((user > 0) ? true : false);
  } catch (error) {
    throw error;
  }
};
UserService.deleteUser = async(id) => {
  try {
    const  data = await User.remove({
      _id: id
    });
    return data;
  } catch (error) {
    throw error;
  }
};

UserService.deleteDevice =async(id,device_id)=>{
try {
  const  data = await DeviceModel.update(
    { user_id: id,'device.device_id':device_id },
    { $pull: { 
    "device" : {"device_id":device_id},
  } }
  );
  return data;
} catch (error) {
  throw error;
}
};
UserService.getDevice = async()=>{
  try {
    return await DeviceModel.find({},{_id:0,updatedAt:0,createdAt:0,user_id:0,__v:0});
  } catch (error) {
    throw error
  }
}
export default UserService;