import OrderModel from "./order.model";
import ProductModel from "../Product/Product.model";
const fs = require('fs');
var mongoose = require('mongoose');
import settings from '../../../settings';
let config = require('./../../../config/' + settings.environment + '.config').default;
import moment from "moment";
import { stat } from "fs";

const OrderService = {
  getOrderById: async id => {
    try {
    let order = await OrderModel.findOne({_id:id})
                                .populate({
                                 path:'product.product_detail',
                                 select:'product_front_image product_back_image product_type quantity',
                                 populate:{
                                 path:'supplier_id brand',
                                 select:'name brand_name'
                                }
                              })  
              let p =[], obj ={};
              obj._id = order._id;
              obj.user_id = order.user_id;
              obj.order_number = order.order_number;
              obj.order_date = order.order_date;
              obj.total_amount_aed = order.total_amount_aed
              obj.total_amount_usd = order.total_amount_usd
              obj.order_status = order.order_status;
              obj.callback_request = order.callback_request; 
              if(order.product){           
           order.product.forEach(element => {
         let tempobj ={}
          tempobj.product_detail ={};
          tempobj.quantity = element.quantity;
          tempobj._id = element._id;
          tempobj.product_detail.deal ={}
          tempobj.product_detail.supplier_id ={}
          if(element.product_detail){
          tempobj.product_detail.supplier_id._id = element.product_detail.supplier_id._id
          tempobj.product_detail.supplier_id.name = element.product_detail.supplier_id.name;
          tempobj.product_detail.brand ={}
          tempobj.product_detail.brand._id = element.product_detail.brand._id
          tempobj.product_detail.brand.brand_name = element.product_detail.brand.brand_name
          tempobj.product_detail.brand.url = element.product_detail.brand.url
          tempobj.product_detail._id = element.product_detail._id
          tempobj.product_detail.sales_per_case_usd = element.sales_per_case_usd
          tempobj.product_detail.sales_per_case_aed = element.sales_per_case_aed
          tempobj.product_detail.discount_price_usd = element.discount_price_usd
          tempobj.product_detail.discount_price_aed = element.discount_price_aed
          tempobj.product_detail.product_front_image = element.product_detail.product_front_image
          tempobj.product_detail.product_back_image = element.product_detail.product_back_image;
          tempobj.product_detail.product_type = element.product_detail.product_type;
          tempobj.product_detail.quantity = element.product_detail.quantity;
          tempobj.product_detail.isAdd = element.isAdd;
          if(element&&element.deal&&element.deal.isActive){
            tempobj.product_detail.deal.isActive = element.deal.isActive;
              tempobj.product_detail.deal.offer_price_usd = element.deal.offer_price_usd
              tempobj.product_detail.deal.offer_price_aed = element.deal.offer_price_aed
              tempobj.product_detail.deal.deal_percentage = element.deal.deal_percentage;
          }else{
            tempobj.product_detail.deal.isActive = false;
              tempobj.product_detail.deal.offer_price_usd = null
              tempobj.product_detail.deal.offer_price_aed = null
              tempobj.product_detail.deal.deal_percentage = null;
          }
              
              
            }
           
            
          p.push(tempobj)
        });
      }
        obj.product = p            
      return obj;
    } catch (error) {
      throw error;
    }
  },
  
 
  createOrder: async newOrder => {
    try {
      var savedOrder = await new OrderModel(newOrder).save();
      return savedOrder
    } catch (error) {
      throw error;
    }
  },
  checkProdut:async id =>{
    try {
      return await OrderModel.findOne({'product.product_detail':{$in:Object(id)}})
    } catch (error) {
      throw error;
    }
  },
  updateOrderStatus: async (id,data,flag,pId) => {
    try {
      let update;
      if(flag==1){ // add to array
        update = await OrderModel.update({_id: id},{$push:{product:{$each:data.product}},total_amount_aed:data.total_amount_aed,total_amount_usd:data.total_amount_usd});
      }else if(flag==2){ // edit object
      update = await OrderModel.findOneAndUpdate({
          _id: id
        }, data, {
          strict: true,
          new: true
        });
      }
      else{ //edit array
         update = await OrderModel.update({
        _id: id,'product.product_detail':pId}, 
        { $set: 
          { "product.$.discount_price_usd" : data.product[0].discount_price_usd,
          "product.$.discount_price_aed" : data.product[0].discount_price_aed,
          "product.$.sales_per_case_usd" : data.product[0].sales_per_case_usd,
          "product.$.sales_per_case_aed" : data.product[0].sales_per_case_aed ,
          "product.$.total_price_aed" : data.product[0].total_price_aed ,
          "product.$.total_price_usd" : data.product[0].total_price_usd ,
          "product.$.quantity" : data.product[0].quantity,
          "product.$.deal":data.product[0].deal,
          "product.$.isAdd":data.product[0].isAdd,
          "total_amount_aed" : data.total_amount_aed,
          "total_amount_usd" : data.total_amount_usd}} );
      }
     
      return update;
    } catch (error) {
      throw error;
    }
  },
  checkProduct:async(pId,oId)=>{
    try {
      return await OrderModel.findOne({_id:oId,'product.product_detail':pId});
    } catch (error) {
      throw error
    }
  },
  upDatePrice:async (id,usd,aed)=>{
    try {
       await OrderModel.update({_id: id},{$inc:{total_amount_aed:-aed,total_amount_usd:-usd}});
    } catch (error) {
      throw error;
    }
  },
  updateCallbackStatus: async (order_id,callback_request)=>{
    try {
      const update = await OrderModel.findOneAndUpdate({
        _id: order_id
      }, {callback_request:callback_request}, {
        upsert: true,
        new: true
      });
      return update;
    } catch (error) {
      throw error;
    }
  },

  getAllOrder: async (searchkey,limit, skip) => {
    try {
      let json = {};
      let findObj={};
     if(searchkey!=""){
        findObj = {
          "$or":[{"users.first_name": {
            "$regex": searchkey,
            "$options": "i"
            }},{"users.last_name": {
            "$regex": searchkey,
            "$options": "i"
            }},{"users.email": {
              "$regex": searchkey,
              "$options": "i"
              }},{"users.mobile_number": {
                "$regex": searchkey,
                "$options": "i"
                }},
                {"order_number": {
              "$regex": searchkey,
              "$options": "i"
              }}]       }
      }
      
    const order = await OrderModel.aggregate(
    [{
      '$lookup': {
        from: 'user',
        localField: 'user_id',
        foreignField: '_id',
        as: 'users'
      }
    },{
      $match: findObj
                },
    {$sort: {"createdAt": -1}},
    {
      '$facet': {
        metadata: [{
          $count: "total"
        }
      ],
      order_data: [{
          $skip: skip
        }, {
          $limit: limit
        }]
      }
    }, {
      $project: {
        "_id": 0,
        "totalcount": { $arrayElemAt: ["$metadata.total", 0] },
        "totalPage": { $ceil: { $divide: [{ $arrayElemAt: ["$metadata.total", 0] }, limit] } },
        "order_data": "$order_data"
      }
    }
  ]);
    json.totalpage = Math.ceil(order[0].totalcount / limit);
    json.totalcount = order[0].totalcount;
    json.order = order[0].order_data;
    return json;
    } catch (error) {
      throw error;
    }
  },
  getAllOrderByUser: async (user_id,isFilter,filter,limit,skip) => {
    try {
      let findObj={};
      findObj.user_id = user_id
      if(isFilter){
        let date = moment().subtract(filter, 'days');
        findObj.createdAt = {$gte:date};
        }
      let data =  await OrderModel.find(findObj)
      .populate({
        path:'product.product_detail',
        populate:{
        path:'brand deal',
        select:'brand_name deal_name deal_percentage actual_price_aed offer_price_aed actual_price_usd offer_price_usd'
       }
     }).sort({'createdAt':-1}).skip(skip).limit(limit).lean();
     let count =  await OrderModel.find(findObj)
     .populate({
       path:'product.product_detail',
       populate:{
       path:'brand deal',
       select:'brand_name deal_name deal_percentage actual_price_aed offer_price_aed actual_price_usd offer_price_usd'
      }
    }).count();
    return {order:data,count:count}
    } catch (error) {
      throw error;
    }
  },
  deleteOrderByIds: async (ids) => {
    try {
      var data = await OrderModel.remove({
        _id: {
          $in: ids
        }
      });;
      return data;
    } catch (error) {
      throw error;
    }
  },
  
  removeProductFromOrder :async (order_id,product_id)=>{
    try {
      return await OrderModel.update( {_id:order_id },
      { $pull: {"product": {"product_detail":product_id}}});
    } catch (error) {
      throw error
    }
  }
};
export default OrderService;