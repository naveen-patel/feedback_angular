import DashboardService from "./dashboard.service";
import UserService from "../User/user.service";

import logger from "../../../config/logger.config";
var _ = require("lodash");
import settings from '../../../settings';
let config = require('./../../../config/' + settings.environment + '.config').default;
const MEDIA = config.media;
let mkdirp = require('mkdirp');
var uuidv1 = require('uuid/v1');
const fs = require('fs');
const path = require('path');
var pdf = require('html-pdf');
let Promise = require('bluebird');
import moment from "moment";
const nodemailer = require("nodemailer");
const api_end_point = config.api_end_point;
import EmailService from "../CommonService/email.service";
import SettingsService from '../Settings/settings.service';
import { cpus } from "os";
import ProductService from "../Product/Product.service";
var pdfCount = 0,
  excelCount = 0;
var imgSrc = path.normalize('file://' + __dirname + '../../../../logo/logo.png');
var fmcg_logo = path.normalize('file://' + __dirname + '../../../../logo/FMCG_logo.png');


const DashboardController = {

  getDashboardDetails: async (req, res)=>{
    try {
    let limit = Number(req.query.limit) || 5;
    let offset = (req.query.skip) ? parseInt(req.query.skip) : 1
    let skip = (offset - 1) * limit;
    let selectedTab = Number(req.query.tabIndex) || 0;
    let result = await DashboardService.getDashboardDetails(limit,skip,selectedTab);
    logger.info('Dashboard info');
    return res.send({
      code: 200,
      status: "success",
      message: "Dashboard details listed",
      data: result
    });
    } catch (error) {
      return  res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting Dashboard details",
        data: {}
      });
    }
  },
  getOrderStatus : async (req,res)=>{
    try {
      let result = await DashboardService.getOrderStatus();
      return res.send({
        code: 200,
        status: "success",
        message: "Order status listed",
        data: result
      });
    } catch (error) {
      return  res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting Order status",
        data: {}
      });
    }
  }
};

export default DashboardController;