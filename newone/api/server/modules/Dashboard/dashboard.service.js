import OrderModel from "../Order/order.model";
import UserModel from "../User/user.model";

const DashboardService = {
  getDashboardDetails: async (skip,limit,tab) => {
    try {
      let result ={},totalOrder,pendingOrder,deliveredOrder,tabDetails;
          totalOrder = await OrderModel.find().count();
          pendingOrder = await OrderModel.find({order_status:{ $in:['order_in_progress','order_placed','dispatched']}}).count();
          deliveredOrder = await OrderModel.find({order_status:'delivered'}).count();
          let canclelOrder = await OrderModel.find({order_status:'cancel'}).count();
      switch (tab) {
        case 1:
          tabDetails = await OrderModel.find({order_status:{ $in:['order_in_progress','order_placed','dispatched']}})
                                        .populate({path:'user_id', select:"first_name last_name mobile_number email country_code"})
                                        .sort({"createdAt": -1}).limit(5);
          break;
        case 2:
          tabDetails = await UserModel.find({status:'active',type:'customer'},{password:0}).sort({"createdAt": -1}).limit(5);
            break;
      
        default:
          tabDetails = await OrderModel.find({order_status:'delivered'}).populate({path:'user_id', select:"first_name last_name mobile_number email country_code"}).sort({"createdAt": -1}).limit(5);
          break;
      }
      result.totalOrder = totalOrder;
      result.pendingOrder = pendingOrder;
      result.deliveredOrder = deliveredOrder;
      result.canclelOrder = canclelOrder;
      result.tabDetails = tabDetails;
      return result;
    } catch (error) {
      throw error;
    }
  },
  getOrderStatus : async ()=>{
    try {

      const data =  await OrderModel.aggregate([
        { "$facet": {
          "Total": [
            { "$match" : { "order_status": { "$exists": true }}},
            { "$count": "Total" },
          ],
          "dispatched": [
            { "$match" : {"order_status": { "$exists": true, "$in": ["dispatched"] }}},
            { "$count": "dispatched" }
          ],
          "order_in_progress": [
            { "$match" : {"order_status": { "$exists": true, "$in": ["order_in_progress"] }}},
            { "$count": "order_in_progress" }
          ],
          "delivered": [
            { "$match" : {"order_status": { "$exists": true, "$in": ["delivered"] }}},
            { "$count": "delivered" }
          ],
          "order_placed": [
            { "$match" : {"order_status": { "$exists": true, "$in": ["order_placed"] }}},
            { "$count": "order_placed" }
          ],
          "cancelled": [
            { "$match" : {"order_status": { "$exists": true, "$in": ["cancelled"] }}},
            { "$count": "cancelled" }
          ]
        }},
        { "$project": {
          "Total": { "$arrayElemAt": ["$Total.Total", 0] },
          "dispatched": { "$arrayElemAt": ["$dispatched.dispatched", 0] },
          "delivered": { "$arrayElemAt": ["$delivered.delivered", 0] },
          "order_in_progress": { "$arrayElemAt": ["$order_in_progress.order_in_progress", 0] },
          "order_placed": { "$arrayElemAt": ["$order_placed.order_placed", 0]} ,
          "cancelled": { "$arrayElemAt": ["$cancelled.cancelled", 0] }
        }}
      ])
      let json ={};
      json.Total = data[0].Total || 0
      json.dispatched = data[0].dispatched || 0;
      json.delivered = data[0].delivered || 0;
      json.order_in_progress = data[0].order_in_progress || 0;
      json.order_placed = data[0].order_placed || 0;
      json.cancel = data[0].cancelled || 0;
      return json;
    } catch (error) {
      throw error
    }
  }
};
export default DashboardService;