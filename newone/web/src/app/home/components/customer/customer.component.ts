import { Component, OnInit, ChangeDetectorRef, TemplateRef } from '@angular/core';
import { DataService } from "../../services/data.service";
import { CustomerService } from '../../services/customer.service';
import 'rxjs/Rx';
declare var $: any;
declare var require: any;
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

var _ = require('lodash');
@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {

  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    keyboard: false,
    class: 'modal-dialog'
  }
  modalRef: BsModalRef;
  firstName;
  lastName;
  email;
  phoneNumber;
  countryName;
  pendingStatus;
  perAddress;
  companyName;
  shippAddress;
  cityName;
  landlineNumber;


  imageinfo = {};
  modalRef2: BsModalRef;
  modalRef3: BsModalRef;
  modalRef4: BsModalRef;
  file_details: string;
  records = [];
  upload_records = [];
  openAddUser: boolean = false;
  parentmessage: any;
  loader: boolean = false;
  search: string = '';
  limit: any = 10;
  skip: any = 1;
  count: Number;
  Allrecords = [];
  id: string;
  active: boolean = false;
  remainingRecords: any = 0;
  tabChange: boolean = true;
  status: String = '';
  selectedRecord;
  country_name: string;
  country_code;
  flag = "pending";
  userCompleteRecord = [];
  fmcgForm: FormGroup;
  submitted: boolean = false;
  userId;

  namelist: boolean = true;
  flag1: boolean = false;
  suggestions = [];
  countries: any[];
  flags: boolean = false;



  constructor(
    public ref: ChangeDetectorRef,
    public ds: DataService,
    private modalService: BsModalService,
    public cs: CustomerService,
    public formBuilder: FormBuilder,
  ) { }


  ngOnInit() {
    this.suggestions = this.cs.countries;
    var initial = (this.skip === 1)
    this.cs.getAllcustomersList(this.search, this.limit, this.skip, "customer", this.getStatus()).subscribe(data => {
      this.loader = false;
      this.count = data.totalcount;
      //this.records = data.users;
      this.tabChange = false;
      this.remainingRecords = data.totalcount - (this.skip * this.limit);
      if (data.users.length !== 0) {
        this.country_code = data.users[0].country_code;
      }
      if (initial) {
        this.records = data.users;
        //this.userCompleteRecord = data.users;
      } else {
        this.records = this.records.concat(data.users);
        //this.userCompleteRecord = this.userCompleteRecord.concat(data.users);
      }
      // this.filterRecords(this.userCompleteRecord);
    })
  }

  // filterRecords(users) {
  //   this.records = [];
  //   switch (this.flag) {
  //     case 'pending': {
  //       console.log('pending and inactive')
  //       for (let record of users) {
  //         if (!record.email_confirmation && record.status === "in_active") {
  //           this.records.push(record);
  //         }
  //       }
  //     }
  //       break;
  //     case 'active': {
  //       console.log('active');
  //       for (let record of users) {
  //         if (record.status === "active") {
  //           this.records.push(record);
  //         }
  //       }
  //     }
  //       break;
  //     case 'inactive': {
  //       console.log('inactive')
  //       for (let record of users) {
  //         if (record.email_confirmation && record.status === "in_active") {
  //           this.records.push(record);
  //         }
  //       }
  //     }
  //   }
  // }



  //  for country name
  private countryCode(code) {
    return this.cs.countries.filter(countryItem => countryItem.countryCode === code)[0].name;
  }
  searchUser() {
    this.loader = true;
    this.skip = 1;
    this.cs.getAllcustomersList(this.search, this.limit, this.skip, "customer", this.getStatus()).subscribe(data => {
      this.loader = false;
      this.count = data.totalcount;
      this.remainingRecords = data.totalcount - (this.skip * this.limit);
      this.records = data.users;
    })
  }
  showpendigUsers() {
    this.search = "";
    this.skip = 1;
    this.active = false;
    this.flag = "pending";
    this.ngOnInit();
  }
  showInactiveUsers() {
    this.search = "";
    this.skip = 1;
    this.active = false;
    this.flag = "inactive";
    this.ngOnInit();
  }
  showactiveUsers() {
    this.search = "";
    this.skip = 1;
    this.active = true;
    this.flag = "active";
    this.ngOnInit();
  }
  reload() {
    this.search = "";
    this.skip = 1;
    this.ngOnInit();
  }
  getStatus() {
    if (this.flag == "active") {
      return "active";
    } else if (this.flag == "pending") {
      return 'pending';
    } else {
      return "in_active";
    }
  }
  pageChanged(event) {
    this.skip = event.page;
    this.ngOnInit();
  }
  openUser(record) {
    this.openAddUser = true;
    this.parentmessage = record
  }
  receiveMessage(event) {
    this.openAddUser = event[0];
    this.ngOnInit()
    // this.resetSearchAndSort();
  }
  DeleteRecord(template: TemplateRef<any>, i, id) {
    this.modalRef3 = this.modalService.show(template);
    this.id = id;
  }

  confirmToDelete() {

    // this.ds.deleteUser(this.id).subscribe(data => {
    this.cs.deleteCustomers(this.id).subscribe(data => {
      this.ds.successMessage('FMCG user Deleted Successfully')
      this.reload();
    })
    this.modalRef3.hide();
    this.modalRef3 = null;
  }
  changestatus(value, id) {
    // this.ds.changeStatus(value, id).subscribe(data => {
    this.cs.changeStatus(value, id).subscribe(data => {
      console.log('changestatus');

      this.ngOnInit();
    })
  }
  confirmToChange() {
    let status;
    //  var status = this.selectedRecord.status === ('active' || 'pending') ? 'in_active' : 'active';
    console.log('status', this.selectedRecord.status);

    if ((this.selectedRecord.status == 'in_active') || (this.selectedRecord.status == 'pending') || (this.selectedRecord.status == 'reject')) {
      status = "active";
    } else {
      status = "in_active";
    }
    //  for pending
    console.log(status, 'statussssss');

    if ((this.selectedRecord.status == 'active') || (this.selectedRecord.status == 'in_active')) {
      this.cs.changeStatus(status, this.selectedRecord._id).subscribe(data => {
        console.log('confirmToChange');
        this.skip = 1;
        this.ngOnInit();
        this.modalRef4.hide();
        this.modalRef4 = null;
      })
    } else {
      this.cs.changePendingStatus(status, this.selectedRecord._id).subscribe(data => {
        console.log('confirmToChange');
        this.skip = 1;
        this.ngOnInit();
        this.modalRef4.hide();
        this.modalRef4 = null;
      })
    }
    // this.ds.changeStatus(status, this.selectedRecord._id).subscribe(data => {

  }
  // for reject
  confirmToChangereject() {
    let status;
    status = "reject";

    this.cs.changePendingStatus(status, this.selectedRecord._id).subscribe(data => {
      this.skip = 1;
      this.ngOnInit();
      this.modalRef4.hide();
      this.modalRef4 = null;
    })
  }
  declineToChange() {
    console.log('decline')
    this.skip = 1;
    this.ngOnInit();
    this.modalRef4.hide();
    this.modalRef4 = null
  }
  changeStatus(template: TemplateRef<any>, data) {
    console.log('change status', data)
    this.selectedRecord = data;
    if (this.selectedRecord.status == 'active') {
      this.status = "the status of the user to In-Active ";
    } else if (this.selectedRecord.status == 'in_active') {
      this.status = "the status of the user to Active ";
    } else {
      this.status = "to approve user request ";
    }
    this.modalRef4 = this.modalService.show(template);
  }

  changeStatusReject(template: TemplateRef<any>, data) {
    console.log('change status', data)
    this.selectedRecord = data;
    this.status = "to reject user request ";
    this.modalRef4 = this.modalService.show(template);
  }

  loadMore() {
    this.skip++;
    this.ngOnInit();
  }
  check() {
    console.log('click');
  }

  openModal(template: TemplateRef<any>, i, type) {
    this.fmcgForm = this.formBuilder.group(this.getFormFields(i));
    this.modalRef = this.modalService.show(template, this.config);
    this.firstName = this.records[i].first_name;
    this.lastName = this.records[i].last_name;
    this.email = this.records[i].email;
    this.phoneNumber = this.records[i].mobile_number;
    this.countryName = this.countryCode(this.records[i].country_code);
    this.pendingStatus = this.records[i].status == 'in_active' ? 'In-active' : this.records[i].status;
    this.perAddress = this.records[i].permanent_address;
    this.shippAddress = this.records[i].shipping_address;
    this.companyName = this.records[i].company_name;
    this.cityName = this.records[i].city;
    this.landlineNumber = this.records[i].phone_number;
    this.country_code = this.records[i].country_code;
    this.userId = this.records[i]._id;
  }
  closeModal() {
    this.modalRef.hide();
    this.modalRef = null
  }

  // for excel pdf doenload 
  generateExcel() {
    this.cs.generateProductExcel().subscribe((data) => {
      window.open(data.path)
      this.ds.successMessage("Excel Downloaded Successfully");
    })
  }

  // for category form
  getFormFields(i) {
    return {
      phone_number: [this.records[i].mobile_number, Validators.required],
      country_code: [this.records[i].country_code, Validators.required],
    }
  }
  onSubmit() {
    this.submitted = true;
    console.log('call');
    let userDetails = {};
    userDetails["first_name"] = this.firstName;
    userDetails["last_name"] = this.lastName;
    userDetails["email"] = this.email;
    userDetails["permanent_address"] = this.perAddress;
    userDetails["shipping_address"] = this.shippAddress;
    userDetails["company_name"] = this.companyName;
    userDetails["mobile_number"] = this.phoneNumber;
    userDetails["city"] = this.cityName;
    userDetails["country_code"] = this.country_code;
    userDetails["city"] = this.cityName;
    console.log('userrrrrr', userDetails);
    if (this.flags) {
      this.fmcgForm.get('country_code').setErrors({ 'notFound': true })
      return false;
    } else {
      if (this.fmcgForm.valid) {
        this.submitsection(userDetails);
      }
    }
  }
  submitsection(userData) {
    var id = this.userId
    this.cs.updateFMCGUser(id, userData).subscribe(data => {
      this.closeModal();
      this.reload();
      this.cs.successMessage('User Details Updated Successfully')
    });
  }

  get f() { return this.fmcgForm.controls; }
  SelectName(data) {
    this.flags = false;
    this.flag1 = true;
    this.namelist = true;
    (document.getElementById('suggestName') as HTMLInputElement).value = data.countryCode;
    this.fmcgForm.controls['country_code'].setValue(data.countryCode);
    this.country_code = data.countryCode;
    this.fmcgForm.get('country_code').setErrors(null);
  }
  suggest() {
    console.log('in input');
    this.flag1 = false;
    this.fmcgForm.get('country_code').markAsPristine();
    this.fmcgForm.get('country_code').markAsUntouched();
    this.namelist = false;
    // console.log(((document.getElementById('suggestName') as HTMLInputElement).value));
    // console.log(this.suggestions);
    this.suggestions = this.cs.countries
      .filter(c => c.countryCode.match(((document.getElementById('suggestName') as HTMLInputElement).value)));

    let temp = this.cs.countries.filter(c => c.countryCode == (((document.getElementById('suggestName') as HTMLInputElement).value)));
    if (temp.length > 0) {
      console.log('inside if');
      this.flag1 = true;
      this.namelist = true;
      (document.getElementById('suggestName') as HTMLInputElement).value = temp[0].countryCode;
      this.fmcgForm.controls['country_code'].setValue(temp[0].countryCode);
      this.country_code = temp[0].countryCode;
      this.fmcgForm.get('country_code').setErrors(null);
      this.flags = false;
    } else {
      this.submitted = true;
      console.log('inside else');
      this.flags = true;
      this.fmcgForm.get('country_code').setErrors({ 'notFound': true })
    }
    console.log('formmmmmm', this.submitted, this.f.country_code.errors);

  }
  onChange() {
    setTimeout(() => { this.namelist = true; }, 500)
  }

  forclear() {
    this.flag1 = false;
    this.fmcgForm.controls['country_code'].setValue("");
    (document.getElementById('suggestName') as HTMLInputElement).value = '';
    this.fmcgForm.get('country_code').setErrors({ 'required': true })
  }
}
