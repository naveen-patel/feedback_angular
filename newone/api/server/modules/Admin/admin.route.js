import express from "express";
import admins from "../Admin/admin.controller";
import UserAuthController from "./../UserAuthentication/userAuth.controller";

const router = express.Router();

//Get all admins data
router.get('/admins', UserAuthController.verifySuperAdmin, admins.getAlladmins);

//Add a new admin
router.post('/admins', UserAuthController.verify, admins.addAdmin);

//find a user by user id
router.get('/admins/:user_id', UserAuthController.verify, admins.getAdminByAdminId);

//update admin by user id
router.put('/admins/:user_id', UserAuthController.verify, admins.updateAdminByAdminId);

router.post('/change_status/:id', UserAuthController.verifySuperAdmin, admins.changeStatus);

export default router;