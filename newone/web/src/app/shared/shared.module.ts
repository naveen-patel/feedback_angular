import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToastyModule } from 'ng2-toasty';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ImageCropperModule } from 'ngx-image-cropper';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { TabsModule } from 'ngx-bootstrap/tabs';

@NgModule({
  imports: [
    CommonModule,
    ToastyModule.forRoot(),
    ModalModule.forRoot(),
    ImageCropperModule,
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
    CarouselModule.forRoot(),
    TooltipModule.forRoot(),
    PaginationModule.forRoot(),
    TabsModule.forRoot()
  ],
  declarations: [],
  providers: [
  ],
  exports: [
    ReactiveFormsModule,
    RouterModule,
    ToastyModule,
    ModalModule,
    BsDropdownModule,
    AccordionModule,
    ImageCropperModule,
    TooltipModule,
    PaginationModule,
    TabsModule
  ]
})
export class SharedModule { }
