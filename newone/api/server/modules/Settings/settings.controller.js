import settingService from "./settings.service";
import settings from "../../../settings";
let config = require('./../../../config/' + settings.environment + '.config').default;
const s_key = config.s_key;
const SettingsController = {};
const Cryptr = require('cryptr');
const cryptr = new Cryptr(s_key);



SettingsController.getAllsettings = async (req, res) => {
    try {
        let setting = await settingService.getAllsettings();
        if (setting)
            setting.password = cryptr.decrypt(setting.password);

        return res.status(200).send({
            code: 200,
            status: "success",
            message: "updated successfully",
            data: setting
        });

    } catch (error) {
        res.status(400).send({
            code: 400,
            status: "error",
            message: "Error in getting ",
            data: []
        });
    }
}

SettingsController.Updatesettings = async (req, res) => {
    try {

        let settings = {
            host: req.body.host,
            email: req.body.email,
            port:req.body.port,
            password: cryptr.encrypt(req.body.password),
            otp_duration : req.body.otp_duration

        };
        let id = req.params.id;

        const updatesetting = await settingService.Updatesettings(settings, id);
        res.status(200).send({
            code: 200,
            status: "success",
            message: "updated successfully",
            data: updatesetting
        });

    } catch (error) {
        res.status(400).send({
            code: 400,
            status: "error",
            message: "Error in Updating ",
            data: []
        });
    }
}

SettingsController.UpdateOTPSettings = async (req,res) =>{
    try {

        let settings = {
            otp_duration : req.body.otp_duration
        };
        let id = req.params.id;

        const updatesetting = await settingService.Updatesettings(settings, id);
        res.status(200).send({
            code: 200,
            status: "success",
            message: "updated successfully",
            data: updatesetting
        });

    } catch (error) {
        res.status(400).send({
            code: 400,
            status: "error",
            message: "Error in Updating  " ,
            data: []
        });
    } 
}



export default SettingsController;