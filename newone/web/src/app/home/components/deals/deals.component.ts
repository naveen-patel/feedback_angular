import { Component, OnInit, ChangeDetectorRef, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { CustomerService } from '../../services/customer.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from "ngx-spinner";
import { DatePipe } from '@angular/common'

@Component({
  selector: 'app-deals',
  templateUrl: './deals.component.html',
  styleUrls: ['./deals.component.scss'],
  providers: [DatePipe]
})
export class DealsComponent implements OnInit {

  countries: any[];
  suggestions = [];
  offerPers;
  actualAED;
  actualUSD;
  flags: boolean = false;

  minDate1: Date = new Date();
  minDate2: Date;
  startDate: any;
  endDate: any;

  modalRef2: BsModalRef;
  modalRef3: BsModalRef;
  modalRef4: BsModalRef;
  modalRef5: BsModalRef;
  remainingRecords = 0;
  id: string;
  page = 1;
  isSelectAll: boolean = false;
  selectedIds = [];
  loader: boolean = false;
  search: string = '';
  limit = 10;
  records = [];
  productId;
  count: Number;
  editDeals: string = 'Add';
  viewOnly: Boolean = false
  submitted: boolean = false;
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    keyboard: false,
    class: 'modal-dialog'
  }
  bsConfig =
    {
      showWeekNumbers: false,
      containerClass: 'theme-red',
      changeMonth: true,
      dateInputFormat: 'DD/MM/YYYY',
      changeYear: true,
    }
  dealForm: FormGroup;
  editNew: any;
  addNew: any;
  namelist: boolean = false;
  flag: boolean = false;
  constructor(
    public cs: CustomerService,
    private modalService: BsModalService,
    public formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private DateFormat: DatePipe,
  ) {
  }

  ngOnInit() {
    this.cs.forProductDrop().subscribe(data => {
      this.countries = data;
      console.log(this.countries, "for dropdown");

    });
    this.getDeals();
  }

  // for get all deal's details
  getDeals() {
    this.remainingRecords = 0;
    this.page = 1;
    this.isSelectAll = false;
    this.selectedIds = [];

    this.cs.getDeals(this.search, this.limit, this.page).subscribe(data => {
      this.loader = false;
      this.count = data.totalcount;
      this.records = data.deal;
      this.selectedIds = [];
      this.isSelectAll = false;
      this.remainingRecords = data.totalcount - (this.page * this.limit);
      this.records.forEach((element) => {
        element.isSelect = false;
      })
    });
  }

  searchProduct() {
    this.getDeals();
  }


  // for select single record (deals)
  selectRecord(index) {
    if (this.records[index].isSelect) {
      var i = this.selectedIds.indexOf(this.records[index]._id);
      if (i !== -1) {
        this.selectedIds.splice(i, 1);
      }
    } else {
      this.selectedIds.push(this.records[index]._id)
    }
    this.records[index].isSelect = !this.records[index].isSelect
    this.isSelectAll = (this.selectedIds.length === this.records.length)
  }
  // for select multiple record (deals)
  selectAllRecord() {
    this.selectedIds = [];
    if (this.isSelectAll) {
      this.isSelectAll = false
      this.records.forEach((element) => {
        element.isSelect = false;
      })
    } else {
      this.isSelectAll = true;
      this.records.forEach((element) => {
        element.isSelect = true;
        this.selectedIds.push(element._id)
      })
    }
  }

  // for deals form
  getFormFields(i) {
    console.log(this.records[i]);

    if (i != 'null') {
      this.editDeals = 'Edit';
      this.startDate = this.records[i].deal_start_date;
      this.endDate = this.records[i].deal_end_date;
      this.productId = this.records[i].products[0]._id
      return {
        _id: [this.records[i]._id],
        deal_name: [this.records[i].deal_name, Validators.required],
        product_name: [this.records[i].brands.length != 0 ? this.records[i].brands[0].brand_name : "", Validators.required],
        deal_percentage: [this.records[i].deal_percentage, Validators.required],
        actual_price_aed: [this.records[i].actual_price_aed, Validators.required],
        offer_price_aed: [this.records[i].offer_price_aed, Validators.required],
        actual_price_usd: [this.records[i].actual_price_usd, Validators.required],
        offer_price_usd: [this.records[i].offer_price_usd, Validators.required,],
        deal_start_date: [this.records[i].deal_start_date, Validators.required],
        deal_end_date: [this.records[i].deal_end_date, Validators.required,],
      }
    } else {
      this.editDeals = 'Add';
      this.startDate = '';
      this.endDate = '';
      return {
        deal_name: ["", Validators.required],
        product_name: ["", Validators.required],
        deal_percentage: ["", Validators.required],
        actual_price_aed: ["", Validators.required],
        offer_price_aed: ["", Validators.required],
        actual_price_usd: ["", Validators.required],
        offer_price_usd: ["", Validators.required],
        deal_start_date: ["", Validators.required],
        deal_end_date: ["", Validators.required],
      }
    }
  }
  forDisabled() { return true }
  openModal(template: TemplateRef<any>, i, type) {
    this.namelist = true;
    if ((type == 'Add') || (type == 'Edit') && (type != 'Open')) {
      this.editDeals = type;
      this.viewOnly = false;
      this.modalRef = this.modalService.show(template, this.config);
      this.dealForm = this.formBuilder.group(this.getFormFields(i));
      this.submitted = false;
    } else if (type == 'Open') {
      this.viewOnly = true;
      this.modalRef = this.modalService.show(template, this.config);
      this.dealForm = this.formBuilder.group(this.getFormFields(i));
      this.editDeals = "";

    }
  }

  onSubmit() {
    this.submitted = true;
    if (this.flags) {
      this.dealForm.get('product_name').setErrors({ 'notFound': true })
    }
    console.log('value', this.dealForm.valid);
    console.log(this.dealForm.value);
    if (new Date(this.dealForm.controls['deal_start_date'].value).setHours(0, 0, 0, 0) > new Date(this.dealForm.controls['deal_end_date'].value).setHours(0, 0, 0, 0)) {
      this.dealForm.get('deal_end_date').setErrors({ 'startdate': true });
      return false
    }
    if (this.dealForm.valid) {

      if (this.flags) {
        console.log("inside if", this.suggestions);
        this.dealForm.get('product_name').setErrors({ 'notFound': true })
        return false;
      } else {
        if (this.editDeals == 'Edit') {
          let formValue = Object.assign({}, this.dealForm.value);
          this.editNew = formValue;
        } else {
          let formValue = Object.assign({}, this.dealForm.value);
          this.addNew = formValue;
        }
        this.submitsection();
      }
    }
  }
  findInvalidControls() {
    const invalid = [];
    const controls = this.dealForm.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    console.log('invalid', invalid);

    return invalid;
  }
  get f() { return this.dealForm.controls; }

  submitsection() {
    if (this.editDeals == 'Edit') {
      var id = this.editNew._id;
      this.cs.updateDeal(id, this.editNew, this.productId).subscribe(data => {
        this.closeModal();
        this.reload();
        this.cs.successMessage('Deal Updated Successfully')
      });
    } else {
      this.cs.addDeal(this.addNew, this.productId).subscribe(data => {
        if (data.code == 200) {
          this.closeModal();
          this.reload();
          this.cs.successMessage('New Deal added Successfully')
        }
        else {
          this.closeModal();
          this.reload();
          this.cs.errorMessage('One deal is already active on this product!!')
        }
      });
    }
  }
  isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }

  // for product name list

  SelectName(data) {
    console.log("in select");
    this.flags = false;
    this.flag = true;
    this.namelist = true;
    this.productId = data._id;
    (document.getElementById('suggestName') as HTMLInputElement).value = data.brand.brand_name;
    this.dealForm.controls['actual_price_aed'].setValue(data.sales_per_case_aed);
    this.dealForm.controls['actual_price_usd'].setValue(data.sales_per_case_usd);
    this.dealForm.get('product_name').setErrors(null);
  }
  suggest() {
    console.log('in input');
    this.flag = false;
    this.dealForm.get('product_name').markAsPristine();
    this.dealForm.get('product_name').markAsUntouched();
    this.namelist = false;
    console.log(((document.getElementById('suggestName') as HTMLInputElement).value).toLowerCase());
    console.log(this.suggestions);
// testing start

let num1 = ((document.getElementById('suggestName') as HTMLInputElement).value).toLowerCase();

if (num1 != '' && num1!= undefined) {
  num1 = num1.charAt(0);
  var matches = num1[0].match(/\d+/g);
  if (matches != null) {
    this.suggestions = this.countries
      .filter(c => c.product_code.toLowerCase().match(((document.getElementById('suggestName') as HTMLInputElement).value).toLowerCase()));
  } else {
    this.suggestions = this.countries
      .filter(c => c.brand.brand_name.toLowerCase().match(((document.getElementById('suggestName') as HTMLInputElement).value).toLowerCase()));
  }
} else {
  this.suggestions = this.countries
    .filter(c => c.brand.brand_name.toLowerCase().match(((document.getElementById('suggestName') as HTMLInputElement).value).toLowerCase()));
}
// testing end 


    // this.suggestions = this.countries
    //   .filter(c => c.brand.brand_name.toLowerCase().match(((document.getElementById('suggestName') as HTMLInputElement).value).toLowerCase()));

    let temp = this.countries.filter(c => c.brand.brand_name.toLowerCase() == (((document.getElementById('suggestName') as HTMLInputElement).value).toLowerCase()));
    // if ((document.getElementById('suggestName') as HTMLInputElement).value == this.suggestions[0].brand_name) {
    if (temp.length > 0) {
      console.log('match');
      this.flag = true;
      this.namelist = true;
      this.productId = temp[0]._id;
      (document.getElementById('suggestName') as HTMLInputElement).value = temp[0].brand.brand_name;
      this.dealForm.controls['actual_price_aed'].setValue(temp[0].sales_per_case_aed);
      this.dealForm.controls['actual_price_usd'].setValue(temp[0].sales_per_case_usd);
      this.dealForm.get('product_name').setErrors(null);
      this.offerCal();
      this.flags = false;
    } else {
      this.dealForm.controls['actual_price_aed'].setValue("");
      this.dealForm.controls['actual_price_usd'].setValue("");
      this.dealForm.controls['offer_price_aed'].setValue("");
      this.dealForm.controls['offer_price_usd'].setValue("");
      this.flags = true;
      // this.dealForm.get('product_name').setErrors({ 'notFound': true })
    }

    console.log('when click', this.namelist, this.suggestions);
  }
  onChange() {
    setTimeout(() => { this.namelist = true; this.offerCal() }, 500)
    console.log("change in");
  }
  forclear() {
    this.flag = false;
    this.dealForm.controls['actual_price_aed'].setValue("");
    this.dealForm.controls['actual_price_usd'].setValue("");
    this.dealForm.controls['offer_price_aed'].setValue("");
    this.dealForm.controls['offer_price_usd'].setValue("");
    (document.getElementById('suggestName') as HTMLInputElement).value = '';
    this.dealForm.get('product_name').setErrors({ 'required': true })
  }

  // for offer cost calculation
  offerCal() {
    if (((document.getElementById('offerPer') as HTMLInputElement).value) && ((document.getElementById('actualAED') as HTMLInputElement).value)) {
      this.offerPers = (document.getElementById('offerPer') as HTMLInputElement).value
      this.actualAED = (document.getElementById('actualAED') as HTMLInputElement).value
      let offerCoastAed: any = (this.actualAED - ((this.actualAED * this.offerPers) / 100)).toFixed(2)
      let offerCoastUsd: any = ((offerCoastAed / 3.67250).toFixed(2));
      this.dealForm.controls['offer_price_aed'].setValue(offerCoastAed);
      this.dealForm.controls['offer_price_usd'].setValue(offerCoastUsd);
    } else {
      this.dealForm.controls['offer_price_aed'].setValue("");
      this.dealForm.controls['offer_price_usd'].setValue("");
      console.log('not have both');
    }
  }

  // for end date 
  forEndDate() {
    this.dealForm.get('deal_end_date').setErrors(null);
  }


  // for delete record
  DeleteRecord(template: TemplateRef<any>, i, id) {
    this.modalRef3 = this.modalService.show(template);
    this.id = id;
  }
  confirmToDelete() {
    this.cs.deleteDeals(this.id).subscribe(data => {
      this.cs.successMessage("Deal Deleted Successfully");
      this.ngOnInit();
    })
    this.modalRef3.hide();
    this.modalRef3 = null
  }
  confirmToMultiDelete() {
    this.cs.deleteDeals(this.selectedIds).subscribe(data => {
      this.cs.successMessage("Deals Deleted Successfully");
      this.ngOnInit();
    })
    this.modalRef4.hide();
    this.modalRef4 = null
  }
  DeleteRecords(template: TemplateRef<any>) {
    this.modalRef4 = this.modalService.show(template);
  }

  // for modal close 
  closeModal() {
    this.modalRef.hide();
    this.modalRef = null
  }
  // for reload page after changes 
  reload() {
    this.search = "";
    this.page = 1;
    this.ngOnInit();
  }
  // for load more Record(page 2)
  loadMore() {
    this.page++;
    this.cs.getDeals(this.search, this.limit, this.page).subscribe(data => {
      var product = data.deal;
      this.isSelectAll = false;
      product.forEach((element) => {
        element.isSelect = false;
      })
      this.records = this.records.concat(product);
      this.remainingRecords = data.totalcount - (this.page * this.limit);
    });
  }

}
