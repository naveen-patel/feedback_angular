import express from "express";
import brandController from "./brand.controller";
import UserAuthController from "../UserAuthentication/userAuth.controller";


const brandRouter = express.Router();

brandRouter.post('/brand',UserAuthController.verify,brandController.createBrand);
brandRouter.put('/brand/:brand_id',UserAuthController.verify,brandController.updateBrand);
brandRouter.get('/brand',UserAuthController.verify,brandController.getAllBrand);
brandRouter.get('/brands',UserAuthController.verify, brandController.getBrands);
brandRouter.get('/brand/:brand_id',UserAuthController.verify,brandController.getBrandById);
// brandRouter.get('/brand/brand_name/:category_id',UserAuthController.verify,brandController.getBrandNameById)
// brandRouter.get('/category/product/:category_id',UserAuthController.verify,brandController.searchProductCategory)
brandRouter.delete('/brand/:brand_id',UserAuthController.verify,brandController.deleteBrand);
export default brandRouter;