import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrandWiseProductComponent } from './brand-wise-product.component';

describe('BrandWiseProductComponent', () => {
  let component: BrandWiseProductComponent;
  let fixture: ComponentFixture<BrandWiseProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrandWiseProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrandWiseProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
