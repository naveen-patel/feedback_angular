import DealModel from "./deal.model";
const fs = require('fs');
var mongoose = require('mongoose');
import settings from '../../../settings';
let config = require('./../../../config/' + settings.environment + '.config').default;
import moment from "moment";

const DealService = {
  getDealById: async id => {
    try {
    let deal = await DealModel.findOne({_id: Object(id)}).populate({path:'product_id',name:'product_type',
                                                            populate:{
                                                              path:'brand',
                                                              select:'brand_name'
                                                            } });

                                                                     
      return deal;
    } catch (error) {
      throw error;
    }
  },
  getDealByProductId:async id =>{
    try {
      let date = moment().startOf('day').format("YYYY-MM-DD")
      return await DealModel.findOne({product_id:Object(id),deal_end_date:{$gt : date.toString()}}).populate({path:'product_id',name:'product_type',populate:{
                                                                                        path:'brand',
                                                                                        select:'brand_name'
                                                                                      } });

    } catch (error) {
      throw error;
    }
  },
  getActiveDeal:async location =>{
    try {
      let date = moment().startOf('day').format("YYYY-MM-DD")
      // return await DealModel.find({deal_end_date:{$gt : date.toString()}}).populate({path:'product_id',name:'product_type'
      // ,match: { stock_location: "UK"}
      // ,populate:{
      //   path:'brand',
      //   select:'brand_name'
      // } });


      return await DealModel.aggregate([
        {
          '$lookup': {
            from: 'product',
            localField: 'product_id',
            foreignField: '_id',
            as: 'products'
          }
          
        },
        {
          $unwind: "$products"
        },
        {
          '$lookup': {
            from: 'brand',
            localField: 'products.brand',
            foreignField: '_id',
            as: 'brands'
          }
        },
        {
          $unwind: "$brands"
        },
        {
          $match: { $and: [ 
            {'deal_end_date':{$gt : new Date(date)}},
            // {'products.stock_location':"Uk"},
            {
              "products.stock_location": {
                "$regex": location,
                "$options": "i"
              }
            }
          ]}
                    },
        {$sort: {"createdAt": -1}},
       { $project: {
         'id':1,
         "deal_name":1,
         "deal_percentage":1,
         "actual_price_aed":1,
         "offer_price_aed":1,
         "actual_price_usd":1,
         "offer_price_usd":1,
         "deal_start_date":1,
         "deal_end_date":1,
        "product_id":{$mergeObjects:['$products',{"brand":"$brands"}]}   
          }}

      ])
    } catch (error) {
      throw error;
    }
  },
  createDeal: async newDeal => {
    try {
      var savedDeal = await new DealModel(newDeal).save();
      return savedDeal
    } catch (error) {
      throw error;
    }
  },
  updateDeal: async (data,id) => {
    try {
      const update = await DealModel.findOneAndUpdate({
        _id: id
      }, data, {
        upsert: true,
        new: true
      });
      return update;
    } catch (error) {
      throw error;
    }
  },
  getAllDeal: async (searchkey,limit, skip) => {
    try {
      let json = {};
    const deal = await DealModel.aggregate(
    [{
      '$lookup': {
        from: 'product',
        localField: 'product_id',
        foreignField: '_id',
        as: 'products'
      }
      
    },
    {
      '$lookup': {
        from: 'brand',
        localField: 'products.brand',
        foreignField: '_id',
        as: 'brands'
      }
    },{
      $match: {
        "$or":[{"products.product_name": {
                "$regex": searchkey,
                "$options": "i"
                }},{"deal_name": {
                "$regex": searchkey,
                "$options": "i"
                }}],  
                  }
                },
    {$sort: {"createdAt": -1}},
    {
      '$facet': {
        metadata: [{
          $count: "total"
        }
      ],
      deal_data: [{
          $skip: skip
        }, {
          $limit: limit
        }]
      }
    }, {
      $project: {
        "_id": 0,
        "totalcount": { $arrayElemAt: ["$metadata.total", 0] },
        "totalPage": { $ceil: { $divide: [{ $arrayElemAt: ["$metadata.total", 0] }, limit] } },
        "deal_data": "$deal_data"
      }
    }
  ]);
    json.totalpage = Math.ceil(deal[0].totalcount / limit);
    json.totalcount = deal[0].totalcount;
    json.deal = deal[0].deal_data;
    return json;
    } catch (error) {
      throw error;
    }
  },
  deleteDealByIds: async (ids) => {
    try {
      var data = await DealModel.remove({
        _id: {
          $in: ids
        }
      });;
      return data;
    } catch (error) {
      throw error;
    }
  }
};
export default DealService;