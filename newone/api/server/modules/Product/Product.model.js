import mongoose from 'mongoose';
// mongoose.set('debug', true)
var Schema = mongoose.Schema;

const ProductSchema = mongoose.Schema({
    supplier_id: {
        type: Schema.Types.ObjectId,
        ref: "supplier"
    },
    category_id:{
        type: Schema.Types.ObjectId,
        ref: "category"
    },
    brand:{
        type: Schema.Types.ObjectId,
        ref: "brand"
    },
    deal:{
    type:Schema.Types.ObjectId,
    ref: "deal"
    },
    product_name: {
        type: String,
    },
    // brand_name: {
    //     type: String,
    // },
    product_code: {
        type: String
    },
    code:{
        type: Number
    },
    product_type: {
        type: String
    },
    stock_location:{
        type: String
    },
    fmcg_availability :{
        type:Boolean,
        default:false
    },
    expiry_date: {
        type: Date
    },
    ru_per_case: {
        type: String,
    },
    ean_code_ru: {
        type: String
    },
    ean_code_case: {
        type: String
    },
    language: {
        type: String
    },
    origin: {
        type: String
    },
    quantity: {
        type: Number
    },
    sales_per_case_aed: {
        type: String
    },
    sales_per_case_usd: {
        type: String
    },
    cost_price_per_case: {
        type: String
    },
    description: {
        type: String
    },
    size: {
        type: String
    },
    product_front_image: {
        type: String
    },
    product_back_image: {
        type: String
    },
}, {
    collection: 'product',
    timestamps: true
});

let ProductModel = mongoose.model('product', ProductSchema);

export default ProductModel