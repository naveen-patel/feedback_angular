import CartService from "./cart.service";
import SequenceService from "../Sequence/Sequence.service";
import logger from "../../../config/logger.config";
var _ = require("lodash");
let Promise = require('bluebird');


const cartController = {
  getCartById: async (req, res) => {
    try {
      const cart = await CartService.getCartById(req.params.cart_id);
      logger.info(" Getting cart by id = " + req.params.cart_id);
      res.send({
        code: 200,
        status: "success",
        message: "List the cart",
        data: cart
      });
    } catch (error) {
      logger.error("Error in getting cart :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting cart",
        data: {}
      });
    }
  },
  addProductInCart: async (req, res) => {
    try {
      if(!req.body && !req.body.product_id) res.semd({code:400,message:"Invalid  API call"});
      let cart ={};
          cart.user_id = req.params.user_id;
          cart.product_id = req.body.product_id;
          cart.quantity = req.body.quantity;
       let cartData = await CartService.addProductInCart(cart);
      const carts = await CartService.getCartById(cartData._id);
      const count = await CartService.getAllCartProductByUser(req.params.user_id,null);
          logger.info("product added to cart for "+req.params.user_id);
          return res.send({
            code: 204,
            status: "success",
            message: "product added  to cart succesfully",
            data: carts,
            count:count
          });
      }
     catch (error) {
      logger.error("Error in adding product to cart "+error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error adding product to cart ",
        data: []
      });
    }
  },
  updateProductInCart: async (req,res)=>{
    try {
      if(!req.body) res.semd({code:400,message:"Invalid call"});
      let cart ={};
          cart.user_id = req.params.user_id;
          cart.product_id = req.body.product_id;
          cart.quantity = req.body.quantity;
      let cartData = await CartService.updateProductInCart(cart);
          logger.info("product updated in cart for "+req.params.user_id);
          return res.send({
            code: 204,
            status: "success",
            message: "product updated succesfully",
            data: cartData
          });
      }
     catch (error) {
      logger.error("Error in updating product to cart "+error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error updating product to cart ",
        data: []
      });
    }
  },
  getAllCartProductByUser: async (req, res) => {
    try {
      let user_id = req.params.user_id;
      let limit = Number(req.query.limit) || 10;
      let offset = (req.query.skip) ? parseInt(req.query.skip) : 1
      let skip = (offset - 1) * limit;
      const cartData = await CartService.getAllCartProductByUser(user_id,limit,skip);
      logger.info(" Getting all products");
      res.send({
        code: 200,
        status: "success",
        message: "Listing All products",
        data: cartData
      });
    } catch (error) {
      logger.error("Error in getting produts :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting orders record",
        data: []
      });
    }
  },
  removeProductFromCart: async (req, res) => {
    try {
      var id = req.params.id;
      var data = await CartService.removeProductFromCart(id);
      res.send({
        code: 200,
        status: "success",
        message: "product  is successfully removed from cart",
        data: data
      });
    } catch (error) {
      res.status(400).send({
        code: 400,
        status: "error",
        message: "error in removing product from cart",
        data: {}
      });

    }
  }
};
export default cartController;