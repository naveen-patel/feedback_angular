import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import * as $ from 'jquery';
import { Logout } from './../../../auth/store/actions/auth.actions';
import { Store } from '@ngrx/store';
import * as fromAuth from '../../../auth/store/reducers';
import { DataService } from '../../../home/services/data.service'
import { l } from '@angular/core/src/render3';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  @Input() isLoggedIn: false;
  @Output() logoutClick = new EventEmitter<boolean>();

sideHide = false;

  view: Boolean = false
  shrinkshow: Boolean = false;
  isSuperAdmin: Boolean = false;
  loggedIn: any;
  loggedInId: String;
  loggedInname: String;
  loggedInlast_name: String;
  designation: String;
  noti_list = [];
  total_count: any;
  constructor(
    private store: Store<fromAuth.State>,
    private router: Router,
    private route: ActivatedRoute,
    private ds: DataService
  ) { }


  ngOnInit() {
    let localdata = localStorage.getItem('user');
    this.loggedIn = JSON.parse(String(localdata));
    this.isSuperAdmin = this.loggedIn.type === 'super_admin' ? true : false;
    this.loggedInId = this.loggedIn.profile_image;
    this.loggedInname = this.loggedIn.first_name;
    this.loggedInlast_name = this.loggedIn.last_name;
    this.designation = this.loggedIn.designation;
    this.total_count = 0;

    /* off-canvas sidebar toggle */
    $('[data-toggle=offcanvas]').click(function () {
      $('.row-offcanvas').toggleClass('active');
      $('.collapse').toggleClass('in').toggleClass('hidden-xs').toggleClass('visible-xs');
    });


    $('#noti_Button').click(function () {

      // TOGGLE (SHOW OR HIDE) NOTIFICATION WINDOW.
      $('#notifications').fadeToggle('fast', 'linear', function () {
      });
      return false;
    });
    $(document).click(function () {
      $('#notifications').hide();
    });

    $('#notifications').click(function () {
      return false;
    });
    $('#mobile-menu-trigger').click(function () {
      if ($(".menu-and-user").is(":visible")) {
        $(".menu-and-user").hide()
      } else {
        $(".menu-and-user").show()
      }

    });
    $('#mobile-menu').click(function () {
      $(".menu-and-user").hide()
    });
  }
  show_shrink() {
    this.shrinkshow = true;
  }
  show_expand() {
    this.shrinkshow = false;
  }

  logout() {
    this.store.dispatch(new Logout());
  }

  currentRoute(currRoute) {
    if (this.router.url.indexOf(currRoute) !== -1) {
      return true;
    }
    return false;
  }

  currentSubRoute(currRoute) {
    this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        if (e.url.indexOf(currRoute) !== -1) {
          return true;
        } 
        return false;
      }
    })
  }
  sidemenuhide() {
    console.log('click');
    this.sideHide = !this.sideHide
  }
}
