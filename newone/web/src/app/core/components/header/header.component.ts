import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DataService } from '../../../home/services/data.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() isLoggedIn: false;
  @Output() logoutClick = new EventEmitter<boolean>();

  loggedIn: any
  loggedInId: String;
  loggedInname: String
  loggedInlast_name: String;
  total_count: String;
  noti_list = [];
  constructor(
    public ds: DataService,
    public route: Router
  ) { }

  ngOnInit() {
    let localdata = localStorage.getItem('user');
    this.loggedIn = JSON.parse(localdata);
    this.loggedInId = this.loggedIn.profile_image;
    this.loggedInname = this.loggedIn.first_name;
    this.loggedInlast_name = this.loggedIn.last_name;

    $("#inner").hide();
    $("#outer").on('click', function () {
      $("#inner").toggle();
    });
  }
  logout() { this.logoutClick.emit(); }

}
