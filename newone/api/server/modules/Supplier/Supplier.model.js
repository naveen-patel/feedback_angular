import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const SupplierSchema = mongoose.Schema({
    name: {
        type: String
    },
    contact_person:{
        type:String
    },
    email_id: {
        type: String
    },
    country_code :{
        type:String
    },
    phone_number: {
        type: String
    },
    address: {
        type: String,
    },
    business_card_image_1: {
        type: String
    },
    business_card_image_2: {
        type: String
    },
    supplier_code: {
        type: String
    },
    code:{
        type:Number
    }
}, {
    collection: 'supplier',
    timestamps: true
});

let SupplierModel = mongoose.model('supplier', SupplierSchema);

export default SupplierModel