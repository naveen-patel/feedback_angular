import { Component, OnInit, TemplateRef, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataService } from '../../services/data.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { CompressorService } from '../../services/compressor.service';
import { map, expand } from 'rxjs/operators';
import { NgxImageCompressService } from 'ngx-image-compress';

@Component({
  selector: 'app-manage-supplier',
  templateUrl: './manage-supplier.component.html',
  styleUrls: ['./manage-supplier.component.scss']
})
export class ManageSupplierComponent implements OnInit {

  modalRef: BsModalRef;
  myform: FormGroup;
  message = false;
  addNew: any;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    keyboard: false,
    class: 'modal-lg'
  }
  editSupplier: boolean = false;
  submitted: boolean = false
  editNew: any;

  businessImage1: any = "";
  businessImageName1: any = "";
  businessImage2: any = "";
  businessImageName2: any = "";

  formData: any = new FormData();
  couponType: String = "";
  errorImageSize1: Boolean = false;
  errorImageSize2: Boolean = false
  viewOnly: Boolean = false;
  deleteImage1: Boolean = false;
  deleteImage2: Boolean = false;

  constructor(
    private ds: DataService,
    private route: ActivatedRoute,
    public formBuilder: FormBuilder,
    private modalService: BsModalService,
    private compressor: CompressorService,
    private imageCompress: NgxImageCompressService

  ) {

  }
  @Input() childMessage: any;

  @Output() messageEvent = new EventEmitter<any>();

  @ViewChild("template") addTemplate: TemplateRef<any>;
  getFormFields() {
    if (this.editSupplier) {
      this.businessImage1 = this.childMessage.business_card_image_1;
      this.businessImage2 = this.childMessage.business_card_image_2;
      return {
        _id: [this.childMessage._id],
        name: [{ value: this.childMessage.name, disabled: this.viewOnly }, Validators.required],
        contact_person: [{ value: this.childMessage.contact_person, disabled: this.viewOnly }, Validators.required,],
        email: [{ value: this.childMessage.email_id, disabled: this.viewOnly }, Validators.required],
        country_code: [{ value: this.childMessage.country_code, disabled: this.viewOnly }, Validators.required],
        phone_number: [{ value: this.childMessage.phone_number, disabled: this.viewOnly }, Validators.required],
        address: [{ value: this.childMessage.address, disabled: this.viewOnly }, Validators.required]
      }
    } else {
      return {
        name: ["", Validators.required],
        contact_person: ["", Validators.required],
        email: ["", Validators.required],
        country_code: ["", Validators.required],
        phone_number: ["", Validators.required],
        address: ["", Validators.required],
      }
    }
  }


  ngOnInit() {
    if (this.childMessage) {
      this.editSupplier = true;
      this.viewOnly = this.childMessage.isViewOnly
    } else {
      this.viewOnly = false
    }
    setTimeout(() => {
      this.myform = this.formBuilder.group(this.getFormFields());
    }, 1000);
    setTimeout(() => {
      this.openModal(this.addTemplate)
    }, 1000);
  }


  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.config);
  }
  onSubmit() {
    this.submitted = true;
    if (this.myform.valid) {
      if (this.editSupplier) {
        let formValue = Object.assign({}, this.myform.value);
        this.editNew = formValue;
        this.editNew.delete_card_1 = this.deleteImage1;
        this.editNew.delete_card_2 = this.deleteImage2;
        this.formData.set('bodyData', JSON.stringify(this.editNew))
      } else {
        let formValue = Object.assign({}, this.myform.value);
        this.addNew = formValue;
        this.formData.set('bodyData', JSON.stringify(this.addNew))
      }
      this.submitsection();
    }
  }

  get f() { return this.myform.controls; }

  submitsection() {
    if (this.editSupplier) {
      this.ds.updateSupplier(this.childMessage._id, this.formData).subscribe(data => {
        this.closeModel();
      })
    } else {
      this.ds.addSupplier(this.formData).subscribe(data => {
        this.closeModel();
      });
    }
  }

  closeModel() {
    this.modalRef.hide();
    this.modalRef = null
    this.messageEvent.emit([this.message, this.addNew, this.editNew])
  }



  onUploadImage(event) {

    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file: File = fileList[0];
      // var img = $("#preview img");
      // img.file = file;
      var reader = new FileReader();
      // reader.onload = (function (aImg) { return function (e) { aImg.src = e.target.result; }; })(img);
      reader.readAsDataURL(file);
      reader.onload = () => {
        // this.filename = file.name;
        this.formData.set("business_image_2", event.target.files[0], file.name);
        // this.businessImageName1 = file.name;
        this.businessImageName2 = file.name;
        this.businessImage2 = reader.result;

      };
    }
    // this.imageChangedEvent = event;



  }



  data: FileList;
  compressedImages = [];
  recursiveCompress = (image: File, index, array) => {
    if (image) {
      return this.compressor.compress(image).pipe(
        map(response => {

          //Code block after completing each compression
          this.compressedImages.push(response);
          return {
            data: response,
            index: index + 1,
            array: array,
          };
        }),
      );
    }

  }

  //process files for upload
  public process(event, image) {
    this.data = event.target.files;
    var filesize = ((this.data[0].size / 1024) / 1024).toFixed(4);
    if (Number(filesize) < 1.1) {
      var obj = {};
      const compress = this.recursiveCompress(this.data[0], 0, this.data);
      // .pipe(
      //   expand(res => {
      //     return this.recursiveCompress(this.data[res.index], res.index, this.data);
      //   }),
      // );
      compress.subscribe(res => {
        if (res.index > res.array.length - 1) {
          var reader = new FileReader();
          // reader.onload = (function (aImg) { return function (e) { aImg.src = e.target.result; }; })(img);
          reader.readAsDataURL(this.compressedImages[0]);
          reader.onload = () => {
            // this.filename = file.name;
            if (image === 'business_card_image1') {
              this.deleteImage1 = false;
              this.formData.set("business_card_image_1", this.compressedImages[0], this.compressedImages[0].name);
              this.businessImageName1 = this.compressedImages[0].name;
              this.businessImage1 = reader.result;
              this.errorImageSize1 = false;
              this.compressedImages = []
            } else {
              this.deleteImage2 = false;
              this.formData.set("business_card_image_2", this.compressedImages[0], this.compressedImages[0].name);
              this.businessImageName2 = this.compressedImages[0].name;
              this.businessImage2 = reader.result;
              this.errorImageSize2 = false;
              this.compressedImages = [];
            }



            //this.promoSlider = reader.result;

          };


          //Code block after completing all compression
        }
      });
    } else {
      this.ds.errorMessage('Plesae Upload Image size lesser than 1 MB')
      if (image === 'business_card_image1') {
        this.errorImageSize1 = true;
      } else {
        this.errorImageSize2 = true;
      }
    }
  }
  imageDelete(type) {
    if (type === 'image1') {
      this.deleteImage1 = true;
      this.businessImage1 = "";
      this.formData.set("business_card_image_1", "");
    } else if (type === 'image2') {
      this.deleteImage2 = true;
      this.businessImage2 = "";
      this.formData.set("business_card_image_2", "");
    }
  }
  compressFile(type) {
    this.imageCompress.uploadFile().then(({ image, orientation }) => {
      console.warn('Size in bytes was:', this.imageCompress.byteCount(image));
      // var quanity = 15, radio = 10;
      // var length = this.imageCompress.byteCount(image) / 1000;
      if(this.imageCompress.byteCount(image)>30000){
        this.imageCompressToMinSize(image, type,orientation)
      }else{
        var block = image.split(";");
          // Get the content type of the image
          var contentType = block[0].split(":")[1];// In this case "image/gif"
          // get the real base64 content of the file
          var realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."

          // Convert it to a blob to upload
          var blob = this.b64toBlob(realData, contentType);
          if (type === 'business_card_image1') {
            this.deleteImage1 = false;
            this.businessImage1 = image;
            this.formData.set("business_card_image_1", blob, 'business_card_image1');
          } else {
            this.deleteImage2 = false;
            this.businessImage2 = image;
            this.formData.set("business_card_image_2", blob, 'business_card_image2');
          }
          console.warn('Size in bytes is now:', this.imageCompress.byteCount(image));
      }

    });

  }
  imageCompressToMinSize(image, type,orientation) {
    this.imageCompress.compressFile(image, orientation, 50, 50).then(
      result => {
        console.warn('Size in bytes was:', this.imageCompress.byteCount(image));
        //this.imgResultAfterCompress = result;
        if (this.imageCompress.byteCount(result) < 40000) {
          var block = result.split(";");
          // Get the content type of the image
          var contentType = block[0].split(":")[1];// In this case "image/gif"
          // get the real base64 content of the file
          var realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."

          // Convert it to a blob to upload
          var blob = this.b64toBlob(realData, contentType);
          if (type === 'business_card_image1') {
            this.deleteImage1 = false;
            this.businessImage1 = result;
            this.formData.set("business_card_image_1", blob, 'business_card_image1');
          } else {
            this.deleteImage2 = false;
            this.businessImage2 = result;
            this.formData.set("business_card_image_2", blob, 'business_card_image2');

          }

          console.warn('Size in bytes is now:', this.imageCompress.byteCount(result));
        } else {
          this.imageCompressToMinSize(result, type,orientation);
        }
      }
    );
  }
  b64toBlob(b64Data, contentType) {
    contentType = contentType || '';
    var sliceSize = 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);

      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      var byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }
}
