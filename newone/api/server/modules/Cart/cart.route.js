import express from "express";
import cartController from "./cart.controller";
import UserAuthController from "../UserAuthentication/userAuth.controller";

const cartRouter = express.Router();

cartRouter.post('/cart/:user_id',UserAuthController.verify,cartController.addProductInCart);
cartRouter.put('/cart/:user_id',UserAuthController.verify,cartController.updateProductInCart);
cartRouter.get('/carts/:user_id',UserAuthController.verify,cartController.getAllCartProductByUser);
cartRouter.get('/cart/:cart_id',UserAuthController.verify,cartController.getCartById);
cartRouter.delete('/cart/:id',UserAuthController.verify,cartController.removeProductFromCart);

export default cartRouter;