import mongoose from 'mongoose';
var Schema = mongoose.Schema;

const CartSchema = mongoose.Schema({
    product_id:{
        type: Schema.Types.ObjectId,
        ref: "product"
    },
    user_id:{
        type: Schema.Types.ObjectId,
        ref: "user"
    },
    quantity:{
        type:Number,
        default:0
    } 
}, {
    collection: 'cart',
    timestamps: true
});

let CartModel = mongoose.model('cart', CartSchema);

export default CartModel;