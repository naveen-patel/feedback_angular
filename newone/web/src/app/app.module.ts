import { AuthModule } from './auth/auth.module';
import { CommonModule } from '@angular/common';
import { CoreModule } from './core/core.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { StoreModule } from '@ngrx/store';
import { StoreRouterConnectingModule, RouterStateSerializer } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';

import { AppComponent } from './core/containers/app/app.component';

import { AppRoutingModule } from './app-routing.module';
import { reducers, metaReducers } from './core/store/reducers/root.reducer';
import { CustomRouterStateSerializer } from './shared/utils';

import { environment } from '../environments/environment';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { FullCalendarModule } from 'ng-fullcalendar';
import { TimepickerModule } from 'ngx-bootstrap';
import { AccordionModule } from 'ngx-bootstrap';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { MessagingService } from './core/messaging-service';
import { DataService } from './home/services/data.service';
import { CompressorService } from './home/services/compressor.service';
import { CustomerService } from './home/services/customer.service';

import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { CKEditorModule } from 'ng2-ckeditor';
import { TooltipModule } from 'ngx-bootstrap';
import { OrgChartModule } from 'ng-org-chart';

import {NgxImageCompressService} from 'ngx-image-compress';

// RX imports
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/exhaustMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/finally';
import 'rxjs/add/operator/retry';
import 'rxjs/add/observable/of';
import { NgxSpinnerModule } from "ngx-spinner";
// import {  } from './pie-chart.component'
import { ChartsModule } from 'ng2-charts';
import { CounterModule } from 'ngx-counter';

@NgModule({

  imports: [
    ChartsModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CommonModule,
    AngularMultiSelectModule,
    FullCalendarModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    NgxSpinnerModule,
    /**
     * StoreModule.forRoot is imported once in the root module, accepting a reducer
     * function or object map of reducer functions. If passed an object of
     * reducers, combineReducers will be run creating your application
     * meta-reducer. This returns all providers for an @ngrx/store
     * based application.
     */
    StoreModule.forRoot(reducers, { metaReducers }),

    /**
     * @ngrx/router-store keeps router state up-to-date in the store.
     */
    StoreRouterConnectingModule,

    /**
     * Store devtools instrument the store retaining past versions of state
     * and recalculating new states. This enables powerful time-travel
     * debugging.
     *
     * To use the debugger, install the Redux Devtools extension for either
     * Chrome or Firefox
     *
     * See: https://github.com/zalmoxisus/redux-devtools-extension
     */
    !environment.production ? StoreDevtoolsModule.instrument() : [],

    /**
     * EffectsModule.forRoot() is imported once in the root module and
     * sets up the effects class to be initialized immediately when the
     * application starts.
     *
     * See: https://github.com/ngrx/platform/blob/master/docs/effects/api.md#forroot
     */
    EffectsModule.forRoot([]),

    CoreModule.forRoot(),

    AuthModule.forRoot(),

    BsDatepickerModule.forRoot(),

    TimepickerModule.forRoot(),

    AccordionModule.forRoot(),
    NgSelectModule,
    FormsModule,
    CKEditorModule,
    TooltipModule.forRoot(),
    OrgChartModule,
    CounterModule.forRoot()
  ],
  providers: [
    DataService,
    CustomerService,
    CompressorService,
    NgxImageCompressService,
    /**
     * The `RouterStateSnapshot` provided by the `Router` is a large complex structure.
     * A custom RouterStateSerializer is used to parse the `RouterStateSnapshot` provided
     * by `@ngrx/router-store` to include only the desired pieces of the snapshot.
     */
    { provide: RouterStateSerializer, useClass: CustomRouterStateSerializer },
  ],
  bootstrap: [AppComponent],
  declarations: []
})
export class AppModule { }
