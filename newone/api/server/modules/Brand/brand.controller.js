import BrandService from "./brand.service";
import logger from "../../../config/logger.config";
var _ = require("lodash");
import settings from '../../../settings';
let config = require('./../../../config/' + settings.environment + '.config').default;
const MEDIA = config.media;
let mkdirp = require('mkdirp');
var uuidv1 = require('uuid/v1');
const fs = require('fs');
let Promise = require('bluebird');

const path = require('path');
var pdf = require('html-pdf');
import moment from "moment";
const nodemailer = require("nodemailer");
import EmailService from "../CommonService/email.service";
import SettingsService from '../Settings/settings.service';
import ProductService from "../Product/Product.service";
var pdfCount = 0,
  excelCount = 0;
var imgSrc = path.normalize('file://' + __dirname + '../../../../logo/logo.png');


const BrandController = {
  getBrandById: async (req, res) => {
    try {
      const brand = await BrandService.getBrandById(req.params.brand_id);
      logger.info(" Getting brand by id = " + req.params.brand_id);
      res.send({
        code: 200,
        status: "success",
        message: "List the brand",
        data: brand
      });
    } catch (error) {
      logger.error("Error in getting brand :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting brand",
        data: {}
      });
    }
  },
  createBrand: async (req, res) => {
    try {
      let url;
      let bodyData = JSON.parse(JSON.stringify(req.body));
      let data = bodyData.bodyData ? JSON.parse(bodyData.bodyData) : req.body;
      if (req.files && req.files.url) {
        let imageFile = req.files.url;
        if (imageFile) {
          let extension = imageFile.mimetype.split("/")[1];

          let filePath = MEDIA.local_file_path + "brand_image" + "/";
          let file_name = uuidv1() + "-" + Date.now() + '.' + extension;
          url = "/static/brand_image" + "/" + file_name;
          if (!MEDIA.useS3) {
            await moveFile(imageFile, filePath, file_name)
              .catch((error) => {
                imageError = true;
                return badResponse(res, "Image Upload Failed");
              });
          }
        }
      }
      let brand ={};
          brand.brand_name = data.brand_name;
          brand.description = data.description;
          brand.url = url;
          brand.categoryDetails = []
        Promise.map(data.category,item=>{
          brand.categoryDetails.push({category:item.id})
          if(brand.categoryDetails.length === data.category.length) return;
        }).then(async()=>{
          let savedBrand = await BrandService.createBrand(brand);
        if (savedBrand.url && savedBrand.url !== "") {
          savedBrand.url = config.api_end_point + savedBrand.url
        } else {
          savedBrand.url = ""
        }

        logger.info("New brand is created");
        return res.send({
          code: 200,
          status: "success",
          message: "New brand is created",
          data: savedBrand
        });
        }).catch((error)=>{
            logger.error("Error in creating brand "+error);
            res.status(400).send({
              code: 400,
              status: "error",
              message: "Error in creating brand",
              data: []
            });
          })
        
      }
     catch (error) {
      logger.error("Error in Creating brand :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in Creating brand",
        data: []
      });
    }
  },
  updateBrand: async (req, res) => {
    try {
      let url;
      let bodyData = JSON.parse(JSON.stringify(req.body));
      let data = bodyData.bodyData ? JSON.parse(bodyData.bodyData) : req.body;
      if (req.files && req.files.url) {
        let imageFile = req.files.url;
        if (imageFile) {
          let extension = imageFile.mimetype.split("/")[1];

          let filePath = MEDIA.local_file_path + "brand_image" + "/";
          let file_name = uuidv1() + "-" + Date.now() + '.' + extension;
          url = "/static/brand_image" + "/" + file_name;
          if (!MEDIA.useS3) {
            await moveFile(imageFile, filePath, file_name)
              .catch((error) => {
                imageError = true;
                return badResponse(res, "Image Upload Failed");
              });
          }
        }
      }
      let brand ={};
      brand.brand_name = data.brand_name;
      brand.description = data.description;
      brand.url = url;
      brand.categoryDetails = []
    Promise.map(data.category,item=>{
      brand.categoryDetails.push({category:item.id})
      if(brand.categoryDetails.length === data.category.length) return;
    }).then(async()=>{
      let savedBrand = await BrandService.updateBrand(brand,req.params.brand_id);
    if (savedBrand.url && savedBrand.url !== "") {
      savedBrand.url = config.api_end_point + savedBrand.url
    } else {
      savedBrand.url = ""
    }

    logger.info(" brand is updated");
    return res.send({
      code: 200,
      status: "success",
      message: "brand is updated",
      data: savedBrand
    });
    }).catch((error)=>{
        logger.error("Error in updating brand "+error);
        res.status(400).send({
          code: 400,
          status: "error",
          message: "Error in updating brand",
          data: []
        });
      })
    } catch (error) {
      console.log(error)
      logger.error("Error in brand Category :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in brand Category",
        data: {}
      });
    }
  },
  getAllBrand: async (req, res) => {
    try {
      var search;
    if (_.isEmpty(req.query.searchkey)) {
      search = '';
    } else {
      search = req.query.searchkey
    }
    let limit = Number(req.query.limit) ;
    let offset = parseInt(req.query.skip);
    let skip = (offset - 1) * limit;
      const brandData = await BrandService.getAllBrand(search, limit, skip);
      logger.info(" Getting all brands");
      res.send({
        code: 200,
        status: "success",
        message: "Listing All brands",
        data: brandData
      });
    } catch (error) {
      logger.error("Error in getting brands :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting brands record",
        data: []
      });
    }
  },
  getBrands: async (req,res)=>{
    try {
      const brandData = await BrandService.getBrands();
      logger.info(" Getting  brands");
      res.send({
        code: 200,
        status: "success",
        message: "Listing brands",
        data: brandData
      });
    } catch (error) {
      logger.error("Error in getting brands :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting brands record",
        data: []
      });
    }
  },
  deleteBrand: async (req, res) => {
    try {
      var ids = (req.params.brand_id).split(',');
      let inProduct = await ProductService.checkBrands(ids,1);
      if(inProduct) return res.send({code:200,status:"Exist"});
      var data = await BrandService.deletebrandByIds(ids);
      res.send({
        code: 200,
        status: "success",
        message: "brand is successfully deleted",
        data: data
      });
    } catch (error) {
      res.status(400).send({
        code: 400,
        status: "error",
        message: "brand does not delete",
        data: {}
      });

    }
  }
};
let badResponse = function (res, msg) {
  res.status(400).send({
    code: 400,
    status: "error",
    message: msg,
    data: []
  });
};
let moveFile = async (media_file, filePath, file_name) => {
  return await new Promise((resolve, reject) => {

    mkdirp(filePath, function (err) {
      if (err) {
        logger.error(err);
        reject({
          code: 400,
          status: "error",
          message: "Error in Uploading file",
          data: []
        });
      } else {
        media_file.mv(filePath + file_name, function (err) {
          if (err) {
            logger.error(err);
            reject({
              code: 400,
              status: "error",
              message: "Error in Uploading file",
              data: []
            });
          } else {
            logger.log("File Moved")
            resolve({
              code: 200,
              status: "success",
              message: "Uploaded Successfully",
              data: []
            })
          }
        });
      }
    });
  });
}

export default BrandController;