import express from "express";
import ProductController from "./Prdouct.controller"
import UserAuthController from "../UserAuthentication/userAuth.controller";


const ProductRouter = express.Router();

ProductRouter.get('/product/:id', UserAuthController.verify, ProductController.getProductById);
ProductRouter.get('/product', UserAuthController.verify, ProductController.getProductByPagination);
ProductRouter.get('/product/drop_down/list',  ProductController.getProduct);
ProductRouter.get('/product/brand/brand_name', UserAuthController.verify, ProductController.getProductByBrand);
ProductRouter.get('/product/brand/brand_name/fmcg/filter', UserAuthController.verify, ProductController.getProductByBrandFMCG);
ProductRouter.get('/product/report/excel', UserAuthController.verify, ProductController.createExcel);
ProductRouter.get('/product/report/excel/:file_name', ProductController.excelFileDownload);
ProductRouter.get('/product/report/pdf/:file_name', ProductController.pdfFileDownload);
ProductRouter.get('/product/email/pdf/:email', ProductController.pdfEmailSend)
ProductRouter.get('/product/email/excel/:email', ProductController.excelEmailSend)
ProductRouter.get('/brand/img/:category_id',ProductController.getBrandImage);
ProductRouter.get('/product/report/pdf',ProductController.createPDF);
ProductRouter.post('/product/:supplier_id', UserAuthController.verify, ProductController.createProduct);
ProductRouter.put('/product/:product_id', UserAuthController.verify, ProductController.updateProduct);
ProductRouter.delete('/product/:product_id', UserAuthController.verify, ProductController.deleteProduct);
export default ProductRouter;