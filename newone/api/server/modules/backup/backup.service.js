import backupModel from '../backup/backup.model';
import moment from "moment";

const backupService ={
    add:async (data)=>{
        try {
            return new backupModel(data).save();
        } catch (error) {
            throw error
        }
    },
    getFile : async ()=>{
        try {
            
            let afterSevenDays = moment().endOf('day').subtract(7,'days');
            return await backupModel.find({createdAt:{$lte:afterSevenDays}}).lean();
        } catch (error) {
            throw error
        }
    },
    deleteFile : async (id)=>{
        try {
            return await backupModel.remove({fileId:id});
        } catch (error) {
            throw error
        }
    }
}
export default backupService;