import UserService from "../User/user.service";
import UserAuthCtrl from "../UserAuthentication/userAuth.controller";
import UserAuthService from "../UserAuthentication/userAuth.service";
import CustomerService from "../Customer/customer.service";
import logger from "../../../config/logger.config";
let masterData  = require('../masterdata.json');
var access = require("safe-access");
import moment from "moment";
var _ = require('lodash');
let excelCount =0;
let pdf = require('html-pdf');
import settings from './../../../settings';
let config = require('./../../../config/' + settings.environment + '.config').default;
let mkdirp = require('mkdirp');
//let ffmpeg = require('fluent-ffmpeg');;
const fs = require('fs');
const UTF8 = 'utf8';
var path = require("path");
const webEndPoint = config.web_end_point;
const MEDIA = config.media;
var imgSrc = path.normalize('file://' + __dirname + '../../../../logo/logo.png');
var fmcg_logo = path.normalize('file://' + __dirname + '../../../../logo/FMCG_logo.png');
const UserController = {};

UserController.getAllUsers = async (req, res) => {
  try {
    var search;
  if (_.isEmpty(req.query.searchkey)) {
    search = '';
  } else {
    search = req.query.searchkey
  }
  let limit = Number(req.query.limit) || 10;
  let offset = (req.query.skip) ? parseInt(req.query.skip) : 1
  let skip = (offset - 1) * limit;
  let type = req.query.type || 'rider'
  let status = req.query.status|| 'active'
    const users = await UserService.getAllUsers(search, limit, skip,type,status);
    logger.info(" Getting all Users");
    res.send({
      code: 200,
      status: "success",
      message: "Listing All Users",
      data: users
    });
  } catch (error) {
    logger.error("Error in getting User record :" + error);
    res.status(400).send({
      code: 400,
      status: "error",
      message: "Error in getting user record",
      data: []
    });
  }
};

UserController.updateBusinessAdminbyBussinessAdminId = async (req, res) => {
  try {
    const user_id = req.params.user_id;
    let businessAdmin = {};
    businessAdmin.user_id = user_id;

    access(req, "body.first_name") == undefined
      ? 0
      : (businessAdmin.first_name = access(req, "body.first_name"));
    access(req, "body.last_name") == undefined
      ? 0
      : (businessAdmin.last_name = access(req, "body.last_name"));
    access(req, "body.phone_number") == undefined
      ? 0
      : (businessAdmin.phone_number = access(req, "body.phone_number"));
    access(req, "body.email") == undefined
      ? 0
      : (businessAdmin.email = access(req, "body.email"));
    access(req, "body.status") == undefined
      ? 0
      : (businessAdmin.status = access(req, "body.status"));


      let file = req.files;
      let imageUrl = ""
      if (file && file.image) {
        let imageFile = file.image;
        if(imageFile)
        {
        let extension = imageFile.mimetype.split("/")[1];
  
        let filePath = MEDIA.local_file_path + user_id + "/" + "profile_image" + "/";
        let file_name = user_id + "_profile_" + Date.now() + '.' + extension;
        imageUrl = config.api_end_point + "/static/" + user_id + "/" + "profile_image" + "/" + file_name;
        if (!MEDIA.useS3) {
  
          await moveFile(imageFile, filePath, file_name)
            .catch((error) => {
              return badResponse(res, "Image Upload Failed");
            });
        }
      }
      businessAdmin.logo = imageUrl;
      }


    const updatedBusinessAdmin = await UserService.updateBusinessAdminbyBussinessAdminId(
      user_id,
      businessAdmin
    );

    let payload = {
      user_id: updatedBusinessAdmin._id,
      first_name: updatedBusinessAdmin.first_name,
      last_name: updatedBusinessAdmin.last_name,
      email: updatedBusinessAdmin.email,
      phone_number: updatedBusinessAdmin.phone_number,
      logo: updatedBusinessAdmin.logo,
      type: updatedBusinessAdmin.type
    };

    res.status(200).send({
      code: 200,
      status: "success",
      message: "updated successfully",
      data:  await UserAuthService.getJwtToken(payload)
    });
  } catch (error) {
    res.status(400).send({
      code: 400,
      status: "error",
      message: "Error in update ",
      data: []
    });
  }
};


UserController.updateBusinessAdminbyBussinessAdminId = async (req, res) => {
  try {
    const user_id = req.params.user_id;
    let businessAdmin = {};
    businessAdmin.user_id = user_id;

    (access(req, "body.first_name") == undefined ||  access(req, "body.first_name") == "null")
      ? 0
      : (businessAdmin.first_name = access(req, "body.first_name"));
    (access(req, "body.last_name") == undefined ||  access(req, "body.last_name") == "null")
      ? 0
      : (businessAdmin.last_name = access(req, "body.last_name"));
    (access(req, "body.phone_number") == undefined || access(req, "body.phone_number") == "null")
      ? 0
      : (businessAdmin.phone_number = access(req, "body.phone_number"));
      /*
    access(req, "body.email") == undefined
      ? 0
      : (businessAdmin.email = access(req, "body.email"));
    access(req, "body.status") == undefined
      ? 0
      : (businessAdmin.status = access(req, "body.status"));
      */


      let file = req.files;
      let imageUrl = ""
      if (file && file!="null" && file.image) {
        let imageFile = file.image;
        if(imageFile)
        {
        let extension = imageFile.mimetype.split("/")[1];
  
        let filePath = MEDIA.local_file_path + user_id + "/" + "profile_image" + "/";
        let file_name = user_id + "_profile_" + Date.now() + '.' + extension;
        imageUrl = config.api_end_point + "/static/" + user_id + "/" + "profile_image" + "/" + file_name;
        if (!MEDIA.useS3) {
  
          await moveFile(imageFile, filePath, file_name)
            .catch((error) => {
              console.log(error);
              return badResponse(res, "Image Upload Failed");
            });
        }
        businessAdmin.logo = imageUrl;
      }

      }


    const updatedBusinessAdmin = await UserService.updateBusinessAdminbyBussinessAdminId(
      user_id,
      businessAdmin
    );

    let payload = {
      user_id: updatedBusinessAdmin._id,
      first_name: updatedBusinessAdmin.first_name,
      last_name: updatedBusinessAdmin.last_name,
      email: updatedBusinessAdmin.email,
      phone_number: updatedBusinessAdmin.phone_number,
      logo: updatedBusinessAdmin.logo,
      type: updatedBusinessAdmin.type
    };

    res.status(200).send({
      code: 200,
      status: "success",
      message: "updated successfully",
      data:  await UserAuthService.getJwtToken(payload)
    });
  } catch (error) {
    res.status(400).send({
      code: 400,
      status: "error",
      message: "Error in update ",
      data: []
    });
  }
};


UserController.userChangePassword = async (req, res) => {
  try {
  
    const user_id = req.params.user_id;
    let businessAdmin = {};
    businessAdmin.user_id = user_id;

    businessAdmin.old_password = req.body.old_password;
    businessAdmin.password = req.body.password;
    businessAdmin.confirm_password =req.body.confirm_password;
    if(businessAdmin.old_password!=businessAdmin.password && businessAdmin.password===businessAdmin.confirm_password)
    {
      const checkChangePasswordStatus = await UserService.checkChangePassword(user_id,businessAdmin)
      if(checkChangePasswordStatus==false)
      {
        res.status(400).send({
          code: 400,
          status: "error",
          message: "Invalid Old Password",
        });
      }
      else
      {
      const updatedBusinessAdmin = await UserService.userChangePassword(
        user_id,
        businessAdmin
      );  
      res.status(200).send({
        code: 200,
        status: "success",
        message: "Password Changed Successfully. " + updatedBusinessAdmin.email,
        data:  updatedBusinessAdmin
      });
    }
    }
    else
    {
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Invalid Password ",
      });
    } 
} catch (error) {
  res.status(400).send({
    code: 400,
    status: "error",
    message: "Error in update",
    data: []
  });
}
};



UserController.updateUserProfile = async (req, res) => {
  try {
    const user_id = req.params.user_id;
    let userUpdate = {};
    userUpdate.user_id = user_id;

    (access(req, "body.first_name") == undefined ||  access(req, "body.first_name") == "null")
      ? 0
      : (userUpdate.first_name = access(req, "body.first_name"));
    (access(req, "body.last_name") == undefined ||  access(req, "body.last_name") == "null")
      ? 0
      : (userUpdate.last_name = access(req, "body.last_name"));
    (access(req, "body.phone_number") == undefined || access(req, "body.phone_number") == "null")
      ? 0
      : (userUpdate.phone_number = access(req, "body.phone_number"));
      let file = req.files;
      let imageUrl = ""
      if (file && file!="null" && file.image) {
        let imageFile = file.image;
        if(imageFile)
        {
        let extension = imageFile.mimetype.split("/")[1];
  
        let filePath = MEDIA.local_file_path + user_id + "/" + "profile_image" + "/";
        let file_name = user_id + "_profile_" + Date.now() + '.' + extension;
        imageUrl = config.api_end_point + "/static/" + user_id + "/" + "profile_image" + "/" + file_name;
        if (!MEDIA.useS3) {
  
          await moveFile(imageFile, filePath, file_name)
            .catch((error) => {
              console.log(error);
              return badResponse(res, "Image Upload Failed");
            });
        }
        userUpdate.logo = imageUrl;
      }

      }


    const updatedUser = await UserService.updateBusinessAdminbyBussinessAdminId(
      user_id,
      userUpdate
    );

    let payload = {
      user_id: updatedUser._id,
      first_name: updatedUser.first_name,
      last_name: updatedUser.last_name,
      email: updatedUser.email,
      phone_number: updatedUser.phone_number,
      logo: updatedUser.logo,
      type: updatedUser.type
    };

    res.status(200).send({
      code: 200,
      status: "success",
      message: "updated successfully",
      data:  await UserAuthService.getJwtToken(payload)
    });
  } catch (error) {
    res.status(400).send({
      code: 400,
      status: "error",
      message: "Error in update ",
      data: []
    });
  }
};


UserController.addUser = async (req, res) => {
  try {
    let User = {};
    User.first_name = req.body.first_name;
    User.last_name = req.body.last_name;
    user.country_code = req.body.country_code;
    User.phone_number = req.body.phone_number;
    User.email = req.body.email.toLowerCase();
    User.type = req.body.type;
    User.password = req.body.password;
    User.status = req.body.status || 'in_active';
    let savedUser = await UserService.addUser(User);
    await UserAuthCtrl.sendEmailVerification(savedUser.email, savedUser.password_uuid);
    res.send({
      code: 200,
      status: "success",
      message: "Sucessfully added user records",
      data: savedUser
    });
  } catch (error) {
    logger.error("Error in adding User record" + error);
    res.send({
      code: 400,
      status: "error",
      message: "Error in adding User record",
      data: []
    });
  }
};

UserController.getUserByUserId = async (req, res) => {
  try {
    const user_id = req.params.user_id;
    const user = await UserService.getUserByUserId(user_id);
    res.send({
      code: 200,
      status: "success",
      message: "Sucessfully retrived user details",
      data: user
    });
  } catch (error) {
    logger.error("Error in fetching User record" + error);
    res.status(400).send({
      code: 400,
      status: "error",
      message: "Error in fetching User record",
      data: []
    });
  }
};
UserController.changeStatus= async (req, res) => {
  try {
    let status = req.body.status
    const user = await UserService.changeStatus(req.params.id, status);
    res.status(200).send({
      code: 200,
      status: "success",
      message: "user status is  changed",
      data: user
    });
  } catch (error) {
    res.status(401).send({
      code: 401,
      status: "error",
      message: "Error Occured",
      data: {}
    });
  }


};
UserController.getAllUserByType = async (req, res) => {
  try {
    const user_type = req.params.user_type;
    const users = await UserService.getAllUserByType(user_type);
    res.send({
      code: 200,
      status: "success",
      message: "Sucessfully retrived All users details, with type" + user_type,
      data: users
    });
  } catch (error) {
    logger.error("Error in fetching User record" + error);
    res.status(400).send({
      code: 400,
      status: "error",
      message: "Error in fetching Users with type " + user_type,
      data: []
    });
  }
};
UserController.deleteUser = async (req, res) => {
  try {
    const id = req.params.id;
    const users = await UserService.deleteUser(id);
    res.send({
      code: 200,
      status: "success",
      message: "Sucessfully User Deleted",
      data: users
    });
  } catch (error) {
    logger.error("Error in delete User" + error);
    res.status(400).send({
      code: 400,
      status: "error",
      message: "Error in delete User" ,
      data: {}
    });
  }
};



UserController.createExcel = async (req,res)=>{
  try {
    var products = "",
      productList;
      const users = await CustomerService.getAllCustomers('',null,null,'customer','');
    var Excel = require('exceljs');
    var workbook = new Excel.Workbook();
    var worksheet = workbook.addWorksheet('My Sheet');
    worksheet.columns = [{
        header: 'First Name',
        key: 'first_name',
        width: 20
      },
      {
        header: 'Last Name',
        key: 'last_name',
        width: 20
      },
      {
        header: 'Email',
        key: 'email',
        width: 40
      },
      {
        header: 'Mobile Number',
        key: 'mobile_number',
        width: 20
      },
      {
        header: 'Phone Number',
        key: 'phone_number',
        width: 15
      },
      {
        header: 'City',
        key: 'city',
        width: 10
      },
      {
        header: 'Country Name',
        key: 'country_name',
        width: 15
      },
      {
        header: 'Permanent Address',
        key: 'permanent_address',
        width: 40
      },
      {
        header: 'Shipping Address',
        key: 'shipping_address',
        width: 40
      },
      {
        header: 'Company Name',
        key: 'company_name',
        width: 20
      },
      {
        header: 'Status',
        key: 'status',
        width: 10
      }
    ];
    var row = 2
    users.users.map((field) => {
      if(field.status =='in_active'){
        field.status = 'Inactive'
      }else{
        field.status=field.status;
      }
      field.mobile_number = "+"+field.country_code + "-"+field.mobile_number;
      let country= _.find(masterData, { 'code': field.country_name })
      field.country_name = country ?country.name:""
      worksheet.addRow(field);
      worksheet.getCell('N' + row).alignment = {
        vertical: 'bottom',
        horizontal: 'right'
      };
      worksheet.getCell('O' + row).alignment = {
        vertical: 'bottom',
        horizontal: 'right'
      };

      row++;
    })
    worksheet.getColumn('J').numFmt = '0.00';
      worksheet.getColumn('K').numFmt = '0.00';
      worksheet.getColumn('L').numFmt = '0.00';
    excelCount++;
    const fileName = "FMCG_User_excel(" + excelCount + ").xlsx";
    const filePath = path.join(__dirname + "/excel", fileName)
    workbook.xlsx.writeFile(filePath)
      .then(function (data) {
        setTimeout(function () {
          if (fs.existsSync(filePath)) {
            fs.unlinkSync(filePath);
          }
        }, 300000)
        return res.send({
          code: 200,
          status: "success",
          message: "Creating Excel file for FMCG User",
          data: {
            path: config.api_end_point + '/api/users/report/excel/' + fileName,
            extension: "xlsx"
          }
        });
      });
  } catch (error) {
    res.status(400).send({
      code: 400,
      status: "error",
      message: "Error in Creating Excel file for Product",
      data: {}
    });
  }
}
UserController.excelFileDownload = async (req, res) => {
  try {
    var fileName = req.params.file_name
    const filePath = path.join(__dirname + "/excel", fileName)
    res.download(filePath)
  } catch (error) {
    res.status(400).send({
      code: 400,
      status: "error",
      message: "Error occured",
      data: {}
    });
  }
};


let moveFile = async (media_file, filePath, file_name) => {
  return await new Promise((resolve, reject) => {

    mkdirp(filePath, function (err) {
      if (err) {
        logger.error(err);
        reject({
          code: 400,
          status: "error",
          message: "Error in Uploading file",
          data: []
        });
      } else {
        media_file.mv(filePath + file_name, function (err) {
          if (err) {
            logger.error(err);
            reject({
              code: 400,
              status: "error",
              message: "Error in Uploading file",
              data: []
            });
          } else {
            logger.log("File Moved")
            resolve({
              code: 200,
              status: "success",
              message: "Uploaded Successfully",
              data: []
            })
          }
        });
      }
    });
  });
}
export default UserController;
