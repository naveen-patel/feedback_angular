import ReportService from "./report.service";

import logger from "../../../config/logger.config";
var _ = require("lodash");
import settings from '../../../settings';
let config = require('./../../../config/' + settings.environment + '.config').default;
const fs = require('fs');
const path = require('path');
var pdf = require('html-pdf');
var pdfCount = 0;
var imgSrc = path.normalize('file://' + __dirname + '../../../../logo/logo.png');
var fmcg_logo = path.normalize('file://' + __dirname + '../../../../logo/FMCG_logo.png');


const reportController = {

  getReportList: async (req, res)=>{
  if(Number(req.query.index) === 0){
  orderReport(req,res)
  }else if(Number(req.query.index) === 1){
    salesReport(req,res);
  }else{
    lowStockItems(req,res);
  }
  },
  createLowStockReportPDF:async (req,res)=>{
    try {
      var html = fs.readFileSync('./server/EmailTemplates/stock_template_pdf.html', 'utf8');
      var options = {
        "format": "A4",
        "orientation": "portrait",
        'Content_Type': 'application/pdf',
        "border": {
          "top": "0.2in",
          "right": "0.2in",
          "bottom": "0.2in",
          "left": "0.2in"
        },
        "header": {
          "height": "5mm",
          "contents": "<div> <img style='width:70px;height:45px'src='" + fmcg_logo + "' /><img style='width:200px;height:35px'src='" + imgSrc + "' align='right' /></div><hr style='border-color: rgb(255, 0, 0);' size='1'>"
        },
        "footer": {
          "height": "16mm",
          "contents": {
            default: '<hr style="border-color:rgb(255, 0, 0);" size="1"><div style="font-size: 7px;color: #949393;">Maxbrands Marketing</div><div style="font-size: 7px;color: #949393;">Hotline M: 0097154 461 4848</div><div style="font-size: 7px;color: #949393;">PO Box 239492, Dubai, UAE. | T: +971 4250 1134 | E:info@maxbrandsintl.com WEB <a style="font-size: 7px;color: #949393;" href="http://www.maxbrandsintl.com">www.maxbrandsintl.com</a></div>'
          }
        },
        "timeout": "1200000"
      };
      
      let count = 0;
      let defaultBreak = 27;
      let strOrderList='';
      const products = await ReportService.getLowStockReport(null);
       products.report.forEach((product,i) => {
        strOrderList+= `<tr>
        <td style="font-size:  8px">${product.brand.brand_name?product.brand.brand_name:''}</td>
        <td style="font-size:  8px">${product.supplier_id.name?product.supplier_id.name:''}</td>
        <td style="font-size:  8px">${product.quantity?product.quantity:0}</td>`
        count++;
        if((count >=defaultBreak) &&(i+1 < products.report.length)){
          console.log("TCL: count >=defaultBreak", count >=defaultBreak)
          count = 0;
          defaultBreak =33;
          console.log("additional br first" + count)
          strOrderList += "</table><div style='page-break-before: always;break-before: always;'></div><br><br><br><table style='width:100%'>";
        }else if(count===products.report.length){
          strOrderList+="</table>"
        }
       
      });
      pdfCount++; 
      const fileName = "low_stock_report(" + pdfCount + ").pdf";
      const filePath = path.join(__dirname + "/pdf", fileName)
      // strOrderList = strOrderList.replace(/{logo}/g, config.api_end_point + '/image/logo/logo.png');
      var pdftemplate = html
                            .replace(/{strOrderList}/g, strOrderList)
                           
                            ;
      pdftemplate += "<div id='pageHeader'><div style='display:flex;flex-wrap: wrap; justify-content:space-between'> <img style='width:200px;height:40px'src='" + fmcg_logo + "' /> <img style='width:200px;height:40px'src='" + imgSrc + "' /></div><hr style='border-color: rgb(255, 0, 0);' size='1'></div>";
      
      pdf.create(pdftemplate, options).toFile(filePath, function (err, pdfPath) {
        if (err) {
          return res.status(400).send({
            code: 400,
            status: "error",
            message: "Error in getting low stock",
            data: {}
          });
        } else {
          setTimeout(function () {
            if (fs.existsSync(filePath)) {
              fs.unlinkSync(filePath);
            }
          }, 300000)
          return res.send({
            code: 200,
            status: "success",
            message: "Creating PDF file for low stock",
            data: {
              path: config.api_end_point + '/api/report/pdf/' + fileName,
              extension: "pdf"
            }
          });
        }
      });
    } catch (error) {
      return res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting Product",
        data: {}
      });
    }
  },
  createSalesReportPDF:async (req,res)=>{
    // try {
      var html = fs.readFileSync('./server/EmailTemplates/salesReport_pdf.html', 'utf8');
      var options = {
        "format": "A4",
        "orientation": "portrait",
        'Content_Type': 'application/pdf',
        "border": {
          "top": "0.2in",
          "right": "0.2in",
          "bottom": "0.2in",
          "left": "0.2in"
        },
        "header": {
          "height": "5mm",
          "contents": "<div> <img style='width:70px;height:45px'src='" + fmcg_logo + "' /><img style='width:200px;height:35px'src='" + imgSrc + "' align='right' /></div><hr style='border-color: rgb(255, 0, 0);' size='1'>"
        },
        "footer": {
          "height": "16mm",
          "contents": {
            default: '<hr style="border-color:rgb(255, 0, 0);" size="1"><div style="font-size: 7px;color: #949393;">Maxbrands Marketing</div><div style="font-size: 7px;color: #949393;">Hotline M: 0097154 461 4848</div><div style="font-size: 7px;color: #949393;">PO Box 239492, Dubai, UAE. | T: +971 4250 1134 | E:info@maxbrandsintl.com WEB <a style="font-size: 7px;color: #949393;" href="http://www.maxbrandsintl.com">www.maxbrandsintl.com</a></div>'
          }
        },
        "timeout": "1200000"
      };
      
      let count = 0;
      let defaultBreak = 27;
      let strOrderList='';
      let startDate = req.query.startDate;
      let endDate = req.query.endDate;
      let report = await ReportService.getSalesReport(startDate,endDate,null,null);
      let tempArray =[];
      report.order.map(item=>{
        item.product.map(p=>{
          let myobjec={}
          let myIndex =tempArray.map(function(e) { return e._id; }).indexOf(p.product_detail._id)
        if(myIndex ==-1){
                myobjec._id = p.product_detail._id;
                myobjec.brand_name = p.product_detail.brand.brand_name;
                myobjec.quantity = Number(p.quantity);
                myobjec.total_amount_aed = Number(p.total_price_aed)
                myobjec.total_amount_usd = Number(p.total_price_usd)
                tempArray.push(myobjec);
        }else{
          tempArray[myIndex].quantity += Number(p.quantity);
          tempArray[myIndex].total_amount_aed += Number(p.total_price_aed)
          tempArray[myIndex].total_amount_usd +=  Number(p.total_price_usd)
        }
        })
      })
      tempArray.forEach((order,i) => {
        
        strOrderList+= `<tr>
        <td style="font-size:  8px">${order.brand_name?order.brand_name:''}</td>
        <td style="font-size:  8px">${order.quantity}</td>
        <td style="font-size:  8px;text-align:right">${Number(order.total_amount_aed).toFixed(2)}</td>
        <td style="font-size:  8px;text-align:right">${Number(order.total_amount_usd).toFixed(2)}</td>`
        count++;
        if((count >=defaultBreak) &&(i+1 < tempArray.length)){
          count = 0;
          defaultBreak =33;
          console.log("additional br first" + count)
          strOrderList += "</table><div style='page-break-before: always;break-before: always;'></div><br><br><br><table style='width:100%'>";
        }else if(count===tempArray.length){
          strOrderList+="</table>"
        }
       
      });
      pdfCount++; 
      const fileName = "sales_report(" + pdfCount + ").pdf";
      const filePath = path.join(__dirname + "/pdf", fileName)
      // strOrderList = strOrderList.replace(/{logo}/g, config.api_end_point + '/image/logo/logo.png');
      var pdftemplate = html
                            .replace(/{strOrderList}/g, strOrderList)
                           
                            ;
      pdftemplate += "<div id='pageHeader'><div style='display:flex;flex-wrap: wrap; justify-content:space-between'> <img style='width:200px;height:40px'src='" + fmcg_logo + "' /> <img style='width:200px;height:40px'src='" + imgSrc + "' /></div><hr style='border-color: rgb(255, 0, 0);' size='1'></div>";
      
      pdf.create(pdftemplate, options).toFile(filePath, function (err, pdfPath) {
        if (err) {
          return res.status(400).send({
            code: 400,
            status: "error",
            message: "Error in getting sales report",
            data: {}
          });
        } else {
          setTimeout(function () {
            if (fs.existsSync(filePath)) {
              fs.unlinkSync(filePath);
            }
          }, 300000)
          return res.send({
            code: 200,
            status: "success",
            message: "Creating PDF file for sales report",
            data: {
              path: config.api_end_point + '/api/report/pdf/' + fileName,
              extension: "pdf"
            }
          });
        }
      });
    // } catch (error) {
    //   return res.status(400).send({
    //     code: 400,
    //     status: "error",
    //     message: "Error in getting Product",
    //     data: {}
    //   });
    // }
  },
  createOrderReportPDF:async (req,res)=>{
    try {
      var html = fs.readFileSync('./server/EmailTemplates/orderReport_pdf.html', 'utf8');
      var options = {
        "format": "A4",
        "orientation": "portrait",
        'Content_Type': 'application/pdf',
        "border": {
          "top": "0.2in",
          "right": "0.2in",
          "bottom": "0.2in",
          "left": "0.2in"
        },
        "header": {
          "height": "5mm",
          "contents": "<div> <img style='width:70px;height:45px'src='" + fmcg_logo + "' /><img style='width:200px;height:35px'src='" + imgSrc + "' align='right' /></div><hr style='border-color: rgb(255, 0, 0);' size='1'>"
        },
        "footer": {
          "height": "16mm",
          "contents": {
            default: '<hr style="border-color:rgb(255, 0, 0);" size="1"><div style="font-size: 7px;color: #949393;">Maxbrands Marketing</div><div style="font-size: 7px;color: #949393;">Hotline M: 0097154 461 4848</div><div style="font-size: 7px;color: #949393;">PO Box 239492, Dubai, UAE. | T: +971 4250 1134 | E:info@maxbrandsintl.com WEB <a style="font-size: 7px;color: #949393;" href="http://www.maxbrandsintl.com">www.maxbrandsintl.com</a></div>'
          }
        },
        "timeout": "1200000"
      };
      
      let count = 0;
      let defaultBreak = 27;
      let strOrderList='';
      let startDate = req.query.startDate;
      let endDate = req.query.endDate;  
      let report = await ReportService.getOrderReport(startDate,endDate,null,null);
      report.order.forEach((order,i) => {
      let total = order.product.reduce((a, b) => { return a + b.quantity }, 0);
      let day,month;
      if(new Date(order.order_date).getDate()>9 ){
        day = new Date(order.order_date).getDate();
      }else{
        day ='0'+new Date(order.order_date).getDate();
      }
      if(new Date(order.order_date).getMonth()>9 ){
        month = new Date(order.order_date).getMonth();
      }else{
        month ='0'+new Date(order.order_date).getMonth();
      }
      let order_date = day+'-'+month+'-'+new Date(order.order_date).getFullYear();
        strOrderList+= `<tr>
        <td style="font-size:  8px">${order_date?order_date:'NA'}</td>
        <td style="font-size:  8px">${order&&order.order_number&&order.order_number?order.order_number:'NA'}</td>
        <td style="font-size:  8px">${order&&order.user_id&&order.user_id.country_name?order.user_id.country_name:'NA'}</td>
        <td style="font-size:  8px">${total}</td>
        <td style="font-size:  8px;text-align:right">${Number(order.total_amount_aed).toFixed(2)}</td>
        <td style="font-size:  8px;text-align:right">${Number(order.total_amount_usd).toFixed(2)}</td>`
        count++;
        if((count >=defaultBreak) &&(i+1 < report.order.length)){
          count = 0;
          defaultBreak =33;
          console.log("additional br first" + count)
          strOrderList += "</table><div style='page-break-before: always;break-before: always;'></div><br><br><br><table style='width:100%'>";
        }else if(count===report.order.length){
          strOrderList+="</table>"
        }
       
      });
      pdfCount++; 
      const fileName = "order_report(" + pdfCount + ").pdf";
      const filePath = path.join(__dirname + "/pdf", fileName)
      console.log("TCL: filePath", filePath)
      // strOrderList = strOrderList.replace(/{logo}/g, config.api_end_point + '/image/logo/logo.png');
      var pdftemplate = html
                            .replace(/{strOrderList}/g, strOrderList)
                           
                            ;
      pdftemplate += "<div id='pageHeader'><div style='display:flex;flex-wrap: wrap; justify-content:space-between'> <img style='width:200px;height:40px'src='" + fmcg_logo + "' /> <img style='width:200px;height:40px'src='" + imgSrc + "' /></div><hr style='border-color: rgb(255, 0, 0);' size='1'></div>";
      
      pdf.create(pdftemplate, options).toFile(filePath, function (err, pdfPath) {
        if (err) {
          return res.status(400).send({
            code: 400,
            status: "error",
            message: "Error in getting low stock",
            data: {}
          });
        } else {
          setTimeout(function () {
            if (fs.existsSync(filePath)) {
              fs.unlinkSync(filePath);
            }
          }, 300000)
          return res.send({
            code: 200,
            status: "success",
            message: "Creating PDF file for low stock",
            data: {
              path: config.api_end_point + '/api/report/pdf/' + fileName,
              extension: "pdf"
            }
          });
        }
      });
    } catch (error) {
      return res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting Product",
        data: {}
      });
    }
  },
  pdfFileDownload: async (req, res) => {
    try {
      var fileName = req.params.file_name
      const filePath = path.join(__dirname + "/pdf", fileName)
      res.download(filePath)
    } catch (error) {
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error occured",
        data: {}
      });
    }
  },
};
async function salesReport(req,res){
  try {
    
    if(!req.query.startDate && !req.query.endDate) return res.status(400).send({status:"Error",message:"Please select start date and end date"});
    let limit = Number(req.query.limit) || null;
    let offset = parseInt(req.query.skip)
    let skip = (offset - 1) * limit;
    let startDate = req.query.startDate;
    let endDate = req.query.endDate;
    let report = await ReportService.getSalesReport(startDate,endDate,limit,skip);
    let tempArray =[];
    report.order.map(item=>{
      item.product.map(p=>{
        let myobjec={}
        let myIndex =tempArray.map(function(e) { return e._id; }).indexOf(p.product_detail._id)
      if(myIndex ==-1){
              myobjec._id = p.product_detail._id;
              myobjec.brand_name = p.product_detail.brand.brand_name;
              myobjec.quantity = Number(p.quantity);
              myobjec.total_amount_aed = Number(p.total_price_aed)
              myobjec.total_amount_usd = Number(p.total_price_usd)
              tempArray.push(myobjec);
      }else{
        tempArray[myIndex].quantity += Number(p.quantity);
        tempArray[myIndex].total_amount_aed += Number(p.total_price_aed)
        tempArray[myIndex].total_amount_usd +=  Number(p.total_price_usd)
      }
      })
    })
    logger.info('Report sales report is generated');
    res.send({
      code: 200,
      status: "success",
      message: "sales report",
      data: tempArray,
      count:report.count
    });
  } catch (error) {
    res.status(400).send({
      code: 400,
      status: "error",
      message: "Error in getting sales report",
      data: {},
      count:0
    });
  }
}


async function orderReport(req,res){
  try {
    if(!req.query.startDate && !req.query.endDate) return res.status(400).send({status:"Error",message:"Please select start date and end date"});
    let limit = Number(req.query.limit) || 10;
    let offset = (req.query.skip) ? parseInt(req.query.skip) : 1
    let skip = (offset - 1) * limit;
    let startDate = req.query.startDate;
    let endDate = req.query.endDate;
    let report = await ReportService.getOrderReport(startDate,endDate,limit,skip);
    let result_arr = report.order.map((item, index) => {
      let total = item.product.reduce((a, b) => { return a + b.quantity }, 0);
      item['quantity'] = total;
      return item;
    })
    logger.info('order report is generated');
    res.send({
      code: 200,
      status: "success",
      message: "order report",
      data: result_arr,
      count:report.count
    });
  } catch (error) {
    res.status(400).send({
      code: 400,
      status: "error",
      message: "Error in getting order report",
      data: {},
      count:0
    });
  }
}
  async function lowStockItems(req,res){
    try {
      let limit = Number(req.query.limit) || 10;
      let offset = (req.query.skip) ? parseInt(req.query.skip) : 1
      let skip = (offset - 1) * limit;
      let report = await ReportService.getLowStockReport(limit,skip);
      logger.info(' low stock report is generated');
      res.send({
        code: 200,
        status: "success",
        message: "low stock report",
        data: report.report,
        count:report.count
      });
    } catch (error) {
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting   low stock report",
        data: {},
        count:0
      });
    }
}
export default reportController;