import User from "../User/user.model";
import logger from "../../../config/logger.config";
import sha256 from "sha256";
var uuidv1 = require('uuid/v1');

const AdminService = {};

AdminService.getAlladmins = async (searchkey, limit, skip, type, status) => {
    try {
        var findObj = {
            $or: [{
              "first_name": {
                "$regex": searchkey,
                "$options": "i"
              }
            }, {
              "last_name": {
                "$regex": searchkey,
                "$options": "i"
              }
            }, {
              "email": {
                "$regex": searchkey,
                "$options": "i"
              }
            }, ],
            "type": type,
          }
        var json = {};
        const count = await User.find(findObj).count();
        const users = await User.find(findObj).sort({
            "createdAt": -1
          }).limit(limit).skip(skip).lean();
        json.totalcount = count;
        json.users = users
        return json;
    } catch (error) {
        throw error;
    }
}

AdminService.addAdmin = async user => {
    try {
        //user.password = await sha256(user.password);
        let userToAdd = new User(user);
        const savedUser = await userToAdd.save();
        return savedUser;
    } catch (error) {
        throw error;
    }
};


AdminService.getAdminByAdminId = async user_id => {
    try {
        const user = await User.findOne({
            _id: Object(user_id)
        });
        return user;
    } catch (error) {
        throw error;
    }
};

AdminService.updateAdminByAdminId = async (id, data) => {
    try {
        let user = await User.findByIdAndUpdate({_id: Object(id) }, data);
        return user;
    } catch (error) {
        throw error;
    }
}

AdminService.changeStatus = async (id, status) => {
    try {
        const update = await User.findOneAndUpdate({
            _id: id
        }, {
                status: status
            }, {
                strict: true,
                new: true
            });
        return update;
    } catch (error) {
        throw error;
    }
};


export default AdminService;