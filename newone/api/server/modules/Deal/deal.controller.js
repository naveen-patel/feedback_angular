import DealService from "./deal.service";
import ProductService from "../Product/Product.service";
import DeviceService from '../User/user.service';
import logger from "../../../config/logger.config";
var _ = require("lodash");
import settings from '../../../settings';
let config = require('./../../../config/' + settings.environment + '.config').default;
const MEDIA = config.media;
let mkdirp = require('mkdirp');
var uuidv1 = require('uuid/v1');
const fs = require('fs');
const path = require('path');
var pdf = require('html-pdf');
let firebase = require("firebase-admin");
let Promise = require('bluebird');
let serviceAccount = config.firbase;
firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount),
  databaseURL: "https://maxbrand-8f2f9.firebaseio.com"
});
import moment from "moment";
const nodemailer = require("nodemailer");
import EmailService from "../CommonService/email.service";
import SettingsService from '../Settings/settings.service';
import DeviceModel from "../User/user.device.model";
import { cpus } from "os";
var pdfCount = 0,
  excelCount = 0;
var imgSrc = path.normalize('file://' + __dirname + '../../../../logo/logo.png');


const DealController = {
  getDealById: async (req, res) => {
    try {
      const deal = await DealService.getDealById(req.params.deal_id);
      logger.info(" Getting deal by id = " + req.params.deal_id);
      res.send({
        code: 200,
        status: "success",
        message: "List the deal",
        data: deal
      });
    } catch (error) {
      logger.error("Error in getting deal :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting deal",
        data: {}
      });
    }
  },
  getActiveDeal: async (req, res) => {
    try {
      let location = req.params.location;
      const deal = await DealService.getActiveDeal(location);
      logger.info(" Getting active deals");
      res.send({
        code: 200,
        status: "success",
        message: "Getting active deals",
        data: deal
      });
    } catch (error) {
      logger.error("Error in Getting active deals :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in Getting active deals",
        data: {}
      });
    }
  },
  createDeal: async (req, res) => {
    try {
      let deal ={};
          deal.product_id = req.params.product_id;
          deal.deal_name = req.body.deal_name;
          deal.deal_percentage = req.body.deal_percentage;
          deal.actual_price_aed = req.body.actual_price_aed;
          deal.offer_price_aed = req.body.offer_price_aed;
          deal.actual_price_usd = req.body.actual_price_usd;
          deal.offer_price_usd = req.body.offer_price_usd;
          deal.deal_start_date = req.body.deal_start_date;
          deal.deal_end_date = req.body.deal_end_date;
        let dealExist = await DealService.getDealByProductId(req.params.product_id);
        if(dealExist){
          logger.info("Deal with this product"+req.params.product_id+"Active");
          return res.send({
            code: 204,
            status: "Active",
            message: "Currenlty deal is going on this product",
            data: dealExist
          });
        }else{

          let firebaseTokens = await DeviceService.getDevice();
          let tokens1 =[];
          Promise.map(firebaseTokens,(device)=>{
            device.device.forEach(token => {
              if(token&&token.token !== '')
              return tokens1.push(token.token);
            });
           
          }).then(()=>{
            const message = {
              notification: {
                title: 'New Deal',
                body: 'We have new deal for you',
              },
              options : {
                priority: 'high',
                timeToLive: 60 * 60 * 24, // 1 day
              },
              tokens: tokens1,
            }
            logger.info(" deal logs "+message);
            firebase.messaging().sendMulticast(message) .
            then(function(response) {
              console.log("Successfully sent message:", response);
            })
            .catch(function(error) {
              console.log("Error sending message", );
            });

          }).catch(function(error){
            console.log('Token error');
          });
          
         
        let savedDeal = await DealService.createDeal(deal);
                        await ProductService.updateDealInProduct(savedDeal._id,req.params.product_id);
         logger.info("New Deal is created");
        return res.send({
          code: 200,
          status: "success",
          message: "New Deal is created",
          data: savedDeal
        });
        }
        
      }
     catch (error) {
      logger.error("Error in Creating Deal :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in Creating Deal",
        data: []
      });
    }
  },
  updateDeal: async (req, res) => {
    try {
     let deal ={};
          deal.deal_name = req.body.deal_name;
          deal.deal_percentage = req.body.deal_percentage;
          deal.actual_price_aed = req.body.actual_price_aed;
          deal.offer_price_aed = req.body.offer_price_aed;
          deal.actual_price_usd = req.body.actual_price_usd;
          deal.offer_price_usd = req.body.offer_price_usd;
          deal.deal_start_date = req.body.deal_start_date;
          deal.deal_end_date = req.body.deal_end_date;
          deal.product_id = req.params.product_id;
       let editedCategory = await DealService.updateDeal(deal,req.params.deal_id);
      //  let firebaseTokens = await DeviceService.getDevice();
          // let tokens1 =[];
          // Promise.map(firebaseTokens,(device)=>{
          //   device.device.forEach(token => {
          //     if(token&&token.token !== '')
          //     return tokens1.push(token.token);
          //   });
          // }).then(()=>{
          //   const message = {
          //     notification: {
          //       title: 'New Deal',
          //       body: 'We have new deal for you',
          //     },
          //     options : {
          //       priority: 'high',
          //       timeToLive: 60 * 60 * 24, // 1 day
          //     },
          //     tokens: tokens1,
          //   }
          //   logger.info(" deal logs "+message);
          //   firebase.messaging().sendMulticast(message) .
          //   then(function(response) {
          //     console.log("Successfully sent message:", response);
          //   })
          //   .catch(function(error) {
          //     console.log("Error sending message");
          //   });

          // }).catch(function(error){
          //   console.log('Token error');
          // });
          
    logger.info(" deal is update");
        return res.send({
          code: 200,
          status: "success",
          message: "deal is update",
          data: editedCategory
        });
    } catch (error) {
      logger.error("Error in Update Deal :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in Update Deal",
        data: {}
      });
    }
  },
  getAllDeal: async (req, res) => {
    try {
      var search;
    if (_.isEmpty(req.query.searchkey)) {
      search = '';
    } else {
      search = req.query.searchkey
    }
    let limit = Number(req.query.limit) || 10;
    let offset = (req.query.skip) ? parseInt(req.query.skip) : 1
    let skip = (offset - 1) * limit;
      const categoryData = await DealService.getAllDeal(search, limit, skip);
      logger.info(" Getting all deals");
      res.send({
        code: 200,
        status: "success",
        message: "Listing All deals",
        data: categoryData
      });
    } catch (error) {
      logger.error("Error in getting deals :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting deals record",
        data: []
      });
    }
  },
  deleteDeal: async (req, res) => {
    try {
      var ids = (req.params.deal_id).split(',');
      var data = await DealService.deleteDealByIds(ids);
      res.send({
        code: 200,
        status: "success",
        message: "Deal is successfully deleted",
        data: data
      });
    } catch (error) {
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Deal Does not delete",
        data: {}
      });

    }
  }
};
export default DealController;