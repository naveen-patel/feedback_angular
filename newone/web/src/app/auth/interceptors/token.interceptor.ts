import { environment } from './../../../environments/environment';
import { Injectable, Injector, EventEmitter } from '@angular/core';
import { Logout } from './../../auth/store/actions/auth.actions';
import { Store } from '@ngrx/store';

import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
  HttpClient
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
// import { _throw } from 'rxjs';
import { _throw } from 'rxjs/observable/throw';
import { AuthService } from '../services/auth.service';
import { retry, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import * as fromAuth from '../../auth/store/reducers';

// @Output() logoutClick = new EventEmitter<boolean>();
@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  check: boolean = true;
  customeError = {error:{message:'Please Login again'}}

  constructor(private injector: Injector,
    private routePath: Router,
    private store: Store<fromAuth.State>,
    public http: HttpClient
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const auth = this.injector.get(AuthService);

    const clonedRequest = request.clone({
      headers: auth.getTokenHeader(),
      url: this.fixUrl(request.url)
    });

    return next.handle(clonedRequest).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.error instanceof ErrorEvent) {
          console.log(error, "front error");
        }
        else {
          let user = localStorage.getItem('user') && localStorage.getItem('x-auth-token') ? true : false;
          if (error.status == 401 && user) {
            console.log(error, "back end", 'user', user);
            this.logout()
            this.check = false;
          }
          else {
            this.check = true;
          }
        }
        console.log('erorrrrrr',error);
        
        return _throw(this.customeError);
        
      })
    );
  }

  private fixUrl(url: string) {
    console.log(url);

    if (this.check) {
      if (url.indexOf('http://') >= 0 || url.indexOf('https://') >= 0) {
        return url;
      } else {
        return environment.API_ENDPOINT + url;
      }
    } else { return null }
  }
  logout() {
    this.store.dispatch(new Logout());
  }
}
