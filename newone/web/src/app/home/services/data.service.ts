import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import {
    HttpClient
} from "@angular/common/http";
import { ToastyService } from "ng2-toasty";
// import { AuthService } from "../../auth/services/auth.service";
import { Router, ActivatedRoute, ActivatedRouteSnapshot, Resolve } from "@angular/router";
import { Pipe, PipeTransform } from '@angular/core';
import { httpFactory } from "@angular/http/src/http_module";
declare var require: any;
var moment = require("moment");
const momentTz = require('moment-timezone');
@Injectable()
export class DataService {

    constructor(
        private http: HttpClient,
        private toastyService: ToastyService
    ) { }
    /**
    *
    * @param {AddProjectData}
    * @returns {Observable<User>}
    * @memberof AuthService
    */
    getAllUserList(searchkey, limit, skip, type, status): Observable<any> {
        let url = "users";
        if (!limit) {
            limit = 10;
        }
        if (!searchkey) {
            searchkey = '';
        }
        if (!skip) {
            skip = 1;
        }
        searchkey = searchkey.replace(/&/g, '%26')
        url += '?searchkey=' + searchkey;
        url += '&limit=' + limit;
        url += '&skip=' + skip;
        url += '&type=' + type;
        url += '&status=' + status;
        return this.http.get<any>(url)
            .map(res => {
                if (res.code === 403) {
                    return res;
                } else {
                    return res.data ? res.data : [];
                }
            })
            .do(
                _ => _,
                (err) => {
                    this.errorMessage("something Went Wrong! Try again later.")
                }
            );
    }

    deleteUser(id): Observable<any> {
        let url = "users/" + id
        return this.http.delete<any>(url, {
            observe: "response"
        })
            .map(res => {
                return res;
            })
            .do(
                _ => _,
                (err) => {
                    
                    this.errorMessage("something Went Wrong! Try again later.")
                }
            );
    }
    changeStatus(value, id): Observable<any> {
        var data = { status: value }
        let url = "change_status/" + id
        return this.http
            .post<any>(url, data, {
                observe: "response"
            })
            .map(res => {
                return res.body.data;
            })
            .do(
                _ => _,
                (err) => {
                    this.errorMessage("something Went Wrong! Try again later.")
                }
            );
    }
    getAllSupplier(searchName, searchCode, searchProduct, limit, page): Observable<any> {
        let url = "/search/supplier";
        if (!limit) {
            limit = 10;
        }
        if (!searchName) {
            searchName = '';
        }
        if (!searchCode) {
            searchCode = '';
        }
        if (!searchProduct) {
            searchProduct = '';
        }
        if (!page) {
            page = 1;
        }
        searchName = searchName.replace(/&/g, '%26')
        searchCode = searchCode.replace(/&/g, '%26')
        searchProduct = searchProduct.replace(/&/g, '%26')
        url += '?searchName=' + searchName + '&searchCode=' + searchCode + '&searchProduct=' + searchProduct;
        url += '&limit=' + limit;
        url += '&page=' + page;
        return this.http.get<any>(url)
            .map(res => {
                if (res.code === 403) {
                    return res;
                } else {
                    return res.data ? res.data : [];
                }
            })
            .do(
                _ => _,
                (err) => {
                    this.errorMessage("something Went Wrong! Try again later.")
                }
            );
    };

    addSupplier(data): Observable<any> {
        let url = "supplier"
        return this.http
            .post<any>(url, data, {
                observe: "response"
            })
            .map(res => {
                this.successMessage('Supplier details added successfully')
                return res.body.data;
            })
            .do(
                _ => _,
                (err) => {
                    this.errorMessage(err.error.message)
                }
            );
    };
    updateSupplier(id, data): Observable<any> {
        let url = "supplier/" + id
        return this.http
            .put<any>(url, data, {
                observe: "response"
            })
            .map(res => {
                this.successMessage('Supplier details updated successfully')
                return res.body.data;
            })
            .do(
                _ => _,
                (err) => {
                    this.errorMessage("something Went Wrong! Try again later.")
                }
            );
    };
    deleteSupplier(id): Observable<any> {
        let url = "supplier/" + id
        return this.http
            .delete<any>(url, {
                observe: "response"
            })
            .map(res => {
                return res.body.data;
            })
            .do(
                _ => _,
                (err) => {
                    this.errorMessage("something Went Wrong! Try again later.")
                }
            );
    };

    getSuppliers(): Observable<any> {
        let url = "all/supplier"
        return this.http
            .get<any>(url, {
                observe: "response"
            })
            .map(res => {
                return res.body.data;
            })
            .do(
                _ => _,
                (err) => {
                    this.errorMessage("something Went Wrong! Try again later.")
                }
            );
    };
    getAllProduct(supplierId, searchkey, searchBrandName, searchProductCode, limit, page): Observable<any> {
        let url = "/supplier/" + supplierId;
        if (!limit) {
            limit = 10;
        }
        if (!searchkey) {
            searchkey = '';
        }
        if (!searchBrandName) {
            searchBrandName = '';
        }
        if (!searchProductCode) {
            searchProductCode = '';
        }
        if (!page) {
            page = 1;
        }
        searchkey = searchkey.replace(/&/g, '%26')
        searchBrandName = searchBrandName.replace(/&/g, '%26')
        searchProductCode = searchProductCode.replace(/&/g, '%26')

        url += '?searchkey=' + searchkey + '&searchBrandName=' + searchBrandName + '&searchProductCode=' + searchProductCode;
        url += '&limit=' + limit;
        url += '&page=' + page;
        return this.http.get<any>(url)
            .map(res => {
                if (res.code === 403) {
                    return res;
                } else {

                    return res.data ? res.data : [];
                }
            })
            .do(
                _ => _,
                (err) => {
                    this.errorMessage("something Went Wrong! Try again later.")
                }
            );
    };
    deleteProduct(id): Observable<any> {
        let url = "product/" + id;
        return this.http.delete<any>(url)
            .map(res => {
                if (res.code === 403) {
                    return res;
                } else {
                    return res;
                }
            })
            .do(
                _ => _,
                (err) => {
                    this.errorMessage("something Went Wrong! Try again later.")
                }
            );
    };

    addProduct(data, supplier_id): Observable<any> {
        let url = "product/" + supplier_id;
        return this.http
            .post<any>(url, data, {
                observe: "response"
            })
            .map(res => {
                this.successMessage('Product Added Successfully')
                return res.body.data;
            })
            .do(
                _ => _,
                (err) => {
                    this.errorMessage("something Went Wrong! Try again later.")
                }
            );
    };
    updateProduct(id, data): Observable<any> {
        let url = "product/" + id
        return this.http
            .put<any>(url, data, {
                observe: "response"
            })
            .map(res => {
                this.successMessage('Product updated Successfully')
                return res.body.data;
            })
            .do(
                _ => _,
                (err) => {
                    this.errorMessage("something Went Wrong! Try again later.")
                }
            );
    };


    getAllAdminList(searchkey, limit, skip, type, status): Observable<any> {
        let url = "admins";
        if (!limit) {
            limit = 10;
        }
        if (!searchkey) {
            searchkey = '';
        }
        if (!skip) {
            skip = 1;
        }
        searchkey = searchkey.replace(/&/g, '%26')
        url += '?searchkey=' + searchkey;
        url += '&limit=' + limit;
        url += '&skip=' + skip;
        url += '&type=' + type;
        url += '&status=' + status;
        return this.http.get<any>(url)
            .map(res => {
                if (res.code === 403) {
                    return res;
                } else {
                    return res.data ? res.data : [];
                }
            })
            .do(
                _ => _,
                (err) => this.errorMessage("something Went Wrong! Try again later.")
            );
    }
    generateSupplierExcel(ids): Observable<any> {
        let url = "supplier/report/excel"
        if (ids.length > 0) {
            url += "?ids=" + ids
        }
        return this.http
            .get<any>(url, {
                observe: "response"
            })
            .map(res => {
                return res.body.data;
            })
            .do(
                _ => _,
                (err) =>
                    this.errorMessage("something Went Wrong! Try again later.")
            );
    }
    generateProductExcel(key, values): Observable<any> {
        var newValues = []
        values.forEach(element => {
            newValues.push(element.replace(/&/g, '%26'))
        });
        let url = 'product/report/excel?' + key + '=' + newValues
        return this.http
            .get<any>(url, {
                observe: "response"
            })
            .map(res => {
                return res.body.data;
            })
            .do(
                _ => _,
                (err) =>
                    this.errorMessage("something Went Wrong! Try again later.")
            );
    }
    generateProductPDF(key, values): Observable<any> {
        var newValues = []
        values.forEach(element => {
            newValues.push(element.replace(/&/g, '%26'))
        });

        let url = '/product/report/pdf?' + key + '=' + newValues
        return this.http.get<any>(url)
            .map(res => {
                if (res.code === 403) {
                    return res;
                } else {
                    return res.data ? res.data : [];
                }
            })
            .do(
                _ => _,
                () =>
                    this.errorMessage("something Went Wrong! Try again later.")
            );
    };


    addadmin(data): Observable<any> {
        let url = "admins"
        return this.http
            .post<any>(url, data, {
                observe: "response"
            })
            .map(res => {
                return res.body;
            }).do(
                _ => _,
                (err) => {
                    this.errorMessage(err.error.message)
                }
            );
    };
    generateSupplierPDF(ids): Observable<any> {
        let url = "supplier/report/pdf"
        if (ids.length > 0) {
            url += "?ids=" + ids;
        }
        return this.http
            .get<any>(url, {
                observe: "response"
            })
            .map(res => {
                return res.body.data;
            })
            .do(
                _ => _,
                (err) => {
                    this.errorMessage(err.error.message)
                }
            );
    };
    updateadmin(id, data): Observable<any> {
        let url = "admins/" + id
        return this.http
            .put<any>(url, data, {
                observe: "response"
            })
            .map(res => {
                return res;
            }).do(
                _ => _,
                (err) => {
                    this.errorMessage(err.error.message)
                }
            );
    };
    getBrandList(): Observable<any> {
        let url = "brands"
        return this.http
            .get<any>(url, {
                observe: "response"
            })
            .map(res => {
                return res.body.data;
            })
            .do(
                _ => _,
                (err) => {
                    this.errorMessage("something Went Wrong! Try again later.")
                }
            );
    };
    deleteadmin(id): Observable<any> {
        let url = "admins/" + id
        return this.http
            .delete<any>(url, {
                observe: "response"
            })
            .map(res => {

                return res.body.data;
            })
            .do(
                _ => _,
                (err) => {
                    this.errorMessage("something Went Wrong! Try again later.")
                });
    };
    getProductsByBrands(supllier, list, searchkey, limit, page): Observable<any> {
        var brandList = []
        var supplier = [];
        list.forEach(element => {
            brandList.push(element.replace(/&/g, '%26'))
        });
        supllier.forEach(element => {
            supplier.push(element.replace(/&/g, '%26'))
        });
        searchkey = searchkey.replace(/&/g, '%26');
        let url = "product/brand/brand_name?page=" + page + "&searchkey=" + searchkey + "&limit=" + limit + "&brand=" + brandList + "&supplier=" + supplier;
        return this.http.get<any>(url)
            .map(res => {
                if (res.code === 403) {
                    return res;
                } else {
                    return res.data ? res.data : [];
                }
            })
            .do(
                _ => _,
                () =>
                    this.errorMessage("something Went Wrong! Try again later.")
            );
    };

    sendEmailPDF(email, key, values): Observable<any> {
        var list = []
        values.forEach(element => {
            list.push(element.replace(/&/g, '%26'))
        });
        let url = '/product/email/pdf/' + email + '?' + key + '=' + list
        return this.http.get<any>(url)
            .map(res => {
                if (res.code === 403) {
                    return res;
                } else {
                    return res.data ? res.data : [];
                }
            })
            .do(
                _ => _,
                () =>
                    this.errorMessage("something Went Wrong! Try again later.")
            );
    };
    sendEmailExcel(email, key, values): Observable<any> {
        var list = []
        values.forEach(element => {
            list.push(element.replace(/&/g, '%26'))
        });
        let url = "product/email/excel/" + email + '?' + key + '=' + list
        return this.http
            .get<any>(url, {
                observe: "response"
            })
            .map(res => {
                return res.body.data;
            })
            .do(
                _ => _,
                (err) =>
                    this.errorMessage("something Went Wrong! Try again later.")
            );
    }


    getsettings(): Observable<any> {
        let url = "settings";
        return this.http.get<any>(url)
            .map(res => {
                if (res.code === 403) {
                    return res;
                } else {
                    return res.data;
                }
            })
            .do(
                _ => _,
                (err) => {
                    this.errorMessage("something Went Wrong! Try again later.");
                }
            );
    }

    updateSettings(data, id): Observable<any> {
        let url = "settings/" + id;
        return this.http
            .post<any>(url, data, {
                observe: "response"
            })
            .map(res => {
                return res.body.data;
            })
            .do(
                _ => _,
                (err) => {
                    this.errorMessage("something Went Wrong! Try again later.")
                }
            );
    }
    successMessage(message) {
        this.toastyService.success({
            title: "Success",
            msg: message
        })
    }
    errorMessage(message) {
        this.toastyService.error({
            title: "Error",
            msg: message
        })
    }
    warningMessage(message) {
        this.toastyService.warning({
            title: "Warning",
            msg: message
        })
    }
}
@Injectable()
export class supplierResolve implements Resolve<any>{
    constructor(private BDS: DataService) { }
    resolve(route: ActivatedRouteSnapshot) {
        return this.BDS.getAllProduct(route.params.id, '', '', '', '', '');
    }
}


@Pipe({ name: 'floor' })
export class FloorPipe implements PipeTransform {
    /**
     *
     * @param value
     * @returns {number}
     */
    transform(value: number): number {
        return Math.floor(value);
    }
}

@Pipe({
    name: "timeFormat"
})
export class TimeFormatPipe {
    transform(value: string): string {
        if (value) {
            return momentTz.tz(value, 'Asia/kolkata').format("hh.mm a");
        } else {
            return "-";
        }
    }
}
@Pipe({
    name: "medium_date"
})
export class DateFormatShort {
    transform(value: Date): string {
        if (value) {
            let d = new Date(value)
            return moment(d).format('DD/MM/YYYY');
        } else {
            return ''
        }
    }
}
@Pipe({
    name: "formatDateWithTime"
})
export class DateWithTimeFormatShort {
    transform(value: Date): string {
        if (value) {
            let d = new Date(value)
            return moment(d).format('DD/MM/YYYY hh:mm:ss A');
        } else {
            return '';
        }
    }
}
@Pipe({
    name: "statusRequestCash"
})
export class StatusRequestCash {
    transform(value: String): string {
        if (value) {
            switch (value) {
                case 'to_approve':
                    return 'To Approve'
                case 'approved':
                    return 'Approved'
                case 'decline':
                    return 'Declined'
                default:
                    return ''
            }
        } else {
            return '';
        }
    }
}

@Pipe({
    name: "strtonum"
})
export class StringToNumber {
    transform(value: any) {
        return Number(value * 100).toFixed(0)
    }
}

@Pipe({
    name: "localtimeFormat"
})
export class LocalTimeFormatPipe {
    transform(value: string): string {
        if (value) {
            return moment.utc(value).format("hh.mm a");
        } else {
            return "-";
        }
    }
}


