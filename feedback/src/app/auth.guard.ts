import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { DataService } from './data.service';
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private authService: DataService, private router: Router
  ) {}
  // canActivate(): boolean {
  //   if (this.authService.loggedIn()) {
  //     return true;
  //   } else {
  //     this.router.navigateByUrl('/login');
  //     return false;
  //   }
  // }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (state.url.includes('/login')) {
      if (this.authService.loggedIn()) {
        this.router.navigate(['/dashboard']);
        return false;
      } else {
        return true;
      }
    } else if (state.url.includes('/dashboard')) {
      if (this.authService.loggedIn()) {
        return true;
      } else {
        this.router.navigate(['/login']);
        return false;
      }
    } else {
      // console.log(state.url);
    }
  }
}
