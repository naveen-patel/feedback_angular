import express from "express";
import DealController from "./deal.controller";
import UserAuthController from "../UserAuthentication/userAuth.controller";

const dealRouter = express.Router();

dealRouter.post('/deal/:product_id',UserAuthController.verify,DealController.createDeal);
dealRouter.put('/deal/:product_id/:deal_id',UserAuthController.verify,DealController.updateDeal);
dealRouter.get('/deal',UserAuthController.verify,DealController.getAllDeal);
dealRouter.get('/deals/:location',UserAuthController.verify,DealController.getActiveDeal);
dealRouter.get('/deal/:deal_id',UserAuthController.verify,DealController.getDealById);
dealRouter.delete('/deal/:deal_id',UserAuthController.verify,DealController.deleteDeal);

export default dealRouter;
