import BrandModel from "./brand.model";
import ProductModel from "../Product/Product.model";
const fs = require('fs');
var _ = require("lodash");
var mongoose = require('mongoose');
import settings from '../../../settings';
let config = require('./../../../config/' + settings.environment + '.config').default;
const MEDIA = config.media;
const BrandService = {
  getBrandById: async id => {
    try {
    let brand = await BrandModel.findOne({_id: Object(id)}).populate(
      {
      path: 'categoryDetails.category',
      select: 'category_name'
    }
    )
      return brand;
    } catch (error) {
      throw error;
    }
  },
  createBrand: async newBrand => {
    /////ok////
    try {
      var savedBrand = await new BrandModel(newBrand).save();
      return savedBrand
    } catch (error) {
      throw error;
    }
  },
  updateBrand: async (data,id) => {
    try {
      const update = await BrandModel.findOneAndUpdate({
        _id: id
      }, data, {
        strict: true,
        upsert: true
      });
      return update;
    } catch (error) {
      throw error;
    }
  },
  getAllBrand: async (searchkey,limit, skip) => {
    try {
      var json = {};
      
      const count = await BrandModel.find({
        "brand_name": {
            "$regex": searchkey,
            "$options": "i"
          }
      }).populate( {
        path: 'categoryDetails.category',
        select: 'category_name'
      }).count();
      let cat;

      if(limit===0){  
        cat = await BrandModel.find({
          "brand_name": {
            "$regex": searchkey,
            "$options": "i"
          }
        }).sort({
          "createdAt": -1
        }).populate( {
          path: 'categoryDetails.category',
          select: 'category_name'
        }).lean();
      }else{
        cat = await BrandModel.find({
          "brand_name": {
            "$regex": searchkey,
            "$options": "i"
          }
        }).populate( {
          path: 'categoryDetails.category',
          select: 'category_name'
        }).sort({
          "createdAt": -1
        }).limit(limit).skip(skip).lean();
      }
      var brandArray = []
      cat.forEach(element => {
        if (element.url && element.url !== "") {
          element.url = config.api_end_point + element.url;
        } else {
          element.url = "";
        }
       
        brandArray.push(element)
      });
      json.totalcount = count;
      json.brand = brandArray;
      return json;
    } catch (error) {
      throw error;
    }
  },
  getBrands: async () => {
    try { 
     return await BrandModel.find().sort({"createdAt": -1}) 
    } catch (error) {
      throw error;
    }
  },
  searchProductCategory :async (searchkey,category_id,limit,skip)=>{
    try {
      let searchQuery = {'$and':[{category_id:Object(category_id)},{product_name:{
        "$regex": searchkey,
        "$options": "i"
      }}]};
     let count = await ProductModel.find(searchQuery).count();
     let product = await ProductModel.find(searchQuery).sort({
      "createdAt": -1
    }).limit(limit).skip(skip).lean();
    let json ={};
    json.totalcount = count;
    json.product = product
    return json;
    } catch (error) {
      throw error;
    }
  },
  getBrandNameById: async (id) => {
  try {
    const Product = await ProductModel.find({category_id:Object(id)},
    {product_name:0,product_code:0,code:0,product_type:0,stock_location:0,fmcg_availability:0,expiry_date:0,ru_per_case:0,ean_code_ru:0,
      ean_code_case:0,language:0,origin:0,quantity:0,sales_per_case_aed:0,sales_per_case_usd:0,cost_price_per_case:0,
      description:0,size:0,product_front_image:0,product_back_image:0,supplier_id:0,category_id:0,_id:0,updatedAt:0,createdAt:0});
      return Product;

    // const Product = await BrandModel.aggregate([
    //   {"$lookup":{
    //     from:'product',
    //     localField:'id',
    //     foreignField:'category_id',
    //     as:'products'
    //   }},
    //   {
    //     $unwind:{'$products'}
    //   },{
    //     $project:{
    //       'category_id':1,
    //       'category_name':1,
    //       'product_name':'$products.product_name',
    //       'product_id':'$products._id'
    //     }
    //   }
    // ]);
    // return Product;
  } catch (error) {
    throw error;
  }
  },

  deletebrandByIds: async (ids) => {
    try {
      var data = await BrandModel.remove({
        _id: {
          $in: ids
        }
      });;
      return data;
    } catch (error) {
      throw error;
    }
  },
  checkCategory: async (ids) =>{
    try {
      var data = await BrandModel.findOne({
        'categoryDetails.category': {
          $in: ids
        }
      });;
      return data;
    } catch (error) {
      throw error;
    }
  }
};
export default BrandService;