import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { DataService } from '../../app/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { log } from 'util';
@Component({
  selector: 'app-forgotpass',
  templateUrl: './forgotpass.component.html',
  styleUrls: ['./forgotpass.component.css']
})
export class ForgotpassComponent implements OnInit {
  loginForm: FormGroup;
  setPass: FormGroup;
  object;
  passform = true;
  constructor(
    private formBuilder: FormBuilder,
    // tslint:disable-next-line:no-shadowed-variable
    private DataService: DataService,
    private toastr: ToastrService,
    private route: Router
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email_id: ['', [Validators.required, Validators.pattern(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/)]],
    });
    this.setPass = this.formBuilder.group({
      otp: ['', [Validators.required]]
    });
  }

  emailVerify(id): void {
    if (this.loginForm.valid === false) {
      this.loginForm.get('email_id').markAsDirty();
      return;
    } else {
      if (this.loginForm.valid) {
        this.DataService.forgotPassword(this.loginForm.value)
          .subscribe(userData => {
            if (userData.success === true) {
              this.toastr.success('OTP will send within 5 minuts on your registered email');
              document.getElementById('login1').setAttribute('disabled', 'disabled');
              document.getElementById('emailSubmit').setAttribute('disabled', 'disabled');
              document.getElementById('passwordForm').setAttribute('class', 'afterPasswordForm');
              this.passform = false;
            } else {
              this.toastr.error(userData.message, 'please enter valid email ');
            }
          });
      } else {
        alert('Please fill form properly!!!');
      }
    }
  }

  setPassword(id): void {
    if (this.setPass.valid === false) {
      this.setPass.get('otp').markAsDirty();
      return;
    } else {
      if (this.setPass.valid) {
        this.object = {email_id: this.loginForm.get('email_id').value, otp: this.setPass.get('otp').value};
        console.log(this.object);
        this.DataService.setNewPassword(this.object)
          .subscribe(userData => {
            if (userData.success === true) {
              this.toastr.success('Your new password sent on your registered emial!!');
              this.route.navigate(['/login']);
              this.passform = false;
            } else {
              this.toastr.error(userData.message, 'please enter valid OTP ');
            }
          });
      } else {
        alert('Please fill form properly!!!');
      }
    }
  }

}
