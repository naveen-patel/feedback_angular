import { Component, OnInit, ChangeDetectorRef, TemplateRef, ViewChild } from '@angular/core';
import { CustomerService } from '../../services/customer.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Record } from 'immutable';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CompressorService } from '../../services/compressor.service';
import { NgxImageCompressService } from 'ngx-image-compress';
import { map, expand } from 'rxjs/operators';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.scss']
})
export class BrandComponent implements OnInit {

  modalRef2: BsModalRef;
  modalRef3: BsModalRef;
  modalRef4: BsModalRef;
  modalRef5: BsModalRef;
  deleteImage1: Boolean = false;
  url: any = "";
  errorImageSize1: Boolean = false;

  selectCategoryId = 'c';
  categoryList = [];

  file_details: string;
  records = [];
  upload_records = [];
  openAddProduct: boolean = false;
  parentmessage: any;
  loader: boolean = false;
  search: string = '';
  limit = 10;
  skip = 1;
  modalRef: BsModalRef;
  count: Number;
  currentpage: Number = 1;
  Allrecords = [];
  editCategory: string = 'Add';
  id: string;
  isSelectAll: boolean = false;
  selectedIds = [];
  openBrandWiseProduct: boolean = false
  dropdownList = [];
  brandList = [];
  selectedItems = [];
  remainingRecords = 0;
  mailFileType = ''
  sendEMailId = ''
  Add: Boolean = true
  addNew: any;
  viewOnly: Boolean = false
  submitted: boolean = false;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    keyboard: false,
    class: 'modal-dialog'
  }
  categoryForm: FormGroup;
  editNew: any;
  formData: any = new FormData();
  flag = false;

  constructor(
    public ds: DataService,
    public cs: CustomerService,
    private modalService: BsModalService,
    public formBuilder: FormBuilder,
    private compressor: CompressorService,
    private imageCompress: NgxImageCompressService
  ) { }

  dropdownSettings = {
    singleSelection: false,
    text: "Select Category",
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    enableSearchFilter: true,
    classes: "myclass custom-class",
    enableCheckAll: true
  };

  ngOnInit() {
    console.log('flag Value --> ', this.cs.flag);
    if (this.cs.flag) {
      setTimeout(() => { this.formCategory(); }, 1000)
    }
    this.dropdownList = [];
    this.selectedItems = [];
    this.cs.getAllCategory().subscribe(data => {

      var i = 1;
      data.category.forEach(element => {
        this.dropdownList.push({
          "id": element._id,
          "itemName": element.category_name
        });
        i++;
      });
    })
    this.getCategory()
  }
  @ViewChild("category") addBrand: TemplateRef<any>;
  // for get all category details
  getCategory() {
    this.remainingRecords = 0;
    this.skip = 1;
    this.isSelectAll = false;
    this.selectedIds = [];

    this.cs.getBrand(this.search, this.limit, this.skip).subscribe(data => {
      console.log("data", data.brand[0]);
      this.loader = false;
      this.count = data.totalcount;
      this.records = data.brand;
      this.selectedIds = [];
      this.isSelectAll = false;
      this.remainingRecords = data.totalcount - (this.skip * this.limit);
      this.records.forEach((element) => {
        element.isSelect = false;
      })
    });
  }

  formCategory() {
    console.log('this is formCategory')
    let a = []
    let b = this.cs.categoryDet;
    console.log(b);
    let temp = {}
    temp['id'] = b[0]._id;
    temp['itemName'] = b[0].category_name;
    a.push(temp)

    this.selectedItems = a;
    console.log("this is from from category -->", this.selectedItems);

    this.openModal(this.addBrand, 'null', 'Add')
    this.cs.forBrandModal("", false)
  }

  // for searching category
  searchProduct() {
    this.getCategory();
  }
  // for select single record (category)
  selectRecord(index) {
    if (this.records[index].isSelect) {
      var i = this.selectedIds.indexOf(this.records[index]._id);
      if (i !== -1) {
        this.selectedIds.splice(i, 1);
      }
    } else {
      this.selectedIds.push(this.records[index]._id)
    }
    this.records[index].isSelect = !this.records[index].isSelect
    this.isSelectAll = (this.selectedIds.length === this.records.length)
  }
  // for select multiple record (category)
  selectAllRecord() {
    this.selectedIds = [];
    if (this.isSelectAll) {
      this.isSelectAll = false
      this.records.forEach((element) => {
        element.isSelect = false;
      })
    } else {
      this.isSelectAll = true;
      this.records.forEach((element) => {
        element.isSelect = true;
        this.selectedIds.push(element._id)
      })
    }
  }
  // for category form

  getFormFields(i) {
    if (i != 'null') {
      this.editCategory = 'Edit';
      let a = []
      for (let index1 = 0; index1 < this.records[i].categoryDetails.length; index1++) {
        let temp = {}
        temp['id'] = this.records[i].categoryDetails[index1].category._id;
        temp['itemName'] = this.records[i].categoryDetails[index1].category.category_name;
        a.push(temp)
      }

      this.id = this.records[i]._id
      this.selectedItems = a;
      this.url = this.records[i].url;
      return {
        _id: [this.records[i]._id],
        brand_name: [this.records[i].brand_name, Validators.required],
        description: [this.records[i].description, Validators.required,],
        category: [this.records[i].categoryDetails, Validators.required],
      }
    } else {
      this.editCategory = 'Add';
      this.url = '';
      this.selectedItems = [];
      return {
        brand_name: ["", Validators.required],
        description: ["", Validators.required],
        category: ["", Validators.required],
      }
    }
  }
  forDisabled() { return true }
  openModal(template: TemplateRef<any>, i, type) {
    // this.cs.getAllCategory().subscribe(data => {
    //   this.categoryList = data.category;
    //   this.categoryForm = this.formBuilder.group(this.getFormFields(i));
    //   setTimeout(() => {
    //     this.selectCategoryId = this.records[i].category_id.category_id;
    //   }, 1000);
    // });

    if ((type == 'Add') || (type == 'Edit') && (type != 'Open')) {
      this.editCategory = type;
      this.viewOnly = false;
      this.modalRef = this.modalService.show(template, this.config);
      this.categoryForm = this.formBuilder.group(this.getFormFields(i));
      this.submitted = false;
    } else if (type == 'Open') {
      this.viewOnly = true;
      this.modalRef = this.modalService.show(template, this.config);
      this.categoryForm = this.formBuilder.group(this.getFormFields(i));
      this.editCategory = "";
    }
  }
  onSubmit() {

    this.brandList = [];
    this.remainingRecords = 0;
    this.selectedItems.forEach(element => {
      this.brandList.push(element.id);
    });
    this.skip = 1;
    this.isSelectAll = false;
    this.selectedIds = [];
    console.log(this.categoryForm.get("category").value)
    console.log(this.categoryForm.valid, this.categoryForm.value);
    this.submitted = true;

    this.categoryForm.get("")
    if (this.categoryForm.valid) {
      if (this.editCategory == 'Edit') {
        let formValue = Object.assign({}, this.categoryForm.value);
        this.editNew = formValue;
        this.editNew.delete_card_1 = this.deleteImage1;
        this.formData.set('bodyData', JSON.stringify(this.editNew))
      } else {
        let formValue = Object.assign({}, this.categoryForm.value);
        this.addNew = formValue;
        this.formData.set('bodyData', JSON.stringify(this.addNew))
      }
      this.submitsection();
    }
  }

  get f() { return this.categoryForm.controls; }


  submitsection() {
    if (this.editCategory == 'Edit') {
      var id = this.editNew._id;
      this.cs.updateBrand(id, this.formData).subscribe(data => {
        this.closeModal();
        this.reload();
        this.cs.successMessage('Brand Updated Successfully')
      });
    } else {
      this.cs.addBrand(this.formData).subscribe(data => {
        this.closeModal();
        this.reload();
        this.cs.successMessage('New Brand added Successfully')
      });
    }
  }
  onItemChange1(value) {
    this.selectCategoryId = value;
  }
  isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }

  // for delete record
  DeleteRecord(template: TemplateRef<any>, i, id) {
    this.modalRef3 = this.modalService.show(template);
    this.id = id;
    console.log("id", this.id);

  }
  confirmToDelete() {
    this.cs.deleteBrand(this.id).subscribe(data => {
      console.log(data)
      if (data.status == 'Exist') {
        this.ds.warningMessage(" Brand exist in product, can't delete it");
        this.ngOnInit();
      } else {
        this.ds.successMessage("Brand Deleted Successfully");
        this.ngOnInit();
      }
      // this.cs.successMessage("Brand Deleted Successfully");
      // this.ngOnInit();
    })
    this.modalRef3.hide();
    this.modalRef3 = null
  }
  confirmToMultiDelete() {
    this.cs.deleteBrand(this.selectedIds).subscribe(data => {
      if (data.status == 'Exist') {
        this.ds.warningMessage(" Brands exist in product, can't delete it");
        this.ngOnInit();
      } else {
        this.ds.successMessage("Brands Deleted Successfully");
        this.ngOnInit();
      }
    })
    this.modalRef4.hide();
    this.modalRef4 = null
  }
  DeleteRecords(template: TemplateRef<any>) {
    this.modalRef4 = this.modalService.show(template);
  }

  // for modal close 
  closeModal() {
    this.modalRef.hide();
    this.modalRef = null

  }
  // for reload skip after changes 
  reload() {
    this.search = "";
    this.skip = 1;
    this.ngOnInit();
  }
  // for load more Record(skip 2)
  loadMore() {
    this.skip++;
    this.cs.getBrand(this.search, this.limit, this.skip).subscribe(data => {
      var product = data.brand;
      this.isSelectAll = false;
      product.forEach((element) => {
        element.isSelect = false;
      })
      this.records = this.records.concat(product);
      this.remainingRecords = data.totalcount - (this.skip * this.limit);
    });
  }
  // for image 

  compressFile(type) {
    this.imageCompress.uploadFile().then(({ image, orientation }) => {

      console.warn('Size in bytes was:', this.imageCompress.byteCount(image));

      if (this.imageCompress.byteCount(image) > 30000) {
        this.imageCompressToMinSize(image, type, orientation)
      } else {
        //this.imgResultAfterCompress = result;
        var block = image.split(";");
        // Get the content type of the image
        var contentType = block[0].split(":")[1];// In this case "image/gif"
        // get the real base64 content of the file
        var realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."

        // Convert it to a blob to upload
        var blob = this.b64toBlob(realData, contentType);
        if (type === 'url') {
          this.deleteImage1 = false;
          this.url = image;
          this.formData.set("url", blob, 'url');
        } else {
          // this.deleteImage2 = false;

          // this.product_back_image = image;
          // this.formData.set("product_back_image", blob, 'product_back_image');
        }

        console.warn('Size in bytes is now:', this.imageCompress.byteCount(image));
      }
    });
  }

  imageCompressToMinSize(image, type, orientation) {
    this.imageCompress.compressFile(image, orientation, 50, 50).then(
      result => {
        console.warn('Size in bytes was:', this.imageCompress.byteCount(image));
        if (this.imageCompress.byteCount(result) < 40000) {
          var block = result.split(";");
          var contentType = block[0].split(":")[1];
          var realData = block[1].split(",")[1];
          var blob = this.b64toBlob(realData, contentType);
          if (type === 'url') {
            this.deleteImage1 = false;
            this.url = image;
            this.formData.set("url", blob, 'url');
          } else {
            // this.deleteImage2 = false;

            // this.product_back_image = image;
            // this.formData.set("product_back_image", blob, 'product_back_image');
          }

          console.warn('Size in bytes is now:', this.imageCompress.byteCount(result));
        } else {
          this.imageCompressToMinSize(result, type, orientation);
        }
      }
    );
  }
  b64toBlob(b64Data, contentType) {
    contentType = contentType || '';
    var sliceSize = 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);

      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      var byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }
  imageData: any;
  openImagePreview: Boolean = false;
  imagePreview(data) {
    if (data) {
      console.log('img call');

      this.openImagePreview = true;
      this.imageData = data
    }
  }
  closeImagePreview() {
    console.log('close call');

    this.imageData = "";
    this.openImagePreview = false
  }

  imageDelete(type) {
    if (type === 'image1') {
      this.deleteImage1 = true;
      this.url = "";
      this.formData.set("url", "");

    }
  }

  data: FileList;
  compressedImages = [];
  recursiveCompress = (image: File, index, array) => {


    if (image) {
      return this.compressor.compress(image).pipe(
        map(response => {

          //Code block after completing each compression
          this.compressedImages.push(response);
          return {
            data: response,
            index: index + 1,
            array: array,
          };
        }),
      );
    }

  }

  public process(event, image) {
    this.data = event.target.files;
    var filesize = ((this.data[0].size / 1024) / 1024).toFixed(4);
    // if (Number(filesize) < 1.1) {


    var obj = {};
    const compress = this.recursiveCompress(this.data[0], 0, this.data);
    // .pipe(
    //   expand(res => {
    //     return this.recursiveCompress(this.data[res.index], res.index, this.data);
    //   }),
    // );
    compress.subscribe(res => {
      if (res.index > res.array.length - 1) {
        var reader = new FileReader();
        // reader.onload = (function (aImg) { return function (e) { aImg.src = e.target.result; }; })(img);
        reader.readAsDataURL(this.compressedImages[0]);
        reader.onload = () => {
          // this.filename = file.name;
          if (image === 'url') {
            this.deleteImage1 = false;
            this.formData.set("url", this.compressedImages[0], this.compressedImages[0].name);
            console.log('form data while selecting', this.formData);
            this.url = reader.result;
            this.errorImageSize1 = false;
            this.compressedImages = []
          }
        };
      }
    });
  }


}
