import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { DataService } from '../home/services/data.service'

@Injectable()
export class MessagingService {

  loggedIn: any;
  private messaging;
  user_id: string;

  private messageSource = new Subject()
  currentMessage = this.messageSource.asObservable()

  constructor(
    private ds: DataService
  ) {
    firebase.initializeApp({
      'messagingSenderId': '474809603922'
    });
    this.messaging = firebase.messaging();
    this.messaging.usePublicVapidKey("BC4lEzKOPffXtaRHxz_k_vlNoargzohjPNb4nAGwh2fF6a_gAQDw87S1NZNffFpNQHIhNkwrxipdAzgIHVqP4MM");
  }



  // get permission to send messages
  getPermission() {
    this.messaging.requestPermission()
      .then(() => {
        return this.messaging.getToken()
      })
      .then(token => {
        this.saveToken(token)
      })
      .catch((err) => {
        console.log('Unable to get permission to notify.', err);
      });
  }

  saveToken(token) {
    let localdata = localStorage.getItem('user');
    this.loggedIn = JSON.parse(localdata);
    if(this.loggedIn){
      this.user_id = this.loggedIn.user_id;
      var data = {
        user_id: this.user_id,
        platform: "web",
        token: token
      }
    }
  }
}