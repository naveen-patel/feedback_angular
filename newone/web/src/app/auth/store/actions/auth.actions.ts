import { Action } from '@ngrx/store';
import { User, Authenticate } from '../../models/user';

export const LOGIN = '[Auth] Login';
export const LOGOUT = '[Auth] Logout';
export const LOGIN_SUCCESS = '[Auth] Login Success';
export const LOGIN_FAILURE = '[Auth] Login Failure';
export const LOGIN_REDIRECT = '[Auth] Login Redirect';
export const REGISTER = '[Auth] Register';
export const AUTHORIZED = '[Auth] Authorized';
export const SHOW_PRELOADER = "[Auth] SHOW PRELOADER";
export const HIDE_PRELOADER = "[Auth] HIDE PRELOADER";
export const REGISTER_SUCCESS = "[Auth] Register Success";

export class Login implements Action {
  readonly type = LOGIN;

  constructor(public payload: Authenticate) { }
}

export class Register implements Action {
  readonly type = REGISTER;

  constructor(public payload: Authenticate) { }
}

export class LoginSuccess implements Action {
  readonly type = LOGIN_SUCCESS;

  constructor(public payload:  User ) { }
}

export class LoginFailure implements Action {
  readonly type = LOGIN_FAILURE;

  constructor(public payload: any) { }
}

export class LoginRedirect implements Action {
  readonly type = LOGIN_REDIRECT;

  constructor(public payload: any = {}) { }
}

export class Logout implements Action {
  readonly type = LOGOUT;

  constructor(public payload: any = {}) { }
}

export class Authorized implements Action {
  readonly type = AUTHORIZED;
}

export class ShowPreLoader implements Action {
  readonly type = SHOW_PRELOADER;
}

export class HidePreLoader implements Action {
  readonly type = HIDE_PRELOADER;
}

export class RegisterSuccess implements Action {
  readonly type = REGISTER_SUCCESS;
}

export type Actions =
  | Login
  | LoginSuccess
  | LoginFailure
  | LoginRedirect
  | Logout
  | ShowPreLoader
  | HidePreLoader
  | RegisterSuccess;
