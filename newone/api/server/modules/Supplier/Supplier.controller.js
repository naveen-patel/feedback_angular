import SupplierService from "./Supplier.service";
import ProductService from "../Product/Product.service";
import logger from "../../../config/logger.config";
var _ = require("lodash");
import settings from '../../../settings';
let config = require('./../../../config/' + settings.environment + '.config').default;
const MEDIA = config.media;
let mkdirp = require('mkdirp');
var uuidv1 = require('uuid/v1');
const fs = require('fs');
const path = require('path');
var pdf = require('html-pdf');
var pdfCount = 0,
  excelCount = 0;
var imgSrc = path.normalize('file://' + __dirname + '../../../../logo/logo.png');
var Excel = require('exceljs');

const SupplierController = {
  getSupplierById: async (req, res) => {
    try {
      var supplierId = req.params.id
      var searchName = (_.isEmpty(req.query.searchkey)) ? '' : req.query.searchkey;
      // var searchCode = (_.isEmpty(req.query.searchProductCode))? '':req.query.searchProductCode;
      // var searchBrand = (_.isEmpty(req.query.searchBrandName))? '':req.query.searchBrandName;
      let limit = Number(req.query.limit) || 10;
      let offset = (req.query.page) ? parseInt(req.query.page) : 1
      let skip = (offset - 1) * limit;

      const supplier = await SupplierService.getSupplierById(supplierId);
      const product = await ProductService.getProductByPagination(searchName, limit, skip, supplierId);
      supplier.product = product;
      logger.info(" Getting Supplier by id = " + supplierId);
      return res.send({
        code: 200,
        status: "success",
        message: "List the Supplier",
        data: supplier
      });
    } catch (error) {
      logger.error("Error in getting Supplier :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting  Supplier",
        data: {}
      });
    }
  },
  getAllSupplier: async (req, res) => {
    try {
      const Supplier = await SupplierService.getAllSupplier();
      res.send({
        code: 200,
        status: "success",
        message: "List the Supplier",
        data: Supplier
      });
    } catch (error) {
      logger.error("Error in getting Supplier :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting  Suppliers",
        data: {}
      });
    }

  },
  getSupplierByPagination: async (req, res) => {
    try {
      var search;
      if (_.isEmpty(req.query.searchkey)) {
        search = '';
      } else {
        search = req.query.searchkey
      }
      let limit = Number(req.query.limit) || 10;
      let offset = (req.query.page) ? parseInt(req.query.page) : 1
      let skip = (offset - 1) * limit;

      const Supplier = await SupplierService.getSupplierByPagination(search, limit, skip);

      logger.info(" Getting Supplier");
      res.send({
        code: 200,
        status: "success",
        message: "List the Supplier",
        data: Supplier
      });
    } catch (error) {
      logger.error("Error in getting Supplier :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting  Suppliers",
        data: {}
      });
    }
  },
  getSearchSupplierByPagination: async (req, res) => {
    try {
      var searchByName = (_.isEmpty(req.query.searchName)) ? '' : req.query.searchName;
      var searchByCode = (_.isEmpty(req.query.searchName)) ? '' : req.query.searchName;
      var searchByProduct = (_.isEmpty(req.query.searchName)) ? '' : req.query.searchName;

      let limit = Number(req.query.limit) || 10;
      let offset = (req.query.page) ? parseInt(req.query.page) : 1
      let skip = (offset - 1) * limit;


      const Supplier = await SupplierService.getSearchSupplierByPagination(searchByName, searchByCode, searchByProduct, limit, skip);

      logger.info(" Getting Supplier");
      res.send({
        code: 200,
        status: "success",
        message: "List the Supplier",
        data: Supplier
      });
    } catch (error) {
      logger.error("Error in getting Supplier :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting  Suppliers",
        data: {}
      });
    }
  },

  createSupplier: async (req, res) => {

    try {
      var bodyData = JSON.parse(JSON.stringify(req.body));
      var data = bodyData.bodyData ? JSON.parse(bodyData.bodyData) : req.body;
      var imageError = false;
      var supplier = {}
      let imageUrl1 = "";
      let imageUrl2 = "";
      var errorMessage = "";
      var requiredFail = false;
      if (!data.name || data.name === "") {
        requiredFail = true;
        errorMessage = 'Name is required'
      } else if (!data.phone_number || data.phone_number == "") {
        requiredFail = true;
        errorMessage = 'Contact Number is required'
      } else if (!data.email || data.email === "") {
        requiredFail = true;
        errorMessage = 'Email id is required'
      } else if (!data.address || data.address === "") {
        requiredFail = true;
        errorMessage = 'Address is required'
      } else if (!data.contact_person || data.contact_person === "") {
        requiredFail = true;
        errorMessage = 'Contact Person is required'
      }
      if (requiredFail) {
        return res.status(400).send({
          code: 400,
          status: "error",
          message: errorMessage,
          data: {}
        });
      }

      var getSupplierbyPhoneNumber = await SupplierService.getSupplierbyPhoneNumber(data.phone_number, '');
      if (getSupplierbyPhoneNumber.length > 0) {
        return res.status(400).send({
          code: 400,
          status: "error",
          message: "Contact Number is already here",
          data: {}
        });
      }
      var getSupplierbyEmail = await SupplierService.getSupplierbyEmail(data.email, '');
      if (getSupplierbyEmail.length > 0) {
        return res.status(400).send({
          code: 400,
          status: "error",
          message: "Email is already here",
          data: {}
        });
      }

      if (req.files && req.files.business_card_image_1) {
        let imageFile = req.files.business_card_image_1;
        if (imageFile) {
          let extension = imageFile.mimetype.split("/")[1];

          let filePath = MEDIA.local_file_path + "business_card_image" + "/";
          let file_name = uuidv1() + "-" + Date.now() + '.' + extension;

          //          imageUrl = config.api_end_point + "/static/business_card_image" + "/" + file_name;
          imageUrl1 = "/static/business_card_image" + "/" + file_name;
          if (!MEDIA.useS3) {
            await moveFile(imageFile, filePath, file_name)
              .catch((error) => {
                imageError = true;
                return badResponse(res, "Image Upload Failed");
              });
          }
        }
      }
      if (req.files && req.files.business_card_image_2) {
        let imageFile = req.files.business_card_image_2;
        if (imageFile) {
          let extension = imageFile.mimetype.split("/")[1];

          let filePath = MEDIA.local_file_path + "business_card_image" + "/";
          let file_name = uuidv1() + "-" + Date.now() + '.' + extension;
          //          imageUrl = config.api_end_point + "/static/business_card_image" + "/" + file_name;
          imageUrl2 = "/static/business_card_image" + "/" + file_name;
          if (!MEDIA.useS3) {
            await moveFile(imageFile, filePath, file_name)
              .catch((error) => {
                console.log(error)
                imageError = true;
                return badResponse(res, "Image Upload Failed");
              });
          }
        }
      }

      if (!imageError) {
        var supplierCode = 1;
        var lastSupplier = await SupplierService.getLastSupplier();
        if (lastSupplier.length > 0) {
          supplierCode = lastSupplier[0].code + 1;
        }

        supplier.name = data.name || "";
        supplier.contact_person = data.contact_person || "";
        supplier.email_id = data.email || "";
        supplier.country_code = data.country_code || "";
        supplier.phone_number = data.phone_number || "";
        supplier.address = data.address || "";
        supplier.business_card_image_1 = imageUrl1 || "";
        supplier.business_card_image_2 = imageUrl2 || "";
        supplier.code = supplierCode;
        supplier.supplier_code = (data.name.replace(/\s/g, '') + "supp").substring(0, 4) + supplierCode


        var savedSupplier = await SupplierService.createSupplier(supplier);
        if (savedSupplier.business_card_image_1 && savedSupplier.business_card_image_1 !== "") {
          savedSupplier.business_card_image_1 = config.api_end_point + savedSupplier.business_card_image_1
        } else {
          savedSupplier.business_card_image_1 = ""
        }
        if (savedSupplier.business_card_image_2 && savedSupplier.business_card_image_2 !== "") {
          savedSupplier.business_card_image_2 = config.api_end_point + savedSupplier.business_card_image_2
        } else {
          savedSupplier.business_card_image_2 = ""
        }
        logger.info("New Supplier is created");
        return res.send({
          code: 200,
          status: "success",
          message: "New Supplier is created",
          data: savedSupplier
        });
      }
    } catch (error) {
      logger.error("Error in Creating Supplier :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in Creating Supplier",
        data: []
      });
    }
  },
  updateSupplier: async (req, res) => {
    try {
      var bodyData = JSON.parse(JSON.stringify(req.body));
      var data = bodyData.bodyData ? JSON.parse(bodyData.bodyData) : req.body;
      //      var data = req.body;
      var id = req.params.id;
      var imageError = false;
      var supplier = {}
      let imageUrl1 = "";
      let imageUrl2 = "";
      var errorMessage = "";
      var requiredFail = false;
      if (!data.name || data.name === "") {
        requiredFail = true;
        errorMessage = 'Name is required'
      } else if (!data.phone_number || data.phone_number === "") {
        requiredFail = true;
        errorMessage = 'Contact Number is required'
      } else if (!data.email || data.email === "") {
        requiredFail = true;
        errorMessage = 'Email is required'
      } else if (!data.address || data.address === "") {
        requiredFail = true;
        errorMessage = 'Address is required'
      } else if (!data.contact_person || data.contact_person === "") {
        requiredFail = true;
        errorMessage = 'Contact Person is required'
      }
      if (requiredFail) {
        return res.status(400).send({
          code: 400,
          status: "error",
          message: errorMessage,
          data: {}
        });
      }
      var getSupplierbyPhoneNumber = await SupplierService.getSupplierbyPhoneNumber(data.phone_number, id);
      if (getSupplierbyPhoneNumber.length > 0) {
        return res.status(400).send({
          code: 400,
          status: "error",
          message: "Contact Number is already here",
          data: {}
        });
      }
      var getSupplierbyEmail = await SupplierService.getSupplierbyEmail(data.email, id);
      if (getSupplierbyEmail.length > 0) {
        return res.status(400).send({
          code: 400,
          status: "error",
          message: "Email is already here",
          data: {}
        });
      }

      if (req.files && req.files.business_card_image_1) {
        let imageFile = req.files.business_card_image_1;
        if (imageFile) {
          let extension = imageFile.mimetype.split("/")[1];

          let filePath = MEDIA.local_file_path + "business_card_image" + "/";
          let file_name = uuidv1() + "-" + Date.now() + '.' + extension;
          //          imageUrl = config.api_end_point + "/static/business_card_image" + "/" + file_name;
          imageUrl1 = "/static/business_card_image" + "/" + file_name;
          if (!MEDIA.useS3) {
            await moveFile(imageFile, filePath, file_name)
              .catch((error) => {
                console.log(error)
                return badResponse(res, "Image Upload Failed");
                imageError = true;
              });
          }
        }
      }
      if (req.files && req.files.business_card_image_2) {
        let imageFile = req.files.business_card_image_2;
        if (imageFile) {
          let extension = imageFile.mimetype.split("/")[1];

          let filePath = MEDIA.local_file_path + "business_card_image" + "/";
          let file_name = uuidv1() + "-" + Date.now() + '.' + extension;
          //          imageUrl = config.api_end_point + "/static/business_card_image" + "/" + file_name;
          imageUrl2 = "/static/business_card_image" + "/" + file_name;
          if (!MEDIA.useS3) {
            await moveFile(imageFile, filePath, file_name)
              .catch((error) => {
                console.log(error)
                return badResponse(res, "Image Upload Failed");
                imageError = true;
              });
          }
        }
      }
      if (!imageError) {
        var getSupplier = await SupplierService.getSupplier(id);
        supplier.name = data.name || "";
        supplier.contact_person = data.contact_person || "";
        supplier.email_id = data.email || "";
        supplier.country_code = data.country_code || "";
        supplier.phone_number = data.phone_number || "";
        supplier.address = data.address || "";

        supplier.supplier_code = (data.name.replace(/\s/g, '') + "supp").substring(0, 4) + getSupplier.code
        if (JSON.parse(data.delete_card_1)) {
          supplier.business_card_image_1 = "";
          var imageUrl = getSupplier.business_card_image_1;
          var fileUrl = imageUrl.replace('/static/', MEDIA.local_file_path)
          console.log(fileUrl)
          if (fs.existsSync(fileUrl)) {
            console.log(fileUrl)
            fs.unlinkSync(fileUrl);
          }
        } else if (imageUrl1 !== "") {
          supplier.business_card_image_1 = imageUrl1 || ""
        }
        if (JSON.parse(data.delete_card_2)) {
          console.log('delete business_card_image_2')
          supplier.business_card_image_2 = "";
          var imageUrl = getSupplier.business_card_image_2;
          var fileUrl = imageUrl.replace('/static/', MEDIA.local_file_path)
          console.log(fileUrl)
          if (fs.existsSync(fileUrl)) {
            console.log(fileUrl)
            fs.unlinkSync(fileUrl);
          }
        } else if (imageUrl2 !== "") {
          supplier.business_card_image_2 = imageUrl2 || ""
        }

        var savedSupplier = await SupplierService.updateSupplier(id, supplier);
        if (savedSupplier.business_card_image_1 && savedSupplier.business_card_image_1 !== "") {
          savedSupplier.business_card_image_1 = config.api_end_point + savedSupplier.business_card_image_1
        } else {
          savedSupplier.business_card_image_1 = ""
        }
        if (savedSupplier.business_card_image_2 && savedSupplier.business_card_image_2 !== "") {
          savedSupplier.business_card_image_2 = config.api_end_point + savedSupplier.business_card_image_2
        } else {
          savedSupplier.business_card_image_2 = ""
        }
        logger.info("Supplier is updated");
        return res.send({
          code: 200,
          status: "success",
          message: "Supplier is updated",
          data: savedSupplier
        });
      }
    } catch (error) {
      logger.error("Error in update Supplier :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in Update Supplier",
        data: []
      });
    }
  },
  downloadSupplierAsExcel: async (req, res) => {
    try {
      var supplier;
      if (req.query.ids) { 
        var ids = (req.query.ids).split(',');
        supplier = await SupplierService.getSuppliersByIds(ids);
      } else {
        supplier = await SupplierService.getAllSupplier();
      }
      //var supplier = await SupplierService.getAllSupplier();

      var Excel = require('exceljs');
      var workbook = new Excel.Workbook();
      var worksheet = workbook.addWorksheet('My Sheet');
      worksheet.columns = [{
          header: 'Supplier Name',
          key: 'name',
          width: 20
        },{
          header: 'Supplier Code',
          key: 'supplier_code',
          width: 20
        },{
          header: 'Contact Person',
          key: 'contact_person',
          width: 20
        },{
          header: 'Email',
          key: 'email_id',
          width: 30
        },{
          header: 'Phone Number',
          key: 'phone_number',
          width: 20
        },{
          header: 'Address',
          key: 'address',
          width: 100
        }]
        supplier.map((field) => {
          field.phone_number =("+" + field.country_code + " " + field.phone_number);
          worksheet.addRow(field);
        });
        var fileName = "supplier_excel(" + excelCount + ").xlsx";
        var filePath = path.join(__dirname + "/excel", fileName)

        workbook.xlsx.writeFile(filePath)
        .then(function (data) {
          setTimeout(function () {
            if (fs.existsSync(filePath)) {
              fs.unlinkSync(filePath);
            }
          }, 300000)
          return res.send({
            code: 200,
            status: "success",
            message: "Supplier excel created done",
            data: {
              path: config.api_end_point + '/api/supplier/report/excel/' + fileName,
              extension: "xlsx"
            }
          });
        });

      // var csvDataString = "Supplier Name,Supplier Code,Email,Contact Number,Address\r\n";
      // supplier.map((field) => {
      //   csvDataString += field.name + ',' + field.supplier_code + '\,' + field.email_id + ',' + ("+" + field.country_code + " " + field.phone_number) + ',"' + field.address.replace(/\n/g, ' ') + '",\r\n';
      // });

      // //const fileName = uuidv1() + '-' + Date.now() + ".csv";
      // excelCount++;
      // //const fileName = "Supplier"+uuidv1() + '-' + Date.now() + ".pdf";
      // //const fileName = "supplier_excel(" + excelCount + ").xlsx";
      // //const fileName = uuidv1() + '-' + Date.now() + ".csv";

      // const filePath = path.join(__dirname + "/excel", fileName)
      // fs.writeFile(filePath, csvDataString, function (err) {
      //   if (err) {
      //     return res.status(400).send({
      //       code: 400,
      //       status: "error",
      //       message: "Error in Creating Excel file for Supplier",
      //       data: {}
      //     });
      //   } else {
      //     setTimeout(function () {
      //       if (fs.existsSync(filePath)) {
      //         fs.unlinkSync(filePath);
      //       }
      //     }, 300000)
      //     return res.send({
      //       code: 200,
      //       status: "success",
      //       message: "Supplier excel create done",
      //       data: {
      //         path: config.api_end_point + '/api/supplier/report/excel/' + fileName,
      //         extension:"xlsx"
      //       }
      //     });
      //   }
      // });

    } catch (error) {
      logger.error("Error in Creating Excel file for Supplier :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in Creating Excel file for Supplier",
        data: {}
      });
    }

  },
  excelFileDownload: async (req, res) => {
    try {
      var fileName = req.params.file_name
      const filePath = path.join(__dirname + "/excel", fileName)
      res.download(filePath)
    } catch (error) {
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error occured",
        data: {}
      });
    }
  },
  createPDF: async (req, res) => {
    try {
      var logo = "<img src='" + config.api_end_point + "/image/logo/logo.png'></img>"
      var html = fs.readFileSync('./server/EmailTemplates/supplier_template_pdf.html', 'utf8');
      var options = {
        "format": "A4",
        "orientation": "portrait",
        'Content_Type': 'application/pdf',
        "border": {
          "top": "0.2in",
          "right": "0.2in",
          "bottom": "0.2in",
          "left": "0.2in"
        },
        "header": {
          "height": "5mm",
          "contents": ""
        },
        "footer": {
          "height": "16mm",
          "contents": {
            default: '<hr style="border-color:rgb(255, 0, 0);" size="1"><div style="font-size: 7px;color: #949393;">Maxbrands Marketing</div><div style="font-size: 7px;color: #949393;">Hotline M: 0097154 461 4848</div><div style="font-size: 7px;color: #949393;">PO Box 239492, Dubai, UAE. | T: +971 4250 1134 | E:info@maxbrandsintl.com WEB <a style="font-size: 7px;color: #949393;" href="http://www.maxbrandsintl.com">www.maxbrandsintl.com</a></div>'
          }
        },
        "timeout": "120000"
      }; 
      var strSupplierList = "",supplierList;
      if (req.query.ids) {
        var ids = (req.query.ids).split(',');
        supplierList = await SupplierService.getSuppliersByIds(ids);
      } else {
        supplierList = await SupplierService.getAllSupplier();
      }
      var count = 0;
      var first = true;
      supplierList.forEach(supplier => {

        // if ((count % 7) === 0) {
        //   console.log('add header')
        //   strSupplierList = strSupplierList + "<img src='{logo}' class='logo'>    <hr class='line' size='1'>"
        // }

        // strSupplierList += "<div style='margin-left:20px;'><table cellSpacing='0' cellPadding='0' border='0'>" +
        //   "<tr><td class='vAlignTop' style='width:15%;" +
        //   "'>Supplier CODE</td><td class='vAlignTop'>:" + supplier.supplier_code+"dfijngdfsjgds" + "</td><td style='width:60%;' rowspan='9'><div style='text-align:center;'><div class='imagetag'><img src='" + (supplier.business_card_image_1 ? config.api_end_point + supplier.business_card_image_1 : "") + "' class='ImgProduct'></div>&nbsp;&nbsp;&nbsp<div class='imagetag'><img src='" + (supplier.business_card_image_2 ? config.api_end_point + supplier.business_card_image_2 : "") + "' class='ImgProduct'></div></div></td></tr>" +
        //   "<tr><td class='vAlignTop'> Supplier NAME</td><td class='vAlignTop'>:" + alterWord(supplier.name) + "</td></tr>" +
        //   "<tr><td class='vAlignTop'>Email</td><td class='vAlignTop'>:" + alterWord(supplier.email_id) + "</td></tr>" +
        //   "<tr><td class='vAlignTop'>Contact Number</td><td class='vAlignTop'>:" + supplier.country_code + ' ' + supplier.phone_number + "</td></tr>" +
        //   "<tr><td class='vAlignTop'> Address</td><td class='vAlignTop'>:" + alterWord(supplier.address) + "</td></tr>" +
        //   "</div><br>"
 
        strSupplierList += "<div style='margin-left:20px;'><table cellSpacing='0' cellPadding='0' border='0'>" +
          "<tr style='height:15px;'><td class='vAlignTop' style='width:15%;" +
          "'>Supplier code</td><td class='vAlignTop'>:" +supplier.supplier_code + "</td><td style='width:50%;' rowspan='6'><div style='text-align:center;'><div class='imagetag'><img src='" + (supplier.business_card_image_1 ?  path.normalize('file://' + (supplier.business_card_image_1.replace("/static/",MEDIA.local_file_path))): "")+ "' class='ImgProduct'></div>&nbsp;&nbsp;&nbsp<div class='imagetag'><img src='" + (supplier.business_card_image_2 ?  path.normalize('file://' + (supplier.business_card_image_2.replace("/static/",MEDIA.local_file_path))): "") + "' class='ImgProduct'></div></div></td></tr>" +
          "<tr><td class='vAlignTop'> Supplier Name</td><td class='vAlignTop'>:" + alterWord(supplier.name) + "</td></tr>" +
          "<tr><td class='vAlignTop'>Contact Person</td><td class='vAlignTop'>:" + alterWord(supplier.contact_person?supplier.contact_person:'NA') + "</td></tr>" +
          "<tr><td class='vAlignTop'>Email</td><td class='vAlignTop'>:" + alterWord(supplier.email_id) + "</td></tr>" +
          "<tr><td class='vAlignTop'>Contact Number</td><td class='vAlignTop'>:" +"+"+ supplier.country_code + supplier.phone_number+ "</td></tr>" +
          "<tr><td class='vAlignTop'> Address</td><td class='vAlignTop'>:" + alterWord(supplier.address) + "</td></tr>" +
          "</div><br>"
        // if (first) {
        //   first = false;
        //   console.log((count % 6 === 0)+ " ---- " +(count !== supplierList.length));
        //   if (count % 6 === 0 && count !== supplierList.length) {
        //     console.log("additional first br" +count+supplier.supplier_code)
        //     //console.log(strSupplierList)
        //     strSupplierList = strSupplierList+"<br><br><br><br><br><br>"
        //   }
        // } else {
        //   var s_count = count - 6;
        //   if (s_count % 7 === 0 && count !== supplierList.length) {
        //     console.log("additional second br")
        //     strSupplierList += "<br><br><br><br><br><br>"
        //   }
        // }
        if (count % 7 === 0 && count != 0 && count !== supplierList.length) {
          //console.log(strSupplierList)
          strSupplierList +=  "<div style='page-break-before: always;break-before: always;'></div><br><br><br>"
        }
        count++;
      });
      pdfCount++;
      //const fileName = "Supplier"+uuidv1() + '-' + Date.now() + ".pdf";
      const fileName = "supplier_pdf(" + pdfCount + ").pdf";
      const filePath = path.join(__dirname + "/pdf", fileName)
      // var result = "<div id='pageHeader'><img src='" + logo + "' /><div style='text-align: center;'>Author: Marc Bachmann</div></div>";
      // console.log(result)
      strSupplierList = strSupplierList.replace(/{logo}/g, config.api_end_point + '/image/logo/logo.png')
      var pdftemplate = html.replace(/{logo}/g, config.api_end_point + '/image/logo/logo.png').replace(/{supplierlist}/g, strSupplierList);
      //console.log(pdftemplate)
      pdftemplate += "<div id='pageHeader'><img style='width:200px;height:40px'src='" + imgSrc + "' /> <hr style='border-color: rgb(255, 0, 0);' size='1'></div>";
      
      pdf.create(pdftemplate, options).toFile(filePath, function (err, pdfPath) {
        if (err) {
          return res.status(400).send({
            code: 400,
            status: "error",
            message: "Error in getting Product",
            data: {}
          });
        } else {
          setTimeout(function () {
            if (fs.existsSync(filePath)) {
              fs.unlinkSync(filePath);
            }
          }, 300000)
          return res.send({
            code: 200,
            status: "success",
            message: "Creating PDF file for Supplier",
            data: {
              path: config.api_end_point + '/api/supplier/report/pdf/' + fileName,
              extension:"pdf"
            }
          });
        }
      });
    } catch (error) {
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting supplier",
        data: {}
      });
    }
  },
  pdfFileDownload: async (req, res) => {
    try {
      var fileName = req.params.file_name
      const filePath = path.join(__dirname + "/pdf", fileName)
      res.download(filePath)
    } catch (error) {
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error occured",
        data: {}
      });
    }
  },
  deleteSuppliers: async (req, res) => {
    try {
      var ids = (req.params.id).split(',');

      var suppliers = await SupplierService.getSuppliersByIds(ids);
      var products = await ProductService.getProductsBySupplierIds(ids);
      var deleteSuppliers = await SupplierService.deleteSuppliersByIds(ids);
      var deleteProducts = await ProductService.deleteProductsBySupplierIds(ids);
      res.send({
        code: 200,
        status: "success",
        message: "Supplier Deleted Successfully",
        data: {}
      });
      suppliers.forEach((supplierData) => {
        if (supplierData.business_card_image_1 && supplierData.business_card_image_1 != '' && supplierData.business_card_image_1 != null && supplierData.business_card_image_1 != undefined) {
          var imageUrl = supplierData.business_card_image_1;
          var fileUrl = imageUrl.replace('/static/', MEDIA.local_file_path)
          console.log(fileUrl)
          if (fs.existsSync(fileUrl)) {
            console.log(fileUrl)
            fs.unlinkSync(fileUrl);
          }
        }
        if (supplierData.business_card_image_2 && supplierData.business_card_image_2 != '' && supplierData.business_card_image_2 != null && supplierData.business_card_image_2 != undefined) {
          var imageUrl = supplierData.business_card_image_2;
          var fileUrl = imageUrl.replace('/static/', MEDIA.local_file_path)
          console.log(fileUrl)
          if (fs.existsSync(fileUrl)) {
            console.log(fileUrl)
            fs.unlinkSync(fileUrl);
          }
        }
      });
      products.forEach((productData) => {
        productData.expiry_date = productData.expiry_date ? productData.expiry_date : '';

        if (productData.product_front_image && productData.product_front_image != '' && productData.product_front_image != null && productData.product_front_image != undefined) {
          var imageUrl = productData.product_front_image;
          var fileUrl = imageUrl.replace('/static/', MEDIA.local_file_path)
          console.log(fileUrl)
          if (fs.existsSync(fileUrl)) {
            console.log(fileUrl)
            fs.unlinkSync(fileUrl);
          }

        }
        if (productData.product_back_image && productData.product_back_image != '' && productData.product_back_image != null && productData.product_back_image != undefined) {
          var imageUrl = productData.product_back_image;
          var fileUrl = imageUrl.replace('/static/', MEDIA.local_file_path)
          console.log(fileUrl)
          if (fs.existsSync(fileUrl)) {
            console.log(fileUrl)
            fs.unlinkSync(fileUrl);
          }
        }
      });
    } catch (error) {
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error occured",
        data: {}
      });
    }
  }
};

let badResponse = function (res, msg) {
  res.status(400).send({
    code: 400,
    status: "error",
    message: msg,
    data: []
  });
};

let moveFile = async (media_file, filePath, file_name) => {
  return await new Promise((resolve, reject) => {
    mkdirp(filePath, function (err) {
      if (err) {
        logger.error(err);
        reject({
          code: 400,
          status: "error",
          message: "Error in Uploading file",
          data: []
        });
      } else {
        media_file.mv(filePath + file_name, function (err) {
          if (err) {
            logger.error(err);
            reject({
              code: 400,
              status: "error",
              message: "Error in Uploading file",
              data: []
            });
          } else {
            logger.log("File Moved")
            resolve({
              code: 200,
              status: "success",
              message: "Uploaded Successfully",
              data: []
            })
          }
        });
      }
    });
  });
}
let alterWord = function (word) {
  //return word.match(/.{1,20}/g).join('<br>');
  return word;
}
export default SupplierController;