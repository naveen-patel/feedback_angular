import mongoose from 'mongoose';
var Schema = mongoose.Schema;

const BrandSchema = mongoose.Schema({
    categoryDetails:[
        {category:{
            type: Schema.Types.ObjectId,
            ref: "category"
        }}
    ],
    brand_name: {
        type: String,
    },
    description: {
        type: String,
    },
    url:{
        type:String,
        default:''
    }
    
}, {
    collection: 'brand',
    timestamps: true
});

let BrandModel = mongoose.model('brand', BrandSchema);

export default BrandModel