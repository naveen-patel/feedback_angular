import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { _throw } from 'rxjs/observable/throw';
import { User, Authenticate } from '../models/user';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastyService } from 'ng2-toasty';
import { concatStatic } from 'rxjs/operator/concat';

@Injectable()
export class AuthService {
  constructor(public http: HttpClient, public toastyService: ToastyService) { }

  /**
   *
   *
   * @param {Authenticate} { username, password }
   * @returns {Observable<User>}
   * @memberof AuthService
   */
  login({ username, password }: Authenticate): Observable<User> {

    return this.http
      .post<{ data: User }>('super_admin/login', { email_id: username, password, type: 'super_admin' }, { observe: 'response' })
      .map(res => {
        const user = res.body.data;
        this.setTokenInLocalStorage(res);
        return user;
      })
      .do(
        _ => _,
        (err) => {
          this.errorMessage(err.error.message);
        }
      );
  }


  errorMessage(msg) {
    this.toastyService.error({
      title: 'ERROR!!',
      msg: msg,
    })
  }
  successMessage(msg) {
    this.toastyService.success({
      title: 'ERROR!!',
      msg: msg,
    })
  }
  /**
   *
   *
   * @param {Authenticate} { name, username, password }
   * @returns {Observable<User>}
   * @memberof AuthService
   */
  register({ name, username, password }: Authenticate): Observable<User> {
    return this.http
      .post<{ data: User }>('auth', { name, email: username, password }, { observe: 'response' })
      .map(res => {
        const user = res.body.data;
        this.setTokenInLocalStorage(res);
        return user;
      })
      .do(
        _ => _,
        (err) => this.errorMessage(err.error.message)
      );
  }
  forgetPassword(data): Observable<any> {
    return this.http
      .post<any>('forget_password', data)
      .map(res => {
        if (res.code === 200) {
          this.successMessage('Link sent to yours email')
        } else {
          this.errorMessage(res.message)
        }
        return res;
      })
      .do(
        _ => _,
        (err) => {
          this.errorMessage(err.error.message);
        });
  }
  changePassword(data): Observable<any> {
    return this.http
      .post<any>('change_password', data)
      .map(res => {
        return res;
      })
      .do(
        _ => _,
        (err) => this.errorMessage(err.error.message)
      );
  }

  /**
   *
   *
   * @returns {Observable<boolean>}
   * @memberof AuthService
   */
  // authorized(): Observable<boolean> {
  //   return this.http
  //     .get<{ status: boolean }>('users/check_authenticated')
  //     .retry(2)
  //     .map(body => body.status);
  // }
  authorized(): Observable<any> {
    return this.http.get<any>("users/check_authenticated").map(body => body);
  }
  /**
   *
   *
   * @returns {Observable<User>}
   * @memberof AuthService
   */
  current_user(): Observable<User> {
    return this.http
      .get<User>('users/whoami')
      .map(body => body);
  }

  /**
   *
   *
   * @returns
   *
   * @memberof AuthService
   */
  // logout() {
  //   return this.http
  //     .delete<{ success: boolean }>('auth/sign_out')
  //     .map(body => {
  //       localStorage.removeItem('user');
  //       return body.success;
  //     });
  // }
  logout() {
    localStorage.removeItem("user");
    localStorage.removeItem("x-auth-token");

    return this.http.delete<{ success: boolean }>("auth/sign_out").map(body => {
      localStorage.removeItem("user");
      localStorage.removeItem("x-auth-token");

      return body.success;
    });
  }

  /**
   *
   *
   * @returns {{}}
   * @memberof AuthService
   */
  // getTokenHeader(): HttpHeaders {
  //   const user = ['undefined', null]
  //     .indexOf(localStorage.getItem('user')) === -1 ?
  //     JSON.parse(localStorage.getItem('user')) : {};

  //   return new HttpHeaders({
  //     'Content-Type': 'application/json',
  //     'token-type': 'Bearer',
  //     'access_token': user.access_token || [],
  //     'client': user.client || [],
  //     'uid': user.uid || []
  //   });
  // }

  getTokenHeader(): HttpHeaders {
    const user = ['undefined', null]
      .indexOf(localStorage.getItem('user')) === -1 ?
      JSON.parse(localStorage.getItem('user')) : {};

    // return new HttpHeaders({
    //   'Content-Type': 'application/json',
    //   'token-type': 'Bearer',
    //   'access_token': user.access_token || [],
    //   'client': user.client || [],
    //   'uid': user.uid || []
    // });
    const token = localStorage.getItem("x-auth-token");
    return new HttpHeaders({
      //  "Content-Type": "application/json",
      "x-auth-token": token || []
    });
  }

  // private setTokenInLocalStorage(res): void {
  //   const user_data = {
  //     ...res.body.data,
  //     access_token: res.headers.get('access-token'),
  //     client: res.headers.get('client')
  //   };
  //   const jsonData = JSON.stringify(user_data);
  //   localStorage.setItem('user', jsonData);
  // }

  private setTokenInLocalStorage(res): void {
    const user_data = {
      ...res.body.data
    };
    const jsonData = JSON.stringify(user_data);
    localStorage.setItem("user", jsonData);
    localStorage.setItem("x-auth-token", res.body.data.token);
  }
}
