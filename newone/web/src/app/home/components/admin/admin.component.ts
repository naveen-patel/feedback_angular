import { Component, OnInit, ChangeDetectorRef, TemplateRef } from '@angular/core';
import { DataService } from "../../services/data.service";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import 'rxjs/Rx';
declare var $: any;
declare var require: any;
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ninvoke } from 'q';
var _ = require('lodash');

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  imageinfo = {};
  modalRef2: BsModalRef;
  modalRef3: BsModalRef;
  modalRef4:BsModalRef;
  status : String ="" 
  selectedRecord;
  file_details: string;
  records = [];
  upload_records = [];
  openAddUser: boolean = false;
  parentmessage: any;
  loader: boolean = false;
  search: string = '';
  limit: any =10;
  page: any = 1;
  count: Number;
  Allrecords = [];
  id: string;
  active: boolean = true;
  editAdmin: string = 'Add';

  modalRef: BsModalRef;
  adminform: FormGroup;
  message = false;
  addNew: any;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    keyboard: false,
    class: 'modal-dialog'
  }
  submitted: boolean = false
  editNew: any;
  formData: any = new FormData();
  remainingRecords: any = 0;


  constructor(
    public ref: ChangeDetectorRef,
    public ds: DataService,
    private modalService: BsModalService,
    public formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    var initial = (this.page === 1)
    this.ds.getAllAdminList(this.search, this.limit, this.page, "admin", this.getStatus()).subscribe(data => {
      this.loader = false;
      this.count = data.totalcount;
      this.remainingRecords = data.totalcount - (this.page * this.limit);
      if (initial) {
        this.records = data.users;
      } else {
        this.records = this.records.concat(data.users);
      }
    })
  }
  searchUser() {
    this.loader = true;
    this.ds.getAllAdminList(this.search, this.limit, this.page, "admin", this.getStatus()).subscribe(data => {
      this.loader = false;
      this.count = data.totalcount;
      this.records = data.users;
    })
  }
  showInactiveUsers() {
    this.search = "";
    this.page = 1;
    this.active = false
    this.ngOnInit();
  }
  showactiveUsers() {
    this.search = "";
    this.page = 1;
    this.active = true;
    this.ngOnInit();
  }
  reload() {
    this.search = "";
    this.page = 1;
    this.ngOnInit();
  }
  getStatus() {
    if (this.active) {
      return "active";
    } else {
      return "in_active";
    }
  }
  pageChanged(event) {
    this.page = event.page;
    this.ngOnInit();
  }

  DeleteRecord(template: TemplateRef<any>, i, id) {
    this.modalRef3 = this.modalService.show(template);
    this.id = id;
  }
  confirmToDelete() {
    this.ds.deleteUser(this.id).subscribe(data => {
      this.reload();
      this.ds.successMessage('User deleted Successfully')

    })
    this.modalRef3.hide();
    this.modalRef3 = null
  }

  

  getFormFields(i) {
    if (i != 'null') {
      this.editAdmin = 'Edit';
      return {
        _id: [this.records[i]._id],

        first_name: [this.records[i].first_name, Validators.required,],
        last_name: [this.records[i].last_name, Validators.required,],
        email: [this.records[i].email, Validators.required],
        country_code: ["" + this.records[i].country_code, Validators.required],
        phone_number: [this.records[i].phone_number, Validators.required]
      }
    } else {
      this.editAdmin = 'Add';
      return {
        first_name: ["", Validators.required],
        last_name: ["", Validators.required],
        email: ["", Validators.required],
        country_code: ["", Validators.required],
        phone_number: ["", Validators.required]
      }
    }
  }

  openModal(template: TemplateRef<any>, i, type) {
    this.modalRef = this.modalService.show(template, this.config);
    this.adminform = this.formBuilder.group(this.getFormFields(i));
    
    
    this.editAdmin = type;
    this.submitted = false
  }
  onSubmit() {
    this.submitted = true;

    if (this.adminform.valid) {
      if (this.editAdmin == 'Edit') {
        let formValue = Object.assign({}, this.adminform.value);
        this.editNew = formValue;
      } else {
        let formValue = Object.assign({}, this.adminform.value);
        this.addNew = formValue;
      }
      this.submitsection();
    }
  }

  get f() { return this.adminform.controls; }

  submitsection() {
    if (this.editAdmin == 'Edit') {
      var id = this.editNew._id;
      this.ds.updateadmin(id, this.editNew).subscribe(data => {
        this.closeModal();
        this.reload();
        this.ds.successMessage('User Updated Successfully')
      });
    } else {
      this.ds.addadmin(this.addNew).subscribe(data => {
        this.closeModal();
        this.reload();
        this.ds.successMessage('New User added Successfully')
      });
    }
  }
  isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }

  closeModal() {
    this.modalRef.hide();
    this.modalRef = null
  }
  loadMore(){
    this.page++;
    this.ngOnInit();
  }
  confirmToChange() {
    var status = this.selectedRecord.status === 'active' ? 'in_active' : 'active';
    this.ds.changeStatus(status, this.selectedRecord._id).subscribe(data => {
      this.page=1;
      this.ngOnInit();
      this.modalRef4.hide();
      this.modalRef4 = null
    })
  }
  declineToChange(){
    
    this.page=1;
    this.ngOnInit();
    this.modalRef4.hide();
    this.modalRef4 = null
  }
  changeStatus(template: TemplateRef<any>,data) {
    this.status = data.status === 'active' ? 'In-Active' : 'Active';
    this.modalRef4 = this.modalService.show(template);
    this.selectedRecord=data
  }
  // changestatus(data) {
  //   var status = data.status === 'active' ? 'in_active' : 'active'
  //   this.ds.changeStatus(status, data._id).subscribe(data => {
  //     this.ngOnInit();
  //   })
  // }
}
