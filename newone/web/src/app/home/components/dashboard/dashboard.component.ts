import { Component, OnInit, ChangeDetectorRef, TemplateRef, NgZone, AfterViewInit, OnDestroy, ElementRef } from '@angular/core';
import { DataService } from "../../services/data.service";
import { CustomerService } from '../../services/customer.service';
import 'rxjs/Rx';
declare var $: any;
declare var require: any;
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ActivatedRoute, Router } from '@angular/router'

// import * as am4core from "@amcharts/amcharts4/core";
// import * as am4charts from "@amcharts/amcharts4/charts";
// import am4themes_animated from "@amcharts/amcharts4/themes/animated";

import { Chart } from 'chart.js';
// am4core.useTheme(am4themes_animated);
var _ = require('lodash');
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, AfterViewInit {

  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    keyboard: false,
    class: 'modal-dialog'
  }

  // private chart: am4charts.XYChart;s

  modalRef: BsModalRef;
  firstName;
  lastName;
  email;
  phoneNumber;
  countryName;
  pendingStatus;
  perAddress;
  companyName;
  shippAddress;
  cityName;
  landlineNumber;

  deliveredOrder;
  pendingOrders;
  totalOrders;

  imageinfo = {};
  modalRef2: BsModalRef;
  modalRef3: BsModalRef;
  modalRef4: BsModalRef;
  file_details: string;
  records = [];
  upload_records = [];
  openAddUser: boolean = false;
  parentmessage: any;
  loader: boolean = false;
  search: string = '';
  limit: any = 10;
  skip: any = 1;
  count: Number;
  Allrecords = [];
  id: string;
  active: boolean = false;
  remainingRecords: any = 0;
  tabChange: boolean = true;
  status: String = '';
  selectedRecord;
  country_name: string;
  country_code;
  flag = "completed";
  userCompleteRecord = [];
  dataSource: Object;
  data: any;
  chart = [];
  constructor(
    public ref: ChangeDetectorRef,
    public ds: DataService,
    private modalService: BsModalService,
    public cs: CustomerService,
    private routePath: Router,
    private zone: NgZone,
    public elementRef: ElementRef
  ) {

  }




  ngAfterViewInit() {

    // this.zone.runOutsideAngular(() => {
    // let chart = am4core.create("chartdiv", am4charts.PieChart);
    // chart.paddingRight = 20;

    // chart.data = [{
    // country: "Lithuania",
    // litres: 501.9
    // }, {
    // country: "Czech Republic",
    // litres: 301.9
    // }, {
    // country: "Ireland",
    // litres: 201.1
    // }];

    // let pieSeries = chart.series.push(new am4charts.PieSeries());
    // pieSeries.dataFields.value = "litres";
    // pieSeries.dataFields.category = "country";

    // });

  }

  // ngOnDestroy() {
  //   this.zone.runOutsideAngular(() => {
  //   if (this.chart) {
  //   this.chart.dispose();
  //   }
  //   });
  //   }

  ngOnInit() {
    let order_in_progress;
    // let order_received;
    let dispatched;
    let order_placed;
    let delivered;
    let cancel_order;
    this.cs.getOrderCountForChart().subscribe(data => {
      console.log('data', data);
      // order_received = data.Total;
      delivered = data.delivered;
      dispatched = data.dispatched;
      order_in_progress = data.order_in_progress;
      order_placed = data.order_placed;
      cancel_order = data.cancel
    })
    console.log("3");

    var initial = (this.skip === 1)
    this.cs.getAllOrderDetails(this.getStatus()).subscribe(data => {
      console.log("data", data);
      this.deliveredOrder = data.deliveredOrder;
      this.pendingOrders = data.pendingOrder;
      this.totalOrders = data.totalOrder;
      this.loader = false;
      this.count = data.totalcount;
      this.tabChange = false;
      this.remainingRecords = data.totalcount - (this.skip * this.limit);
      if (data.tabDetails.length !== 0) {
        this.country_code = data.tabDetails[0].country_code;
      }
      if (initial) {
        console.log(this.records);

        this.records = data.tabDetails;
        //this.userCompleteRecord = data.users;
      } else {
        this.records = this.records.concat(data.tabDetails);
        //this.userCompleteRecord = this.userCompleteRecord.concat(data.users);
      }
      // this.filterRecords(this.userCompleteRecord);
      this.forChart(delivered, dispatched, order_in_progress, order_placed, cancel_order)
    })
  }

  forChart(delivered, dispatched, order_in_progress, order_placed, cancel_order) {

    this.chart = new Chart(this.elementRef.nativeElement.querySelector('canvas').getContext('2d'), {
      type: 'pie',
      data: {
        labels: ['Order Placed', 'Order In-Progress', 'Dispatched', 'Delivered', 'Order Cancelled'],
        datasets: [{
          label: '# of Votes',
          data: [order_placed, order_in_progress, dispatched, delivered, cancel_order],
          backgroundColor: [
            // 'rgba(255, 99, 132, 0.2)',
            // 'rgba(54, 162, 235, 0.2)',
            // 'rgba(255, 206, 86, 0.2)',
            // 'rgba(75, 192, 192, 0.2)',
            // 'rgba(153, 102, 255, 0.2)',
            // 'rgba(255, 159, 64, 0.2)'
            '#33E3FF',
            '#FF5733',
            '#262FBB',
            
            '#f8a004',
            '#3FFF33',
            
            '#3fa5f4',

          ],
          borderColor: [
            // 'rgba(255, 99, 132, 1)',
            // 'rgba(54, 162, 235, 1)',
            // 'rgba(255, 206, 86, 1)',
            // 'rgba(75, 192, 192, 1)',
            // 'rgba(153, 102, 255, 1)',
            // 'rgba(255, 159, 64, 1)',
            // '#3fa5f4',
            // '#FF5733',
            // '#f8a004',
            // '#2ec689',
            // '#33E3FF',
            // '#3FFF33'
            '#ffffff'
          ],
          borderWidth: 2
        }]
      },
      options: {
        legend: {
          display: false,
          position: "left"
        },
        scales: {
          xAxes: [{
            display: false
          }],
          yAxes: [{
            display: false
          }],
        }
      }
    });
  }

  //  for country name
  private countryCode(code) {
    return this.cs.countries.filter(countryItem => countryItem.countryCode === code)[0].name;
  }

  showpendigUsers() {
    this.search = "";
    this.skip = 1;
    this.active = false;
    this.flag = "completed";
    this.ngOnInit();
  }
  showInactiveUsers() {
    this.search = "";
    this.skip = 1;
    this.active = false;
    this.flag = "registered";
    this.ngOnInit();
  }
  showactiveUsers() {
    this.search = "";
    this.skip = 1;
    this.active = true;
    this.flag = "pending";
    this.ngOnInit();
  }
  reload() {
    this.search = "";
    this.skip = 1;
    this.ngOnInit();
  }
  moreRecords() {
    if (this.flag == "registered") {
      console.log(this.flag, "flag in more record");
      this.routePath.navigate(['customer']);
    } if (this.flag == "pending") {
      console.log(this.flag, "flag in more record");
      this.routePath.navigate(['orders']);
    }
    if (this.flag == "completed") {
      console.log(this.flag, "flag in more record");
      this.routePath.navigate(['orders']);
    }
  }
  getStatus() {
    if (this.flag == "completed") {
      return 0;
    } else if (this.flag == "pending") {
      return 1;
    } else {
      return 2;
    }
  }
  pageChanged(event) {
    this.skip = event.page;
    this.ngOnInit();
  }
  openUser(record) {
    this.openAddUser = true;
    this.parentmessage = record
  }
  receiveMessage(event) {
    this.openAddUser = event[0];
    this.ngOnInit()
    // this.resetSearchAndSort();
  }

  // changestatus(value, id) {
  //   // this.ds.changeStatus(value, id).subscribe(data => {
  //   this.cs.changeStatus(value, id).subscribe(data => {
  //     console.log('changestatus');

  //     this.ngOnInit();
  //   })
  // }
  // confirmToChange() {
  //   let status;
  //   //  var status = this.selectedRecord.status === ('active' || 'pending') ? 'in_active' : 'active';
  //   console.log('status', this.selectedRecord.status);

  //   if ((this.selectedRecord.status == 'in_active') || (this.selectedRecord.status == 'pending') || (this.selectedRecord.status == 'reject')) {
  //     status = "active";
  //   } else {
  //     status = "in_active";
  //   }
  //   //  for pending
  //   console.log(status, 'statussssss');

  //   if ((this.selectedRecord.status == 'active') || (this.selectedRecord.status == 'in_active')) {
  //     this.cs.changeStatus(status, this.selectedRecord._id).subscribe(data => {
  //       console.log('confirmToChange');
  //       this.skip = 1;
  //       this.ngOnInit();
  //       this.modalRef4.hide();
  //       this.modalRef4 = null;
  //     })
  //   } else {
  //     this.cs.changePendingStatus(status, this.selectedRecord._id).subscribe(data => {
  //       console.log('confirmToChange');
  //       this.skip = 1;
  //       this.ngOnInit();
  //       this.modalRef4.hide();
  //       this.modalRef4 = null;
  //     })
  //   }
  //   // this.ds.changeStatus(status, this.selectedRecord._id).subscribe(data => {

  // }
  // // for reject
  // confirmToChangereject() {
  //   let status;
  //   status = "reject";

  //   this.cs.changePendingStatus(status, this.selectedRecord._id).subscribe(data => {
  //     this.skip = 1;
  //     this.ngOnInit();
  //     this.modalRef4.hide();
  //     this.modalRef4 = null;
  //   })
  // }
  // declineToChange() {
  //   console.log('decline')
  //   this.skip = 1;
  //   this.ngOnInit();
  //   this.modalRef4.hide();
  //   this.modalRef4 = null
  // }
  // changeStatus(template: TemplateRef<any>, data) {
  //   console.log('change status', data)
  //   this.selectedRecord = data;
  //   if (this.selectedRecord.status == 'active') {
  //     this.status = "the status of the user to In-Active ";
  //   } else if (this.selectedRecord.status == 'in_active') {
  //     this.status = "the status of the user to Active ";
  //   } else {
  //     this.status = "to approve user request ";
  //   }
  //   this.modalRef4 = this.modalService.show(template);
  // }

  // changeStatusReject(template: TemplateRef<any>, data) {
  //   console.log('change status', data)
  //   this.selectedRecord = data;
  //   this.status = "to reject user request ";
  //   this.modalRef4 = this.modalService.show(template);
  // }

  // loadMore() {
  //   this.skip++;
  //   this.ngOnInit();
  // }
  // check() {
  //   console.log('click');
  // }

  // openModal(template: TemplateRef<any>, i, type) {
  //   this.modalRef = this.modalService.show(template, this.config);
  //   this.firstName = this.records[i].first_name;
  //   this.lastName = this.records[i].last_name;
  //   this.email = this.records[i].email;
  //   this.phoneNumber = '+' + this.records[i].country_code + "-" + this.records[i].mobile_number;
  //   this.countryName = this.countryCode(this.records[i].country_code);
  //   this.pendingStatus = this.records[i].status == 'in_active' ? 'In-active' : this.records[i].status;
  //   this.perAddress = this.records[i].permanent_address;
  //   this.shippAddress = this.records[i].shipping_address;
  //   this.companyName = this.records[i].company_name;
  //   this.cityName = this.records[i].city;
  //   this.landlineNumber = this.records[i].phone_number;
  // }
  // closeModal() {
  //   this.modalRef.hide();
  //   this.modalRef = null
  // }

  // public pieChartLabels:string[] = ['Chrome', 'Safari', 'Firefox','Internet Explorer','Other'];
  // public pieChartData:number[] = [40, 20, 20 , 10,10];
  // public pieChartType:string = 'pie';

  // // events
  // public chartClicked(e:any):void {
  //   console.log(e);
  // }

  // public chartHovered(e:any):void {
  //   console.log(e);
  // }
}

