import mongoose from 'mongoose';
var Schema = mongoose.Schema;

const CategorySchema = mongoose.Schema({

    category_name: {
        type: String,
    },
    description: {
        type: String,
    },
    logo:{
        type:String
    },
    priority:{
        type:Number
    }
    
}, {
    collection: 'category',
    timestamps: true
});

let CategoryModel = mongoose.model('category', CategorySchema);

export default CategoryModel