import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
// import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.css']
})
export class DemoComponent implements OnInit {
  email = new FormControl('', [Validators.required, Validators.pattern(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/)]);
  constructor() { }
  ngOnInit() {
  }
  getErrorMessage() {
    console.log('required', this.email.hasError('required'));
    console.log('pattern', this.email.hasError('pattern'));
    return this.email.hasError('required') ? 'You must enter a value' :
        this.email.hasError('pattern') ? 'Not a valid email' :
            '';
  }
}
