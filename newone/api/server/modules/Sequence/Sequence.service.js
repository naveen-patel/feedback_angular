import SequenceModel from "./Sequence.model";
const fs = require('fs');
var mongoose = require('mongoose');
import settings from '../../../settings';
let config = require('./../../../config/' + settings.environment + '.config').default;

const SupplierService = {
  getSequenceBykey: async key => {
    //// ok ////
    try {
      const sequence = await SequenceModel.find({
        key: key
      });
      if (sequence.length > 0) {
        return sequence[0].sequence_no;
      } else {
        return 0;
      }
    } catch (error) {
      throw error;
    }
  },
  updateSequenceBykey: async (key, sequence) => {
    //// ok ////
    try {
      if (sequence === 1) {
        var insert = await new SequenceModel({ key: key, sequence_no: sequence}).save();
        return insert;
      } else {
        const update = await SequenceModel.findOneAndUpdate({
          key: key
        }, {
          sequence_no: sequence
        }, {
          strict: true,
          new: true
        });
        return update;

      }
    } catch (error) {
      throw error;
    }
  },
};
export default SupplierService;