import { Component, OnInit } from '@angular/core';
import { DataService } from '../../app/data.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})


export class RegistrationComponent implements OnInit {
  image;
  registrationForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    // tslint:disable-next-line:no-shadowed-variable
    private DataService: DataService,
    private toastr: ToastrService,
    private route: Router) { }
  ngOnInit() {
    this.registrationForm = this.formBuilder.group({
      email_id: ['', [Validators.required, Validators.pattern(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/)]],
      profile_photo: [, [Validators.required]],
      name: ['', [Validators.required, Validators.pattern(/^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/)]]
    });
  }
  // for image upload
  fileUpload(event) {
    this.image = event[0];
  }
  // for submit form
  onSubmit(): void {
    if (this.registrationForm.valid === false) {
      this.registrationForm.get('email_id').markAsDirty();
      this.registrationForm.get('profile_photo').markAsDirty();
      this.registrationForm.get('name').markAsDirty();
      return;
    } else {
      if (this.registrationForm.valid) {
        const formdata = new FormData();
        formdata.append('email_id', this.registrationForm.get('email_id').value);
        formdata.append('name', this.registrationForm.get('name').value);
        formdata.append('profile_photo', this.image);
        this.DataService.getRegistration(formdata)
          .subscribe(userData => {
            console.log(userData);
            console.log('user data', userData.success);
            if (userData.success === true) {
              // alert('Registration success');
              window.localStorage.setItem('userData', JSON.stringify(userData));
              this.toastr.success('Registration Done!!!');
              this.route.navigate(['/login']);
            } else {
              alert(userData.message);
            }
          });
      } else {
        alert('Please fill form properly!!!');
      }
    }
  }
}
