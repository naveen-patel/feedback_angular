import express from "express";
import Customers from './customer.controller';
import UserAuthController from "./../UserAuthentication/userAuth.controller";

const router = express.Router();

//Get all customers data
router.get('/customers', UserAuthController.verifySuperAdmin, Customers.getAllCustomers);

//get all country
router.get('/countries',Customers.getAllCountries);
// update a  customer
router.put('/customers/:customer_id', UserAuthController.verify, Customers.updateCustomerProfile);

router.post('/customerChangePassword/:customer_id', UserAuthController.verify, Customers.customerChangePassword);

//Check Authenticated
router.get('/customer/check_authenticated', UserAuthController.verifySuperAdmin, (req, res) => {
    return res.status(200).send({
        code: 200,
        message: "Authenticated",
        customer: req.userToken.data
    });
});

//generate OTP 
router.get('/customer/otp/:email/:type', Customers.generateOTP);
router.get('/customer/forget_password/:otp/:email',Customers.verifyOtp);
router.post('/customer/forgetChangePassword/:email', Customers.forgetChangePassword);
// router.post('/customer/email_verification/:code/:id',Customers.emailVerification)
router.post('/customer/change_status/:id',  UserAuthController.verifySuperAdmin, Customers.changeStatus);
router.post('/customer/takeAction/:id',UserAuthController.verifySuperAdmin, Customers.takeAction)
router.delete('/customer/:id', UserAuthController.verifySuperAdmin, Customers.deleteCustomer);


export default router;