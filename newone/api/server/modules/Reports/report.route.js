import express from "express";
import reportController from "./report.controller";
import UserAuthController from "../UserAuthentication/userAuth.controller";

const reportRouter = express.Router();

reportRouter.get('/report',UserAuthController.verify,reportController.getReportList);
reportRouter.get('/report/low/pdf', UserAuthController.verify,reportController.createLowStockReportPDF);
reportRouter.get('/report/sales/pdf', UserAuthController.verify,reportController.createSalesReportPDF);
reportRouter.get('/report/order/pdf', reportController.createOrderReportPDF);
reportRouter.get('/report/pdf/:file_name', reportController.pdfFileDownload);

export default reportRouter;