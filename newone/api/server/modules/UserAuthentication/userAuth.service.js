import jwt from 'jsonwebtoken';
const userAuthService = {};
const JWT_SECRET = "this is a JWTSECRET!!";

userAuthService.getJwtToken = async (payload) => {

    const token = await jwt.sign({
        data: payload
    }, JWT_SECRET, { expiresIn: '3d' });
    payload.token = token;

    return payload;
}

userAuthService.verifyAndDecode = (token) => {
    let decodedPayload = {};
    try {
        decodedPayload = jwt.verify(token, JWT_SECRET);
        if (decodedPayload.exp >= (Date.now() / 1000)) {
            decodedPayload.valid = true;
        } else
            decodedPayload.valid = false;
    } catch (error) {
        return decodedPayload.valid = false;
    }
    return decodedPayload;
};

userAuthService.registerToken = async (tokenData) => {
    try {
    RegisterToken.findById(tokenData.user_id, async (err, data) => {  
    // Handle any possible database errors
    if (err) {
         throw err;
    } else {
        // Update each attribute with any possible attribute that may have been submitted in the body of the request
        // If that attribute isn't in the request body, default back to whatever it was before.
        data.user_id = tokenData.user_id;
        data.webToken = tokenData.webToken || data.webToken;
        data.androidToken = tokenData.androidToken || data.androidToken;
        data.iphoneToken = tokenData.webToken || data.iphoneToken;
   try {
        let tokenToAdd = new RegisterToken(data);
        const savedtoken = await tokenToAdd.save();
        return savedtoken;
    } catch (error) {
        throw error;
     }
    }
});
 } catch (error) {
         throw error;
    }

};

export default userAuthService;


