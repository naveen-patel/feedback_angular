import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, ErrorObserver } from 'rxjs';
@Injectable({

  providedIn: 'root'
})
export class DataService {

  constructor(private httpclient: HttpClient) { }
  public BASE_URL = '//localhost:3001/';

  // httpOptions = {
  //   headers: new HttpHeaders({
  //     'Content-Type': 'application/json',
  //     // tslint:disable-next-line:max-line-length
  //     Authorization: this.getToken()
  //   })
  // };

  // for login
  getLogin(obj): Observable<any> {
    return this.httpclient.post(this.BASE_URL + 'login', obj);
  }
  // for forgot password OTP
  forgotPassword(obj): Observable<any> {
    return this.httpclient.post(this.BASE_URL + 'forgotPassword', obj);
  }

  // for setvnew password
  setNewPassword(obj): Observable<any> {
    return this.httpclient.post(this.BASE_URL + 'setPassword', obj);
  }

  getToken() {
    const tokens = JSON.parse(window.sessionStorage.getItem('token'));
    if (tokens != null) {
      return tokens.token;
    } else {
      return false;
    }
  }
  // for registation
  getRegistration(obj): Observable<any> {
    return this.httpclient.post(this.BASE_URL + 'user_registration', obj);
  }

  // for dashboard data

  getDashboard(): Observable<any> {
      return this.httpclient.get(this.BASE_URL + 'dashboard');
  }

  // for receiver list
  addFeedback(): Observable<any> {
    return this.httpclient.get(this.BASE_URL + 'receiver');
  }

  // for save feedback
  saveFeedback(obj): Observable<any> {
    return this.httpclient.post(this.BASE_URL + 'feedbacksave', obj);
  }

  // for checking user in logged in or not
  loggedIn() {
    return !!sessionStorage.getItem('token');
  }
}
