import 'rxjs/add/operator/take';
import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';
import { CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import * as Auth from '../store/actions/auth.actions';
import * as fromAuth from '../store/reducers';
import { HttpInterceptingHandler } from '@angular/common/http/src/module';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable()
export class RouteGuardService implements CanActivate {

  constructor(    private router: Router,
    private store: Store<fromAuth.State>) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.store
      .select(fromAuth.getUser)
      .map(user => {
        
         var role = JSON.parse(String(user));
         if(role){
          if (role.type!=='super_admin') {
            this.router.navigate(['/user'])
            return true;
          }
          return true;
         }
      })
      .take(1);
  }
}
