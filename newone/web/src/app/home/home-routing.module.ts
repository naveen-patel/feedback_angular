import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './components/user/user.component';
import { SupplierComponent } from './components/supplier/supplier.component';
import { ProductComponent } from './components/product/product.component';
import { ViewSupplierDetailsComponent } from './components/view-supplier-details/view-supplier-details.component';
import { supplierResolve } from './services/data.service';
import { AdminComponent } from './components/admin/admin.component';
import { SettingsComponent } from './components/settings/settings.component';
import { RouteGuardService } from '../auth/services/route-guard.service';
import { SelctComponent } from './components/selct/selct.component';
import { CustomerComponent } from './components/customer/customer.component';
import { CategoryComponent } from './components/category/category.component';
import { DealsComponent } from './components/deals/deals.component';
import { OrdersComponent } from './components/orders/orders.component';
import { ReportsComponent } from './components/reports/reports.component';
import { BrandComponent } from './components/brand/brand.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
// import { DemoComponent } from './components/demo/demo.component';



const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'user', component: UserComponent, },
  { path: 'orders', component: OrdersComponent },
  { path: 'dashboard', component: DashboardComponent },
  // { path: 'demo', component: DemoComponent },
  { path: 'reports/:id', component: ReportsComponent },
  { path: 'deals', component: DealsComponent },
  { path: 'category', component: CategoryComponent },
  { path: 'brand', component: BrandComponent },
  { path: 'customer', component: CustomerComponent },
  { path: 'supplier', component: SupplierComponent },
  { path: 'product', component: ProductComponent },
  { path: 'select', component: SelctComponent },
  {
    path: 'supplier/:id', component: ViewSupplierDetailsComponent,
    resolve: {
      supplierDetails: supplierResolve
    }
  },
  { path: 'admin', component: AdminComponent, canActivate: [RouteGuardService], },
  { path: 'settings', component: SettingsComponent, canActivate: [RouteGuardService], },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
