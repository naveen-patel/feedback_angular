import express from "express";
import dashboardController from "./dashboard.controller";
import UserAuthController from "../UserAuthentication/userAuth.controller";

const dashboardRouter = express.Router();

dashboardRouter.get('/dashboard',UserAuthController.verify,dashboardController.getDashboardDetails);
dashboardRouter.get('/dashboard/order/status',UserAuthController.verify,dashboardController.getOrderStatus)
export default dashboardRouter;