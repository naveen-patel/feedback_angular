import { SharedModule } from './../shared/shared.module';
import {
  NgModule, CUSTOM_ELEMENTS_SCHEMA,
  NO_ERRORS_SCHEMA
} from '@angular/core';
import { CommonModule } from '@angular/common';


import { HomeRoutingModule } from './home-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataService,  FloorPipe, TimeFormatPipe, DateFormatShort,DateWithTimeFormatShort,StatusRequestCash,StringToNumber, LocalTimeFormatPipe, supplierResolve } from './services/data.service';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { FullCalendarModule } from 'ng-fullcalendar';
import { TimepickerModule } from 'ngx-bootstrap';
import { AccordionModule } from 'ngx-bootstrap';
import { WebCamModule } from 'ack-angular-webcam';
import { NgSelectModule } from '@ng-select/ng-select';
import { CKEditorModule } from 'ng2-ckeditor';
import { TooltipModule } from 'ngx-bootstrap';
import { OrgChartModule } from 'ng-org-chart';
import { UserComponent } from './components/user/user.component';
import { SupplierComponent } from './components/supplier/supplier.component';
import { ManageSupplierComponent } from './components/manage-supplier/manage-supplier.component';
import { ProductComponent } from './components/product/product.component';
import { ManageProductComponent } from './components/manage-product/manage-product.component';
import { AdminComponent } from './components/admin/admin.component';
import { SettingsComponent } from './components/settings/settings.component';
import { ViewSupplierDetailsComponent } from './components/view-supplier-details/view-supplier-details.component';
import { BrandWiseProductComponent } from './components/brand-wise-product/brand-wise-product.component';
import { SelctComponent } from './components/selct/selct.component';
import { ImagePreviewComponent } from './components/image-preview/image-preview.component';
import { CustomerComponent } from './components/customer/customer.component';
import { CategoryComponent } from './components/category/category.component';
import { ManageCategoryComponent } from './components/manage-category/manage-category.component';
import { DealsComponent } from './components/deals/deals.component';
import { OrdersComponent } from './components/orders/orders.component';
import { ReportsComponent } from './components/reports/reports.component';
import { BrandComponent } from './components/brand/brand.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';

const COMPONENTS = [
];

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule,
    AngularMultiSelectModule,
    FullCalendarModule,
    TimepickerModule,
    AccordionModule,
    WebCamModule,
    NgSelectModule,
    CKEditorModule,
    TooltipModule.forRoot(),
    OrgChartModule
  ],
  providers: [
    DataService,
    supplierResolve
  ],
  declarations: [
    ...COMPONENTS,
    FloorPipe,
    TimeFormatPipe,
    DateFormatShort,
    DateWithTimeFormatShort,
    StatusRequestCash,
    StringToNumber,
    LocalTimeFormatPipe,
    UserComponent,
    SupplierComponent,
    ManageSupplierComponent,
    ProductComponent,
    ManageProductComponent,
    AdminComponent,
    SettingsComponent,
    ViewSupplierDetailsComponent,
    BrandWiseProductComponent,
    SelctComponent,
    ImagePreviewComponent,
    CustomerComponent,
    CategoryComponent,
    ManageCategoryComponent,
    DealsComponent,
    OrdersComponent,
    ReportsComponent,
    BrandComponent,
    DashboardComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]

})
export class HomeModule { }
