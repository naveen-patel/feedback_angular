import CategoryService from "./category.service";
import SequenceService from "../Sequence/Sequence.service";
import logger from "../../../config/logger.config";
import ProductService from "../Product/Product.service";
import BrandService from "../Brand/brand.service"; 
var _ = require("lodash");
import settings from '../../../settings';
let config = require('./../../../config/' + settings.environment + '.config').default;
const MEDIA = config.media;
let mkdirp = require('mkdirp');
var uuidv1 = require('uuid/v1');
const fs = require('fs');
const path = require('path');
var pdf = require('html-pdf');
import moment from "moment";
const nodemailer = require("nodemailer");
import EmailService from "../CommonService/email.service";
import SettingsService from '../Settings/settings.service';
var pdfCount = 0,
  excelCount = 0;
var imgSrc = path.normalize('file://' + __dirname + '../../../../logo/logo.png');


const CategoryController = {
  getCategoryById: async (req, res) => {
    try {
      const category = await CategoryService.getCategoryById(req.params.category_id);
      logger.info(" Getting category by id = " + req.params.category_id);
      res.send({
        code: 200,
        status: "success",
        message: "List the category",
        data: category
      });
    } catch (error) {
      logger.error("Error in getting category :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting category",
        data: {}
      });
    }
  },
  createCategory: async (req, res) => {
    try {
      let logo;
      let bodyData = JSON.parse(JSON.stringify(req.body));
      let data = bodyData.bodyData ? JSON.parse(bodyData.bodyData) : req.body;
      if (req.files && req.files.logo) {
        let imageFile = req.files.logo;
        if (imageFile) {
          let extension = imageFile.mimetype.split("/")[1];

          let filePath = MEDIA.local_file_path + "category_image" + "/";
          let file_name = uuidv1() + "-" + Date.now() + '.' + extension;
          logo = "/static/category_image" + "/" + file_name;
          if (!MEDIA.useS3) {
            await moveFile(imageFile, filePath, file_name)
              .catch((error) => {
                imageError = true;
                return badResponse(res, "Image Upload Failed");
              });
          }
        }
      }
      let category ={};
          category.category_name = data.category_name;
          category.description = data.description;
          category.logo = logo;
          category.priority = Number(data.priority)?Number(data.priority):1;
        let savedCategory = await CategoryService.createCategory(category);
        if (savedCategory.logo && savedCategory.logo !== "") {
          savedCategory.logo = config.api_end_point + savedCategory.logo
        } else {
          savedCategory.logo = ""
        }

        logger.info("New Category is created");
        return res.send({
          code: 200,
          status: "success",
          message: "New Category is created",
          data: savedCategory
        });
      }
     catch (error) {
      logger.error("Error in Creating Category :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in Creating Category",
        data: []
      });
    }
  },
  updateCategory: async (req, res) => {
    try {let logo;
      let bodyData = JSON.parse(JSON.stringify(req.body));
      let data = bodyData.bodyData ? JSON.parse(bodyData.bodyData) : req.body;
      if (req.files && req.files.logo) {
        let imageFile = req.files.logo;
        if (imageFile) {
          let extension = imageFile.mimetype.split("/")[1];

          let filePath = MEDIA.local_file_path + "category_image" + "/";
          let file_name = uuidv1() + "-" + Date.now() + '.' + extension;
          logo = "/static/category_image" + "/" + file_name;
          if (!MEDIA.useS3) {
            await moveFile(imageFile, filePath, file_name)
              .catch((error) => {
                imageError = true;
                return badResponse(res, "Image Upload Failed");
              });
          }
        }
      }
      let category ={};
      category.category_name = data.category_name;
      category.description = data.description;
      category.logo = logo;
      category.priority = Number(data.priority)?Number(data.priority):1;
    let editedCategory = await CategoryService.updateCategory(category,req.params.category_id);
    if (editedCategory.logo && editedCategory.logo !== "") {
      editedCategory.logo = config.api_end_point + editedCategory.logo
    } else {
      editedCategory.logo = ""
    }
    logger.info(" Category is update");
        return res.send({
          code: 200,
          status: "success",
          message: "Category is update",
          data: editedCategory
        });
    } catch (error) {
      logger.error("Error in Update Category :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in Update Category",
        data: {}
      });
    }
  },
  getAllCategory: async (req, res) => {
    try {
      var search;
    if (_.isEmpty(req.query.searchkey)) {
      search = '';
    } else {
      search = req.query.searchkey
    }
    let limit = Number(req.query.limit) ;
    let offset = parseInt(req.query.skip);
    let skip = (offset - 1) * limit;
      const categoryData = await CategoryService.getAllCategory(search, limit, skip);
      logger.info(" Getting all categories");
      res.send({
        code: 200,
        status: "success",
        message: "Listing All categories",
        data: categoryData
      });
    } catch (error) {
      logger.error("Error in getting categories :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting categories record",
        data: []
      });
    }
  },
  searchProductCategory: async(req,res )=>{
    try {
      var searchByName = (_.isEmpty(req.query.searchName)) ? '' : req.query.searchName;
      let limit = Number(req.query.limit) || 10;
      let offset = (req.query.page) ? parseInt(req.query.page) : 1
      let skip = (offset - 1) * limit;
      let category_id = req.params.category_id;
      let product = await CategoryService.searchProductCategory(searchByName,category_id,limit, skip);
      logger.info(" getting  product by categoey");
      res.send({
        code: 200,
        status: "success",
        message: "product found",
        data: product
      });
      
    } catch (error) {
      logger.error(" Getting error"+error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Something went wrong..",
        data: {}
      });
    }
  },
  getBrandNameById : async (req,res) =>{
    try {
      let category_id = req.params.category_id;
      let brands = await CategoryService.getBrandNameById(category_id);
      logger.info(" Getting brand");
      if(brands){
        res.send({
          code: 200,
          status: "success",
          message: "list of brands",
          data: brands
        });
      }else{
        res.send({
          code: 204,
          status: "success",
          message: "No data found",
          data: []
        });
      }
    } catch (error) {
      logger.error(" Getting error"+error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Something went wrong..",
        data: {}
      });
    }
  },
  deleteCategory: async (req, res) => {
    try {
      var ids = (req.params.category_id).split(',');
      let inProduct = await ProductService.checkBrands(ids,0);
      if(inProduct) return res.send({code:200,status:"Exist"});
      let isbrand = await BrandService.checkCategory(ids)
      if(isbrand) return res.send({code:200,status:"Exist"});
      var data = await CategoryService.deleteCategoryByIds(ids);

      res.send({
        code: 200,
        status: "success",
        message: "category is successfully deleted",
        data: data
      });
    } catch (error) {
      res.status(400).send({
        code: 400,
        status: "error",
        message: "category Does not delete",
        data: {}
      });

    }
  }
};
let badResponse = function (res, msg) {
  res.status(400).send({
    code: 400,
    status: "error",
    message: msg,
    data: []
  });
};
let moveFile = async (media_file, filePath, file_name) => {
  return await new Promise((resolve, reject) => {

    mkdirp(filePath, function (err) {
      if (err) {
        logger.error(err);
        reject({
          code: 400,
          status: "error",
          message: "Error in Uploading file",
          data: []
        });
      } else {
        media_file.mv(filePath + file_name, function (err) {
          if (err) {
            logger.error(err);
            reject({
              code: 400,
              status: "error",
              message: "Error in Uploading file",
              data: []
            });
          } else {
            logger.log("File Moved")
            resolve({
              code: 200,
              status: "success",
              message: "Uploaded Successfully",
              data: []
            })
          }
        });
      }
    });
  });
}

export default CategoryController;