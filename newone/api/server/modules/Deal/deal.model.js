import mongoose from 'mongoose';
var Schema = mongoose.Schema;

const DealSchema = mongoose.Schema({
    product_id:{
        type: Schema.Types.ObjectId,
        ref: "product"
    },
    deal_name:{
        type:String
    },
    deal_percentage:{
        type:String
    },
    actual_price_aed: {
        type: String
    },
    offer_price_aed: {
        type: String
    },
    actual_price_usd: {
        type: String
    },
    offer_price_usd: {
        type: String
    },
    deal_start_date: {
        type: Date
    },
    deal_end_date: {
        type: Date
    }
    
}, {
    collection: 'deal',
    timestamps: true
});

let DealModel = mongoose.model('deal', DealSchema);

export default DealModel