import { Component, OnInit, ChangeDetectorRef, TemplateRef } from '@angular/core';
import { DataService } from "../../services/data.service";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import 'rxjs/Rx';
declare var $: any;
declare var require: any;
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { concatStatic } from 'rxjs/operator/concat';
import { CustomerService } from '../../services/customer.service';
var _ = require('lodash');

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  records;
  settingsform: FormGroup;
  submitted: boolean = false;
  save: boolean = false;
  id: string = 'new'
  otpForm: FormGroup;
  constructor(
    public ref: ChangeDetectorRef,
    public ds: DataService,
    public cs: CustomerService,
    private modalService: BsModalService,
    public formBuilder: FormBuilder,

  ) { }

  ngOnInit() {
    this.otpForm = this.formBuilder.group({
      otp_duration: ['', [Validators.required]],
    });
    this.ds.getsettings().subscribe(data => {
      this.records = data;
      console.log(this.records.otp_duration)
      this.settingsform = this.formBuilder.group(this.getFormFields());
    });
    this.settingsform = this.formBuilder.group(this.getFormFields());
    setTimeout(() => {
      this.otpForm.controls['otp_duration'].setValue(this.records.otp_duration);
    }, 500)
  }

  getFormFields() {
    if (this.records) {

      this.id = this.records._id;
      return {
        host: [this.records.host != null ? this.records.host : "", Validators.required],
        port: [this.records.port != null ? this.records.port : ""],
        // tslint:disable-next-line:max-line-length
        otp_duration: [this.records.otp_duration != null ? this.records.otp_duration : "", Validators.required],
        email: [this.records.email != null ? this.records.email : "", Validators.required],
        password: [this.records.password != null ? this.records.password : "", Validators.required]
      };
    } else {
      return {
        host: ["", Validators.required],
        port: [""],
        otp_duration: [""],
        email: ["", Validators.required],
        password: ["", Validators.required]
      };
    }

  }
  get f() { return this.settingsform.controls; }
  get g() { return this.otpForm.controls; }

  // for smtp setting
  onSubmit() {
    this.submitted = true;
    if (this.settingsform.valid) {
      let formValue = Object.assign({}, this.settingsform.value);
      this.ds.updateSettings(formValue, this.id).subscribe(res => {
        this.ds.successMessage('Setting Saved')
        this.ds.getsettings().subscribe((data) => {
          this.records = data;
          this.submitted = false;
        });
      });
    }
  }
  otpSave() {
    this.save = true;
    if (this.otpForm.valid) {
      let formValue = Object.assign({}, this.otpForm.value);
      this.cs.saveOtpDuration(formValue, this.id).subscribe(res => {
        this.ds.successMessage('OTP Saved')
        this.save = false;
      });
    }
  }
}
