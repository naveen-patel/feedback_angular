import { Component, OnInit, TemplateRef, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
@Component({
  selector: 'app-image-preview',
  templateUrl: './image-preview.component.html',
  styleUrls: ['./image-preview.component.scss']
})
export class ImagePreviewComponent implements OnInit {

  constructor(
    private modalService: BsModalService,
  ) { }
  @Input() childMessage: any;

  @Output() messageEvent = new EventEmitter<any>();

  @ViewChild("template") addTemplate: TemplateRef<any>;
  imageData:any;
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    keyboard: false,
    class: 'modal-sm'
  }
  ngOnInit() {
    this.imageData =this.childMessage;
    setTimeout(() => {
      this.openModal(this.addTemplate)
    }, 1000);
  }
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.config);
  }
  closeModel(){
    this.modalRef.hide();
    this.modalRef = null
    this.messageEvent.emit()
  }

}
