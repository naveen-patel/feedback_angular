import { Component, OnInit, ChangeDetectorRef, TemplateRef } from '@angular/core';
import { CustomerService } from '../../services/customer.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Record } from 'immutable';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CompressorService } from '../../services/compressor.service';
import { NgxImageCompressService } from 'ngx-image-compress';
import { map, expand } from 'rxjs/operators';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  modalRef2: BsModalRef;
  modalRef3: BsModalRef;
  modalRef4: BsModalRef;
  modalRef5: BsModalRef;
  deleteImage1: Boolean = false;
  logo: any = "";
  errorImageSize1: Boolean = false;

  file_details: string;
  records = [];
  upload_records = [];
  openAddProduct: boolean = false;
  parentmessage: any;
  loader: boolean = false;
  search: string = '';
  limit = 10;
  skip = 1;
  modalRef: BsModalRef;
  count: Number;
  currentpage: Number = 1;
  Allrecords = [];
  editCategory: string = 'Add';
  id: string;
  isSelectAll: boolean = false;
  selectedIds = [];
  openBrandWiseProduct: boolean = false
  dropdownList = [];
  brandList = [];
  selectedItems = [];
  remainingRecords = 0;
  mailFileType = ''
  sendEMailId = ''
  Add: Boolean = true
  addNew: any;
  viewOnly: Boolean = false
  submitted: boolean = false;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    keyboard: false,
    class: 'modal-dialog'
  }
  categoryForm: FormGroup;
  editNew: any;
  formData: any = new FormData();
  constructor(
    public cs: CustomerService,
    private modalService: BsModalService,
    public formBuilder: FormBuilder,
    private compressor: CompressorService,
    private imageCompress: NgxImageCompressService
  ) { }

  ngOnInit() {

    this.getCategory()
  }
  // for get all category details
  getCategory() {
    this.remainingRecords = 0;
    this.skip = 1;
    this.isSelectAll = false;
    this.selectedIds = [];

    this.cs.getCategory(this.search, this.limit, this.skip).subscribe(data => {
      this.loader = false;
      this.count = data.totalcount;
      this.records = data.category;
      this.selectedIds = [];
      this.isSelectAll = false;
      this.remainingRecords = data.totalcount - (this.skip * this.limit);
      this.records.forEach((element) => {
        element.isSelect = false;
      })
    });
  }

  // for searching category
  searchProduct() {
    this.getCategory();
  }
  // for select single record (category)
  selectRecord(index) {
    if (this.records[index].isSelect) {
      var i = this.selectedIds.indexOf(this.records[index]._id);
      if (i !== -1) {
        this.selectedIds.splice(i, 1);
      }
    } else {
      this.selectedIds.push(this.records[index]._id)
    }
    this.records[index].isSelect = !this.records[index].isSelect
    this.isSelectAll = (this.selectedIds.length === this.records.length)
  }
  // for select multiple record (category)
  selectAllRecord() {
    this.selectedIds = [];
    if (this.isSelectAll) {
      this.isSelectAll = false
      this.records.forEach((element) => {
        element.isSelect = false;
      })
    } else {
      this.isSelectAll = true;
      this.records.forEach((element) => {
        element.isSelect = true;
        this.selectedIds.push(element._id)
      })
    }
  }

  // for category form
  getFormFields(i) {
    if (i != 'null') {
      this.editCategory = 'Edit';
      this.logo = this.records[i].logo;
      return {
        _id: [this.records[i]._id],
        category_name: [this.records[i].category_name, Validators.required],
        description: [this.records[i].description, Validators.required,],
        priority: [this.records[i].priority, Validators.required,],
      }
    } else {
      this.editCategory = 'Add';
      this.logo = '';
      return {
        category_name: ["", Validators.required],
        description: ["", Validators.required],
        priority: ["", Validators.required],
      }
    }
  }
  forDisabled() { return true }
  openModal(template: TemplateRef<any>, i, type) {
    if ((type == 'Add') || (type == 'Edit') && (type != 'Open')) {
      this.editCategory = type;
      this.viewOnly = false;
      this.modalRef = this.modalService.show(template, this.config);
      this.categoryForm = this.formBuilder.group(this.getFormFields(i));
      this.submitted = false;
    } else if (type == 'Open') {
      this.viewOnly = true;
      this.modalRef = this.modalService.show(template, this.config);
      this.categoryForm = this.formBuilder.group(this.getFormFields(i));
      this.editCategory = "";

    }
  }
  onSubmit() {
    this.submitted = true;
    if (this.categoryForm.valid) {
      if (this.editCategory == 'Edit') {
        let formValue = Object.assign({}, this.categoryForm.value);
        this.editNew = formValue;
        this.editNew.delete_card_1 = this.deleteImage1;
        this.formData.set('bodyData', JSON.stringify(this.editNew))
      } else {
        let formValue = Object.assign({}, this.categoryForm.value);
        this.addNew = formValue;
        this.formData.set('bodyData', JSON.stringify(this.addNew))
      }
      this.submitsection();
    }
  }

  get f() { return this.categoryForm.controls; }


  submitsection() {
    if (this.editCategory == 'Edit') {
      var id = this.editNew._id;
      this.cs.updateCategory(id, this.formData).subscribe(data => {
        this.closeModal();
        this.reload();
        this.cs.successMessage('Category Updated Successfully')
      });
    } else {
      this.cs.addCategory(this.formData).subscribe(data => {
        this.closeModal();
        this.reload();
        this.cs.successMessage('New Category added Successfully')
      });
    }
  }
  isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }

  // for delete record
  DeleteRecord(template: TemplateRef<any>, i, id) {
    this.modalRef3 = this.modalService.show(template);
    this.id = id;
  }
  confirmToDelete() {
    this.cs.deleteCategory(this.id).subscribe(data => {
      if (data.status == 'Exist') {
        this.cs.warningMessage("Category exist in product, can't delete it");
        this.ngOnInit();
      } else {
        this.cs.successMessage("Category Deleted Successfully");
        this.ngOnInit();
      }
    })
    this.modalRef3.hide();
    this.modalRef3 = null
  }
  confirmToMultiDelete() {
    this.cs.deleteCategory(this.selectedIds).subscribe(data => {
      if (data.status == 'Exist') {
        this.cs.warningMessage("Categories exist in product, can't delete it");
        this.ngOnInit();
      } else {
        this.cs.successMessage("Categories Deleted Successfully");
        this.ngOnInit();
      }
    })
    this.modalRef4.hide();
    this.modalRef4 = null
  }
  DeleteRecords(template: TemplateRef<any>) {
    this.modalRef4 = this.modalService.show(template);
  }

  // for modal close 
  closeModal() {
    this.modalRef.hide();
    this.modalRef = null
  }
  // for reload skip after changes 
  reload() {
    this.search = "";
    this.skip = 1;
    this.ngOnInit();
  }
  // for load more Record(skip 2)
  loadMore() {
    this.skip++;
    this.cs.getCategory(this.search, this.limit, this.skip).subscribe(data => {
      var product = data.category;
      this.isSelectAll = false;
      product.forEach((element) => {
        element.isSelect = false;
      })
      this.records = this.records.concat(product);
      this.remainingRecords = data.totalcount - (this.skip * this.limit);
    });
  }





  // for image 

  compressFile(type) {
    this.imageCompress.uploadFile().then(({ image, orientation }) => {

      console.warn('Size in bytes was:', this.imageCompress.byteCount(image));

      if (this.imageCompress.byteCount(image) > 30000) {
        this.imageCompressToMinSize(image, type, orientation)
      } else {
        //this.imgResultAfterCompress = result;
        var block = image.split(";");
        // Get the content type of the image
        var contentType = block[0].split(":")[1];// In this case "image/gif"
        // get the real base64 content of the file
        var realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."

        // Convert it to a blob to upload
        var blob = this.b64toBlob(realData, contentType);
        if (type === 'logo') {
          this.deleteImage1 = false;
          this.logo = image;
          this.formData.set("logo", blob, 'logo');
        } else {
          // this.deleteImage2 = false;

          // this.product_back_image = image;
          // this.formData.set("product_back_image", blob, 'product_back_image');
        }

        console.warn('Size in bytes is now:', this.imageCompress.byteCount(image));
      }
    });
  }

  imageCompressToMinSize(image, type, orientation) {
    this.imageCompress.compressFile(image, orientation, 50, 50).then(
      result => {
        console.warn('Size in bytes was:', this.imageCompress.byteCount(image));
        if (this.imageCompress.byteCount(result) < 40000) {
          var block = result.split(";");
          var contentType = block[0].split(":")[1];
          var realData = block[1].split(",")[1];
          var blob = this.b64toBlob(realData, contentType);
          if (type === 'logo') {
            this.deleteImage1 = false;
            this.logo = image;
            this.formData.set("logo", blob, 'logo');
          } else {
            // this.deleteImage2 = false;

            // this.product_back_image = image;
            // this.formData.set("product_back_image", blob, 'product_back_image');
          }

          console.warn('Size in bytes is now:', this.imageCompress.byteCount(result));
        } else {
          this.imageCompressToMinSize(result, type, orientation);
        }
      }
    );
  }
  b64toBlob(b64Data, contentType) {
    contentType = contentType || '';
    var sliceSize = 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);

      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      var byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }
  imageData: any;
  openImagePreview: Boolean = false;
  imagePreview(data) {
    if (data) {


      this.openImagePreview = true;
      this.imageData = data
    }
  }
  closeImagePreview() {


    this.imageData = "";
    this.openImagePreview = false
  }

  imageDelete(type) {
    if (type === 'image1') {
      this.deleteImage1 = true;
      this.logo = "";
      this.formData.set("logo", "");

    }
  }

  data: FileList;
  compressedImages = [];
  recursiveCompress = (image: File, index, array) => {


    if (image) {
      return this.compressor.compress(image).pipe(
        map(response => {

          //Code block after completing each compression
          this.compressedImages.push(response);
          return {
            data: response,
            index: index + 1,
            array: array,
          };
        }),
      );
    }

  }

  public process(event, image) {
    this.data = event.target.files;
    var filesize = ((this.data[0].size / 1024) / 1024).toFixed(4);
    // if (Number(filesize) < 1.1) {


    var obj = {};
    const compress = this.recursiveCompress(this.data[0], 0, this.data);
    // .pipe(
    //   expand(res => {
    //     return this.recursiveCompress(this.data[res.index], res.index, this.data);
    //   }),
    // );
    compress.subscribe(res => {
      if (res.index > res.array.length - 1) {
        var reader = new FileReader();
        // reader.onload = (function (aImg) { return function (e) { aImg.src = e.target.result; }; })(img);
        reader.readAsDataURL(this.compressedImages[0]);
        reader.onload = () => {
          // this.filename = file.name;
          if (image === 'logo') {
            this.deleteImage1 = false;
            this.formData.set("logo", this.compressedImages[0], this.compressedImages[0].name);

            this.logo = reader.result;
            this.errorImageSize1 = false;
            this.compressedImages = []
          }
        };
      }
    });
  }


}
