import express from "express";
import Users from './user.controller';
import UserAuthController from "./../UserAuthentication/userAuth.controller";

const router = express.Router();

//Get all users data
router.get('/users', UserAuthController.verifySuperAdmin, Users.getAllUsers);

//Add a new user
router.post('/users', UserAuthController.verify, Users.addUser);

//update a new user
router.post('/user/:user_id', UserAuthController.verify, Users.updateUserProfile);

router.post('/userChangePassword/:user_id', UserAuthController.verify, Users.userChangePassword);

//Check Authenticated
router.get('/users/check_authenticated', UserAuthController.verifySuperAdmin, (req, res) => {
    return res.status(200).send({
        code: 200,
        message: "Authenticated",
        user: req.userToken.data
    });
});

//find a user by user id
router.get('/users/:user_id', UserAuthController.verify, Users.getUserByUserId);

router.post('/change_status/:id', UserAuthController.verifySuperAdmin, Users.changeStatus);

router.delete('/users/:id', UserAuthController.verifySuperAdmin, Users.deleteUser);
router.get('/users/report/excel', UserAuthController.verify,Users.createExcel);
router.get('/users/report/excel/:file_name', Users.excelFileDownload);

//Add a new user with business
// router.post('/register/user/with/business', Users.addBusinessUser);

// router.post('/users/sendEmail', UserAuthController.sendEmailVerification);

export default router;