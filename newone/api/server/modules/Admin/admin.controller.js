import settings from '../../../settings';
let config = require('./../../../config/' + settings.environment + '.config').default;
import AdminService from "../Admin/admin.service";
import logger from "../../../config/logger.config";
import UserService from "../User/user.service";
import SettingsService from '../Settings/settings.service';
import EmailService from "../CommonService/email.service";
const fs = require('fs');
const path = require('path'); 
import sha256 from "sha256";


var _ = require('lodash');

const AdminController = {};


AdminController.getAlladmins = async (req, res) => {
    try {
        var search;
        if (_.isEmpty(req.query.searchkey)) {
            search = '';
        } else {
            search = req.query.searchkey
        }
        let limit = Number(req.query.limit) || 10;
        let offset = (req.query.skip) ? parseInt(req.query.skip) : 1
        let skip = (offset - 1) * limit;
        let type = req.query.type || null;
        let status = req.query.status || 'active'
        const users = await AdminService.getAlladmins(search, limit, skip, type, status);
        logger.info(" Getting all Admins");
        res.send({
            code: 200,
            status: "success",
            message: "Listing All Admins",
            data: users
        });
    } catch (error) {
        logger.error("Error in getting User record :" + error);
        res.status(400).send({
            code: 400,
            status: "error",
            message: "Error in getting user record",
            data: []
        });
    }
};

AdminController.addAdmin = async (req, res) => {
    try {
        let User = {};
        let userEmailExists = await UserService.getUserByEmailId(req.body.email, '');
        if (!_.isEmpty(userEmailExists)) {
            return res.status(400).send({
                code: 400,
                status: "error",
                message: "User/Email id already exists",
                data: []
            });
        }
        let userPhoneNumberExists = await UserService.getUserByPhoneNumber(req.body.phone_number, '');
        if (!_.isEmpty(userPhoneNumberExists)) {
            return res.status(400).send({
                code: 400,
                status: "error",
                message: "Phone Number is already exists",
                data: []
            });
        }
        var length = 8,
            charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
            password = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            password += charset.charAt(Math.floor(Math.random() * n));
        }
       
        User.first_name = req.body.first_name;
        User.last_name = req.body.last_name;
        User.country_code = req.body.country_code;
        User.phone_number = req.body.phone_number;
        User.email = req.body.email.toLowerCase();
        User.type = 'admin';
        User.status = 'in_active';
        User.password = await sha256(password);
        let savedUser = await AdminService.addAdmin(User);
        res.send({
            code: 200,
            status: "success",
            message: "Sucessfully added admin records",
            data: savedUser
        });
        let setting = await SettingsService.getAllsettings();
        var imagepath = config.api_end_point + '/image/logo/';

        
        let pwdShareHtml = await fs.readFileSync(
            path.join(__dirname, "../../EmailTemplates/password_share.html"),
            "utf-8"
          );
          pwdShareHtml = pwdShareHtml.replace('{firstname}', req.body.first_name)
          .replace("{password}", password)
          .replace('{logo}', imagepath + 'maxbrands.png')
          .replace('{phone}', imagepath + 'phone.png')
          .replace('{whatsapp}', imagepath + 'whatsapp.png')
          .replace('{mail}', imagepath + 'email.png');
        EmailService.sendEmail(
            req.body.email.toLowerCase(),
            "Your FMCG Markets account details",
            pwdShareHtml, setting, []
          );

    } catch (error) {
        logger.error("Error in adding admin record" + error);
        res.send({
            code: 400,
            status: "error",
            message: "Error in adding admin record",
            data: {}
        });
    }
};

AdminController.getAdminByAdminId = async (req, res) => {
    try {
        const user_id = req.params.user_id;
        const user = await AdminService.getAdminByAdminId(user_id);
        res.send({
            code: 200,
            status: "success",
            message: "Sucessfully retrived Admin details",
            data: user
        });
    } catch (error) {
        logger.error("Error in fetching Admin record" + error);
        res.status(400).send({
            code: 400,
            status: "error",
            message: "Error in fetching Admin record",
            data: []
        });
    }
};

AdminController.updateAdminByAdminId = async (req, res) => {
    try {
        let User = {};
        console.log(req.params.user_id)
        console.log(req.body);
        let user_id = req.params.user_id;

        let userEmailExists = await UserService.getUserByEmailId(req.body.email, user_id);
        if (!_.isEmpty(userEmailExists)) {
            return res.status(400).send({
                code: 400,
                status: "error",
                message: "User/Email id already exists",
                data: []
            });
        }
        let userPhoneNumberExists = await UserService.getUserByPhoneNumber(req.body.phone_number, user_id);
        if (!_.isEmpty(userPhoneNumberExists)) {
            return res.status(400).send({
                code: 400,
                status: "error",
                message: "Phone Number is already exists",
                data: []
            });
        }
        User.first_name = req.body.first_name;
        User.last_name = req.body.last_name;
        User.country_code = req.body.country_code;
        User.phone_number = req.body.phone_number;
        User.email = req.body.email.toLowerCase();
        let savedUser = await AdminService.updateAdminByAdminId(user_id, User);
        res.send({
            code: 200,
            status: "success",
            message: "Sucessfully updated admin records",
            data: savedUser
        });
    } catch (error) {
        logger.error("Error in updating admin record" + error);
        res.send({
            code: 400,
            status: "error",
            message: "Error in updating admin record",
            data: []
        });
    }
};

AdminController.changeStatus = async (req, res) => {
    try {
        let status = req.body.status
        const user = await AdminService.changeStatus(req.params.id, status);
        res.status(200).send({
            code: 200,
            status: "success",
            message: "user status is  changed",
            data: user
        });
    } catch (error) {
        res.status(401).send({
            code: 401,
            status: "error",
            message: "Error Occured",
            data: {}
        });
    }
};


export default AdminController;