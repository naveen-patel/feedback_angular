import { Component, OnInit, TemplateRef, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../../services/data.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
@Component({
  selector: 'app-brand-wise-product',
  templateUrl: './brand-wise-product.component.html',
  styleUrls: ['./brand-wise-product.component.scss']
})
export class BrandWiseProductComponent implements OnInit {

  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    keyboard: false,
    class: 'modal-lg'
  }
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  remainingRecords=0;
  limit=50;
  page=1;
  records = [];
  brandList=[];
  isSelectAll: boolean = false;
  selectedIds = [];


  constructor(
    private ds: DataService,
    private route: ActivatedRoute,
    private modalService: BsModalService,
  ) {

  }
  @Input() childMessage: any;

  @Output() messageEvent = new EventEmitter<any>();

  @ViewChild("template") addTemplate: TemplateRef<any>;


  ngOnInit() {
    setTimeout(() => {
      this.openModal(this.addTemplate)
    }, 1000);
    this.dropdownList=[];
    this.ds.getBrandList().subscribe(data => {
      var i=1;
      data.forEach(element => {
        this.dropdownList.push({
          "id":i,
          "itemName":element
        })
        i++;
      });
    })
    // this.dropdownList = [
    //   { "id": 1, "itemName": "India" },
    //   { "id": 2, "itemName": "Singapore" },
    //   { "id": 3, "itemName": "Australia" },
    //   { "id": 4, "itemName": "Canada" },
    //   { "id": 5, "itemName": "South Korea" },
    //   { "id": 6, "itemName": "Germany" },
    //   { "id": 7, "itemName": "France" },
    //   { "id": 8, "itemName": "Russia" },
    //   { "id": 9, "itemName": "Italy" },
    //   { "id": 10, "itemName": "Sweden" }
    // ];
    // this.selectedItems = [
    //   { "id": 2, "itemName": "Singapore" },
    //   { "id": 3, "itemName": "Australia" },
    //   { "id": 4, "itemName": "Canada" },
    //   { "id": 5, "itemName": "South Korea" }
    // ];
    this.dropdownSettings = {
      singleSelection: false,
      text: "Select Brand",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };
  }
  onItemSelect(item: any) {
    this.getProducts();
  }
  OnItemDeSelect(item: any) {
    this.getProducts();
  }
  onSelectAll(items: any) {
    this.getProducts();
  }
  onDeSelectAll(items: any) {
    this.getProducts();
  }
  getProducts(){
    this.brandList=[];
    this.selectedItems.forEach(element => {
      this.brandList.push(element.itemName);
    });
    this.page=1;
    this.isSelectAll=false;
    this.selectedIds=[];
    if(this.brandList.length===0){
      this.records=[];
    }else{
      this.ds.getProductsByBrands(['all'],this.brandList,'',this.limit,this.page).subscribe(data => {
        
        this.records=data.product
        this.remainingRecords=(data.totalpage*this.limit)-this.limit;
        this.records.forEach((element) => {
          element.isSelect = false;
        })
      });
    }
  }
  loadMore(){
    this.page++;
    this.ds.getProductsByBrands(['all'],this.brandList,'',this.limit,this.page).subscribe(data => {
      var product =data.product;
      this.isSelectAll=false;
      product.forEach((element) => {
        element.isSelect = false;
      })
      this.records=this.records.concat(product);
      this.remainingRecords=(data.totalpage*this.limit)-this.records.length;
    });
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.config);
  }

  closeModel() {
    this.modalRef.hide();
    this.modalRef = null
  }
  selectRecord(index) {
    if (this.records[index].isSelect) {
      var i = this.selectedIds.findIndex(id => this.records[index]._id);
      if (i !== -1) {
        this.selectedIds.splice(i, 1);
      }
    } else {
      this.selectedIds.push(this.records[index]._id)
    }
    this.records[index].isSelect = !this.records[index].isSelect
    this.isSelectAll=(this.selectedIds.length===this.records.length)
  }
  selectAllRecord() {
    this.selectedIds=[];
    if(this.isSelectAll){
      this.isSelectAll=false
      this.records.forEach((element) => {
        element.isSelect = false;
      })
    }else{
      this.isSelectAll=true;
      this.records.forEach((element) => {
        element.isSelect = true;
        this.selectedIds.push(element._id)
      })
    }
  }
  generatePDF(){
    // this.ds.generateProductPDFbyIds(this.selectedIds).subscribe((data)=>{
    //   window.open(data.path)
    // });
  }

}
