import express from "express";
import SupplierController from "./Supplier.controller"
import UserAuthController from "./../UserAuthentication/userAuth.controller";


const SupplierRouter = express.Router();

SupplierRouter.get('/supplier/:id', UserAuthController.verify, SupplierController.getSupplierById);
SupplierRouter.get('/all/supplier', UserAuthController.verify, SupplierController.getAllSupplier);
SupplierRouter.get('/supplier', UserAuthController.verify, SupplierController.getSupplierByPagination);
SupplierRouter.get('/search/supplier', UserAuthController.verify, SupplierController.getSearchSupplierByPagination);
SupplierRouter.get('/supplier/report/excel', UserAuthController.verify, SupplierController.downloadSupplierAsExcel);
SupplierRouter.get('/supplier/report/excel/:file_name', SupplierController.excelFileDownload);
SupplierRouter.get('/supplier/report/pdf/:file_name', SupplierController.pdfFileDownload);
SupplierRouter.post('/supplier', UserAuthController.verify, SupplierController.createSupplier);
SupplierRouter.put('/supplier/:id', UserAuthController.verify, SupplierController.updateSupplier);
SupplierRouter.delete('/supplier/:id', UserAuthController.verify, SupplierController.deleteSuppliers);
SupplierRouter.get('/supplier/report/pdf',UserAuthController.verify, SupplierController.createPDF);



export default SupplierRouter;