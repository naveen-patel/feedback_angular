import { Component, OnInit, HostBinding } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { fadeInAnimation } from '../../../shared/animations/fade-in.animation';
import { Store } from '@ngrx/store';
import * as fromAuth from '../../store/reducers';
import * as Auth from '../../store/actions/auth.actions';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { validateConfig } from '@angular/router/src/config';
import { ToastyService } from 'ng2-toasty';
import {AuthService} from '../../services/auth.service';


@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  redirectSubs: Subscription;
  returnUrl: string;
  id:string=''
  email:string='';
  errors=[]

  constructor(public formBuilder: FormBuilder,
    public store: Store<fromAuth.State>,
    private route: ActivatedRoute,
    private router: Router,
    private ds : AuthService,
    private toasty: ToastyService,
  ) { }

  form: FormGroup = this.formBuilder.group({
    password: ['', Validators.required],
    confirm_password:['',Validators.required]
  });

  ngOnInit() {
    this.id=this.route.snapshot.params['id'];
    this.email=this.route.snapshot.params['email']
  }
  submit(){
    let formValue = Object.assign({}, this.form.value);
  
    if(formValue.password===formValue.confirm_password){
      if(this.validatePassword(formValue.password)){
        var obj={
          email:this.email,
          uuid:this.id,
          password : formValue.password
        }
        this.ds.changePassword(obj).subscribe(data => {
          this.ds.successMessage('Successfully Password Changed ')
          this.router.navigate(['/auth/login'])
        });
        this.form = this.formBuilder.group({
          password: ['', Validators.required],
          confirm_password:['',Validators.required]
        });
      }
      
    }else{
      this.ds.errorMessage("Password not match")
    }
    
  }
   validatePassword(p) {
    var p
        this.errors = [];
    if (p.length < 6) {
        this.errors.push("Your password must be at least 6 characters"); 
    }
    if (p.search(/[A-Z]/) < 0) {
        this.errors.push("Your password must contain at least Capital letter.");
    }
    // if (p.search(/[0-9]/) < 0) {
    //     this.errors.push("Your password must contain at least one digit."); 
    // }
    if (this.errors.length > 0) {
        return false;
    }
    return true;
}

}
