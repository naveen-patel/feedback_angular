import { Component, OnInit, ChangeDetectorRef, TemplateRef } from '@angular/core';
import { DataService } from "../../services/data.service"
import 'rxjs/Rx';
declare var $: any;
declare var require: any;
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
var _ = require('lodash');

@Component({
  selector: 'app-supplier',
  templateUrl: './supplier.component.html',
  styleUrls: ['./supplier.component.scss']
})
export class SupplierComponent implements OnInit {

  modalRef2: BsModalRef;
  modalRef3: BsModalRef;
  modalRef4: BsModalRef;

  file_details: string;
  records = [];
  upload_records = [];
  openAddSupplier: boolean = false;
  parentmessage: any;
  loader: boolean = false;
  searchName: string = '';
  searchCode: string = '';
  searchProduct: string = '';
  limit: any = 10;
  page: any = 1;
  count: Number;
  Allrecords = [];
  id: string;
  isSelectAll: boolean = false;
  selectedIds = [];
  remainingRecords: any = 0;

  constructor(
    public ref: ChangeDetectorRef,
    public ds: DataService,
    private modalService: BsModalService,
  ) { }


  ngOnInit() {
    this.selectedIds = [];
    this.isSelectAll = false;
    var initial = (this.page === 1)
    this.ds.getAllSupplier(this.searchName, this.searchCode, this.searchProduct, this.limit, this.page).subscribe(data => {
      this.loader = false;
      this.count = data.totalcount;
      this.remainingRecords = data.totalcount - (this.page * this.limit);
      if (initial) {
        this.records = data.supplier;
        this.records.forEach((element) => {
          element.isSelect = false;
        })
      } else {
        var suppliers = data.supplier
        suppliers.forEach((element) => {
          element.isSelect = false;
        });
        this.records = this.records.concat(suppliers)
      }

    })
  }
  searchSupplier() {
    this.ngOnInit();
  }
  reload() {
    this.searchName = "";
    this.searchCode = "";
    this.searchProduct = "";
    this.page = 1;
    this.ngOnInit();
  }

  pageChanged(event) {
    this.page = event.page;
    this.ngOnInit();
  }
  openSupplier() {
    this.openAddSupplier = true;
    this.parentmessage = ""
  }
  openEditSupplier(supplier, view) {
    supplier.isViewOnly = view
    this.openAddSupplier = true;
    this.parentmessage = supplier;
  }
  receiveMessage(event) {
    this.openAddSupplier = false;
    this.reload();
  }
  DeleteRecord(template: TemplateRef<any>, i, id) {
    this.modalRef3 = this.modalService.show(template);
    this.id = id;
  }
  confirmToDelete() {
    this.ds.deleteSupplier(this.id).subscribe(data => {
      this.ds.successMessage('Supplier Deleted Successfully')
      this.reload();
    })
    this.modalRef3.hide();
    this.modalRef3 = null
  }
  confirmToMultiDelete() {
    this.ds.deleteSupplier(this.selectedIds).subscribe(data => {
      this.ds.successMessage('Suppliers Deleted Successfully')
      this.reload();
    })
    this.modalRef4.hide();
    this.modalRef4 = null
  }
  DeleteRecords(template: TemplateRef<any>) {
    this.modalRef4 = this.modalService.show(template);
  }
  selectRecord(index) {
    if (this.records[index].isSelect) {
      var i = this.selectedIds.indexOf(this.records[index]._id);
      if (i !== -1) {
        this.selectedIds.splice(i, 1);
      }
    } else {
      this.selectedIds.push(this.records[index]._id)
    }
    this.records[index].isSelect = !this.records[index].isSelect
    this.isSelectAll = (this.selectedIds.length === this.records.length)
  }
  selectAllRecord() {
    this.selectedIds = [];
    if (this.isSelectAll) {
      this.isSelectAll = false
      this.records.forEach((element) => {
        element.isSelect = false;
      })
    } else {
      this.isSelectAll = true;
      this.records.forEach((element) => {
        element.isSelect = true;
        this.selectedIds.push(element._id)
      })
    }
  }
  generateExcel() {
    if (this.isSelectAll) {
      this.ds.generateSupplierExcel([]).subscribe(data => {
        window.open(data.path)
        this.ds.successMessage('Pdf Downloaded Successfully')
      })
    } else if (this.selectedIds.length > 0) {
        this.ds.generateSupplierExcel(this.selectedIds).subscribe(data => {
          window.open(data.path)
          this.ds.successMessage('Excel Downloaded Successfully')
        })
      } else {
        this.ds.errorMessage("Please Choose any of the suppliers to download!")
      }
  }
  generatePDF() {
    if (this.isSelectAll) {
      this.ds.generateSupplierPDF([]).subscribe(data => {
        window.open(data.path)
        this.ds.successMessage('Pdf Downloaded Successfully')
      })
    } else if (this.selectedIds.length > 0) {
      this.ds.generateSupplierPDF(this.selectedIds).subscribe(data => {
        window.open(data.path)
        this.ds.successMessage('Pdf Downloaded Successfully')
      })
    } else {
      this.ds.errorMessage("Please Choose any of the suppliers to download!")
    }
  }
  loadMore() {
    this.page++;
    this.ngOnInit();
  }

}
