import SupplierModel from "./Supplier.model";
const fs = require('fs');
var mongoose = require('mongoose');
import settings from '../../../settings';
let config = require('./../../../config/' + settings.environment + '.config').default;

const SupplierService = {
  getSupplierById: async id => {
    //// ok ////
    try {
      const Supplier = await SupplierModel.findById(id).lean();
      if (Supplier) {
        // Supplier.supplier_code = (Supplier.name.replace(/\s/g, '') + "supp").substring(0, 4) + Supplier.supplier_code
        if (Supplier.business_card_image_1 && Supplier.business_card_image_1 !== "") {
          Supplier.business_card_image_1 = config.api_end_point + Supplier.business_card_image_1;
        } else {
          Supplier.business_card_image_1 = "";
        }
        if (Supplier.business_card_image_2 && Supplier.business_card_image_2 !== "") {
          Supplier.business_card_image_2 = config.api_end_point + Supplier.business_card_image_2;
        } else {
          Supplier.business_card_image_2 = "";
        }
        Supplier.product={}
      }
      return Supplier;
    } catch (error) {
      throw error;
    }
  },
  getSupplier: async id => {
    //// ok ////
    try {
      const Supplier = await SupplierModel.findById(id);
      return Supplier;
    } catch (error) {
      throw error;
    }
  },
  getAllSupplier: async () => {
    try {
      const suppliers = await SupplierModel.find({}).sort({
        'name': 1
      });
      return suppliers;
    } catch (error) {
      throw error;
    }
  },
  getSupplierByPagination: async (searchkey, limit, skip) => {
    /////////ok//////
    try {
      var json = {},
        findObj = {
          "name": {
            "$regex": searchkey,
            "$options": "i"
          }
        }
      const count = await SupplierModel.find(findObj).count();
      const supplier = await SupplierModel.find(findObj).sort({
        "createdAt": -1
      }).skip(skip).limit(limit).lean();
      var supplierArray = []
      supplier.forEach(element => {
        // element.supplier_code = (element.name.replace(/\s/g, '') + "prod").substring(0, 4) + element.supplier_code;
        if (element.business_card_image_1 && element.business_card_image_1 !== "") {
          element.business_card_image_1 = config.api_end_point + element.business_card_image_1;
        } else {
          element.business_card_image_1 = "";
        }
        if (element.business_card_image_2 && element.business_card_image_2 !== "") {
          element.business_card_image_2 = config.api_end_point + element.business_card_image_2;
        } else {
          element.business_card_image_2 = "";
        }
        supplierArray.push(element)
      });

      json.totalpage = Math.ceil(count / limit);
      json.totalcount = count;
      json.supplier = supplierArray;
      return json;
    } catch (error) {
      throw error;
    }
  },
  getSearchSupplierByPagination: async (searchName, searchCode, searchPrdouct, limit, skip) => {
    /////////ok//////
    try {
      var search=[{
        name: {
          "$regex":searchName,
          "$options": "i"
        }
      },
      {
        supplier_code: {
          "$regex": searchCode,
          "$options": "i"
        }
      },
      
    ];
    
    if(searchPrdouct!==''){
      search.push({
        'products.brand_name': {
          "$regex": searchPrdouct,
          "$options": "i"
        }
      })
    }
     var searchObj={
      $or: search
    }
      const supplier = await SupplierModel.aggregate(
        [{
            '$lookup': {
              from: 'product',
              localField: '_id',
              foreignField: 'supplier_id',
              as: 'products'
            }
          }, {
            $match: searchObj
          },
          {$sort: {"createdAt": -1}},
          {
            '$facet': {
              metadata: [{
                $count: "total"
              }, {
                $addFields: {
                  page: (1)
                }
              }],
              supplier_data: [{
                $skip: skip
              }, {
                $limit: limit
              }]
            }
          }, {
            $project: {
              "_id": 0,
              "totalcount": { $arrayElemAt: ["$metadata.total", 0] },
              "totalPage": { $ceil: { $divide: [{ $arrayElemAt: ["$metadata.total", 0] }, limit] } },
              "supplier_data": "$supplier_data"
            }
          }
        ]);
      var supplierArray = []
      supplier[0].supplier_data.forEach(element => {
        // element.supplier_code = (element.name.replace(/\s/g, '') + "prod").substring(0, 4) + element.supplier_code;
        if (element.business_card_image_1 && element.business_card_image_1 !== "") {
          element.business_card_image_1 = config.api_end_point + element.business_card_image_1;
        } else {
          element.business_card_image_1 = "";
        }
        if (element.business_card_image_2 && element.business_card_image_2 !== "") {
          element.business_card_image_2 = config.api_end_point + element.business_card_image_2;
        } else {
          element.business_card_image_2 = "";
        }
        supplierArray.push(element)
      });
var json={}
      json.totalpage = Math.ceil(supplier[0].totalcount / limit);
      json.totalcount = supplier[0].totalcount;
      json.supplier = supplierArray;
      return json;
    } catch (error) {
      throw error;
    }
  },
  getSupplierbyPhoneNumber: async (phoneNumber, id) => {
    /////////ok//////
    try {
      var findObj = {};
      if (id === '') {
        findObj = {
          phone_number: phoneNumber
        }
      } else {
        var arr = [];
        var objId = mongoose.Types.ObjectId(id);
        arr.push(objId);
        findObj = {
          _id: {
            "$nin": arr
          },
          phone_number: phoneNumber
        }
      }
      const Supplier = await SupplierModel.find(findObj);
      return Supplier;
    } catch (error) {
      throw error;
    }

  },
  getSupplierbyEmail: async (email, id) => {
    /////////ok//////
    try {
      var findObj = {};
      if (id === '') {
        findObj = {
          email_id: email
        }
      } else {
        var arr = [];
        var objId = mongoose.Types.ObjectId(id);
        arr.push(objId);
        findObj = {
          _id: {
            "$nin": arr
          },
          email_id: email
        }
      }
      const Supplier = await SupplierModel.find(findObj);
      return Supplier;
    } catch (error) {
      throw error;
    }

  },
  getLastSupplier: async () => {
    /////////ok//////
    try {
      const lastSupplier = await SupplierModel.find({}).sort({
        createdAt: -1
      }).limit(1);
      return lastSupplier;
    } catch (error) {
      throw error;
    }
  },


  createSupplier: async newSupplier => {
    /////////ok//////
    try {
      var savedSupplier = await new SupplierModel(newSupplier).save();
      return savedSupplier
    } catch (error) {
      throw error;
    }
  },
  updateSupplier: async (id, data) => {
    /////////ok//////
    try {
      const update = await SupplierModel.findOneAndUpdate({
        _id: id
      }, data, {
        strict: true,
        new: true
      });
      return update;
    } catch (error) {
      throw error;
    }
  },
  getSuppliersByIds: async (ids) => {
    try {
      var data = await SupplierModel.find({
        _id: {
          $in: ids
        }
      }).sort({
        'name': 1
      });
      return data;
    } catch (error) {
      throw error;
    }
  },
  deleteSuppliersByIds: async (ids) => {
    try {
      var data = await SupplierModel.remove({
        _id: {
          $in: ids
        }
      });;
      return data;
    } catch (error) {
      throw error;
    }
  }
};
export default SupplierService;