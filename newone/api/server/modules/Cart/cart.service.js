import CartModel from "./cart.model";
const fs = require('fs');
var mongoose = require('mongoose');
import settings from '../../../settings';
let config = require('./../../../config/' + settings.environment + '.config').default;
import moment from "moment";

const CartService = {
  getCartById: async id => {
    try {
    let cart = await CartModel.findOne({_id: Object(id)})
                              .populate({path:'product_id',name:'product_name',
                                populate:{
                                path: 'deal brand',
                                select: 'deal_end_date offer_price_usd offer_price_aed deal_start_date deal_percentage brand_name'
       
                              }});
      let tempobj={};
      let tempCartObj ={};
          tempCartObj._id = cart._id;
          tempCartObj.user_id = cart.user_id;
          tempCartObj.quantity = cart.quantity
          tempCartObj.product_id ={};
          tempCartObj.product_id._id = cart.product_id._id
          tempCartObj.product_id.supplier_id= cart.product_id.supplier_id
          tempCartObj.product_id.category_id = cart.product_id.category_id
          tempCartObj.product_id.brand = cart.product_id.brand
          tempCartObj.product_id.stock_location = cart.product_id.stock_location
          tempCartObj.product_id.code = cart.product_id.code
          tempCartObj.product_id.product_code = cart.product_id.product_code
          tempCartObj.product_id.product_type = cart.product_id.product_type
          tempCartObj.product_id.product_name = cart.product_id.product_name
          tempCartObj.product_id.expiry_date = cart.product_id.expiry_date
          tempCartObj.product_id.ru_per_case = cart.product_id.ru_per_case
          tempCartObj.product_id.ean_code_ru = cart.product_id.ean_code_ru
          tempCartObj.product_id.ean_code_case = cart.product_id.ean_code_case
          tempCartObj.product_id.language = cart.product_id.language
          tempCartObj.product_id.origin = cart.product_id.origin
          tempCartObj.product_id.quantity = cart.product_id.quantity
          tempCartObj.product_id.sales_per_case_aed = cart.product_id.sales_per_case_aed
          tempCartObj.product_id.sales_per_case_usd = cart.product_id.sales_per_case_usd
          tempCartObj.product_id.product_front_image = cart.product_id.product_front_image
          tempCartObj.product_id.product_back_image = cart.product_id.product_back_image
          tempCartObj.product_id.description = cart.product_id.description
          tempCartObj.product_id.size = cart.product_id.size
          if(cart.product_id.deal){
            tempCartObj.product_id.deal = cart.product_id.deal
          }else{
            tempCartObj.product_id.deal ={}
          }
   if(tempCartObj.product_id.deal && tempCartObj.product_id.deal.isActive){
       
          let date = moment().startOf('day').format("YYYY-MM-DD")
          let month = new Date(tempCartObj.product_id.deal.deal_end_date).getMonth()+1;
          let day;
        if(new Date(tempCartObj.product_id.deal.deal_end_date).getDate()<=9){
          day ='0'+new Date(tempCartObj.product_id.deal.deal_end_date).getDate()
        }else{
          day = new Date(tempCartObj.product_id.deal.deal_end_date).getDate()
        }
          let customDate = new Date(tempCartObj.product_id.deal.deal_end_date).getFullYear()+'-'+month+'-'+day;
          if(customDate >= date.toString()){
          tempobj.isActive = true;
          tempobj.sales_per_case_usd = tempCartObj.product_id.deal.offer_price_usd
          tempobj.sales_per_case_aed = tempCartObj.product_id.deal.offer_price_aed
          tempobj.deal_percentage = tempCartObj.product_id.deal.deal_percentage
          Object.assign(tempCartObj.product_id.deal ,tempobj)

             
          }else{
            tempobj.isActive = false;
            tempobj.sales_per_case_usd = null
            tempobj.sales_per_case_aed = null
            tempobj.deal_percentage = null
            Object.assign(tempCartObj.product_id.deal ,tempobj)

          }
          
        }else{
          tempobj.isActive = false;
          tempobj.sales_per_case_usd = null
          tempobj.sales_per_case_aed = null
          tempobj.deal_percentage = null

          Object.assign(tempCartObj.product_id.deal,tempobj)
        
        }                 
      return tempCartObj;
    } catch (error) {
      throw error;
    }
  },
 
  addProductInCart: async cart  => {
    try {
      let savedCart = await new CartModel(cart).save();
      return savedCart
    } catch (error) {
      throw error;cart
    }
  },
  updateProductInCart: async (data) => {
    try {
      const update = await CartModel.findOneAndUpdate({
        user_id: data.user_id,
        product_id:data.product_id
      }, data, {
        upsert: true,
        new: true
      });
      return update;
    } catch (error) {
      throw error;
    }
  },
  getAllCartProductByUser: async (id,limit,skip) => {
    try {
      if(limit== null){
        return await CartModel.find({user_id:id})
        .populate({path:'product_id',
                   populate:{path:'deal brand',
                             select:'deal_end_date offer_price_aed offer_price_usd brand_name'}
                                                }).count();
      }
    let count =   await CartModel.find({user_id:id})
                      .populate({path:'product_id',
                                 populate:{path:'deal brand',
                                           select:'deal_end_date offer_price_aed offer_price_usd brand_name'}
                                                              }).count();
       
       
     let  cart =   await CartModel.find({user_id:id})
                                .populate({path:'product_id',
                                 populate:{path:'deal brand',
                                select:'deal_end_date offer_price_aed offer_price_usd deal_percentage brand_name'}
                                              }).sort({
                                                 "createdAt": -1
                                              }).skip(skip).limit(limit).lean();
                                            
     let tempArray =[];
      cart.forEach(element => {
        let tempCartObj ={};
          tempCartObj._id = element._id;
          tempCartObj.user_id = element.user_id;
          tempCartObj.quantity = element.quantity
          tempCartObj.product_id ={};
          tempCartObj.product_id._id = element.product_id._id 
          tempCartObj.product_id.supplier_id= element.product_id.supplier_id
          tempCartObj.product_id.category_id = element.product_id.category_id
          tempCartObj.product_id.brand = element.product_id.brand
          tempCartObj.product_id.stock_location = element.product_id.stock_location
          tempCartObj.product_id.code = element.product_id.code
          tempCartObj.product_id.product_code = element.product_id.product_code
          tempCartObj.product_id.product_type = element.product_id.product_type
          tempCartObj.product_id.product_name = element.product_id.product_name
          tempCartObj.product_id.expiry_date = element.product_id.expiry_date
          tempCartObj.product_id.ru_per_case = element.product_id.ru_per_case
          tempCartObj.product_id.ean_code_ru = element.product_id.ean_code_ru
          tempCartObj.product_id.ean_code_case = element.product_id.ean_code_case
          tempCartObj.product_id.language = element.product_id.language
          tempCartObj.product_id.origin = element.product_id.origin
          tempCartObj.product_id.quantity = element.product_id.quantity
          tempCartObj.product_id.sales_per_case_aed = element.product_id.sales_per_case_aed
          tempCartObj.product_id.sales_per_case_usd = element.product_id.sales_per_case_usd
          tempCartObj.product_id.product_front_image = element.product_id.product_front_image
          tempCartObj.product_id.product_back_image = element.product_id.product_back_image
          tempCartObj.product_id.description = element.product_id.description
          tempCartObj.product_id.size = element.product_id.size
          if(cart&&cart.product_id&&cart.product_id.deal){
            tempCartObj.product_id.deal = element.product_id.deal
          }else{
            tempCartObj.product_id.deal ={}
          }
        
        let tempobj={};
        let date = moment().startOf('day').format("YYYY-MM-DD")
      if( element&&element.product_id&&element.product_id.deal&&element.product_id.deal){
        let month = new Date(element.product_id.deal.deal_end_date).getMonth()+1;
        let day;
        if(new Date(element.product_id.deal.deal_end_date).getDate()<=9){
          day ='0'+new Date(element.product_id.deal.deal_end_date).getDate()
        }else{
          day = new Date(element.product_id.deal.deal_end_date).getDate()
        }
        let customDate = new Date(element.product_id.deal.deal_end_date).getFullYear()+'-'+month+'-'+day;
        if(customDate >= date.toString()){
          tempobj.isActive = true;
          tempobj.sales_per_case_usd = element.product_id.deal.offer_price_usd
          tempobj.sales_per_case_aed = element.product_id.deal.offer_price_aed
          tempobj.deal_percentage = element.product_id.deal.deal_percentage
          Object.assign(tempCartObj.product_id.deal,tempobj)
           
        }else{
          tempobj.isActive = false;
          tempobj.sales_per_case_usd = null
          tempobj.sales_per_case_aed = null
          tempobj.deal_percentage = null
          Object.assign(tempCartObj.product_id.deal,tempobj)

          // element.product_id.deal = tempobj
        }
        
      }else{
        tempobj.isActive = false;
        tempobj.sales_per_case_usd = null
        tempobj.sales_per_case_aed = null
        tempobj.deal_percentage = null

        
        // element.product_id.deal = {}
        Object.assign(tempCartObj.product_id.deal,tempobj)
        // element.product_id.deal = tempobj
      }  
      tempArray.push(tempCartObj)      ;                                                     
    })
    let json ={}
    json.totalpage = Math.ceil(count / limit);
    json.totalcount = count;
    json.cart = tempArray
    return json;
    } catch (error) {
      throw error;
    }
  },
  removeProductFromCart: async (id) => {
    try {
      var data = await CartModel.remove({
        _id:id
      });;
      return data;
    } catch (error) {
      throw error;
    }
  }
};
export default CartService;