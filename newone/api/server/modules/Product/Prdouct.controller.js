import ProductService from "./Product.service";
import SequenceService from "../Sequence/Sequence.service";
import OrderService from "../Order/order.service";
import logger from "../../../config/logger.config";
var _ = require("lodash");
import settings from '../../../settings';
let config = require('./../../../config/' + settings.environment + '.config').default;
const MEDIA = config.media;
let mkdirp = require('mkdirp');
var uuidv1 = require('uuid/v1');
const fs = require('fs');
const path = require('path');
var pdf = require('html-pdf');
import moment from "moment";
const nodemailer = require("nodemailer");
import EmailService from "../CommonService/email.service";
import SettingsService from '../Settings/settings.service';
import OrderModel from "../Order/order.model";
var pdfCount = 0,
  excelCount = 0;
var imgSrc = path.normalize('file://' + __dirname + '../../../../logo/logo.png');


const ProductController = {
  getProductById: async (req, res) => {
    try {
      const Product = await ProductService.getProductById(req.params.id);
      logger.info(" Getting Product by id = " + req.params.id);
      res.send({
        code: 200,
        status: "success",
        message: "List the Product",
        data: Product
      });
    } catch (error) {
      logger.error("Error in getting Product :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting Product",
        data: {}
      });
    }
  },
  getProductByPagination: async (req, res) => {
    try {
      var search;
      if (_.isEmpty(req.query.searchkey)) {
        search = '';
      } else {
        search = req.query.searchkey
      }
      let limit = Number(req.query.limit) || 10;
      let offset = (req.query.page) ? parseInt(req.query.page) : 1
      let skip = (offset - 1) * limit;

      const Product = await ProductService.getProductByPagination(search, limit, skip, "","");

      logger.info(" Getting Product");
      res.send({
        code: 200,
        status: "success",
        message: "List the Products",
        data: Product
      });
    } catch (error) {
      logger.error("Error in getting Product :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting  Product",
        data: {}
      });
    }
  },

  createProduct: async (req, res) => {
    try {
      var bodyData = JSON.parse(JSON.stringify(req.body));
      var data = bodyData.bodyData ? JSON.parse(bodyData.bodyData) : req.body;
      var supplierId = req.params.supplier_id;
      var imageError = false;
      var product = {}
      let frontImageUrl = "";
      let backImageUrl = "";
      var fieldValidation = await ProductController.productFieldValidation(data);
      var errorMessage = fieldValidation.message;
      var requiredFail = fieldValidation.error;
      if (requiredFail) {
        return res.status(400).send({
          code: 400,
          status: "error",
          message: errorMessage,
          data: {}
        });
      }

      if (req.files && req.files.product_front_image) {
        let imageFile = req.files.product_front_image;
        if (imageFile) {
          let extension = imageFile.mimetype.split("/")[1];

          let filePath = MEDIA.local_file_path + "product_image" + "/";
          let file_name = uuidv1() + "-" + Date.now() + '.' + extension;
          frontImageUrl = "/static/product_image" + "/" + file_name;
          if (!MEDIA.useS3) {
            await moveFile(imageFile, filePath, file_name)
              .catch((error) => {
                imageError = true;
                return badResponse(res, "Image Upload Failed");
              });
          }
        }
      }
      // else {
      //   return res.status(400).send({
      //     code: 400,
      //     status: "error",
      //     message: 'Product Front Image is required',
      //     data: {}
      //   });
      // }
      if (req.files && req.files.product_back_image) {
        let imageFile = req.files.product_back_image;
        if (imageFile) {
          let extension = imageFile.mimetype.split("/")[1];
          let filePath = MEDIA.local_file_path + "product_image" + "/";
          let file_name = uuidv1() + "-" + Date.now() + '.' + extension;
          backImageUrl = "/static/product_image" + "/" + file_name;
          if (!MEDIA.useS3) {
            await moveFile(imageFile, filePath, file_name)
              .catch((error) => {
                imageError = true;
                return badResponse(res, "Image Upload Failed");
              });
          }
        }
      }
      // else {
      //   return res.status(400).send({
      //     code: 400,
      //     status: "error",
      //     message: 'Product Back Image is required',
      //     data: {}
      //   });
      // }
      if (!imageError) {
        var lastProductcode = await SequenceService.getSequenceBykey('product');
        var productCode = lastProductcode + 1;
        product.supplier_id = supplierId || "";
        product.category_id = data.category_id || " ";
        product.brand = data.brand_id || " ";
        // product.brand_name = data.brand_name.trim() || "";
        product.stock_location = data.stock_location || " ";
        product.fmcg_availability = data.fmcg_availability || false;
        product.code = productCode
        product.product_code = await digitProductCode(productCode);
        product.product_type = data.product_type || "";
        product.product_name = data.product_name || "";
        product.expiry_date = data.expiry_date || "";
        product.ru_per_case = data.ru_per_case || "";
        product.ean_code_ru = data.ean_code_ru || "";
        product.ean_code_case = data.ean_code_case || "";
        product.language = data.language || "";
        product.origin = data.origin || "";
        product.quantity = data.quantity || "";
        product.sales_per_case_aed = data.sales_per_case_aed || "";
        product.sales_per_case_usd = data.sales_per_case_usd || "";
        product.product_front_image = frontImageUrl
        product.product_back_image = backImageUrl
        product.cost_price_per_case = data.cost_price_per_case || "";
        product.description = data.description || "";
        product.size = data.size || "";


        var savedProduct = await ProductService.createProduct(product);
        savedProduct.expiry_date = savedProduct.expiry_date ? savedProduct.expiry_date : '';
        await SequenceService.updateSequenceBykey('product', productCode)
        if (savedProduct.product_front_image && savedProduct.product_front_image !== "") {
          savedProduct.product_front_image = config.api_end_point + savedProduct.product_front_image
        } else {
          savedProduct.product_front_image = ""
        }
        if (savedProduct.product_back_image && savedProduct.product_back_image !== "") {
          savedProduct.product_back_image = config.api_end_point + savedProduct.product_back_image
        } else {
          savedProduct.product_back_image = ""
        }

        logger.info("New Product is created");
        return res.send({
          code: 200,
          status: "success",
          message: "New Product is created",
          data: savedProduct
        });
      }
    } catch (error) {
      logger.error("Error in Creating Product :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in Creating Product",
        data: []
      });
    }
  },
  updateProduct: async (req, res) => {
    try {
      var bodyData = JSON.parse(JSON.stringify(req.body));
      var data = bodyData.bodyData ? JSON.parse(bodyData.bodyData) : req.body;
      var id = req.params.product_id;
      var imageError = false;
      var product = {}
      let frontImageUrl = "";
      let backImageUrl = "";
      var fieldValidation = await ProductController.productFieldValidation(data)
      var errorMessage = fieldValidation.message;
      var requiredFail = fieldValidation.error;
      // if (!data.name || data.name === "") {
      //   requiredFail = true;
      //   errorMessage = 'Name is required'
      // }

      if (requiredFail) {
        return res.status(400).send({
          code: 400,
          status: "error",
          message: errorMessage,
          data: {}
        });
      }



      if (req.files && req.files.product_front_image) {
        let imageFile = req.files.product_front_image;
        if (imageFile) {
          let extension = imageFile.mimetype.split("/")[1];

          let filePath = MEDIA.local_file_path + "product_image" + "/";
          let file_name = uuidv1() + "-" + Date.now() + '.' + extension;
          frontImageUrl = "/static/product_image" + "/" + file_name;
          if (!MEDIA.useS3) {
            await moveFile(imageFile, filePath, file_name)
              .catch((error) => {
                console.log(error)
                return badResponse(res, "Image Upload Failed");
                imageError = true;
              });
          }
        }
      }
      if (req.files && req.files.product_back_image) {
        let imageFile = req.files.product_back_image;
        if (imageFile) {
          let extension = imageFile.mimetype.split("/")[1];

          let filePath = MEDIA.local_file_path + "product_image" + "/";
          let file_name = uuidv1() + "-" + Date.now() + '.' + extension;
          backImageUrl = "/static/product_image" + "/" + file_name;
          if (!MEDIA.useS3) {
            await moveFile(imageFile, filePath, file_name)
              .catch((error) => {
                console.log(error)
                return badResponse(res, "Image Upload Failed");
                imageError = true;
              });
          }
        }
      }
      if (!imageError) {
        //product.supplier_id = data.supplier_id || "";
        product.category_id = data.category_id || ""
        //var getproduct = await ProductService.getProduct(id);
        product.brand = data.brand_id || " ";
        // product.brand_name = data.brand_name.trim() || "";
        //product.product_code = productCode
        product.product_type = data.product_type || "";
        product.stock_location = data.stock_location || " ";
        product.fmcg_availability = data.fmcg_availability || false;
        //product.product_name = data.product_name || "";
        product.expiry_date = data.expiry_date || "";
        product.ru_per_case = data.ru_per_case || "";
        product.ean_code_ru = data.ean_code_ru || "";
        product.ean_code_case = data.ean_code_case || "";
        product.language = data.language || "";
        product.origin = data.origin || "";
        product.quantity = data.quantity || "";
        product.sales_per_case_aed = data.sales_per_case_aed || "";
        product.sales_per_case_usd = data.sales_per_case_usd || "";
        // product.product_front_image = frontImageUrl
        // product.product_back_image = backImageUrl
        product.cost_price_per_case = data.cost_price_per_case || "";
        product.description = data.description || "";
        product.size = data.size || "";

        if (JSON.parse(data.delete_card_1)) {
          console.log('delete business_card_image_1')
          product.product_front_image = ""
        } else if (frontImageUrl !== "") {
          product.product_front_image = frontImageUrl || ""
        }
        if (JSON.parse(data.delete_card_2)) {
          console.log('delete business_card_image_2')
          product.product_back_image = "";
        } else if (backImageUrl !== "") {
          product.product_back_image = backImageUrl || ""
        }

        // if (frontImageUrl !== "") {
        //   product.product_front_image = frontImageUrl || ""
        // }
        // if (backImageUrl !== "") {
        //   product.product_back_image = backImageUrl || ""
        // }

        var savedProduct = await ProductService.updateProduct(id, product);
        savedProduct.expiry_date = savedProduct.expiry_date ? savedProduct.expiry_date : '';
        if (savedProduct.product_front_image && savedProduct.product_front_image !== "") {
          savedProduct.product_front_image = config.api_end_point + savedProduct.product_front_image
        } else {
          savedProduct.product_front_image = ""
        }
        if (savedProduct.product_back_image && savedProduct.product_back_image !== "") {
          savedProduct.product_back_image = config.api_end_point + savedProduct.product_back_image
        } else {
          savedProduct.product_back_image = ""
        }
        logger.info("Product is updated");
        return res.send({
          code: 200,
          status: "success",
          message: "Product is updated",
          data: savedProduct
        });
      }
    } catch (error) {
      logger.error("Error in Update Product :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in Update Product",
        data: {}
      });
    }
  },
  productFieldValidation: async (data) => {

    // if (!data.product_name || data.product_name === '') {
    //   return {
    //     error: true,
    //     message: "Product Name is required"
    //   }
    // }
    // if (!data.product_type || data.product_type === '') {
    //   return {
    //     error: true,
    //     message: "Product Type is required"
    //   }
    // }
    if (!data.brand_id || data.brand_id.trim() === '') {
      return {
        error: true,
        message: "Brand id is required"
      }
    }
    // if (!data.expiry_date || data.expiry_date === '') {
    //   return {
    //     error: true,
    //     message: "Expiry is required"
    //   }
    // }
    // if (!data.ru_per_case || data.ru_per_case === '') {
    //   return {
    //     error: true,
    //     message: "Ru per Case is required"
    //   }
    // }
    // if (!data.ean_code_ru || data.ean_code_ru === '') {
    //   return {
    //     error: true,
    //     message: "EAN Code RU is required"
    //   }
    // }
    // if (!data.ean_code_case || data.ean_code_case === '') {
    //   return {
    //     error: true,
    //     message: "EAN Code Case is required"
    //   }
    // }
    // if (!data.language || data.language === '') {
    //   return {
    //     error: true,
    //     message: "Language is required"
    //   }
    // }
    // if (!data.origin || data.origin === '') {
    //   return {
    //     error: true,
    //     message: "Origin is required"
    //   }
    // }
    // if (!data.quantity || data.quantity === '') {
    //   return {
    //     error: true,
    //     message: "Quantity is required"
    //   }
    // }
    // if (!data.sales_per_case_aed || data.sales_per_case_aed === '') {
    //   return {
    //     error: true,
    //     message: "Sales Per Case AED is required"
    //   }
    // }
    // if (!data.sales_per_case_usd || data.sales_per_case_usd === '') {
    //   return {
    //     error: true,
    //     message: "Sales Per Case USD is required"
    //   }
    // }
    // if (!data.cost_price_per_case || data.cost_price_per_case === '') {
    //   return {
    //     error: true,
    //     message: "Cost Price per Case is required"
    //   }
    // }
    // if (!data.description || data.description === '') {
    //   return {
    //     error: true,
    //     message: "Description is required"
    //   }
    // }
    // if (!data.size || data.size === '') {
    //   return {
    //     error: true,
    //     message: "Size is required"
    //   }
    // }
    return {
      error: false,
      message: ""
    }
  },
  getBrands: async (req, res) => {
    try {
      var brands = _.sortBy(await ProductService.getBrands());
      return res.send({
        code: 200,
        status: "success",
        message: "List the brand",
        data: brands
      });
    } catch (error) {
      logger.error("Error in getting brands :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting brands",
        data: []
      });
    }
  },
  getProductByBrand: async (req, res) => {
    try {
      
      var brands = [],
        isAll = true
      if (req.query.brand === 'all') {
        isAll = true
      } else {
        isAll = false
        var strBrandName = req.query.brand.toLowerCase();
        brands = strBrandName.split(",");
      }
      var suppliers = [],
         isSupplier = true
      if (req.query.supplier === 'all') {
        isSupplier = true
      } else {
        isSupplier = false
        var strSupplierName = req.query.supplier.toLowerCase();
        suppliers = strSupplierName.split(",");
      }
      let limit = Number(req.query.limit) || 10;
      let offset = (req.query.page) ? parseInt(req.query.page) : 1
      let skip = (offset - 1) * limit;
      let searchkey = req.query.searchkey || ''
      var products = await ProductService.getProductByBrand(searchkey, isAll, brands, isSupplier,suppliers,limit, skip)
      return res.send({
        code: 200,
        status: "success",
        message: "List the Product",
        data: products
      });
    } catch (error) {
      logger.error("Error in getting Product :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting Product",
        data: []
      });
    }
  },
  getProduct: async (req,res) =>{
    try {
      
      var products = await ProductService.getProducts();
      return res.send({
        code: 200,
        status: "success",
        message: "List the Product",
        data: products
      });
    } catch (error) {
      logger.error("Error in getting Product :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting Product",
        data: []
      });
    }
  },
  getProductByBrandFMCG: async (req, res) => {
    try {
      var brands = [],
        isAll = true,isLocation = true,locations =[];
      if (req.query.brand === 'all') {
        isAll = true
      } else {
        isAll = false
        var strBrandName = req.query.brand.toLowerCase();
        brands = strBrandName.split(",");
      }
      if(req.query.location === 'all'){
        isLocation = true;
      }else{
        isLocation = false;
        let strLocationName = req.query.location.toLowerCase();
        locations = strLocationName.split(',');
      }
      let categorys = [],
      isCategory = true
    if(req.query.category){
   if (req.query.category === 'all') {
    isCategory = true
   } else {
    isCategory = false
     let strCategoryName = req.query.category.toLowerCase();
     categorys = strCategoryName.split(",");
   }
  }
      let limit = Number(req.query.limit) || 10;
      let offset = (req.query.page) ? parseInt(req.query.page) : 1
      let skip = (offset - 1) * limit;
      let searchkey = req.query.searchkey || ''
      var products = await ProductService.getProductByBrandFMCG(searchkey, isAll, brands, limit, skip,isLocation,locations,isCategory,categorys)
       res.send({
        code: 200,
        status: "success",
        message: "List the Product",
        data: products
      });
    } catch (error) {
      logger.error("Error in getting Product :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting Product",
        data: []
      });
    }
  },
  getBrandImage: async (req,res) =>{
    try {
      var search;
      if (_.isEmpty(req.query.searchkey)) {
        search = '';
      } else {
        search = req.query.searchkey
      }
      var ids = (req.params.category_id).split(",");
      const brandImg = await ProductService.getBrandImage(ids,search);
      logger.info(" Getting Brand Images");
      res.send({
        code: 200,
        status: "success",
        message: "List the Brand Images",
        data: brandImg
      });
    } catch (error) {
      logger.error("Error in getting Brand Images :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting Brand Images",
        data: {}
      });
}
  },
  // downloadProductAsExcel: async (req, res) => {
  //   try {
  //     var products = await ProductService.getAllProduct();
  //     var isFirst = true;
  //     var csvDataString = "Supplier Name,Product Name,Product Code,Brand Name,Category Name,Product Type,Description,Size,Language,Ru / Case,Quantity,COST price per case ,sale price per case(AED) ,sale price per case(USD) ,Origin,EAN Code (RU),EAN Code (Case)\r\n";
  //     if (products.length > 0) {
  //       var stringId = "";
  //       products.map((field) => {
  //         //console.log(field)
  //         console.log((Number(field.sales_per_case_aed)).toFixed(2))
  //         var supplierName = "";
  //         //if (!(stringId === String(field._id))) {
  //         supplierName = field.name
  //         //}
  //         csvDataString += supplierName + "," + field.product_name + "," + field.product_code + "," + field.brand.brand_name + "," + field.category_id.category_name + "," + field.product_type + "," + field.description + "," + field.size + "," + field.language + "," + field.ru_per_case + "," + field.quantity + "," + field.cost_price_per_case + "," + (Number(field.sales_per_case_aed)).toFixed(2) + " ," + field.sales_per_case_usd + "," + field.origin + "," + field.ean_code_ru + "," + field.ean_code_case + ",\r\n";
  //         stringId = String(field._id)
  //       });
  //     }
  //     excelCount++;
  //     const fileName = "product_excel(" + excelCount + ").xlsx";
  //     //const fileName = uuidv1() + '-' + Date.now() + ".csv";
  //     const filePath = path.join(__dirname + "/excel", fileName)
  //     fs.writeFile(filePath, csvDataString, function (err) {
  //       if (err) {
  //         console.log(err)
  //         return res.status(400).send({
  //           code: 400,
  //           status: "error",
  //           message: "Error in Creating Excel file for Product",
  //           data: {}
  //         });
  //       } else {
  //         setTimeout(function () {
  //           if (fs.existsSync(filePath)) {
  //             fs.unlinkSync(filePath);
  //           }
  //         }, 300000)
  //         return res.send({
  //           code: 200,
  //           status: "success",
  //           message: "Creating Excel file for Product",
  //           data: {
  //             path: config.api_end_point + '/api/product/report/excel/' + fileName,
  //             extension: "xlsx"
  //           }
  //         });
  //       }
  //     });
  //   } catch (error) {
  //     console.log(error)
  //     logger.error("Error in getting Product :" + error);
  //     res.status(400).send({
  //       code: 400,
  //       status: "error",
  //       message: "Error in getting Product",
  //       data: {}
  //     });
  //   }

  // },
  excelFileDownload: async (req, res) => {
    try {
      var fileName = req.params.file_name
      const filePath = path.join(__dirname + "/excel", fileName)
      res.download(filePath)
      //res.download("./server/modules/product/excel/"+fileName);
    } catch (error) {
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error occured",
        data: {}
      });
    }
  },
  pdfFileDownload: async (req, res) => {
    try {
      var fileName = req.params.file_name
      const filePath = path.join(__dirname + "/pdf", fileName)
      res.download(filePath)
      //res.download("./server/modules/product/excel/"+fileName);
    } catch (error) {
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error occured",
        data: {}
      });
    }
  },
  pdfEmailSend: async (req, res) => {
    try {
      var toMail = req.params.email;
      let setting = await SettingsService.getAllsettings();
      let fileShareHtml = await fs.readFileSync(
        path.join(__dirname, "../../EmailTemplates/file_share.html"),
        "utf-8"
      );
      var imagepath = config.api_end_point + '/image/logo/';

      fileShareHtml = fileShareHtml.replace(
        "FILE_TYPE", "pdf"
      ).replace('{unilever}', imagepath + 'unilever.png')
        .replace('{P-and-G}', imagepath + 'pandg.png')
        .replace('{renkitt}', imagepath + 'renkitt.jpg')
        .replace('{beiersdorf}', imagepath + 'beiersdorf.png')
        .replace('{johnson}', imagepath + 'jhonson.png')
        .replace('{logo}', imagepath + 'maxbrands.png')
        .replace('{phone}', imagepath + 'phone.png')
        .replace('{whatsapp}', imagepath + 'whatsapp.png')
        .replace('{mail}', imagepath + 'email.png')

      res.send({
        code: 200,
        status: "success",
        message: "Creating PDF file for Product",
        data: {}
      });

      var html = fs.readFileSync('./server/EmailTemplates/product_template_pdf.html', 'utf8');
      var options = {
        "format": "A4",
        "orientation": "portrait",
        'Content_Type': 'application/pdf',
        "border": {
          "top": "0.2in",
          "right": "0.2in",
          "bottom": "0.2in",
          "left": "0.2in"
        },
        "header": {
          "height": "5mm",
          "contents": ""
        },
        "footer": {
          "height": "16mm",
          "contents": {
            default: '<hr style="border-color:rgb(255, 0, 0);" size="1"><div style="font-size: 7px;color: #949393;">Maxbrands Marketing</div><div style="font-size: 7px;color: #949393;">Hotline M: 0097154 461 4848</div><div style="font-size: 7px;color: #949393;">PO Box 239492, Dubai, UAE. | T: +971 4250 1134 | E:info@maxbrandsintl.com WEB <a style="font-size: 7px;color: #949393;" href="http://www.maxbrandsintl.com">www.maxbrandsintl.com</a></div>'
          }
        },
        "timeout": "1200000"
      };
      var strProductList = "",
        productList;
      if (req.query.brand) {
        var brands = [],
          isAll = true
        if (req.query.brand === 'all') {
          isAll = true
        } else {
          isAll = false
          var strBrandName = req.query.brand.toLowerCase();
          brands = strBrandName.split(",");
        }
        productList = await ProductService.getAllProductForPDF(isAll, brands);
      } else {
        var ids = (req.query.product_id).split(",");
        productList = await ProductService.getProductByIds(ids);
      }

      var count = 0;
      var tableCount = -2;
      var defaultBreak = 4;
      var prodcutCode = 0;
      productList.forEach(product => {
        count++;
        prodcutCode++;
        var length = Math.ceil(product.description.length / 30) === 0 ? 1 : Math.ceil(product.description.length / 30);
        tableCount += (length - 1)
        // console.log((product.product_front_image ? (product.product_front_image.replace("/static/",MEDIA.local_file_path)) : ""))
        // console.log(product.product_front_image ? config.api_end_point + product.product_front_image : "")
        // var imagepath = (product.product_front_image ?  path.normalize('file://' + (product.product_front_image.replace("/static/",MEDIA.local_file_path))): "") ;
        // console.log(product.product_back_image ? config.api_end_point + product.product_back_image : "")
        strProductList += "<div style='margin-left:20px;'><table cellSpacing='0' cellPadding='0' border='0'>" +
          "<tr style='height:15px;'><td class='vAlignTop' style='width:15%;" +
          "'>PRODUCT CODE</td><td class='vAlignTop'>:" + product.product_code + "</td><td style='width:50%;' rowspan='10'><div style='text-align:center;'><div class='imagetag'><img src='" + (product.product_front_image ? path.normalize('file://' + (product.product_front_image.replace("/static/", MEDIA.local_file_path))) : "") + "' class='ImgProduct'></div>&nbsp;&nbsp;&nbsp<div class='imagetag'><img src='" + (product.product_back_image ? path.normalize('file://' + (product.product_back_image.replace("/static/", MEDIA.local_file_path))) : "") + "' class='ImgProduct'></div></div></td></tr>" +
          "<tr><td class='vAlignTop'> BRAND NAME</td><td class='vAlignTop'>:" + alterWord(product.brand?product.brand.brand_name:product.brand_name) + "</td></tr>" +
          "<tr><td class='vAlignTop'> CATEGORY</td><td class='vAlignTop'>:" +alterWord(product.category_name?product.category_name:product.category_id.category_name) +"</td></tr>" +
          "<tr><td class='vAlignTop'> PRODUCT TYPE</td><td class='vAlignTop'>:" + alterWord(product.product_type) + "</td></tr>" +
          "<tr><td class='vAlignTop'>DESCRIPTION</td><td class='vAlignTop'>:" + alterWord(product.description) + "</td></tr>" +
          "<tr><td class='vAlignTop'> EXPIRY DATE</td><td class='vAlignTop'>:" + (product.expiry_date ? moment(product.expiry_date).format("DD-MM-YYYY") : 'NA') + "</td></tr>" +
          "<tr><td class='vAlignTop'> SIZE</td><td class='vAlignTop'>:" +alterWord(product.size) +"</td></tr>" +
          "<tr><td class='vAlignTop'> RU PER CASE</td><td class='vAlignTop'>:" + alterWord(product.ru_per_case) + "</td></tr>" +
          "<tr><td class='vAlignTop'> EAN CODE</td><td class='vAlignTop'>:" + alterWord(product.ean_code_ru) + "</td></tr>" +
          "<tr><td class='vAlignTop'> LANGUAGE</td><td class='vAlignTop'>:" + product.language + "</td></tr>" +
          "<tr><td class='vAlignTop'> QUANTITY</td><td class='vAlignTop'>:" + product.quantity + "</td></tr>" +
          "<tr><td class='vAlignTop'> PRICE PER CASE</td><td class='vAlignTop'>: " + (Number(product.sales_per_case_usd)).toFixed(2) + "$ &nbsp;" + Number(product.sales_per_case_aed).toFixed(2) + "&nbsp;AED</td></tr>" +
          "</table></div>"
          //if (first) {
          console.log(' count ' + count + tableCount);
          var pagebreak = (tableCount < 0) ? defaultBreak : (defaultBreak - Math.ceil(tableCount / 15));
          console.log("pagebreak" +pagebreak);
          //pagebreak =5;
          if ((count >=pagebreak) && (prodcutCode < productList.length)) {
            count = 0;
            tableCount = -2;
            defaultBreak = 4;
            console.log("additional br first" + count)
            strProductList += "<div style='page-break-before: always;break-before: always;'></div><br><br><br>";
          }else{
            strProductList +='<br>'
          }
        // } else {
        //   var s_count = count - 4;
        //   if (s_count % 4 === 0 && count !== productList.length) {
        //     console.log("additional br second"+count)
        //     strProductList += "<div style='page-break-before: always;break-before: always;'></div><br><br><br><br>"
        //   }
        // }
        // if (count % 4 === 0 && count !== productList.length) {
        //   console.log("additional br")
        //   strProductList += "<br><br><br><br><br><br>"
        // }
      });
      pdfCount++;
      const fileName = "product_pdf(" + pdfCount + ").pdf";
      //const fileName = uuidv1() + '-' + Date.now() + ".pdf";
      const filePath = path.join(__dirname + "/pdf", fileName)
      strProductList = strProductList.replace(/{logo}/g, config.api_end_point + '/image/logo/logo.png');
      var pdftemplate = html.replace(/{logo}/g, config.api_end_point + '/image/logo/logo.png').replace(/{productlist}/g, strProductList);
      pdftemplate += "<div id='pageHeader'><img style='width:200px;height:40px'src='" + imgSrc + "' /> <hr style='border-color: rgb(255, 0, 0);' size='1'></div>";
      pdf.create(pdftemplate, options).toFile(filePath, function (err, pdfPath) {
        if (err) {
          return res.status(400).send({
            code: 400,
            status: "error",
            message: "Error in getting Product",
            data: {}
          });
        } else {

          var attachments = [{
            // file on disk as an attachment
            filename: 'product.pdf',
            path: filePath // stream this file
          }]
          EmailService.sendEmail(
            toMail,
            "Our Offers",
            fileShareHtml, setting, attachments
          );
          setTimeout(function () {
            if (fs.existsSync(filePath)) {
              fs.unlinkSync(filePath);
            }
          }, 300000)
        }
      });
    } catch (error) {
      return res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting Product",
        data: {}
      });
    }
  },
   createPDF: async (req, res) => {
    try {
      var html = fs.readFileSync('./server/EmailTemplates/product_template_pdf.html', 'utf8');
            var options = {
        "format": "A4",
        "orientation": "portrait",
        'Content_Type': 'application/pdf',
        "border": {
          "top": "0.2in",
          "right": "0.2in",
          "bottom": "0.2in",
          "left": "0.2in"
        },
        "header": {
          "height": "5mm",
          "contents": "<img style='width:200px;height:40px'src='" + imgSrc + "' /> <hr style='border-color: rgb(255, 0, 0);' size='1'>"
        },
        "footer": {
          "height": "16mm",
          "contents": {
            default: '<hr style="border-color:rgb(255, 0, 0);" size="1"><div style="font-size: 7px;color: #949393;">Maxbrands Marketing</div><div style="font-size: 7px;color: #949393;">Hotline M: 0097154 461 4848</div><div style="font-size: 7px;color: #949393;">PO Box 239492, Dubai, UAE. | T: +971 4250 1134 | E:info@maxbrandsintl.com WEB <a style="font-size: 7px;color: #949393;" href="http://www.maxbrandsintl.com">www.maxbrandsintl.com</a></div>'
          }
        },
        "timeout": "1200000"
      };
      var strProductList = "",
        productList;
      if (req.query.brand) {
        var brands = [],
          isAll = true
        if (req.query.brand === 'all') {
          isAll = true
        } else {
          isAll = false
          var strBrandName = req.query.brand.toLowerCase();
          brands = strBrandName.split(",");
        }
        productList = await ProductService.getAllProductForPDF(isAll, brands);
      } else {
        var ids = (req.query.product_id).split(",");
        productList = await ProductService.getProductByIds(ids);
      }
      
      var count = 0;
      var tableCount = -2;
      var defaultBreak = 4;
      var prodcutCode = 0;

      productList.forEach(product => {
        count++;
        prodcutCode++;
        var length = Math.ceil(product.description.length / 30) === 0 ? 1 : Math.ceil(product.description.length / 30);
        tableCount += (length - 1)
        strProductList += "<div style='margin-left:20px;'><table cellSpacing='0' cellPadding='0' border='0'>" +
        "<tr style='height:15px;'><td class='vAlignTop' style='width:15%;" +
        "'>PRODUCT CODE</td><td class='vAlignTop'>:" + product.product_code + "</td><td style='width:50%;' rowspan='10'><div style='text-align:center;'><div class='imagetag'><img src='" + (product.product_front_image ? path.normalize('file://' + (product.product_front_image.replace("/static/", MEDIA.local_file_path))) : "") + "' class='ImgProduct'></div>&nbsp;&nbsp;&nbsp<div class='imagetag'><img src='" + (product.product_back_image ? path.normalize('file://' + (product.product_back_image.replace("/static/", MEDIA.local_file_path))) : "") + "' class='ImgProduct'></div></div></td></tr>" +
        "<tr><td class='vAlignTop'> BRAND NAME</td><td class='vAlignTop'>:" + alterWord(product.brand?product.brand.brand_name:product.brand_name) + "</td></tr>" +
        "<tr><td class='vAlignTop'> CATEGORY</td><td class='vAlignTop'>:" +alterWord(product.category_name?product.category_name:product.category_id.category_name) +"</td></tr>" +
        "<tr><td class='vAlignTop'> PRODUCT TYPE</td><td class='vAlignTop'>:" + alterWord(product.product_type) + "</td></tr>" +
        "<tr><td class='vAlignTop'>DESCRIPTION</td><td class='vAlignTop'>:" +alterWord(product.description) + "</td></tr>" +
        "<tr><td class='vAlignTop'> EXPIRY DATE</td><td class='vAlignTop'>:" + (product.expiry_date ? moment(product.expiry_date).format("DD-MM-YYYY") : 'NA') + "</td></tr>" +
        "<tr><td class='vAlignTop'> SIZE</td><td class='vAlignTop'>:" +alterWord(product.size) +"</td></tr>" +
        "<tr><td class='vAlignTop'> RU PER CASE</td><td class='vAlignTop'>:" + alterWord(product.ru_per_case) + "</td></tr>" +
        "<tr><td class='vAlignTop'> EAN CODE</td><td class='vAlignTop'>:" +alterWord(product.ean_code_ru) + "</td></tr>" +
        "<tr><td class='vAlignTop'> LANGUAGE</td><td class='vAlignTop'>:" + product.language + "</td></tr>" +
        "<tr><td class='vAlignTop'> QUANTITY</td><td class='vAlignTop'>:" + product.quantity + "</td></tr>" +
        "<tr><td class='vAlignTop'> PRICE PER CASE</td><td class='vAlignTop'>: " + (Number(product.sales_per_case_usd)).toFixed(2) + "$ &nbsp;" + Number(product.sales_per_case_aed).toFixed(2) + "&nbsp;AED</td></tr>" +
        "</table></div>"
        console.log(' count ' + count + tableCount);
        var pagebreak = (tableCount < 0) ? defaultBreak : (defaultBreak - Math.ceil(tableCount / 15));
        console.log("pagebreak" +pagebreak);
        if ((count >=pagebreak) && (prodcutCode < productList.length)) {
          count = 0;
          tableCount = -2;
          defaultBreak = 4;
          console.log("additional br first" + count)
          strProductList += "<div style='page-break-before: always;break-before: always;'></div><br><br><br>";
        }else{
          strProductList +='<br>'
        }
      });
      pdfCount++;
      const fileName = "product_pdf(" + pdfCount + ").pdf";
      const filePath = path.join(__dirname + "/pdf", fileName)
      strProductList = strProductList.replace(/{logo}/g, config.api_end_point + '/image/logo/logo.png');
      var pdftemplate = html.replace(/{logo}/g, config.api_end_point + '/image/logo/logo.png').replace(/{productlist}/g, strProductList);
      pdftemplate += "<div id='pageHeader'><img style='width:200px;height:40px'src='" + imgSrc + "' /> <hr style='border-color: rgb(255, 0, 0);' size='1'></div>";
      pdf.create(pdftemplate, options).toFile(filePath, function (err, pdfPath) {
        if (err) {
          return res.status(400).send({
            code: 400,
            status: "error",
            message: "Error in getting Product",
            data: {}
          });
        } else {
          setTimeout(function () {
            if (fs.existsSync(filePath)) {
              fs.unlinkSync(filePath);
            }
          }, 300000)
          return res.send({
            code: 200,
            status: "success",
            message: "Creating PDF file for Product",
            data: {
              path: config.api_end_point + '/api/product/report/pdf/' + fileName,
              extension: "pdf"
            }
          });
        }
      });
    } catch (error) {
      return res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting Product",
        data: {}
      });
    }
  },
  excelEmailSend: async (req, res) => {
    try {
      var toMail = req.params.email;
      let setting = await SettingsService.getAllsettings();
      let fileShareHtml = await fs.readFileSync(
        path.join(__dirname, "../../EmailTemplates/file_share.html"),
        "utf-8"
      );
      var imagepath = config.api_end_point + '/image/logo/';

      fileShareHtml = fileShareHtml.replace(
        "FILE_TYPE", "excel"
      ).replace('{unilever}', imagepath + 'unilever.png')
      .replace('{P-and-G}', imagepath + 'pandg.png')
      .replace('{renkitt}', imagepath + 'renkitt.jpg')
      .replace('{beiersdorf}', imagepath + 'beiersdorf.png')
      .replace('{johnson}', imagepath + 'jhonson.png')
      .replace('{logo}', imagepath + 'maxbrands.png')
      .replace('{phone}', imagepath + 'phone.png')
      .replace('{whatsapp}', imagepath + 'whatsapp.png')
      .replace('{mail}', imagepath + 'email.png')
      res.send({
        code: 200,
        status: "success",
        message: "Creating Excel file for Product",
        data: {}
      });
      var products = "",
        productList;
      if (req.query.brand) {
        var brands = [];
        if (req.query.brand === 'all') {
          productList = await ProductService.getAllProduct();
        } else {
          var strBrandName = req.query.brand.toLowerCase();
          brands = strBrandName.split(",");
          productList = await ProductService.getAllProductByBrandForExcel(brands);
        }
      } else {
        var ids = (req.query.product_id).split(",");
        productList = await ProductService.getAllProductByIdForExcel(ids);
      }

      var Excel = require('exceljs');
      var workbook = new Excel.Workbook();
      var worksheet = workbook.addWorksheet('My Sheet');
      worksheet.columns = [{
          header: 'Name',
          key: 'name',
          width: 20
        },

        {
          header: 'product_code',
          key: 'product_code',
          width: 15
        },
        {
          header: 'Brand Name',
          key: 'brand_name',
          width: 20
        },
        {
          header: 'Category Name',
          key: 'category_name',
          width: 20
        },
        {
          header: 'Product Type',
          key: 'product_type',
          width: 15
        },
        {
          header: 'Description',
          key: 'description',
          width: 40
        },
        {
          header: 'Size',
          key: 'size',
          width: 10
        },
        {
          header: 'Language',
          key: 'language',
          width: 10
        },
        {
          header: 'Ru / case',
          key: 'ru_per_case',
          width: 15
        },
        {
          header: 'Quantity',
          key: 'quantity',
          width: 10
        },
        {
          header: 'Expiry Date',
          key: 'expiry_date',
          width: 15
        },
        {
          header: 'Cost Price Per Case',
          key: 'cost_price_per_case',
          width: 20
        },
        {
          header: 'Sales Per Case AED',
          key: 'sales_per_case_aed',
          width: 20
        },
        {
          header: 'Sales Per Case USD',
          key: 'sales_per_case_usd',
          width: 20
        },
        {
          header: 'Origin',
          key: 'origin',
          width: 15
        },
        {
          header: 'Ean Code Ru',
          key: 'ean_code_ru',
          width: 30
        },
        {
          header: 'Ean Code Case',
          key: 'ean_code_case',
          width: 30
        },
        {
          header: 'Front Image Url',
          key: 'product_front_image',
          width: 80
        },
        {
          header: 'Back Image Url',
          key: 'product_back_image',
          width: 80
        },
      ];
      var row = 2
      productList.map((field) => {
        // field.product_code = ((field.product_name.replace(/\s/g, '') + "prod").substring(0, 4) + field.product_code)
        field.expiry_date = field.expiry_date?moment(field.expiry_date).format("DD-MM-YYYY"):'NA';

        console.log(field.product_back_image)
        field.cost_price_per_case = Number(field.cost_price_per_case)
        field.sales_per_case_aed = Number(field.sales_per_case_aed)
        field.sales_per_case_usd = Number(field.sales_per_case_usd)
        // field.product_front_image = field.product_front_image ? config.api_end_point + field.product_front_image : '';
        // field.product_back_image = field.product_back_image ? config.api_end_point + field.product_back_image : '';
        console.log(Number(field.ru_per_case))
        if (Number(field.ru_per_case)) {
          field.ru_per_case = Number(field.ru_per_case)
        }
        if (Number(field.quantity)) {
          field.quantity = Number(field.quantity)
        }
        // if (Number(field.ean_code_ru)) {
        //   field.ean_code_ru = Number(field.ean_code_ru)
        // }
        // if (Number(field.ean_code_case)) {
        //   field.ean_code_case = Number(field.ean_code_case)
        // }
        worksheet.addRow(field);
        worksheet.getCell('N' + row).alignment = {
          vertical: 'bottom',
          horizontal: 'right'
        };
        worksheet.getCell('O' + row).alignment = {
          vertical: 'bottom',
          horizontal: 'right'
        };
        if (field.product_front_image) {
          worksheet.getCell('P' + row).value = {
            text: 'Image 1',
            hyperlink: config.api_end_point + field.product_front_image,
            tooltip: 'Front Image'
          };
        }
        if (field.product_back_image) {
          worksheet.getCell('Q' + row).value = {
            text: 'Image 2',
            hyperlink: config.api_end_point + field.product_back_image,
            tooltip: 'Back Image'
          };
        }
        row++;
      });
      worksheet.getColumn('J').numFmt = '0.00';
      worksheet.getColumn('K').numFmt = '0.00';
      worksheet.getColumn('L').numFmt = '0.00';
      excelCount++;
      const fileName = "product_excel(" + excelCount + ").xlsx";
      //const fileName = uuidv1() + '-' + Date.now() + ".xlsx";
      console.log(fileName)
      const filePath = path.join(__dirname + "/excel", fileName)
      workbook.xlsx.writeFile(filePath)
        .then(function (data) {
          var attachments = [{
            // file on disk as an attachment
            filename: 'product.xlsx',
            path: filePath // stream this file
          }]
          EmailService.sendEmail(
            toMail,
            "Our Offers",
            fileShareHtml, setting, attachments
          );
          setTimeout(function () {
            if (fs.existsSync(filePath)) {
              fs.unlinkSync(filePath);
            }
          }, 300000)
        });
    } catch (error) {
      console.log(error)
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in Creating Excel file for Product",
        data: {}
      });
    }
  },
  createExcel: async (req, res) => {
    try {
      var products = "",
        productList;
      if (req.query.brand) {
        var brands = [];
        if (req.query.brand === 'all') {
          productList = await ProductService.getAllProduct();
        } else {
          var strBrandName = req.query.brand.toLowerCase();
          brands = strBrandName.split(",");
          productList = await ProductService.getAllProductByBrandForExcel(brands);
        }
      } else if (req.query.product_id) {
        var ids = (req.query.product_id).split(",");
        productList = await ProductService.getAllProductByIdForExcel(ids);
      } else {
        productList = await ProductService.getAllProduct();
      }

      

      var Excel = require('exceljs');
      var workbook = new Excel.Workbook();
      var worksheet = workbook.addWorksheet('My Sheet');
      worksheet.columns = [{
          header: 'Name',
          key: 'name',
          width: 20
        },
        {
          header: 'Category Name',
          key: 'category_name',
          width: 20
        },
        {
          header: 'Product Code',
          key: 'product_code',
          width: 15
        },
        {
          header: 'Brand Name',
          key: 'brand_name',
          width: 20
        },
        {
          header: 'Product Type',
          key: 'product_type',
          width: 15
        },
        {
          header: 'Description',
          key: 'description',
          width: 40
        },
        {
          header: 'Size',
          key: 'size',
          width: 10
        },
        {
          header: 'Language',
          key: 'language',
          width: 10
        },
        {
          header: 'Ru / case',
          key: 'ru_per_case',
          width: 15
        },
        {
          header: 'Quantity',
          key: 'quantity',
          width: 10
        },
        {
          header: 'Expiry Date',
          key: 'expiry_date',
          width: 20
        },
        {
          header: 'Cost Price Per Case',
          key: 'cost_price_per_case',
          width: 20
        },
        {
          header: 'Sales Per Case AED',
          key: 'sales_per_case_aed',
          width: 20
        },
        {
          header: 'Sales Per Case USD',
          key: 'sales_per_case_usd',
          width: 20
        },
        {
          header: 'Origin',
          key: 'origin',
          width: 15
        },
        {
          header: 'Ean Code Ru',
          key: 'ean_code_ru',
          width: 30
        },
        {
          header: 'Ean Code Case',
          key: 'ean_code_case',
          width: 30
        },
        {
          header: 'Front Image Url',
          key: 'product_front_image',
          width: 80
        },
        {
          header: 'Back Image Url',
          key: 'product_back_image',
          width: 80
        },
      ];
      var row = 2
      productList.map((field) => {
        field.expiry_date = field.expiry_date?moment(field.expiry_date).format("DD-MM-YYYY"):'NA';
        // field.product_code = ((field.product_name.replace(/\s/g, '') + "prod").substring(0, 4) + field.product_code)
        field.cost_price_per_case = Number(field.cost_price_per_case)
        field.sales_per_case_aed = Number(field.sales_per_case_aed)
        field.sales_per_case_usd = Number(field.sales_per_case_usd)
        // field.product_front_image = field.product_front_image ? config.api_end_point + field.product_front_image : '';
        // field.product_back_image = field.product_back_image ? config.api_end_point + field.product_back_image : '';
        console.log(Number(field.ru_per_case))
        if (Number(field.ru_per_case)) {
          field.ru_per_case = Number(field.ru_per_case)
        }
        if (Number(field.quantity)) {
          field.quantity = Number(field.quantity)
        }
        // if (Number(field.ean_code_ru)) {
        //   field.ean_code_ru = Number(field.ean_code_ru)
        // }
        // if (Number(field.ean_code_case)) {
        //   field.ean_code_case = Number(field.ean_code_case)
        // }
        worksheet.addRow(field);
        worksheet.getCell('N' + row).alignment = {
          vertical: 'bottom',
          horizontal: 'right'
        };
        worksheet.getCell('O' + row).alignment = {
          vertical: 'bottom',
          horizontal: 'right'
        };
        if (field.product_front_image) {
          worksheet.getCell('P' + row).value = {
            text: 'Image 1',
            hyperlink: config.api_end_point + field.product_front_image,
            tooltip: 'Front Image'
          };
        }
        if (field.product_back_image) {
          worksheet.getCell('Q' + row).value = {
            text: 'Image 2',
            hyperlink: config.api_end_point + field.product_back_image,
            tooltip: 'Back Image'
          };
        }

        row++;
      });
      worksheet.getColumn('J').numFmt = '0.00';
      worksheet.getColumn('K').numFmt = '0.00';
      worksheet.getColumn('L').numFmt = '0.00';
      excelCount++;
      const fileName = "product_excel(" + excelCount + ").xlsx";
      const filePath = path.join(__dirname + "/excel", fileName)
      workbook.xlsx.writeFile(filePath)
        .then(function (data) {
          setTimeout(function () {
            if (fs.existsSync(filePath)) {
              fs.unlinkSync(filePath);
            }
          }, 300000)
          return res.send({
            code: 200,
            status: "success",
            message: "Creating Excel file for Product",
            data: {
              path: config.api_end_point + '/api/product/report/excel/' + fileName,
              extension: "xlsx"
            }
          });
        });
    } catch (error) {
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in Creating Excel file for Product",
        data: {}
      });
    }
  },
  deleteProduct: async (req, res) => {
    try {
      var ids = (req.params.product_id).split(',');
      var products = await ProductService.getProductByIds(ids);
      let p = await OrderService.checkProdut(ids);
      if(p) return res.send({code:200,status:"Exist"});
      var data = await ProductService.deleteProductByIds(ids);
      res.send({
        code: 200,
        status: "success",
        message: "Product is successfully deleted",
        data: data
      });
      products.forEach((productData) => {
        if (productData.product_front_image && productData.product_front_image != '' && productData.product_front_image != null && productData.product_front_image != undefined) {
          var imageUrl = productData.product_front_image;
          var fileUrl = imageUrl.replace('/static/', MEDIA.local_file_path)
          if (fs.existsSync(fileUrl)) {
            fs.unlinkSync(fileUrl);
          }
        }
        if (productData.product_back_image && productData.product_back_image != '' && productData.product_back_image != null && productData.product_back_image != undefined) {
          var imageUrl = productData.product_back_image;
          var fileUrl = imageUrl.replace('/static/', MEDIA.local_file_path)
          if (fs.existsSync(fileUrl)) {
            fs.unlinkSync(fileUrl);
          }
        }

      });
    } catch (error) {
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Product Does not delete",
        data: {}
      });

    }
  }
};
let alterWord = function (word) {
  //console.log(word.match(/.{1,50}/g))
  //return word ? word.match(/.{1,50}/g).join('<br>') : '';
  return word ? word : "NA";
}
let getLineOfDesctription = function (word) {

  //return word ? word.match(/.{1,50}/g).join('<br>') : '';
  var w = Math.floor((word.length / 50));
  return w;
}

let badResponse = function (res, msg) {
  res.status(400).send({
    code: 400,
    status: "error",
    message: msg,
    data: []
  });
};
let digitProductCode = async (digit) => {
  return await new Promise((resolve, reject) => {
    var len = digit.toString().length
    var strDigit = ''
    switch (len) {
      case 0:
        strDigit = "000000";
        break;
      case 1:
        strDigit = "00000" + digit;
        break;
      case 2:
        strDigit = "0000" + digit;
        break;
      case 3:
        strDigit = "000" + digit;
        break;
      case 4:
        strDigit = "00" + digit;
        break;
      case 5:
        strDigit = "0" + digit;
        break;
      case 6:
        strDigit = "" + digit;
        break;
    }
    resolve(strDigit)
  });

}

let moveFile = async (media_file, filePath, file_name) => {
  return await new Promise((resolve, reject) => {

    mkdirp(filePath, function (err) {
      if (err) {
        logger.error(err);
        reject({
          code: 400,
          status: "error",
          message: "Error in Uploading file",
          data: []
        });
      } else {
        media_file.mv(filePath + file_name, function (err) {
          if (err) {
            logger.error(err);
            reject({
              code: 400,
              status: "error",
              message: "Error in Uploading file",
              data: []
            });
          } else {
            logger.log("File Moved")
            resolve({
              code: 200,
              status: "success",
              message: "Uploaded Successfully",
              data: []
            })
          }
        });
      }
    });
  });
}

export default ProductController;

// async function main() {
//   // Generate test SMTP service account from ethereal.email
//   // Only needed if you don't have a real mail account for testing
//   let testAccount = await nodemailer.createTestAccount();

//   // create reusable transporter object using the default SMTP transport
//   let transporter = nodemailer.createTransport({
//     host: "smtp.gmail.com",
//     port: 465,
//     secure: false, // true for 465, false for other ports
//     auth: {
//       user: 'abs.rajaabinesh@gmail.com', // generated ethereal user
//       pass: 'raja100xxx' // generated ethereal password
//     }
//   });
//   // send mail with defined transport object
//   let info = await transporter.sendMail({
//     from: 'abs.rajaabinesh@gmail.com', // sender address
//     to: "gsm.rajaabinesh@gmail.com", // list of receivers
//     subject: "Hello ✔", // Subject line
//     text: "Hello world?", // plain text body
//     html: "<b>Hello world?</b>" // html body
//   });

//   console.log("Message sent: %s", info.messageId);
//   // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

//   // Preview only available when sending through an Ethereal account
//   console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
//   // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
// }

// main().catch(console.error);