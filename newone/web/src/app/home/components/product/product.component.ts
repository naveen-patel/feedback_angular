import { Component, OnInit, ChangeDetectorRef, TemplateRef } from '@angular/core';
import { DataService } from "../../services/data.service"
import 'rxjs/Rx';
declare var $: any;
declare var require: any;
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
var _ = require('lodash');
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomerService } from '../../services/customer.service';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  modalRef2: BsModalRef;
  modalRef3: BsModalRef;
  modalRef4: BsModalRef;
  modalRef5: BsModalRef;


  file_details: string;
  records = [];
  upload_records = [];
  openAddProduct: boolean = false;
  parentmessage: any;
  loader: boolean = false;
  search: string = '';
  limit = 10;
  page = 1;
  count: Number;
  currentpage: Number = 1;
  Allrecords = [];
  id: string;
  isSelectAll: boolean = false;
  selectedIds = [];
  openBrandWiseProduct: boolean = false
  dropdownList = [];
  dropdownSupplierList = [];
  brandList = [];
  supplier = [];
  selectedItems = [];
  selectedItems1 = [];
  remainingRecords = 0;
  mailFileType = ''
  sendEMailId = ''
  Add: Boolean = true

  emailForm: FormGroup;
  submitted: boolean = false;



  constructor(
    public ref: ChangeDetectorRef,
    public ds: DataService,
    public cs: CustomerService,
    private modalService: BsModalService,
    public formBuilder: FormBuilder,
  ) { }
  dropdownSettings = {
    singleSelection: false,
    text: "Select Brand",
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    enableSearchFilter: true,
    classes: "myclass custom-class",
    enableCheckAll: true

    // checkAll: 'Select all',
    // uncheckAll: 'Unselect all',
    // checked: 'item selected',
    // checkedPlural: 'items selected',
    // searchPlaceholder: 'Find',
    // searchEmptyResult: 'Nothing found...',
    // searchNoRenderText: 'Type in search box to see results...',
    // defaultTitle: 'Select',
    // allSelected: 'All selected',
  };

  dropdownSettings1 = {
    singleSelection: false,
    text: "Select Supplier",
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    enableSearchFilter: true,
    classes: "myclass custom-class",
    enableCheckAll: true
  };
  get f() { return this.emailForm.controls; }

  ngOnInit() {
    this.dropdownList = [];
    this.dropdownSupplierList = [];
    this.brandList = [];
    this.supplier = [];
    // this.selectedItems = [];
    // this.selectedItems1 = [];
    this.ds.getBrandList().subscribe(data => {
      var i = 1;
      data.forEach(element => {
        this.dropdownList.push({
          "id": element._id,
          "itemName": element.brand_name
        });
        // this.selectedItems.push({
        //   "id": i,
        //   "itemName": element
        // });

        i++;
      });
      //this.selectedItems=Array.from(Object.create(this.dropdownList));
      this.getProducts();


    })
    this.ds.getSuppliers().subscribe(data => {

      data.forEach(element => {
        this.dropdownSupplierList.push({
          "id": element._id,
          "itemName": element.name
        });
      });
      this.getProducts();
    })
  }
  onItemChange(value) {
    this.getProducts()
  }
  onItemSelect(item: any) {
    this.getProducts();
  }
  OnItemDeSelect(item: any) {
    this.getProducts();
  }
  onSelectAll(items: any) {
    this.getProducts();
  }
  onDeSelectAll(items: any) {
    this.getProducts();
  }
  getProducts() {
    this.brandList = [];
    this.supplier = [];
    this.remainingRecords = 0;
    this.selectedItems.forEach(element => {
      this.brandList.push(element.itemName);
    });
    this.selectedItems1.forEach(element => {
      this.supplier.push(element.itemName);
    });
    this.page = 1;
    this.isSelectAll = false;
    this.selectedIds = [];
    console.log('brand', this.brandList, 'supplier', this.supplier)
    if ((this.brandList.length === 0) && (this.supplier.length === 0)) {
      this.records = [];
      this.ds.getProductsByBrands(['all'], ['all'], this.search, this.limit, this.page).subscribe(data => {
        this.loader = false;
        this.count = data.totalcount;
        this.records = data.product;
        this.selectedIds = [];
        this.isSelectAll = false;
        this.remainingRecords = data.totalcount - (this.page * this.limit);
        this.records.forEach((element) => {
          element.isSelect = false;
        })
      });
    } else {
      let brand = this.brandList.length != 0 ? this.brandList : ['all'];
      let supplier = this.supplier.length != 0 ? this.supplier : ['all'];
      this.ds.getProductsByBrands(supplier, brand, this.search, this.limit, this.page).subscribe(data => {
        this.loader = false;
        this.count = data.totalcount;
        this.records = data.product;
        this.selectedIds = [];
        this.isSelectAll = false;
        this.remainingRecords = data.totalcount - (this.page * this.limit);
        this.records.forEach((element) => {
          element.isSelect = false;
        })
      });
    }
  }


  loadMore() {

    this.page++;
    var brands = ['all']
    if (this.brandList.length > 0) {
      brands = this.brandList;
    }
    var supplier = ['all']
    if (this.supplier.length > 0) {
      brands = this.supplier;
    }
    this.ds.getProductsByBrands(supplier, brands, this.search, this.limit, this.page).subscribe(data => {
      var product = data.product;
      this.isSelectAll = false;
      product.forEach((element) => {
        element.isSelect = false;
      })
      this.records = this.records.concat(product);
      this.remainingRecords = data.totalcount - (this.page * this.limit);
    });
  }

  searchProduct() {
    this.getProducts();
  }
  reload() {
    this.search = "";
    this.page = 1;
    this.getProducts();
  }

  pageChanged(event) {
    this.page = event.page;
    this.getProducts();
  }
  openProduct() {
    this.openAddProduct = true;
    this.Add = true;
    this.parentmessage = ""
  }
  openEditProduct(product, view) {
    this.Add = false;
    product.isViewOnly = view
    this.openAddProduct = true;
    this.parentmessage = product;
  }
  receiveMessage(event) {
    this.openAddProduct = false;
    if (this.Add) {
      this.ngOnInit();
    } else {
      this.ngOnInit();
    }

  }

  DeleteRecord(template: TemplateRef<any>, i, id) {
    this.modalRef3 = this.modalService.show(template);
    this.id = id;
  }
  confirmToDelete() {
    this.ds.deleteProduct(this.id).subscribe(data => {
      console.log(data);

      // this.ds.successMessage("Product Deleted Successfully");
      // this.ngOnInit();
      if (data.status == 'Exist') {
        this.ds.warningMessage(" Product exist in order, can't delete it");
        this.ngOnInit();
      } else {
        this.ds.successMessage("Product Deleted Successfully");
        this.ngOnInit();
      }
    })
    this.modalRef3.hide();
    this.modalRef3 = null
  }
  confirmToMultiDelete() {
    this.ds.deleteProduct(this.selectedIds).subscribe(data => {
      if (data.status == 'Exist') {
        this.ds.warningMessage("Exist in order");
        this.ngOnInit();
      } else {
        this.ds.successMessage("Products Deleted Successfully");
        this.ngOnInit();
      }

    })
    this.modalRef4.hide();
    this.modalRef4 = null
  }
  DeleteRecords(template: TemplateRef<any>) {
    this.modalRef4 = this.modalService.show(template);
  }
  selectRecord(index) {
    if (this.records[index].isSelect) {
      var i = this.selectedIds.indexOf(this.records[index]._id);
      if (i !== -1) {
        this.selectedIds.splice(i, 1);
      }
    } else {
      this.selectedIds.push(this.records[index]._id)
    }
    this.records[index].isSelect = !this.records[index].isSelect
    this.isSelectAll = (this.selectedIds.length === this.records.length)
  }
  selectAllRecord() {
    this.selectedIds = [];
    if (this.isSelectAll) {
      this.isSelectAll = false
      this.records.forEach((element) => {
        element.isSelect = false;
      })
    } else {
      this.isSelectAll = true;
      this.records.forEach((element) => {
        element.isSelect = true;
        this.selectedIds.push(element._id)
      })
    }
  }
  generateExcel() {
    if (this.isSelectAll) {
      if (this.brandList.length > 0) {
        this.ds.generateProductExcel('brand', this.brandList).subscribe((data) => {
          window.open(data.path)
          this.ds.successMessage("Excel Downloaded Successfully");
        });
      } else {
        this.ds.generateProductExcel('brand', ['all']).subscribe((data) => {
          window.open(data.path)
          this.ds.successMessage("Excel Downloaded Successfully");
        });
      }
    } else if (this.selectedIds.length > 0) {
      this.ds.generateProductExcel('product_id', this.selectedIds).subscribe((data) => {
        window.open(data.path)
        this.ds.successMessage("Excel Downloaded Successfully");
      });
    } else if (this.brandList.length > 0) {
      this.ds.generateProductExcel('brand', this.brandList).subscribe((data) => {
        window.open(data.path)
        this.ds.successMessage("Excel Downloaded Successfully");
      });
    } else {
      this.ds.errorMessage("Please select Brand or Product List");
    }
  }
  generatePDF() {
    if (this.isSelectAll) {
      if (this.brandList.length > 0) {
        this.ds.generateProductPDF('brand', this.brandList).subscribe((data) => {
          window.open(data.path)
          this.ds.successMessage("Pdf Downloaded Successfully");
        });
      } else {
        this.ds.generateProductPDF('brand', ['all']).subscribe((data) => {
          window.open(data.path)
          this.ds.successMessage("Pdf Downloaded Successfully");
        });
      }
    } else if (this.selectedIds.length > 0) {
      this.ds.generateProductPDF('product_id', this.selectedIds).subscribe((data) => {
        window.open(data.path);
        this.ds.successMessage("Pdf Downloaded Successfully");
      });
    } else if (this.brandList.length > 0) {
      this.ds.generateProductPDF('brand', this.brandList).subscribe((data) => {
        window.open(data.path)
        this.ds.successMessage("Pdf Downloaded Successfully");
      });
    } else {
      this.ds.errorMessage("Please select Brand or Product List");
    }
  }
  closeBrandWiseProduct(event) {
    this.openBrandWiseProduct = false;
  }
  getFormFields() {
    return {
      email: ["", Validators.required],
    }
  }
  sendEmailPDF(template: TemplateRef<any>) {
    this.mailFileType = 'pdf'
    this.modalRef5 = this.modalService.show(template);
    this.emailForm = this.formBuilder.group(this.getFormFields());
    this.submitted = false;
    this.sendEMailId = '';
  }
  sendEmailExcel(template: TemplateRef<any>) {
    this.mailFileType = 'excel'
    this.modalRef5 = this.modalService.show(template);
    this.emailForm = this.formBuilder.group(this.getFormFields());
    this.submitted = false;
    this.sendEMailId = '';
  }
  confirmToSendMail() {
    this.submitted = true;
    if (this.emailForm.valid) {
      if (this.mailFileType === 'pdf') {
        if (this.isSelectAll) {
          if (this.brandList.length > 0) {
            this.ds.sendEmailPDF(this.sendEMailId, 'brand', this.brandList).subscribe((data) => {
              this.ds.successMessage('Email Sent Successfully');
              this.modalRef5.hide();
              this.modalRef5 = null
            });
          } else {
            this.ds.sendEmailPDF(this.sendEMailId, 'brand', ['all']).subscribe((data) => {
              this.ds.successMessage('Email Sent Successfully')
              this.modalRef5.hide();
              this.modalRef5 = null
            });
          }
        } else if (this.selectedIds.length > 0) {
          this.ds.sendEmailPDF(this.sendEMailId, 'product_id', this.selectedIds).subscribe((data) => {
            this.ds.successMessage('Email Sent Successfully')
            this.modalRef5.hide();
            this.modalRef5 = null
          });
        } else if (this.brandList.length > 0) {
          this.ds.sendEmailPDF(this.sendEMailId, 'brand', this.brandList).subscribe((data) => {
            this.ds.successMessage('Email Sent Successfully');
            this.modalRef5.hide();
            this.modalRef5 = null
          });
        } else {
          this.ds.errorMessage("Please select Brand or Product List");
        }
      } else {
        if (this.isSelectAll) {
          if (this.brandList.length > 0) {
            this.ds.sendEmailExcel(this.sendEMailId, 'brand', this.brandList).subscribe((data) => {
              this.ds.successMessage('Email Sent Successfully');
              this.modalRef5.hide();
              this.modalRef5 = null
            });
          } else {
            this.ds.sendEmailExcel(this.sendEMailId, 'brand', ['all']).subscribe((data) => {
              this.ds.successMessage('Email Sent Successfully')
              this.modalRef5.hide();
              this.modalRef5 = null
            });
          }
        } else if (this.selectedIds.length > 0) {
          this.ds.sendEmailExcel(this.sendEMailId, 'product_id', this.selectedIds).subscribe((data) => {
            this.ds.successMessage('Email Sent Successfully')
            this.modalRef5.hide();
            this.modalRef5 = null
          });
        } else if (this.brandList.length > 0) {
          this.ds.sendEmailExcel(this.sendEMailId, 'brand', this.brandList).subscribe((data) => {
            this.ds.successMessage('Email Sent Successfully');
            this.modalRef5.hide();
            this.modalRef5 = null
          });
        } else {
          this.ds.errorMessage("Please select Brand or Product List");
        }
      }
    }
  }

}
