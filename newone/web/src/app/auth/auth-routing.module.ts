import { SignUpComponent } from './container/sign-up/sign-up.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './container/login/login.component';
import { ForgetPasswordComponent } from './container/forget-password/forget-password.component';
import { ChangePasswordComponent } from './container/change-password/change-password.component';


const routes: Routes = [
  {path: '', redirectTo: 'auth', pathMatch: 'full'},
  {path: 'auth', children: [
    {path: 'login', component: LoginComponent},
    {path: 'sign-up', component: SignUpComponent},
    {path: 'forget-password', component: ForgetPasswordComponent},
    {path: 'change-password/email/:email/:id', component: ChangePasswordComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
