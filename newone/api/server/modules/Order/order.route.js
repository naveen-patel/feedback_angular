import express from "express";
import orderController from "./order.controller";
import UserAuthController from "../UserAuthentication/userAuth.controller";
import rateLimit  from 'express-rate-limit';

// Rate Limiting
const apiLimit = rateLimit({
    max: 5,// max requests
    windowMs: 1 * 60 * 1000, // i mint of 'ban' / lockout 
    message: 'Too many requests' // message to send
});
const orderRouter = express.Router();

orderRouter.post('/order/:user_id',UserAuthController.verify,orderController.createOrder);
orderRouter.put('/order/:order_id',UserAuthController.verify,orderController.updateOrderStatus);
orderRouter.put('/order/callback_status/:order_id/:user_id',UserAuthController.verify,orderController.updateCallbackStatus)
orderRouter.get('/order',apiLimit,UserAuthController.verify,orderController.getAllOrder);
orderRouter.get('/orders/:user_id',UserAuthController.verify,orderController.getAllOrderByUser);
orderRouter.get('/order/:order_id',orderController.getOrderById);
orderRouter.get('/order/report/pdf', UserAuthController.verify,orderController.createPDF);
orderRouter.get('/order/report/pdf/:file_name', orderController.pdfFileDownload);
orderRouter.delete('/order/:order_id',UserAuthController.verify,orderController.deleteOrder);
orderRouter.delete('/order/product/:order_id/:product_id',UserAuthController.verify,orderController.removeProductFromOrder);
export default orderRouter;