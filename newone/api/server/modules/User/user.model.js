import mongoose from 'mongoose';
var uuidv1 = require('uuid/v1');

const UserSchema = mongoose.Schema({
    first_name: {
        type: String
    },
    last_name: {
        type: String
    },
    country_name:{
        type: String
    },
    country_code:{
        type: String
    },
    permanent_address:{
        type: String
    },
    company_name:{
        type: String
    },
    shipping_address:{
        type: String 
    },
    city:{
        type: String
    },
    mobile_number: {
        type: String
    },
    phone_number: {
        type: String
    },
    email: {
        type: String,
        require: true,
    },
    email_confirmation: {
        type: Boolean,
        require: true,
        default:false
    },
    mobile_verification: {
        type: Boolean,
        require: true,
        default:false
    },
    password: {
        type: String
    },
    type: {
        type: String,
        enum: ['user','super_admin','admin','customer'],
        default:"user"
    },
    status: {
        type: String,
        enum: ['active', 'in_active', 'reject','pending'],
        default:"in_active"
    },
    password_uuid:{
        type: String,
        default: uuidv1()
    },
    // emailVerification : {
    //     code : 
    //         {type: String,
    //         required: false},
    //     expires : 
    //         {type: Date,
    //         required: false}
    //     },
    otp :{
        mcode : 
          {type: String,
           required: false},
        ecode : 
          {type: String,
           required: false},
        expires : 
          {type: Date,
          required: false
          } ,
        isVerified:
          {type:Number,
           default:0 
          }
    }}, { collection: 'user', timestamps: true });

let UserModel = mongoose.model('user', UserSchema);

export default UserModel