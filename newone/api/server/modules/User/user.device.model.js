import mongoose from 'mongoose';
var uuidv1 = require('uuid/v1');
var Schema = mongoose.Schema;

const deviceSchema = mongoose.Schema({
    user_id: {
        type: Schema.Types.ObjectId,
        ref: "user"
    },
   device:[{ 
    token: {
        type: String
    },
    device_id:{
        type: String
    },
    device_palform:{
        type: String
    }
  }]
}, { collection: 'device', timestamps: true });

let DeviceModel = mongoose.model('device', deviceSchema);

export default DeviceModel