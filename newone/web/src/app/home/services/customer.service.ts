import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import {
  HttpClient
} from "@angular/common/http";
import { ToastyService } from "ng2-toasty";
// import { AuthService } from "../../auth/services/auth.service";
import { Router, ActivatedRoute, ActivatedRouteSnapshot, Resolve } from "@angular/router";
import { Pipe, PipeTransform } from '@angular/core';
import { httpFactory } from "@angular/http/src/http_module";
declare var require: any;
var moment = require("moment");
const momentTz = require('moment-timezone');
@Injectable()
export class CustomerService {
  countries: any;
  categories: any;
  flag: any;
  categoryDet: any;
  location: any;
  constructor(
    private http: HttpClient,
    private toastyService: ToastyService
  ) { }
  /**
  *
  * @param {AddProjectData}
  * @returns {Observable<User>}
  * @memberof AuthService
  */

  // for customer list
  getAllcustomersList(searchkey, limit, skip, type, status): Observable<any> {
    let url = "customers";
    if (!limit) {
      limit = 10;
    }
    if (!searchkey) {
      searchkey = '';
    }
    if (!skip) {
      skip = 0;
    }
    searchkey = searchkey.replace(/&/g, '%26');
    url += '?searchkey=' + searchkey;
    url += '&limit=' + limit;
    url += '&skip=' + skip;
    url += '&type=' + type;
    url += '&status=' + status;
    return this.http.get<any>(url)
      .map(res => {
        if (res.code === 403) {
          return res;
        } else {
          return res.data ? res.data : [];
        }
      })
      .do(
        _ => _,
        (err) => {
          this.errorMessage(err.error.message)
        }
      );
  }

  deleteCustomers(id): Observable<any> {
    let url = "customer/" + id
    return this.http.delete<any>(url, {
      observe: "response"
    })
      .map(res => {
        return res;
      })
      .do(
        _ => _,
        (err) => {
          this.errorMessage(err.error.message)
        }
      );
  }

  changeStatus(value, id): Observable<any> {
    var data = { status: value }
    let url = "customer/change_status/" + id
    return this.http
      .post<any>(url, data, {
        observe: "response"
      })
      .map(res => {
        return res.body.data;
      })
      .do(
        _ => _,
        (err) => {
          this.errorMessage(err.error.message)
        }
      );
  }
  // for pending request (send mail)
  changePendingStatus(value, id): Observable<any> {
    var data = { status: value }
    let url = "customer/takeAction/" + id
    return this.http
      .post<any>(url, data, {
        observe: "response"
      })
      .map(res => {
        return res.body.data;
      })
      .do(
        _ => _,
        (err) => {
          this.errorMessage(err.error.message)
        }
      );
  }
  forCountryCode(): Observable<any> {
    let url = "countries"
    return this.http.get<any>(url, {
      observe: "response"
    })
      .map(res => {
        const user = res.body.data;
        this.setCountries(res);
        return res;
      })
      .do(
        _ => _,
        (err) => {
          console.log(err);
          
          this.errorMessage(err.error.message)
        }
      );
  }
  private setCountries(res): void {
    const country_name = res.body.countries;
    this.countries = country_name;
  }
  // Update otp duration in setting section 
  saveOtpDuration(data, id): Observable<any> {
    let url = "settings/otp/" + id;
    return this.http
      .post<any>(url, data, {
        observe: "response"
      })
      .map(res => {
        return res.body.data;
      })
      .do(
        _ => _,
        (err) => {
          this.errorMessage(err.error.message)
        }
      );
  }
  // for get all category
  getCategory(searchkey, limit, skip): Observable<any> {
    searchkey = searchkey.replace(/&/g, '%26');
    let url = "category?skip=" + skip + "&searchkey=" + searchkey + "&limit=" + limit;
    return this.http.get<any>(url)
      .map(res => {
        if (res.code === 403) {
          return res;
        } else {
          this.setCategory(res.data);
          return res.data ? res.data : [];
        }
      })
      .do(
        _ => _,
        (err) =>
          this.errorMessage(err.error.message)
      );
  };

  // for category drop down
  private setCategory(res): void {
    this.categories = res.category
  }

  getAllCategory(): Observable<any> {
    let url = "category"
    return this.http
      .get<any>(url, {
        observe: "response"
      })
      .map(res => {
        return res.body.data;
      })
      .do(
        _ => _,
        (err) => {
          this.errorMessage(err.error.message)
        }
      );
  };
  // for delete category
  deleteCategory(id): Observable<any> {
    let url = "category/" + id;
    return this.http.delete<any>(url)
      .map(res => {
        if (res.code === 403) {
          return res;
        } else {
          return res;
        }
      })
      .do(
        _ => _,
        (err) => {
          this.errorMessage(err.error.message)
        }
      );
  };
  // for add new category
  addCategory(data): Observable<any> {
    let url = "category"
    return this.http
      .post<any>(url, data, {
        observe: "response"
      })
      .map(res => {
        return res.body;
      }).do(
        _ => _,
        (err) => {
          this.errorMessage(err.error.message)
        }
      );
  };
  // for update category details
  updateCategory(id, data): Observable<any> {
    let url = "category/" + id
    return this.http
      .put<any>(url, data, {
        observe: "response"
      })
      .map(res => {
        return res;
      }).do(
        _ => _,
        (err) => {
          this.errorMessage(err.error.message)
        }
      );
  };
    // for update category details
    updateFMCGUser(id, data): Observable<any> {
      let url = "customers/" + id
      return this.http
        .put<any>(url, data, {
          observe: "response"
        })
        .map(res => {
          return res;
        }).do(
          _ => _,
          (err) => {
            this.errorMessage(err.error.message)
          }
        );
    };
  // for get all deals
  getDeals(searchkey, limit, page): Observable<any> {
    searchkey = searchkey.replace(/&/g, '%26');
    let url = "deal?skip=" + page + "&searchkey=" + searchkey + "&limit=" + limit;
    return this.http.get<any>(url)
      .map(res => {
        if (res.code === 403) {
          return res;
        } else {
          this.setCategory(res.data);
          return res.data ? res.data : [];
        }
      })
      .do(
        _ => _,
        (err) =>
          this.errorMessage(err.error.message)
      );
  };

  // for delete Deal
  deleteDeals(id): Observable<any> {
    let url = "deal/" + id;
    return this.http.delete<any>(url)
      .map(res => {
        if (res.code === 403) {
          return res;
        } else {
          return res.data ? res.data : [];
        }
      })
      .do(
        _ => _,
        (err) => {
          this.errorMessage(err.error.message)
        }
      );
  };
  // for add new deals
  addDeal(data, id): Observable<any> {
    let url = "deal/" + id;
    return this.http
      .post<any>(url, data, {
        observe: "response"
      })
      .map(res => {
        return res.body;
      }).do(
        _ => _,
        (err) => {
          this.errorMessage(err.error.message)
        }
      );
  };

  // for update deal details
  updateDeal(id, data, product_id): Observable<any> {
    let url = "deal/" + product_id + "/" + id;
    return this.http
      .put<any>(url, data, {
        observe: "response"
      })
      .map(res => {
        return res;
      }).do(
        _ => _,
        (err) => {
          this.errorMessage(err.error.message)
        }
      );
  };
  // For brand name in drop down 
  forProductDrop(): Observable<any> {
    let url = "product/drop_down/list"
    return this.http.get<any>(url, {
      observe: "response"
    })
      .map(res => {
        return res.body.data;
      })
      .do(
        _ => _,
        (err) => {
          this.errorMessage(err.error.message)
        }
      );
  }

  // for get all order details
  getOrder(searchkey, limit, skip): Observable<any> {
    searchkey = searchkey.replace(/&/g, '%26');
    let url = "order?skip=" + skip + "&searchkey=" + searchkey + "&limit=" + limit;
    return this.http.get<any>(url)
      .map(res => {
        if (res.code === 403) {
          return res;
        } else {
          return res.data ? res.data : [];
        }
      })
      .do(
        _ => _,
        (err) =>
          this.errorMessage(err.error.message)
      );
  };

  // for update order status details
  updateOrder(id, data): Observable<any> {
    let url = "order/" + id
    return this.http
      .put<any>(url, data, {
        observe: "response"
      })
      .map(res => {
        return res;
      }).do(
        _ => _,
        (err) => {
          this.errorMessage(err.error.message)
        }
      );
  };

  // for update order's product status details
  addOrderProduct(id, data): Observable<any> {

    let url = "order/" + id
    return this.http
      .put<any>(url, data, {
        observe: "response"
      })
      .map(res => {
        return res;
      }).do(
        _ => _,
        (err) => {
          this.errorMessage(err.error.message)
        }
      );
  };
  // for get all order details
  getOrderDetails(id): Observable<any> {
    let url = "order/" + id;
    return this.http.get<any>(url)
      .map(res => {
        if (res.code === 403) {
          return res;
        } else {
          return res.data ? res.data : [];
        }
      })
      .do(
        _ => _,
        (err) =>
          this.errorMessage(err.error.message)
      );
  };

  // for delete category
  deleteOrder(id): Observable<any> {
    let url = "order/" + id;
    return this.http.delete<any>(url)
      .map(res => {
        if (res.code === 403) {
          return res;
        } else {
          return res.data ? res.data : [];
        }
      })
      .do(
        _ => _,
        (err) => {
          this.errorMessage(err.error.message)
        }
      );
  };

  // for delete order product
  deleteOrder1(id1, id2, quantity, priceAED, priceUSD): Observable<any> {
    let url = "order/product/" + id1 + "/" + id2 + "?quantity=" + quantity + "&priceAED=" + priceAED + "&priceUSD=" + priceUSD;
    return this.http.delete<any>(url)
      .map(res => {
        if (res.code === 403) {
          return res;
        } else {
          return res.data ? res.data : [];
        }
      })
      .do(
        _ => _,
        (err) => {
          this.errorMessage(err.error.message)
        }
      );
  };
  // for download pdf 
  generateProductPDF(values): Observable<any> {
    // /pdf?' + key + '=' + newValues
    let url = 'order/report/pdf?' + "order_id" + "=" + values
    return this.http.get<any>(url)
      .map(res => {
        if (res.code === 403) {
          return res;
        } else {
          return res.data ? res.data : [];
        }
      })
      .do(
        _ => _,
        (err) =>
          this.errorMessage(err.error.message)
      );
  };

  ///////////// for brand //////////////

  // for update brand details
  updateBrand(id, data): Observable<any> {
    let url = "brand/" + id
    return this.http
      .put<any>(url, data, {
        observe: "response"
      })
      .map(res => {
        return res;
      }).do(
        _ => _,
        (err) => {
          this.errorMessage(err.error.message)
        }
      );
  };

  // for add brand category
  addBrand(data): Observable<any> {
    let url = "brand"
    return this.http
      .post<any>(url, data, {
        observe: "response"
      })
      .map(res => {
        return res.body;
      }).do(
        _ => _,
        (err) => {
          this.errorMessage(err.error.message)
        }
      );
  };

  // for get all brand
  getBrand(searchkey, limit, skip): Observable<any> {
    searchkey = searchkey.replace(/&/g, '%26');
    let url = "brand?skip=" + skip + "&searchkey=" + searchkey + "&limit=" + limit;
    return this.http.get<any>(url)
      .map(res => {
        if (res.code === 403) {
          return res;
        } else {
          this.setCategory(res.data);
          return res.data ? res.data : [];
        }
      })
      .do(
        _ => _,
        (err) =>
          this.errorMessage(err.error.message)
      );
  };

  // for delete category
  deleteBrand(id): Observable<any> {
    let url = "brand/" + id;
    return this.http.delete<any>(url)
      .map(res => {
        if (res.code === 403) {
          return res;
        } else {
          return res;
        }
      })
      .do(
        _ => _,
        (err) => {
          this.errorMessage(err.error.message)
        }
      );
  };

  // for brand page 
  getAllOrderDetails(tablndex, ): Observable<any> {
    let url = "dashboard";
    if (!tablndex) {
      tablndex = 0;
    }
    url += '?tabIndex=' + tablndex;
    return this.http.get<any>(url)
      .map(res => {
        if (res.code === 403) {
          return res;
        } else {
          return res.data ? res.data : [];
        }
      })
      .do(
        _ => _,
        (err) => {
          console.log('eeeeeeeeerrrrrrrr',err);
          
          this.errorMessage(err.error.message)
        }
      );
  }
  // for brand page 
  forBrandModal(category, flag): any {
    this.categoryDet = category
    this.flag = flag;
  };

  // for dashboard pie chart
  getOrderCountForChart(): Observable<any> {
    let url = "dashboard/order/status";
    return this.http.get<any>(url)
      .map(res => {
        if (res.code === 403) {
          return res;
        } else {
          return res.data ? res.data : [];
        }
      })
      .do(
        _ => _,
        (err) => {
          this.errorMessage(err.error.message)
        }
      );
  }
  /////////////////// for report page //////////


  // for get all brand
  getReports(endDate, startDate, limit, skip, index): Observable<any> {

    let url = "report?endDate=" + endDate + "&startDate=" + startDate + "&limit=" + limit + "&skip=" + skip + "&index=" + index;
    return this.http.get<any>(url)
      .map(res => {
        if (res.code === 403) {
          return res;
        } else {
          // this.setCategory(res.data);
          return res ? res : [];
        }
      })
      .do(
        _ => _,
        (err) =>
          this.errorMessage(err.error.message)
      );
  };


  /////////////// for FMCG excel fmcg pdf //////
  generateProductExcel(): Observable<any> {
    let url = 'users/report/excel'
    return this.http
      .get<any>(url, {
        observe: "response"
      })
      .map(res => {
        return res.body.data;
      })
      .do(
        _ => _,
        (err) =>
          this.errorMessage(err.error.message)
      );
  }
  ////////////for report pdf download //////////////

  generateReportPdf(endDate, startDate, values): Observable<any> {
    let url;
    values == 'low' ? url = 'report/low/pdf' : url = 'report/' + values + "/pdf?endDate=" + endDate + "&startDate=" + startDate;
    return this.http.get<any>(url)
      .map(res => {
        if (res.code === 403) {
          return res;
        } else {
          return res.data ? res.data : [];
        }
      })
      .do(
        _ => _,
        (err) =>
          this.errorMessage(err.error.message)
      );
  };

  successMessage(message) {
    this.toastyService.success({
      title: "Success",
      msg: message
    })
  }
  errorMessage(message) {
    this.toastyService.error({
      title: "Error",
      msg: message
    })
  }
  warningMessage(message) {
    this.toastyService.warning({
      title: "Warning",
      msg: message
    })
  }
}
