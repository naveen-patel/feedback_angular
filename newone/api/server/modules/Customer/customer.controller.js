import CustomerService from "../Customer/customer.service";
import UserAuthCtrl from "../UserAuthentication/userAuth.controller";
import UserAuthService from "../UserAuthentication/userAuth.service";
import UserService from "../User/user.service";
import logger from "../../../config/logger.config";
var access = require("safe-access");
import moment, { duration } from "moment";
import countries  from "../../modules/masterdata.json"
var _ = require('lodash');
import settings from "../../../settings";
let config = require("../../../config/" + settings.environment + ".config");
const api_end_point = config.default.api_end_point;
import EmailService from "../CommonService/email.service";
import SettingsService from '../Settings/settings.service';
let mkdirp = require('mkdirp');
import request from 'request';

//let ffmpeg = require('fluent-ffmpeg');;
const fs = require('fs');
const UTF8 = 'utf8';
var path = require("path");
const webEndPoint = config.web_end_point;
const MEDIA = config.media;

const CustomerController = {};

CustomerController.getAllCustomers = async (req, res) => {
  try {
    var search;
  if (_.isEmpty(req.query.searchkey)) {
    search = '';
  } else {
    search = req.query.searchkey
  }
  let limit = Number(req.query.limit) || 10000;
  let offset = (req.query.skip) ? parseInt(req.query.skip) : 1
  let skip = (offset - 1) * limit;
  let type = req.query.type || 'customer'
  let status = req.query.status|| 'active'
    const users = await CustomerService.getAllCustomers(search, limit, skip,type,status);
    logger.info(" Getting all customers");
    res.send({
      code: 200,
      status: "success",
      message: "Listing All customers",
      data: users
    });
  } catch (error) {
    logger.error("Error in getting User customer :" + error);
    res.status(400).send({
      code: 400,
      status: "error",
      message: "Error in getting customer record",
      data: []
    });
  }
};



CustomerController.customerChangePassword = async (req, res) => {
  try {
  
    const customer_id = req.params.customer_id;
    let businessAdmin = {};
    businessAdmin.customer_id = customer_id;

    businessAdmin.old_password = req.body.old_password;
    businessAdmin.password = req.body.password;
    businessAdmin.confirm_password =req.body.confirm_password;
    if(businessAdmin.old_password!=businessAdmin.password && businessAdmin.password===businessAdmin.confirm_password)
    {
      const checkChangePasswordStatus = await CustomerService.checkChangePassword(customer_id,businessAdmin)
      if(checkChangePasswordStatus==false)
      {
        res.status(400).send({
          code: 400,
          status: "error",
          message: "Invalid Old Password",
        });
      }
      else
      {
      const updatedBusinessAdmin = await CustomerService.customerChangePassword(
        customer_id,
        businessAdmin
      );  
      res.status(200).send({
        code: 200,
        status: "success",
        message: "Password Changed Successfully. " + updatedBusinessAdmin.email,
        data:  updatedBusinessAdmin
      });
    }
    }
    else
    {
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Invalid Password ",
      });
    } 
} catch (error) {
  res.status(400).send({
    code: 400,
    status: "error",
    message: "Error in update  ",
    data: []
  });
}
};
CustomerController.forgetChangePassword = async (req, res) => {
  try {
    let isUser = await UserService.getUserByEmailId(req.params.email);
    if(!isUser) return res.status(401).send({code:401,message:"This email is not registered,Please register.",success:false})
    const email = req.params.email;
    let businessAdmin = {};
    businessAdmin.email = email;
    businessAdmin.password = req.body.password;
    businessAdmin.confirm_password =req.body.confirm_password;
    if(businessAdmin.password===businessAdmin.confirm_password)
    {
      const updatedBusinessAdmin = await CustomerService.checkChangePasswordByEmail(
        email,
        businessAdmin
      );  
      if(updatedBusinessAdmin) {
        res.status(200).send({
          code: 200,
          status: "success",
          message: "Password Changed Successfully. " + updatedBusinessAdmin.email,
          data:  updatedBusinessAdmin
        });

      } else{
        res.status(400).send({
          code: 400,
          status: "error",
          message: "invalid access",
        });
      }    
    }
    else
    {
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Invalid Password ",
      });
    } 
} catch (error) {
  res.status(400).send({
    code: 400,
    status: "error",
    message: "Error in update",
    data: []
  });
}
};

CustomerController.generateOTP = async (req,res) => {
  try{
    let result;
    let isUser = await UserService.getUserByEmailId(req.params.email);
    if(!isUser) return res.status(401).send({code:401,message:"This email is not registered,Please register.",success:false})
    if(req.params.type == 'sign_up'){
      result=  await signUpOTP(req.params.email);
    }else{
      result= await forgetPasswordOTP(req.params.email);
    }
    if(result != 0){
      res.status(200).send({
        code: 200,
        status: "success",
        message: "OTP"
      });
    }else{
      res.status(200).send({
        code: 200,
        status: "invalid email",
        message: "OTP"
      });
    }
      
  }catch (error) {
    res.status(401).send({
      code: 401,
      status: "error",
      message: "Error Occured",
      data: {}
    });
  }
  
}
async function forgetPasswordOTP(email){
  let setting = await SettingsService.getAllsettings();
  let duration;
  if(setting && setting.otp_duration){
    duration =   setting.otp_duration || 30;
  }else{
    duration = 30
  }
  let m = '';
  if(duration <= 59){
    m = "minutes"
  }else{
    m = 'hours'
  }
  let eCode = Math.floor(10000 + Math.random() * 90000);
  let mOTP = Math.floor(100000 + Math.random() * 900000);
  let customer = {}
    customer.otp = {};
    customer.otp.ecode = eCode;
    customer.otp.mcode = mOTP;
    customer.otp.expires = new Date().setMinutes(new Date().getMinutes() + duration);
    const customerResult = await  CustomerService.addOTP(email,customer);
    
    if(!customerResult) return 0;
     var imagepath = api_end_point + '/image/logo/';

  let EMAIL_OTP_HTML = await fs.readFileSync(
    path.join(__dirname, "../../EmailTemplates/otp.html"),
    "utf-8"
  );
  EMAIL_OTP_HTML = EMAIL_OTP_HTML.
  replace('{firstname}',customerResult.first_name)
  .replace('{otp}','E'+eCode)
  .replace('{time}',duration+" "+m)
  .replace('{fmcg_logo}', imagepath + 'FMCG_logo.png');
   EmailService.sendEmail(
    customerResult.email,
    "OTP for forget password",
    EMAIL_OTP_HTML,setting,[]
  );
  let sid = 'FMCGmarkets'
  if(customerResult.country_code=='91'|| customerResult.country_code=='+91'){
    sid ='FMCGmt'
  }else if(customerResult.country_code=='+966'|| customerResult.country_code=='966'){
    sid ='FMCGmrkt-AD'
  }else{
    sid = 'FMCGmarkets';
  }
  let message = 'Dear Customer, Your OTP is '+mOTP+'. Do not share your OTP'
           request({
            uri: config.default.sms.endpoint+'mobilenumber='+customerResult.country_code+customerResult.mobile_number+'&message='+message+'&sid='+sid+'&mtype=N'+'&DR=Y',
          },(error, response, body)=>{
          console.log("OTP ->", body,customerResult.country_code+customerResult.mobile_number)
          return 0
            
          })
  ;
}
async function signUpOTP(email){
  let isUser = await UserService.getUserByEmailId(email,'');
  let setting = await SettingsService.getAllsettings();
  let duration;
  if(setting && setting.otp_duration){
    duration =   setting.otp_duration || 30;
  }else{
    duration = 30
  }

  let eCode = Math.floor(10000 + Math.random() * 90000);
  let mOTP = Math.floor(100000 + Math.random() * 900000);
  let customer = {}
  // customer.emailVerification ={};
  // customer.emailVerification.code = code;
  // customer.emailVerification.expires = new Date().setHours(new Date().getHours() + 2); 
  customer.otp = {};
  customer.otp.mcode = mOTP;
  customer.otp.ecode = eCode;
  customer.otp.expires = new Date().setMinutes(new Date().getMinutes() + duration);

  var imagepath = api_end_point + '/image/logo/';

        let EMAIL_VERIFICATION_HTML = await fs.readFileSync(
          path.join(__dirname, "../../EmailTemplates/emailVerification.html"),
          "utf-8"
        );
        const savedUser = await  CustomerService.addOTP(email,customer);
          if(!savedUser) return 0;
        customer.user_id = isUser._id;
        EMAIL_VERIFICATION_HTML = EMAIL_VERIFICATION_HTML.replace(
          "{action_url}",
          api_end_point+
          "/auth/email-verification"
        ).replace('{firstname}',isUser.first_name)
        .replace('{code}','E'+eCode)
        .replace('{fmcg_logo}', imagepath + 'FMCG_logo.png');
        // toMail, subject, body, setting, attachment
         EmailService.sendEmail(
           email,
          "Please confirm your Email account",
          EMAIL_VERIFICATION_HTML,setting,[]
        );
        let sid = 'FMCGmarkets'
      if(isUser.country_code=='91'|| isUser.country_code=='+91'){
        sid ='FMCGmt'
      }else if(isUser.country_code=='+966'|| isUser.country_code=='966'){
        sid ='FMCGmrkt-AD'
      }else{
        sid = 'FMCGmarkets';
      }
            let message = 'Dear Customer, Thank you for registering on FMCG . Your OTP is '+mOTP+'.  Do not share your OTP.'
       request({
          uri: config.default.sms.endpoint+'mobilenumber='+isUser.country_code+isUser.mobile_number+'&message='+message+'&sid='+sid+'&mtype=N'+'&DR=Y',
        },(error, response, body)=>{
        console.log("signUpOTP ->", body,isUser.country_code+isUser.mobile_number)
          return 0
        })
}
CustomerController.verifyOtp = async (req,res) =>{
  try {
    let isUser = await UserService.getUserByEmailId(req.params.email);
    let data = await CustomerService.verifyOtp(req.params.otp,req.params.email,isUser.status);
    if(data){
      res.status(200).send({
        code: 200,
        status: "success",
        message: "OTP verified successfully !",
        email: req.params.email
      });
    }else{
      res.status(400).send({
        code: 400,
        status: "ERROR",
        message: "Invalid OTP Or OTP Expired",
      });
    }
  } catch (error) {
    res.status(401).send({
      code: 401,
      status: "error",
      message: "Error Occured",
      data: {}
    });
  }

}
CustomerController.changeStatus= async (req, res) => {
  try {
    let status = req.body.status
    const customer = await CustomerService.changeStatus(req.params.id, status);
    res.status(200).send({
      code: 200,
      status: "success",
      message: "customer status is  changed",
      data: customer
    });
  } catch (error) {
    res.status(401).send({
      code: 401,
      status: "error",
      message: "Error Occured",
      data: {}
    });
  }
};
CustomerController.takeAction = async (req,res) =>{
  try {
    let status = req.body.status
    let heading,message;  
    if(status == "active"){
      heading = "Your account is Activated";
      message = "Welcome to  FMCG!"
    }else{
      heading = "Your account is rejected";
      message = "Your account is rejected by  FMCG"
    }
    const customer = await CustomerService.changeStatus(req.params.id, status);
    let setting = await SettingsService.getAllsettings();
    var imagepath = api_end_point + '/image/logo/';

    let EMAIL_ACTION_HTML = await fs.readFileSync(
      path.join(__dirname, "../../EmailTemplates/customerConfirmationEmail.html"),
      "utf-8"
    );
    EMAIL_ACTION_HTML = EMAIL_ACTION_HTML.
    replace('{firstname}',customer.first_name)
    .replace('{heading}',heading)
    .replace('{message}',message)
    .replace('{fmcg_logo}', imagepath + 'FMCG_logo.png');
     EmailService.sendEmail(
      customer.email,
      heading,
      EMAIL_ACTION_HTML,setting,[]
    );
    res.status(200).send({
      code: 200,
      status: "success",
      message: "customer status is  changed",
      data: customer
    });
  } catch (error) {
    res.status(401).send({
      code: 401,
      status: "error",
      message: "Error Occured",
      data: {}
    });
  }

}
CustomerController.deleteCustomer = async (req, res) => {
  try {
    const id = req.params.id;
    const users = await CustomerService.deleteCustomer(id);
    res.send({
      code: 200,
      status: "success",
      message: "Sucessfully User Deleted",
      data: users
    });
  } catch (error) {
    logger.error("Error in delete User" + error);
    res.status(400).send({
      code: 400,
      status: "error",
      message: "Error in delete User" ,
      data: {}
    });
  }
};

CustomerController.emailVerification = async (req,res) => {
  try {
    let code = req.params.code;
    let user_id = req.params.id;
    const isExpire = await CustomerService.emailVerification(code,user_id);
    if(isExpire){
      res.status(200).send({
        code: 200,
        status: "success",
        message: "email varified",
      });
    }else{
      res.status(400).send({
        code: 400,
        status: "Error",
        message: "Invalid Code or Code expired !",
      });
      
    }
    
  } 
  catch (error) {
    res.status(401).send({
      code: 401,
      status: "error",
      message: "Error Occured",
      data: {}
    });
  } 
}

CustomerController.updateCustomerProfile = async (req,res) =>{
  try { customer_id
    let customer_id = req.params.customer_id;
    let Customer = {};
    Customer.first_name = req.body.first_name;
    Customer.last_name = req.body.last_name;
    Customer.country_name = req.body.country_name;
    Customer.country_code = req.body.country_code;
    Customer.permanent_address = req.body.permanent_address;
    Customer.company_name = req.body.company_name;
    Customer.shipping_address = req.body.shipping_address;
    Customer.city = req.body.city;
    Customer.mobile_number = req.body.mobile_number;
    Customer.phone_number = req.body.phone_number;
    Customer.email = req.body.email;
    let savedUser = await CustomerService.updateCustomerProfileById(customer_id,Customer);      
      res.status(200).send({
        code: 200,
        status: "success",
        message: "Sucessfully profile updated",
        data: savedUser
      });
    
  } catch (error) {
    logger.error("Error in update" + error);
    res.status(400).send({
      code: 400,
      status: "error",
      message: "Error in update:" + error.message,
      data: []
    });
  }
}

CustomerController.getAllCountries = async (req, res) => {
try {
  res.status(200).send({
    code: 200,
    status: "success",
    message: "countries list",
    countries: countries
  });
} catch (error) {
  res.status(400).send({
    code: 400,
    status: "error",
    message: "Error",
  });
}
}
export default CustomerController;
