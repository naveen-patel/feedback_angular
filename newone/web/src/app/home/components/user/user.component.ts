import { Component, OnInit, ChangeDetectorRef, TemplateRef } from '@angular/core';
import { DataService } from "../../services/data.service";
import 'rxjs/Rx';
declare var $: any;
declare var require: any;
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
var _ = require('lodash');
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  imageinfo = {};
  modalRef2: BsModalRef;
  modalRef3: BsModalRef;
  modalRef4: BsModalRef;
  file_details: string;
  records = [];
  upload_records = [];
  openAddUser: boolean = false;
  parentmessage: any;
  loader: boolean = false;
  search: string = '';
  limit: any = 10;
  skip: any = 1;
  count: Number;
  Allrecords = [];
  id: string;
  active: boolean = true;
  remainingRecords:any = 0;
  tabChange : boolean =true;
  status:String='';
  selectedRecord ;

  constructor(
    public ref: ChangeDetectorRef,
    public ds: DataService,
    private modalService: BsModalService,
  ) { 
  }


  ngOnInit() {
    var initial = (this.skip === 1)
    this.ds.getAllUserList(this.search, this.limit, this.skip, "user", this.getStatus()).subscribe(data => {
      this.loader = false;
      this.count = data.totalcount;
      //this.records = data.users;
      this.tabChange=false;
      this.remainingRecords = data.totalcount - (this.skip * this.limit);

      if(initial){
        this.records = data.users;
      }else{
        this.records = this.records.concat(data.users);
        // data.users.forEach(element => {
        //   this.records.push(element)
        // });
      }
    })
  }
  searchUser() {
    this.loader = true;
    this.ds.getAllUserList(this.search, this.limit, this.skip, "user", this.getStatus()).subscribe(data => {
      this.loader = false;
      this.count = data.totalcount;
      this.records = data.users;
    })
  }
  showInactiveUsers() {
    this.search = "";
    this.skip = 1;
    this.active = false;
    this.ngOnInit();
  }
  showactiveUsers() {
    this.search = "";
    this.skip = 1;
    this.active = true;
    this.ngOnInit();
  }
  reload() {
    this.search = "";
    this.skip = 1;
    this.ngOnInit();
  }
  getStatus() {
    if (this.active) {
      return "active";
    } else {
      return "in_active";
    }
  }
  pageChanged(event) {
    this.skip = event.page;
    this.ngOnInit();
  }
  openUser(record) {
    this.openAddUser = true;
    this.parentmessage = record
  }
  receiveMessage(event) {
    this.openAddUser = event[0];
    this.ngOnInit()
    // this.resetSearchAndSort();
  }
  DeleteRecord(template: TemplateRef<any>, i, id) {
    this.modalRef3 = this.modalService.show(template);
    this.id = id;
  }
  
  confirmToDelete() {

    this.ds.deleteUser(this.id).subscribe(data => {
      this.ds.successMessage('Mobile user Deleted Successfully')
      this.reload();
    })
    this.modalRef3.hide();
    this.modalRef3 = null
  }
  // changestatus(value, id) {
  //   this.ds.changeStatus(value, id).subscribe(data => {
  //     this.ngOnInit();
  //   })
  // }
  confirmToChange() {
    var status = this.selectedRecord.status === 'active' ? 'in_active' : 'active';
    this.ds.changeStatus(status, this.selectedRecord._id).subscribe(data => {
      this.skip=1;
      this.ngOnInit();
      this.modalRef4.hide();
      this.modalRef4 = null
    })
  }
  declineToChange(){
    this.skip=1;
    this.ngOnInit();
    this.modalRef4.hide();
    this.modalRef4 = null
  }
  changeStatus(template: TemplateRef<any>,data) {
    this.status = data.status === 'active' ? 'In-Active' : 'Active';
    this.modalRef4 = this.modalService.show(template);
    this.selectedRecord=data;
  }
  loadMore(){
    this.skip++;
    this.ngOnInit();
  }

}
