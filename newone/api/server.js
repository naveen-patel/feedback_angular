//Max-Brands API CodeBase
//ProjectName : Max-Brands
//Version : 1.0.0
//include all the packages necessary
import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import morgan from 'morgan';
import glob from 'glob';
import helmet  from'helmet';
import xssFilter  from 'x-xss-protection';
import mongoSanitize from'express-mongo-sanitize';
//include the files 
import settings from './settings';
import connectToMongoDb from './config/database.config';
import MongoBackup from './server/modules/backup/backup.controller';
import logger from './config/logger.config';
import apiRouters from './router';
import http from 'http';
const fileUpload = require('express-fileupload');
const path = require("path");
let config = require('./config/' + settings.environment + '.config');
var mkdirp = require('mkdirp');
var CronJob = require('cron').CronJob;
//Logger Configuration
logger.stream = {
    write: function (message, encoding) {
        logger.info(message);
    }
};
//set the port
const port = settings.port;

const app = express();
app.use(helmet());


// app.use('/api',apiLimit);
// Body Parser
app.use(express.json({ limit: '10kb' })); // Body limit is 10
// Data Sanitization against NoSQL Injection Attacks
app.use(mongoSanitize());
// Data Sanitization against XSS attacks
app.use(xssFilter());
const server = http.createServer(app)

connectToMongoDb();

app.use(cors());
app.disable('x-powered-by')
app.use(bodyParser.json({
    extended: true,
    limit: '1mb'
}));
app.use(bodyParser.urlencoded({
    extended: true,
    limit: '1mb'
}));
app.use(fileUpload());
app.use(morgan("dev", {
    "stream": logger.stream
}));

if (!config.default.media.useS3) {

    mkdirp(config.default.media.local_file_path, function (err) {
        if (err) {
            console.log("****************************************************************************")
            console.log("*********************Run the Application in Root Mode **********************")
            console.log("*************Or Make sure the folder has permission to run it*******")
            console.log("*********************This is for mainitiang media files*********************")
            console.log("****************************************************************************")
            console.error(err)
        } else console.log('Media File Path exists: ' + config.default.media.local_file_path)
    });

    app.use('/static', express.static(config.default.media.local_file_path))
    app.use('/image', express.static(__dirname))
}

app.use(express.static(path.join(__dirname, 'dist')));
app.get('/health_check', function (req, res, next) {
    res.sendStatus(200);
});

app.get('/auth/login', function (req, res, next) {
    res.redirect(301, config.default.api_end_point);
});
//include all the necessary routes in the express
apiRouters.forEach(function (apiRoute) {
    app.use('/api', apiRoute);
});
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
});

//deleting the unwanted socketid on server restart
server.listen(port, () => {
    logger.info(`Server started on port : ${port}`);
});

// cron job to take backup and upload on google drive
let job  = new CronJob('0 0 0 * * *', function() {
  logger.info("Backup Done");
  MongoBackup.takeMongoBackup();
  MongoBackup.deleteLastBackUp();
  }, null, true);

