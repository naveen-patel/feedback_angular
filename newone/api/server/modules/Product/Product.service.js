import ProductModel from "./Product.model";
import CategoryModel from "../Category/category.model";
import SupplierModel from "./../Supplier/Supplier.model";
import BrandModel from "../Brand/brand.model";
import moment from "moment";

var mongoose = require('mongoose');
import settings from '../../../settings';
let config = require('./../../../config/' + settings.environment + '.config').default;

const ProductService = {
  getProductById: async id => {
    /////ok////
    try {
      const Product = await ProductModel.findById(id).populate({
        path: 'supplier_id',
        select: 'name'
      })
        .populate({
          path: 'category_id',
          select: 'category_name'
        })
        .populate({
          path: 'brand',
          select: 'brand_name'
        }).populate({
          path: 'deal',
          select: 'deal_end_date offer_price_usd offer_price_aed deal_percentage'
        }).lean();
      if (Product) {
        let tempobj = {};
        if (Product.deal) {
          let date = moment().startOf('day').format("YYYY-MM-DD")
          let month = new Date(Product.deal.deal_end_date).getMonth() + 1;
          let day;
          if (new Date(Product.deal.deal_end_date).getDate() <= 9) {
            day = '0' + new Date(Product.deal.deal_end_date).getDate()
          } else {
            day = new Date(Product.deal.deal_end_date).getDate()
          }
          let customDate = new Date(Product.deal.deal_end_date).getFullYear() + '-' + month + '-' + day;
          if (customDate >= date.toString()) {
            tempobj.isActive = true;
            tempobj.sales_per_case_usd = Product.deal.offer_price_usd
            tempobj.sales_per_case_aed = Number(Product.deal.offer_price_aed).toFixed(2)
            tempobj.deal_percentage = Product.deal.deal_percentage
            Product.deal = tempobj

          } else {
            tempobj.isActive = false;
            tempobj.sales_per_case_usd = null
            tempobj.sales_per_case_aed = null
            tempobj.deal_percentage = null
            Product.deal = tempobj
          }

        } else {
          tempobj.isActive = false;
          tempobj.sales_per_case_usd = null
          tempobj.sales_per_case_aed = null
          tempobj.deal_percentage = null

          Product.deal = tempobj
        }
        Product.sales_per_case_aed = Number(Product.sales_per_case_aed).toFixed(2);
        Product.supplier_detail = Product.supplier_id ? Product.supplier_id : {};
        delete Product.supplier_id;
        //Product.product_code = (Product.product_name.replace(/\s/g, '') + "prod").substring(0, 4) + Product.product_code;
        if (Product.product_front_image && Product.product_front_image !== "") {
          Product.product_front_image = config.api_end_point + Product.product_front_image;
        } else {
          Product.product_front_image = "";
        }
        if (Product.product_back_image && Product.product_back_image !== "") {
          Product.product_back_image = config.api_end_point + Product.product_back_image;
        } else {
          Product.product_back_image = "";
        }
      }
      return Product;
    } catch (error) {
      throw error;
    }
  },
  getProduct: async id => {
    try {
      const Product = await ProductModel.findById(id)

      return Product;
    } catch (error) {
      throw error;
    }
  },
  getAllProductForPDF: async (isAll, brands) => {
    try {
      var findObj = {}

      if (!isAll) {
        var brandNames = [];
        brands.forEach(function (data) {
          var regex = new RegExp(["^", data, "$"].join(""), "i");
          brandNames.push(regex)
        });
        let tempobj = {
          $in: brandNames
        }
        findObj['brands.brand_name'] = tempobj
      }
      const Product = await ProductModel.aggregate(
        [{
          '$lookup': {
            from: 'supplier',
            localField: 'supplier_id',
            foreignField: '_id',
            as: 'suppliers'
          }
        }, {
          $unwind: "$suppliers"
        },
        {
          '$lookup': {
            from: 'category',
            localField: 'category_id',
            foreignField: '_id',
            as: 'categorys'
          }
        }, {
          $unwind: "$categorys"
        },
        {
          '$lookup': {
            from: 'brand',
            localField: 'brand',
            foreignField: '_id',
            as: 'brands'
          }
        }, {
          $unwind: "$brands"
        },
        { $match: findObj },
        {
          $project: {
            'product_code': 1,
            'product_type': 1,
            'product_code': 1,
            'product_type': 1,
            'description': 1,
            'size': 1,
            'language': 1,
            'ru_per_case': 1,
            'quantity': 1,
            'expiry_date': 1,
            'cost_price_per_case': 1,
            'sales_per_case_aed': 1,
            'sales_per_case_usd': 1,
            'origin': 1,
            'ean_code_ru': 1,
            'ean_code_case': 1,
            'product_front_image': 1,
            'product_back_image': 1,
            "_id": '$suppliers._id',
            'name': '$suppliers.name',
            'category_id': '$categorys._id',
            'category_name': '$categorys.category_name',
            'brand_id': '$brands._id',
            'brand_name': '$brands.brand_name'
          }
        }]
      );
      // const Products = await ProductModel.find(findObj);
      return Product;
    } catch (error) {
      throw error;
    }
  },
  getAllProduct: async () => {
    /////ok////
    try {
      const Product = await ProductModel.aggregate(
        [{
          '$lookup': {
            from: 'supplier',
            localField: 'supplier_id',
            foreignField: '_id',
            as: 'suppliers'
          }
        }, {
          $unwind: "$suppliers"
        },
        {
          '$lookup': {
            from: 'category',
            localField: 'category_id',
            foreignField: '_id',
            as: 'categorys'
          }
        }, {
          $unwind: "$categorys"
        },
        {
          '$lookup': {
            from: 'brand',
            localField: 'brand',
            foreignField: '_id',
            as: 'brands'
          }
        }, {
          $unwind: "$brands"
        },
        {
          $project: {
            'product_code': 1,
            'product_type': 1,
            'product_code': 1,
            'product_type': 1,
            'description': 1,
            'size': 1,
            'language': 1,
            'ru_per_case': 1,
            'quantity': 1,
            'expiry_date': 1,
            'cost_price_per_case': 1,
            'sales_per_case_aed': 1,
            'sales_per_case_usd': 1,
            'origin': 1,
            'ean_code_ru': 1,
            'ean_code_case': 1,
            'product_front_image': 1,
            'product_back_image': 1,
            "_id": '$suppliers._id',
            'name': '$suppliers.name',
            'category_id': '$categorys._id',
            'category_name': '$categorys.category_name',
            'brand_id': '$brands._id',
            'brand_name': '$brands.brand_name'
          }
        }]
      );
      return Product;
    } catch (error) {
      throw error;
    }
  },
  getAllProductByBrandForExcel: async (brands) => {
    /////ok////
    // var brandNames = []
    // brands.forEach(function (data) {
    //   var regex = new RegExp(["^", data, "$"].join(""), "i");
    //   brandNames.push(regex)
    // });
    try {
      const Product = await ProductModel.aggregate(
        [{
          '$lookup': {
            from: 'supplier',
            localField: 'supplier_id',
            foreignField: '_id',
            as: 'suppliers'
          }
        }, {
          $unwind: "$suppliers"
        },
        {
          '$lookup': {
            from: 'category',
            localField: 'category_id',
            foreignField: '_id',
            as: 'categorys'
          }
        }, {
          $unwind: "$categorys"
        },
        {
          '$lookup': {
            from: 'brand',
            localField: 'brand',
            foreignField: '_id',
            as: 'brands'
          }
        }, {
          $unwind: "$brands"
        },
        {
          "$match": {
            "brands._id": {
              '$in': brands
            }
          }
        },
        {
          $project: {
            'product_code': 1,
            'product_type': 1,
            'product_code': 1,
            'brand_name': 1,
            'product_type': 1,
            'description': 1,
            'size': 1,
            'language': 1,
            'ru_per_case': 1,
            'quantity': 1,
            'expiry_date': 1,
            'cost_price_per_case': 1,
            'sales_per_case_aed': 1,
            'sales_per_case_usd': 1,
            'origin': 1,
            'ean_code_ru': 1,
            'ean_code_case': 1,
            'product_front_image': 1,
            'product_back_image': 1,
            "_id": '$suppliers._id',
            'name': '$suppliers.name',
            'category_id': '$categorys._id',
            'category_name': '$categorys.category_name',
            'brand_id': '$brands._id',
            'brand_name': '$brands.brand_name'
          }
        }]);
      return Product;
    } catch (error) {
      throw error;
    }
  },
  getAllProductByIdForExcel: async (ids) => {
    /////ok////
    try {
      var idList = [];
      ids.forEach(function (data) {
        idList.push(mongoose.Types.ObjectId(data))
      });
      const Product = await ProductModel.aggregate(
        [{
          '$lookup': {
            from: 'supplier',
            localField: 'supplier_id',
            foreignField: '_id',
            as: 'suppliers'
          }
        }, {
          $unwind: "$suppliers"
        },
        {
          '$lookup': {
            from: 'category',
            localField: 'category_id',
            foreignField: '_id',
            as: 'categorys'
          }
        }, {
          $unwind: "$categorys"
        },
        {
          '$lookup': {
            from: 'brand',
            localField: 'brand',
            foreignField: '_id',
            as: 'brands'
          }
        }, {
          $unwind: "$brands"
        },
        {
          "$match": {
            "_id": {
              '$in': idList
            }
          }
        },
        {
          $project: {
            'product_code': 1,
            'product_type': 1,
            'product_code': 1,
            'brand_name': 1,
            'product_type': 1,
            'description': 1,
            'size': 1,
            'language': 1,
            'ru_per_case': 1,
            'quantity': 1,
            'expiry_date': 1,
            'cost_price_per_case': 1,
            'sales_per_case_aed': 1,
            'sales_per_case_usd': 1,
            'origin': 1,
            'ean_code_ru': 1,
            'ean_code_case': 1,
            'product_front_image': 1,
            'product_back_image': 1,
            "_id": '$suppliers._id',
            'name': '$suppliers.name',
            'category_id': '$categorys._id',
            'category_name': '$categorys.category_name',
            'brand_id': '$brands._id',
            'brand_name': '$brands.brand_name'
          }
        }]);
      return Product;
    } catch (error) {
      throw error;
    }
  },
  getProductByPagination: async (searchkey, limit, skip, supplierId, category_id) => {
    /////ok////
    try {
      var json = {},
        findObj = {};
      if (supplierId === "") {
        findObj = {
          "brand_name": {
            "$regex": searchkey,
            "$options": "i"
          }
        }
      } else {
        findObj = {
          "brand_name": {
            "$regex": searchkey,
            "$options": "i"
          },
          supplier_id: mongoose.Types.ObjectId(supplierId),
          category_id: mongoose.Types.ObjectId(category_id)
        }
      }
      const count = await ProductModel.find(findObj).populate({
        path: 'category_id',
        select: 'category_name'
      }).populate({
        path: 'brand',
        select: 'brand_name'
      }).count();
      const product = await ProductModel.find(findObj).populate({
        path: 'category_id',
        select: 'category_name'
      }).populate({
        path: 'brand',
        select: 'brand_name'
      }).sort({
        "createdAt": -1
      }).skip(skip).limit(limit).lean();
      var productArray = []
      product.forEach(element => {
        //element.product_code = (element.product_name.replace(/\s/g, '') + "prod").substring(0, 4) + element.product_code;
        if (element.product_front_image && element.product_front_image !== "") {
          element.product_front_image = config.api_end_point + element.product_front_image;
        } else {
          element.product_front_image = "";
        }
        if (element.product_back_image && element.product_back_image !== "") {
          element.product_back_image = config.api_end_point + element.product_back_image;
        } else {
          element.product_back_image = "";
        }
        productArray.push(element)
      });
      json.totalpage = Math.ceil(count / limit);
      json.totalcount = count;
      json.product = productArray
      return json;
    } catch (error) {
      throw error;
    }
  },
  getLastProduct: async () => {
    /////ok////
    try {
      const lastProduct = await ProductModel.find({}).sort({
        createdAt: -1
      }).limit(1);
      return lastProduct;
    } catch (error) {
      throw error;
    }
  },
  getProductByIds: async (ids) => {
    try {
      var data = await ProductModel.find({
        _id: {
          $in: ids
        }
      }).populate({
        path: 'supplier_id',
        select: 'name'
      })
        .populate({
          path: 'category_id',
          select: 'category_name'
        })
        .populate({
          path: 'brand',
          select: 'brand_name'
        }).lean();
      return data;
    } catch (error) {
      throw error;
    }
  },

  createProduct: async newProduct => {
    /////ok////
    try {
      var savedProduct = await new ProductModel(newProduct).save();
      return savedProduct
    } catch (error) {
      throw error;
    }
  },
  updateProduct: async (id, data) => {
    /////ok////
    try {
      const update = await ProductModel.findOneAndUpdate({
        _id: id
      }, data, {
        strict: true,
        new: true
      });
      return update;
    } catch (error) {
      throw error;
    }
  },

  getBrands: async () => {
    /////ok////
    try {
      const brands = await ProductModel.aggregate([{
        $group: {
          _id: {
            $toLower: '$brand_name'
          }
        }
      }]);;
      var brandList = []
      brands.forEach(brand => {
        brandList.push(brand._id.toUpperCase())
      });
      return brandList;

    } catch (error) {
      throw error;
    }
  },
  getProductByBrand: async (searchkey, isAll, brands, isSupplier, suppliers, limit, skip) => {
    /////ok////
    var json = {}
    try {
      var findObj = {};
      if (searchkey != "") {
        findObj = {
          $or: [{
            "description": {
              "$regex": searchkey,
              "$options": "i"
            }
          }, {
            "product_type": {
              "$regex": searchkey,
              "$options": "i"
            }
          }, {
            "product_code": {
              "$regex": searchkey,
              "$options": "i"
            }
          }, {
            "brands.brand_name": {
              "$regex": searchkey,
              "$options": "i"
            }
          },]
        }
      }
      if (!isAll) {
        var brandNames = [];
        brands.forEach(function (data) {
          var regex = new RegExp(["^", data, "$"].join(""), "i");
          brandNames.push(regex)
        });
        let tempobj = {
          $in: brandNames
        }
        findObj['brands.brand_name'] = tempobj
      }
      if (!isSupplier) {
        let supplierNames = [];
        suppliers.forEach(function (data) {
          var regex = new RegExp(["^", data, "$"].join(""), "i");
          supplierNames.push(regex)
        });
        let tempobj = {
          $in: supplierNames
        }
        findObj['suppliers.name'] = tempobj
      }
      const product = await ProductModel.aggregate(
        [{
          '$lookup': {
            from: 'supplier',
            localField: 'supplier_id',
            foreignField: '_id',
            as: 'suppliers'
          }
        },
        {
          '$lookup': {
            from: 'category',
            localField: 'category_id',
            foreignField: '_id',
            as: 'categorys'
          }
        },
        {
          '$lookup': {
            from: 'brand',
            localField: 'brand',
            foreignField: '_id',
            as: 'brands'
          }
        },
        { $match: findObj },
        // {$sort: {"createdAt": -1}},
        // { $sort: { "brands.code": -1 } },
        { $sort: { "code": -1 } },

        {
          '$facet': {
            metadata: [{
              $count: "total"
            }
            ],
            product_data: [{
              $skip: skip
            }, {
              $limit: limit
            }]
          }
        }, {
          $project: {
            "_id": 0,
            "totalcount": { $arrayElemAt: ["$metadata.total", 0] },
            "totalPage": { $ceil: { $divide: [{ $arrayElemAt: ["$metadata.total", 0] }, limit] } },
            "product_data": "$product_data"
          }
        }
        ]);

      var productArray = []
      product[0].product_data.forEach(element => {
        if (element.product_front_image && element.product_front_image !== "") {
          element.product_front_image = config.api_end_point + element.product_front_image;
        } else {
          element.product_front_image = "";
        }
        if (element.product_back_image && element.product_back_image !== "") {
          element.product_back_image = config.api_end_point + element.product_back_image;
        } else {
          element.product_back_image = "";
        }
        productArray.push(element)
      });
      json.totalpage = Math.ceil(product[0].totalcount / limit);
      json.totalcount = product[0].totalcount;
      json.product = productArray;
      return json;

    } catch (error) {
      throw error;
    }
  },

  getBrandImage: async (ids, search) => {
    try {
      let findObj = {
        $or: [{
          "brand_name": {
            "$regex": search,
            "$options": "i"
          }
        }]

      }
      findObj['categoryDetails.category'] = {
        $in: ids
      }

      return await BrandModel.find(findObj, { categoryDetails: 0 });
    } catch (error) {
      throw error;
    }
  },
  getProductByBrandFMCG: async (searchkey, isAll, brands, limit, skip, isLocation, locations, isCategory, categorys) => {
    /////ok////
    let json = {}
    try {
      let findObj = {};
      let key1 = searchkey.split(" ")[0];
      let key2 = searchkey.split(" ")[1];
      if (key1 === undefined || key1 == "") key1 = key2;
      if (key2 === undefined || key2 == "") key2 = key1;
      findObj.fmcg_availability = true;
      if (searchkey != "") {
        findObj = {
          $or: [
            //   {
            //   "description": {
            //     "$regex": searchkey,
            //     "$options": "i"
            //   }
            // },
            {
              "product_type": {
                "$regex": key1,
                "$options": "i"
              }
            },
            {
              "product_type": {
                "$regex": key2,
                "$options": "i"
              }
            }, {
              "product_code": {
                "$regex": key1,
                "$options": "i"
              }
            },
            {
              "product_code": {
                "$regex": key2,
                "$options": "i"
              }
            }, {
              "brands.brand_name": {
                "$regex": key1,
                "$options": "i"
              }
            }, {
              "brands.brand_name": {
                "$regex": key2,
                "$options": "i"
              }
            }]
        }
      }
      if (!isAll) {
        var brandNames = [];
        brands.forEach(function (data) {
          var regex = new RegExp(["^", data, "$"].join(""), "i");
          brandNames.push(regex)
        });
        let tempobj = {
          $in: brandNames
        }
        findObj['brands.brand_name'] = tempobj
      }
      if (!isLocation) {
        let locationName = [];
        locations.forEach(data => {
          let regex = new RegExp(["^", data, "$"].join(""), "i");
          locationName.push(regex);
        });
        let tempobj = {
          $in: locationName
        }
        findObj['stock_location'] = tempobj
      }
      if (!isCategory) {
        let categoryName = [];
        categorys.forEach(data => {
          let regex = new RegExp(["^", data, "$"].join(""), "i");
          categoryName.push(regex);
        });
        let tempobj = {
          $in: categoryName
        }
        findObj['categorys.category_name'] = tempobj
      }

      const product = await ProductModel.aggregate(
        [{
          '$lookup': {
            from: 'supplier',
            localField: 'supplier_id',
            foreignField: '_id',
            as: 'suppliers'
          }
        },
        {
          '$lookup': {
            from: 'category',
            localField: 'category_id',
            foreignField: '_id',
            as: 'categorys'
          }
        },
        {
          '$lookup': {
            from: 'brand',
            localField: 'brand',
            foreignField: '_id',
            as: 'brands'
          }
        },
        {
          '$lookup': {
            from: 'deal',
            localField: 'deal',
            foreignField: '_id',
            as: 'deals'
          }
        },
        { $match: findObj },
        { $sort: { "createdAt": -1 } },
        {
          '$facet': {
            metadata: [{
              $count: "total"
            }
            ],
            product_data: [{
              $skip: skip
            }, {
              $limit: limit
            }]
          }
        }, {
          $project: {
            "_id": 0,
            "totalcount": { $arrayElemAt: ["$metadata.total", 0] },
            "totalPage": { $ceil: { $divide: [{ $arrayElemAt: ["$metadata.total", 0] }, limit] } },
            "product_data": "$product_data"
          }
        }
        ]);
      var productArray = []
      product[0].product_data.forEach(element => {
        let tempobj = {};
        let date = moment().startOf('day').format("YYYY-MM-DD")
        if (element.deals && element.deals.length >= 1) {
          let month = new Date(element.deals[0].deal_end_date).getMonth() + 1;
          let day;
          if (new Date(element.deals[0].deal_end_date).getDate() <= 9) {
            day = '0' + new Date(element.deals[0].deal_end_date).getDate()
          } else {
            day = new Date(element.deals[0].deal_end_date).getDate()
          }
          let customDate = new Date(element.deals[0].deal_end_date).getFullYear() + '-' + month + '-' + day;
          if (customDate >= date.toString()) {
            tempobj.isActive = true;
            tempobj.sales_per_case_usd = element.deals[0].offer_price_usd
            tempobj.sales_per_case_aed = element.deals[0].offer_price_aed
            tempobj.deal_percentage = element.deals[0].deal_percentage
            element.deal = {};
            Object.assign(element.deal, tempobj)
            delete element.deals;

          } else {
            tempobj.isActive = false;
            tempobj.sales_per_case_usd = null
            tempobj.sales_per_case_aed = null
            tempobj.deal_percentage = null
            element.deal = {};
            Object.assign(element.deal, tempobj)
            delete element.deals;
          }

        } else {
          tempobj.isActive = false;
          tempobj.sales_per_case_usd = null
          tempobj.sales_per_case_aed = null
          tempobj.deal_percentage = null
          element.deal = {};
          Object.assign(element.deal, tempobj)
          delete element.deals;
        }

        if (element.product_front_image && element.product_front_image !== "") {
          element.product_front_image = config.api_end_point + element.product_front_image;
        } else {
          element.product_front_image = "";
        }
        if (element.product_back_image && element.product_back_image !== "") {
          element.product_back_image = config.api_end_point + element.product_back_image;
        } else {
          element.product_back_image = "";
        }
        productArray.push(element)
      });

      json.totalpage = Math.ceil(product[0].totalcount / limit);
      json.totalcount = product[0].totalcount;
      json.product = productArray;
      return json;

    } catch (error) {
      throw error;
    }
  },
  getProducts: async () => {
    try {
      let product = await ProductModel.find()
        .populate({ path: "brand", select: "brand_name" })
        .populate({ path: "supplier_id", select: "name" })
        .populate({ path: "deal", select: "deal_name deal_percentage offer_price_aed offer_price_usd deal_start_date deal_end_date" })
        .sort({
          "createdAt": -1
        }).lean();
      var productArray = []
      product.forEach(element => {
        let tempobj = {};
        let date = moment().startOf('day').format("YYYY-MM-DD")
        if (element.deal) {
          let month = new Date(element.deal.deal_end_date).getMonth() + 1;
          let day;
          if (new Date(element.deal.deal_end_date).getDate() <= 9) {
            day = '0' + new Date(element.deal.deal_end_date).getDate()
          } else {
            day = new Date(element.deal.deal_end_date).getDate()
          }
          let customDate = new Date(element.deal.deal_end_date).getFullYear() + '-' + month + '-' + day;
          if (customDate >= date.toString()) {
            tempobj.isActive = true;
            tempobj.sales_per_case_usd = element.deal.offer_price_usd
            tempobj.sales_per_case_aed = element.deal.offer_price_aed
            tempobj.deal_percentage = element.deal.deal_percentage
            element.deal = {};
            Object.assign(element.deal, tempobj)

          } else {
            tempobj.isActive = false;
            tempobj.sales_per_case_usd = null
            tempobj.sales_per_case_aed = null
            tempobj.deal_percentage = null
            element.deal = {};
            Object.assign(element.deal, tempobj)
          }

        } else {
          tempobj.isActive = false;
          tempobj.sales_per_case_usd = null
          tempobj.sales_per_case_aed = null
          tempobj.deal_percentage = null
          element.deal = {};
          Object.assign(element.deal, tempobj)


        }


        if (element.product_front_image && element.product_front_image !== "") {
          element.product_front_image = config.api_end_point + element.product_front_image;
        } else {
          element.product_front_image = "";
        }
        if (element.product_back_image && element.product_back_image !== "") {
          element.product_back_image = config.api_end_point + element.product_back_image;
        } else {
          element.product_back_image = "";
        }
        productArray.push(element)
      });
      return productArray
    } catch (error) {
      throw error;
    }

  },
  updateDealInProduct: async (deal_id, id) => {
    try {
      const update = await ProductModel.findOneAndUpdate({
        _id: id
      }, { deal: deal_id }, {
        upsert: true
      });
      return update;
    } catch (error) {
      throw error;
    }
  },
  updateQuantity: async (id, quantity) => {
    try {
      const update = await ProductModel.findOneAndUpdate({
        _id: id
      }, { quantity: quantity }, {
        upsert: true
      });
      return update;
    } catch (error) {
      throw error;
    }
  },
  deleteProduct: async (id) => {
    try {
      var data = await ProductModel.remove({
        "_id": mongoose.Types.ObjectId(id)
      });
      return data;
    } catch (error) {
      throw error;
    }
  },
  deleteProductByIds: async (ids) => {
    try {
      var data = await ProductModel.remove({
        _id: {
          $in: ids
        }
      });;
      return data;
    } catch (error) {
      throw error;
    }
  },
  getProductsBySupplierIds: async (ids) => {
    try {
      var data = await ProductModel.find({
        supplier_id: {
          $in: ids
        }
      });
      return data;
    } catch (error) {
      throw error;
    }
  },
  deleteProductsBySupplierIds: async (ids) => {
    try {
      var data = await ProductModel.remove({
        supplier_id: {
          $in: ids
        }
      });
      return data;
    } catch (error) {
      throw error;
    }
  },
  checkBrands: async (ids, number) => {
    try {
      let obj = {}
      if (number == 1) {
        obj.brand = {
          $in: ids
        }
      } else {
        obj.category_id = {
          $in: ids
        }
      }

      var data = await ProductModel.findOne(obj);
      return data;
    } catch (error) {
      throw error;
    }
  }
};
export default ProductService;