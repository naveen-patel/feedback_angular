import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { DataService } from '../../app/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { AuthService } from "angularx-social-login";
import { SocialUser } from "angularx-social-login";
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  private user: SocialUser;
  private loggedIn: boolean;
  constructor(
    private formBuilder: FormBuilder,
    // tslint:disable-next-line:no-shadowed-variable
    private DataService: DataService,
    private toastr: ToastrService,
    private route: Router,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email_id: ['', [Validators.required, Validators.pattern(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/)]],
      password: ['', [Validators.required, Validators.pattern(/^(?=.*\d).{5,16}$/)]]
    });
    this.authService.authState.subscribe((user) => {
      console.log('init call');

      this.user = user;
      this.loggedIn = (user != null);
      this.socialLogin()
      // console.log('Usersss',this.user);
      // this.toastr.success('Wel-come!!!');
      // window.sessionStorage.setItem('token', JSON.stringify(this.user.authToken));
      // this.route.navigate(['/dashboard']);
    });
  }

  onSubmit(): void {
    if (this.loginForm.valid === false) {
      this.loginForm.get('email_id').markAsDirty();
      this.loginForm.get('password').markAsDirty();
      return;
    } else {
      if (this.loginForm.valid) {
        this.DataService.getLogin(this.loginForm.value)
          .subscribe(userData => {
            if (userData.success === true) {
              this.toastr.success('Wel-come!!!');
              window.sessionStorage.setItem('token', JSON.stringify(userData));
              this.route.navigate(['/dashboard']);
            } else {
              alert(userData.message);
            }
          });
      } else {
        alert('Please fill form properly!!!');
      }
    }


  }
  signInWithGoogle(): void {
    console.log('google click');
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  signOut(): void {
    this.authService.signOut();
  }

  socialLogin() {

  }

  click() {
    console.log('click');
  }
}
