import { Logout } from './../../../auth/store/actions/auth.actions';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { Component } from '@angular/core';
import * as fromAuth from '../../../auth/store/reducers';
import { MessagingService } from '../../messaging-service';
import {
  ToastyService,
  ToastyConfig,
  ToastOptions,
  ToastData
} from "ng2-toasty";
import { Router, NavigationEnd, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  isLoggedIn$: Observable<boolean>;
  preLoader$: Observable<boolean>;
  forLogin: boolean = false;
  constructor(
    private store: Store<fromAuth.State>,
    public msg: MessagingService,
    private toastyService: ToastyService,
    private toastyConfig: ToastyConfig,
    private Router: Router,
  ) {


    this.isLoggedIn$ = this.store.select(fromAuth.getLoggedIn);
    this.preLoader$ = this.store.select(fromAuth.getPreLoader);
    this.toastyConfig.theme = 'default';
  }

  selectedTab: string = "Max Brands"

  logout() { this.store.dispatch(new Logout()); }

  ngOnInit() {
    this.Router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        if (e.url == "/auth/login") {
          this.forLogin = true;
        } else {
          this.forLogin = false;
        }
      }
    });
    

    // if (this.Router.url=)
    this.msg.getPermission();
  }
}
