import UserService from "../User/user.service";
//import GoogleAuthService from "../UserAuthentication/googleAuth.service";
import UserAuthService from "../UserAuthentication/userAuth.service";
import CustomerService from "../Customer/customer.service";
import logger from "../../../config/logger.config";
import sha256 from "sha256";
import {
  error
} from "util";
var uuidv1 = require("uuid/v1");
var _ = require("lodash");
import request from 'request';

var path = require("path");
import * as fs from "fs";
import settings from "../../../settings";
import EmailService from "../CommonService/email.service";
import SettingsService from '../Settings/settings.service';
import DeviceModel from "../User/user.device.model";

let config = require("../../../config/" + settings.environment + ".config");

const api_end_point = config.default.api_end_point;

const UserAuthController = {
  login: async (req, res) => {
    try {
      let email_id = req.body.email_id;
      let password = req.body.password;
      // let type = req.body.type;

      let loggedInUser = await UserService.getLoginEmailOrPhoneNumber(email_id);
      if (loggedInUser) {
        if (loggedInUser.password === sha256(password)) {
          if (loggedInUser.status === "active") {
            let payload = {
              user_id: loggedInUser._id,
              email: loggedInUser.email,
              phone_number: loggedInUser.phone_number,
              type: loggedInUser.type
            };
            res.send({
              code: 200,
              status: "success",
              data: await UserAuthService.getJwtToken(payload),
              message: "Sucessfully Login"
            });
          } else {
            return res.status(401).send({
              code: 401,
              status: "error",
              data: {},
              message: "Your account is inactive"
            });
          }
        } else {
          res.status(401).send({
            code: 401,
            status: "error",
            data: {},
            message: "Invalid Credentials"
          });
        }
      } else {
        res.status(401).send({
          code: 401,
          status: "error",
          data: {},
          message: "Invalid Credentials"
        });
      }
    } catch (error) {
      res.status(401).send({
        code: 401,
        status: "error",
        message: "Invalid credentials",
        data: {}
      });
    }
  },
  customerLogin: async (req, res) => {
    try {
      let email_id = req.body.email;
      let password = req.body.password;
      let device = {};
      device.device = []
      device.device.push({ token: req.body.token || '', device_id: req.body.id || '', device_palform: req.body.os || '' })

      // let type = req.body.type;

      let loggedInUser = await CustomerService.getLoginEmail(email_id);
      if (loggedInUser) {
        if (loggedInUser.password === sha256(password)) {
          if (loggedInUser.status === "active") {
            let payload = {
              user_id: loggedInUser._id,
              email: loggedInUser.email,
              phone_number: loggedInUser.phone_number,
              mobile_number: loggedInUser.mobile_number,
              first_name: loggedInUser.first_name,
              last_name: loggedInUser.last_name,
              country_code: loggedInUser.country_code,
              country_name: loggedInUser.country_name,
              permanent_address: loggedInUser.permanent_address,
              company_name: loggedInUser.company_name,
              shipping_address: loggedInUser.shipping_address,
              city: loggedInUser.city,
              email: loggedInUser.email,
              type: loggedInUser.type
            };
            device.user_id = loggedInUser._id;
            const deviceData = await UserService.findAndUpdateDevice(device);
            if (deviceData.nModified === 0) {
              let savedDevice = await UserService.addDevice(device);
            }
            res.send({
              code: 200,
              status: "success",
              data: await UserAuthService.getJwtToken(payload),
              message: "Sucessfully Login"
            });
          } else {
            return res.status(401).send({
              code: 202,
              status: "error",
              data: {},
              message: "Dear valued customer your account activation request is under process. Please expect a call from our verification team within 24 hours. "
            });
          }
        } else {
          res.status(401).send({
            code: 401,
            status: "error",
            data: {},
            message: "Invalid credentials"
          });
        }
      } else {
        res.status(401).send({
          code: 401,
          status: "error",
          data: {},
          message: "Invalid credentials"
        });
      }
    } catch (error) {
      res.status(401).send({
        code: 401,
        status: "error",
        message: "Invalid credentials",
        data: {}
      });
    }
  },
  superAdminlogin: async (req, res) => {
    try {
      let email_id = req.body.email_id;
      let password = req.body.password;
      // let type = req.body.type;
      let loggedInUser = await UserService.getSuperAdmin(email_id);
      if (loggedInUser && loggedInUser.type !== 'user' && loggedInUser.type !== 'customer') {
        if (loggedInUser.status === 'active') {
          if (loggedInUser.password === sha256(password)) {
            // if (loggedInUser.status === "active") {
            let payload = {
              user_id: loggedInUser._id,
              first_name: loggedInUser.first_name,
              last_name: loggedInUser.last_name,
              email: loggedInUser.email,
              phone_number: loggedInUser.phone_number,
              type: loggedInUser.type
            };
            res.send({
              code: 200,
              status: "success",
              data: await UserAuthService.getJwtToken(payload),
              message: "Sucessfull Login"
            });
          } else {
            res.status(401).send({
              code: 401,
              status: "error",
              data: [],
              message: "Invalid credentials"
            });
          }
        } else {
          res.status(401).send({
            code: 401,
            status: "error",
            data: [],
            message: "Your account is Inactive."
          });
        }

      } else {
        res.status(401).send({
          code: 401,
          status: "error",
          data: [],
          message: "Invalid credentials"
        });
      }
    } catch (error) {
      res.status(401).send({
        code: 401,
        status: "error",
        message: "Invalid credentials",
        data: []
      });
    }
  },
  creatSuperAdmin: async (req, res) => {
    try {
      let User = {};
      User.first_name = req.body.first_name || "";
      User.last_name = req.body.last_name || "";
      User.country_code = req.body.country_code || "";
      User.phone_number = req.body.phone_number || "";
      User.email = req.body.email || "";
      User.type = 'super_admin'
      User.email = User.email.toLowerCase();
      User.status = 'active'
      // User.type = req.body.type;
      User.password = req.body.password;
      let userEmailExists = await UserService.getSuperAdminByEmailId(req.body.email);
      if (!_.isEmpty(userEmailExists)) {
        return res.status(400).send({
          code: 400,
          status: "error",
          message: "Super Admin/Email id already exists",
          data: []
        });
      }
      let savedUser = await UserService.addUser(User);
      User.user_id = savedUser._id;
      let logInData = await UserAuthService.getJwtToken(User);
      delete logInData.password;
      res.setHeader("x-auth-token", logInData.token);
      res.status(200).send({
        code: 200,
        status: "success",
        message: "Super Admin Created Sucessfully",
        data: logInData
      });
    } catch (error) {
      logger.error("Error in Created Super Admin" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in Created Super Admin:" + error.message,
        data: []
      });
    }
  },

  verify: async (req, res, next) => {
    let tokenSignature = req.headers["x-auth-token"];

    if (tokenSignature != undefined) {
      let token = await UserAuthService.verifyAndDecode(tokenSignature);
      if (token) {
        let loggedInUser = await UserService.getUserByUserId(token.data.user_id);
        req.userToken = token;
        if (loggedInUser && token.valid && loggedInUser.status === 'active') {
          return next();
        } else {
          res.status(401).send({
            status: "error",
            code: 401,
            message: "Token Invalid",
            data: {}
          });
        }
      } else {
        res.status(401).send({
          status: "error",
          code: 401,
          message: "Authentication Failure",
          data: {}
        });
      }
    } else {
      res.status(401).send({
        status: "error",
        code: 401,
        message: "Authentication Failure",
        data: {}
      });
    }

  },
  verifySuperAdmin: async (req, res, next) => {
    let tokenSignature = req.headers["x-auth-token"];

    if (tokenSignature != undefined) {
      let token = await UserAuthService.verifyAndDecode(tokenSignature);

      req.userToken = token;
      if (token.valid && (token.data.type === 'super_admin' || token.data.type === 'admin')) {
        return next();
      } else {
        res.status(401).send({
          status: "error",
          code: 401,
          message: "Token Invalid",
          data: {}
        });
      }
    } else {
      res.status(401).send({
        status: "error",
        code: 401,
        message: "Authentication Failure",
        data: {}
      });
    }
  },

  signup: async (req, res) => {
    try {
      let User = {};
      User.first_name = req.body.first_name;
      User.last_name = req.body.last_name;
      User.country_code = req.body.country_code;
      User.phone_number = req.body.phone_number;
      User.email = req.body.email;
      User.email = User.email.toLowerCase();
      // User.type = req.body.type;
      User.password = req.body.password;
      let userEmailExists = await UserService.getUserByEmailId(req.body.email, '');
      if (!_.isEmpty(userEmailExists)) {
        return res.status(400).send({
          code: 400,
          status: "error",
          message: "User/Email id already exists",
          data: []
        });
      }
      let userPhoneNumberExists = await UserService.getUserByPhoneNumber(req.body.phone_number, '');
      if (!_.isEmpty(userPhoneNumberExists)) {
        return res.status(400).send({
          code: 400,
          status: "error",
          message: "Phone Number is already exists",
          data: []
        });
      }
      let savedUser = await UserService.addUser(User);
      // User.user_id = savedUser._id;
      // let logInData = await UserAuthService.getJwtToken(User);
      // delete logInData.password;
      //res.setHeader("x-auth-token", logInData.token);
      res.status(200).send({
        code: 200,
        status: "success",
        message: "Sucessfully Signed Up",
        //data: logInData
        data: {}
      });
    } catch (error) {
      logger.error("Error in sign up" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in sign Up:" + error.message,
        data: []
      });
    }
  },
  //Mobile customer sign up
  customerSignup: async (req, res) => {
    try {
      let setting = await SettingsService.getAllsettings();
      let eCode = Math.floor(10000 + Math.random() * 90000);
      let mOTP = Math.floor(100000 + Math.random() * 900000);
      let duration;
      if (setting && setting.otp_duration) {
        duration = setting.otp_duration || 30;
      } else {
        duration = 30
      }
      let Customer = {};
      Customer.emailVerification = {};
      Customer.first_name = req.body.first_name;
      Customer.last_name = req.body.last_name;
      Customer.country_name = req.body.country_name;
      Customer.country_code = req.body.country_code;
      Customer.permanent_address = req.body.permanent_address;
      Customer.company_name = req.body.company_name;
      Customer.shipping_address = req.body.shipping_address;
      Customer.city = req.body.city;
      Customer.mobile_number = req.body.mobile_number;
      Customer.phone_number = req.body.phone_number;
      Customer.email = req.body.email.toLowerCase();
      Customer.type = 'customer';
      Customer.password = req.body.password;
      Customer.status = 'pending';
      // Customer.emailVerification.code = code;
      // Customer.emailVerification.expires = new Date().setHours(new Date().getHours() + 2);
      Customer.otp = {};
      Customer.otp.mcode = mOTP;
      Customer.otp.ecode = eCode;
      Customer.otp.expires = new Date().setMinutes(new Date().getMinutes() + duration);
      let device = {};
      device.device = []
      device.device.push({ token: req.body.token || '', device_id: req.body.id || '', device_palform: req.body.os || '' })
      let userEmailExists = await UserService.getUserByEmailId(req.body.email, '');
      if (!(req.body.country_code.match("^[0-9]{1,20}$"))) {
        return res.status(400).send({
          code: 400,
          status: "error",
          message: "Invalid country code",
          email_confirmation: userEmailExists.email_confirmation
        });
      }
      if (!_.isEmpty(userEmailExists)) {
        return res.status(400).send({
          code: 400,
          status: "error",
          message: "User/Email id already exists",
          email_confirmation: userEmailExists.email_confirmation
        });
      }
      let userPhoneNumberExists = await UserService.getUserByMobileNumber(req.body.mobile_number, '');
      if (!_.isEmpty(userPhoneNumberExists)) {
        return res.status(400).send({
          code: 400,
          status: "error",
          message: "Phone Number is already exists",
          email_confirmation: userPhoneNumberExists.email_confirmation
        });
      }


      var imagepath = api_end_point + '/image/logo/';

      let EMAIL_VERIFICATION_HTML = await fs.readFileSync(
        path.join(__dirname, "../../EmailTemplates/emailVerification.html"),
        "utf-8"
      );

      // Customer.user_id = savedUser._id;
      EMAIL_VERIFICATION_HTML = EMAIL_VERIFICATION_HTML.replace(
        "{action_url}",
        api_end_point +
        "/auth/email-verification"
      ).replace('{firstname}', Customer.first_name)
        .replace('{code}', 'E' + eCode)
        .replace('{logo}', imagepath + 'maxbrands.png');
      // toMail, subject, body, setting, attachment
      await EmailService.sendEmail(
        Customer.email,
        "Please confirm your Email account",
        EMAIL_VERIFICATION_HTML, setting, []
      );
      let savedUser = await UserService.addUser(Customer);
      device.user_id = savedUser._id;
      // let updateDevice = await UserService.findAndUpdateDevice(device);
      // console.log("TCL: updateDevice", updateDevice)
      // if(updateDevice == null){
      let savedDevice = await UserService.addDevice(device);

      // }
      let sid = 'FMCGmarkets'
      if (req.body.country_code == '91' || req.body.country_code == '+91') {
        sid = 'FMCGmt'
      } else if (req.body.country_code == '+966' || req.body.country_code == '966') {
        sid = 'FMCGmrkt-AD'
      } else {
        sid = 'FMCGmarkets';
      }
      let message = 'Dear Customer, Thank you for registering on FMCG . Your OTP is ' + mOTP + '. Do not share your OTP.'
      request({
        uri: config.default.sms.endpoint + 'mobilenumber=' + req.body.country_code + req.body.mobile_number + '&message=' + message + '&sid=' + sid + '&mtype=N' + '&DR=Y',
      }, (error, response, body) => {
        res.status(200).send({
          code: 200,
          status: "success",
          message: "Sucessfully Signed Up",
          data: { userId: savedUser._id }
        });
      })



    } catch (error) {
      logger.error("Error in sign up" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in sign Up",
        data: []
      });
    }
  },

  customerLogout: async (req, res) => {
    try {
      let device_id = req.query.id;
      let deleteDevice = await UserService.deleteDevice(req.params.id, device_id);
      res.status(200).send({
        code: 200,
        status: "success",
        message: "Sucessfully logout",
        data: deleteDevice
      });
    } catch (error) {
      return res.status(400).send({
        status: "error",
        code: 200,
        message: "Error in logout",
      });
    }
  },

  forgetPassword: async (req, res) => {
    try {
      let email_id = req.body.email;
      var imagepath = api_end_point + '/image/logo/';

      let User = await UserService.getUserByEmailId(email_id, '');
      if (User) {
        let setting = await SettingsService.getAllsettings();

        let FORGET_PASSWORD_HTML = await fs.readFileSync(
          path.join(__dirname, "../../EmailTemplates/forgetPassword.html"),
          "utf-8"
        );
        FORGET_PASSWORD_HTML = FORGET_PASSWORD_HTML.replace(
          "{link}",
          api_end_point +
          "/auth/change-password" +
          "/email/" + encodeURIComponent(email_id) + "/" +
          encodeURIComponent(User.password_uuid)
        ).replace('{firstname}', User.first_name)
          .replace('{logo}', imagepath + 'maxbrands.png');

        // toMail, subject, body, setting, attachment

        EmailService.sendEmail(
          email_id,
          "Reset password",
          FORGET_PASSWORD_HTML, setting, []
        );

        return res.status(200).send({
          status: "success",
          code: 200,
          message: "Forgot password email sent successfully for the registered email " +
            email_id,
          data: []
        });
      } else {
        return res.status(400).send({
          status: "error",
          code: 200,
          message: "Email id not found :  " + email_id,
          data: []
        });
      }
    } catch (error) {
      return res.status(400).send({
        status: "error",
        code: 200,
        message: "Error in forgot password request  :" + req.params.email_id,
        data: []
      });
    }
  },

  changePassword: async (req, res) => {
    try {
      let email_id = req.body.email;
      let new_password = req.body.password;
      let password_uuid = req.body.uuid;

      let User = await UserService.getUserByEmailId(email_id, '');
      if (User.password_uuid === password_uuid) {
        let user = {
          password: await sha256(new_password),
          password_uuid: uuidv1()
        };

        let updatedUser = await UserService.updateUserById(User._id, user);

        return res.status(200).send({
          status: "success",
          code: 200,
          message: "Password Changed Successfully. " + email_id,
          data: []
        });
      } else {
        return res.status(400).send({
          status: "error",
          code: 200,
          message: "Invalid URL",
          data: []
        });
      }
    } catch (error) {
      return res.status(400).send({
        status: "error",
        code: 200,
        message: "Error in passwordchange  :" + req.body.email_id,
        data: []
      });
    }
  },

  ////////////////testing start ///////////////////
   //Mobile retailer sign up
   retailerSignup: async (req, res) => {
    try {
      let setting = await SettingsService.getAllsettings();
      let eCode = Math.floor(10000 + Math.random() * 90000);
      let mOTP = Math.floor(100000 + Math.random() * 900000);
      let duration;
      if (setting && setting.otp_duration) {
        duration = setting.otp_duration || 30;
      } else {
        duration = 30
      }
      let Customer = {};
      Customer.emailVerification = {};
      Customer.first_name = req.body.first_name;
      Customer.last_name = req.body.last_name;
      Customer.country_name = req.body.country_name;
      Customer.country_code = req.body.country_code;
      Customer.permanent_address = req.body.permanent_address;
      Customer.company_name = req.body.company_name;
      Customer.shipping_address = req.body.shipping_address;
      Customer.city = req.body.city;
      Customer.mobile_number = req.body.mobile_number;
      Customer.phone_number = req.body.phone_number;
      Customer.email = req.body.email.toLowerCase();
      Customer.type = 'customer';
      Customer.password = req.body.password;
      Customer.status = 'pending';
      // Customer.emailVerification.code = code;
      // Customer.emailVerification.expires = new Date().setHours(new Date().getHours() + 2);
      Customer.otp = {};
      Customer.otp.mcode = mOTP;
      Customer.otp.ecode = eCode;
      Customer.otp.expires = new Date().setMinutes(new Date().getMinutes() + duration);
      let device = {};
      device.device = []
      device.device.push({ token: req.body.token || '', device_id: req.body.id || '', device_palform: req.body.os || '' })
      let userEmailExists = await UserService.getUserByEmailId(req.body.email, '');
      if (!(req.body.country_code.match("^[0-9]{1,20}$"))) {
        return res.status(400).send({
          code: 400,
          status: "error",
          message: "Invalid country code",
          email_confirmation: userEmailExists.email_confirmation
        });
      }
      if (!_.isEmpty(userEmailExists)) {
        return res.status(400).send({
          code: 400,
          status: "error",
          message: "User/Email id already exists",
          email_confirmation: userEmailExists.email_confirmation
        });
      }
      let userPhoneNumberExists = await UserService.getUserByMobileNumber(req.body.mobile_number, '');
      if (!_.isEmpty(userPhoneNumberExists)) {
        return res.status(400).send({
          code: 400,
          status: "error",
          message: "Phone Number is already exists",
          email_confirmation: userPhoneNumberExists.email_confirmation
        });
      }


      var imagepath = api_end_point + '/image/logo/';

      let EMAIL_VERIFICATION_HTML = await fs.readFileSync(
        path.join(__dirname, "../../EmailTemplates/emailVerification.html"),
        "utf-8"
      );

      // Customer.user_id = savedUser._id;
      EMAIL_VERIFICATION_HTML = EMAIL_VERIFICATION_HTML.replace(
        "{action_url}",
        api_end_point +
        "/auth/email-verification"
      ).replace('{firstname}', Customer.first_name)
        .replace('{code}', 'E' + eCode)
        .replace('{logo}', imagepath + 'maxbrands.png');
      // toMail, subject, body, setting, attachment
      await EmailService.sendEmail(
        Customer.email,
        "Please confirm your Email account",
        EMAIL_VERIFICATION_HTML, setting, []
      );
      let savedUser = await UserService.addUser(Customer);
      device.user_id = savedUser._id;
      // let updateDevice = await UserService.findAndUpdateDevice(device);
      // console.log("TCL: updateDevice", updateDevice)
      // if(updateDevice == null){
      let savedDevice = await UserService.addDevice(device);

      // }
      let sid = 'FMCGmarkets'
      if (req.body.country_code == '91' || req.body.country_code == '+91') {
        sid = 'FMCGmt'
      } else if (req.body.country_code == '+966' || req.body.country_code == '966') {
        sid = 'FMCGmrkt-AD'
      } else {
        sid = 'FMCGmarkets';
      }
      let message = 'Dear Customer, Thank you for registering on FMCG . Your OTP is ' + mOTP + '. Do not share your OTP.'
      request({
        uri: config.default.sms.endpoint + 'mobilenumber=' + req.body.country_code + req.body.mobile_number + '&message=' + message + '&sid=' + sid + '&mtype=N' + '&DR=Y',
      }, (error, response, body) => {
        res.status(200).send({
          code: 200,
          status: "success",
          message: "Sucessfully Signed Up",
          data: { userId: savedUser._id }
        });
      })
    } catch (error) {
      logger.error("Error in sign up" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in sign Up",
        data: []
      });
    }
  }
};

export default UserAuthController;