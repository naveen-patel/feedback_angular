import User from "../User/user.model";
import logger from "../../../config/logger.config";
import sha256 from "sha256";
import UserService from "../User/user.service";
var uuidv1 = require('uuid/v1');
var mongoose = require('mongoose');
var moment = require("moment");
// moment.tz.setDefault("Asia/Singapore");
const CustomerService = {};

CustomerService.getAllCustomers = async (searchkey, limit, skip, type, status) => {
  try {
    var json = {},query;
    if(searchkey!=''){
      query = {
        $or: [{
          "first_name": {
            "$regex": searchkey,
            "$options": "i"
          }
        }, {
          "last_name": {
            "$regex": searchkey,
            "$options": "i"
          }
        }, {
          "email": {
            "$regex": searchkey,
            "$options": "i"
          }
        },
        {"email_confirmation":true},
        {"mobile_verification" : true} ],
        "type": type,
        
      }
    }else{
        query = {
          $or: [{"email_confirmation":true},{"mobile_verification" : true}],
          "type": type
        }
    }
     
    if(status=='pending'){
      query.status = {$in:['pending','reject']}
    }else if(status){
      query.status = status;
    }
    const count = await User.find(query).count();
    const users = await User.find(query,{"password":0}).sort({
      "createdAt": -1
    }).limit(limit).skip(skip).lean();
    json.totalcount = count;
    json.users = users
    return json;
  } catch (error) {
    throw error;
  }
}
CustomerService.changeStatus = async (id, status) => {
  try {
    const update = await User.findOneAndUpdate({
      _id: id
    }, {
      status: status
    }, {
      strict: true,
      new: true
    });
    return update;
  } catch (error) {
    throw error;
  }
};
// ,'otp.expires':{ $gt: new Date(Date.now()) }
CustomerService.verifyOtp = async(otp,email,status) =>{
  try {
    let startDate1 = moment().startOf('day').format("YYYY-MM-DD")

    let obj ={},query={};
    // obj.otp ={};
    obj['otp.isVerified'] =1;
    query.email = email;
    // query.otp ={};
    query['otp.expires'] = {'$gt' : startDate1.toString()}
    if(otp.split('')[0]=='E'){
      query['otp.ecode'] =otp.slice(1)
      obj.email_confirmation = true
      if(status!='in_active'){
        obj.status=status
      }else{
        obj.status='pending'
      }
    }else{
      query['otp.mcode'] = otp;
      obj.mobile_verification = true;
    }
    let user = await User.findOneAndUpdate(query,
      obj,
      {
        strict: true,
        new: true
      });
    return user;
} catch (error) {
    throw error;
}
}
CustomerService.emailVerification = async (code,id) =>{
  try {
    let r =  await User.findOneAndUpdate(
      {_id: id,'emailVerification.code':code,'emailVerification.expires':{$gt : Date.now().toString()}}
      ,{email_confirmation: true,status:'pending'},{
      strict: true,
      new: true
    });
    return r
  } catch (error) {
    throw error;
  }
}

// CustomerService.getUserByPhoneNumber = async (number,id) => {
//   try {
//     var obj = {
//         phone_number: number
//     }
//     if(id!==''){
//       var arr = [];
//         var objId = mongoose.Types.ObjectId(id);
//         arr.push(objId);
//       obj._id = {"$nin": arr}
//     }
//     const user = await User.findOne(obj);
//     return user;
//   } catch (error) {
//     throw error
//   }
// }

// CustomerService.addCustomer = async user => {
//   try {
//     user.password = await sha256(user.password);
//     let userToAdd = new User(user);
//     const savedUser = await userToAdd.save();
//     return savedUser;
//   } catch (error) {
//     throw error;
//   }
// };

CustomerService.getCustomerByCustomerId = async user_id => {
  try {
    const user = await User.findOne({
      _id: Object(user_id)
    });
    return user;
  } catch (error) {
    throw error;
  }
};

CustomerService.getLoginEmail = async EmailId => {
  try {
    const user = await User.findOne({email:EmailId,
      // $or: [{
      //   email: new RegExp("^" + EmailId + "$", "i")
      // }, {
      //   phone_number: EmailId
      // }],
      type: 'customer'
    });
    return user;
  } catch (error) {
    throw error;
  }
};


CustomerService.updateCustomerProfileById = async (id, data) => {
  try {
    let user = await User.findByIdAndUpdate({_id: Object(id) }, data);
    return user;
} catch (error) {
    throw error;
}
};

CustomerService.addOTP = async (email,otp) =>{
  try {
    let user = await User.findOneAndUpdate({email: email }, otp);
    return user;
} catch (error) {
    throw error;
}
};

CustomerService.updateUserById = async (user_id, data) => {
  try {
    const updatedUser = await User.findOneAndUpdate({
      _id: user_id
    }, data, {
      strict: true,
      new: true
    });
    return updatedUser;
  } catch (error) {
    throw error;
  }
};

CustomerService.getUserEmailAvailability = async email => {
  try {
    let user = await User.count({
        email: new RegExp("^" + email + "$", "i")
      },
      null,
      null
    );
    if (user) {
      return false;
    } else {
      return true;
    }
  } catch (error) {
    throw error;
  }
};

CustomerService.customerChangePassword = async (user_id, data) => {
  try {
    data.password = await sha256(data.password);
    data.password_uuid = uuidv1();
    const updatedUser = await User.findOneAndUpdate({
      _id: user_id
    }, data, {
      strict: true,
      new: true
    });
    return updatedUser;
  } catch (error) {
    throw error;
  }
};

CustomerService.checkChangePasswordByEmail = async (email, data) => {
  try {
    let u = await User.findOne({email:email,'otp.isVerified':1});
    data.status = u.status;
    data.password = await sha256(data.password);
    data.password_uuid = uuidv1();
    const updatedUser = await User.findOneAndUpdate({
      email: email,'otp.isVerified':1
    }, data, {
      new: true
    });
    return updatedUser;
  } catch (error) {
    throw error;
  }
};
CustomerService.checkChangePassword = async (user_id, data) => {
  try {
    data.old_password = await sha256(data.old_password);
    const user = await User.count({
      _id: Object(user_id),
      "password": data.old_password
    });
    return ((user > 0) ? true : false);
  } catch (error) {
    throw error;
  }
};
CustomerService.deleteCustomer = async(id) => {
  try {
    const  data = await User.remove({
      _id: id
    });
    return data;
  } catch (error) {
    throw error;
  }
};

export default CustomerService;