import Settings from "./setting.model";
import logger from "../../../config/logger.config";
import sha256 from "sha256";
var uuidv1 = require('uuid/v1');

const SettingsService = {};



SettingsService.getAllsettings = async () => {
    try {
       return await Settings.findOne().lean();
         
    } catch (error) {
        throw error;
    }
};


SettingsService.Updatesettings = async (data, id) => {
    try {
        var savedSettings;
        if (id == 'new') {
            let SettingsToUpdate = new Settings(data);
            savedSettings = await SettingsToUpdate.save();
        } else {
            savedSettings = await Settings.findOneAndUpdate({
                _id: id
            }, data, {
                upsert: true,
                new: true
            });
        }
        return savedSettings;
    } catch (error) {
        throw error;
    }
};



export default SettingsService;