// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  // API_ENDPOINT: 'http://192.168.168.94:8000/api/',
  API_ENDPOINT: 'http://1.6.178.243:8085/api/',
  // API_ENDPOINT: 'http://localhost:8085/api/',

  production: false,
  // firebase: {
  //   apiKey: "",
  //   authDomain: "",
  //   databaseURL: "",
  //   projectId: "",
  //   storageBucket: "",
  //   messagingSenderId: ""
  // }
};
