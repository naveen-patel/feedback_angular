import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { CustomerService } from '../../services/customer.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as moment from 'moment'
import { DataService } from '../../services/data.service';
import { templateJitUrl } from '@angular/compiler';
import { l } from '@angular/core/src/render3';
@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  modalRef3: BsModalRef;
  modalRef5: BsModalRef;
  modalRef4: BsModalRef;
  order_id: string;
  details = "";
  openOrderDetails: boolean = false;
  orderNumber;
  customerName;
  emailId;
  mobileNo;
  shippingAdd;
  orderDate;
  totalAmountAED: Number;
  totalAmountUsd: Number;
  callBack;
  orderDetailstotalPriceAED;
  orderDetailstotalPriceUSD;
  sum: number = 0;
  selectSupplierId = 's';
  supplierList = [];
  brandList = [];
  selectBrandId = 'b';

  Date: Date = new Date();
  remainingRecords = 0;
  search: string = '';
  limit = 10;
  skip = 1;
  loader: boolean = false;
  count: Number;
  orderRecords = [];
  productRecords = [];
  records = [];
  selectStatusId = 's';
  orderStatusArr = [];
  editOrder: string = 'Edit';
  id: string;
  viewOnly: Boolean = false
  submitted: boolean = false;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    keyboard: false,
    class: 'modal-dialog'
  }
  OrderForm: FormGroup;
  modalRef: BsModalRef;
  editNew: any;
  id2;
  user_id1;
  bsConfig =
    {
      showWeekNumbers: false,
      containerClass: 'theme-red',
      changeMonth: true,
      dateInputFormat: 'DD/MM/YYYY',
      changeYear: true,
    }
  minDate1: Date;
  startDate: any;
  editCategory: string = 'Add';
  categoryForm: FormGroup;
  addNew: any;
  discountPriceProduct: any;
  totaltPriceProduct: any;
  deleteQuantity;
  price12;
  suggestions = [];
  offerPers;
  actualAED;
  actualUSD;
  oldEditPriceAED;
  oldEditPriceUSD;
  flags: boolean = false;
  namelist: boolean = false;
  flag: boolean = false;
  productId;
  productinorderId;
  pro_id;
  countries: any[];
  supplier;
  deleteAED;
  deleteUSD;
  quantityEdit;
  totalQuantity;
  orderProductId;
  actualPriceProduct;
  oldDiscountPrice;
  dealObject;
  @ViewChild("confirmForproduct") confirmmodal: TemplateRef<any>;

  constructor(
    public cs: CustomerService,
    private modalService: BsModalService,
    public formBuilder: FormBuilder,
    private ds: DataService,
  ) { }

  ngOnInit() {
    this.cs.forProductDrop().subscribe(data => {
      this.countries = data;
      console.log(this.countries, "for dropdown");

    });
    this.getOrders();
    this.orderStatusArr =
      [{ 'orderStatus': 'Order Placed', 'order_status': 'order_placed' },
      { 'orderStatus': 'Order In-progress', 'order_status': 'order_in_progress' },
      { 'orderStatus': 'Dispatched', 'order_status': 'dispatched' },
      { 'orderStatus': 'Delivered', 'order_status': 'delivered' },
      { 'orderStatus': 'Cancelled', 'order_status': 'cancelled' }];
    this.forDropdown();
  }

  // for search 
  searchProduct() {
    this.getOrders();
  }

  // for get all Order details
  getOrders() {
    this.remainingRecords = 0;
    this.skip = 1;
    this.cs.getOrder(this.search, this.limit, this.skip).subscribe(data => {
      this.loader = false;
      this.count = data.totalcount;
      this.records = data.order;
      this.remainingRecords = data.totalcount - (this.skip * this.limit);
      this.records.forEach((element) => {
        element.isSelect = false;
      })
    });
  }

  // for get order details with all product details
  getOrdersDetails(id) {
    console.log('id 1', id);

    this.pro_id = id
    this.orderRecords = [];
    this.productRecords = [];
    this.id2 = '';
    this.sum = 0
    let main = this;
    this.cs.getOrderDetails(id).subscribe(data => {
      console.log('data', data, this.orderProductId);
      let tempp1 = []
      for (let i = 0; i < data.product.length; i++) {
        tempp1.push(data.product[i].product_detail.brand._id);
      }
      this.orderProductId = tempp1;
      this.loader = false;
      // this.count = data.totalcount;
      this.orderRecords = data;
      this.id2 = data._id;
      this.productRecords = data.product
      this.user_id1 = data.user_id;
      // this.remainingRecords = data.totalcount - (this.skip * this.limit);
      let add;
      for (let index = 0; index < this.productRecords.length; index++) {
        // product.product_detail?.discount_price_aed ? product.product_detail?.discount_price_aed : product.product_detail?.sales_per_case_aed )
        add = ((this.productRecords[index].product_detail.deal.isActive && this.productRecords[index].product_detail.isAdd ? this.productRecords[index].product_detail.deal.offer_price_aed : (this.productRecords[index].product_detail.discount_price_aed ? this.productRecords[index].product_detail.discount_price_aed : this.productRecords[index].product_detail.sales_per_case_aed)) * (this.productRecords[index].quantity))
        main.sum = add + main.sum;
      }
      this.orderDetailstotalPriceAED = main.sum;
      this.orderDetailstotalPriceUSD = ((this.orderDetailstotalPriceAED / 3.67250).toFixed(2));
      // this.records.forEach((element) => {
      //   element.isSelect = false;
      // })
    });
  }

  // for Order form

  getFormFields(i) {
    this.orderNumber = this.records[i].order_number;
    this.customerName = this.records[i].users[0].first_name + " " + this.records[i].users[0].last_name;
    this.emailId = this.records[i].users[0].email;
    this.mobileNo = this.records[i].users[0].mobile_number;
    this.shippingAdd = this.records[i].users[0].shipping_address;
    this.orderDate = moment(this.records[i].order_date).format('DD-MM-YYYY');
    this.totalAmountAED = this.records[i].total_amount_aed;
    this.totalAmountUsd = this.records[i].total_amount_usd;
    this.selectStatusId = this.records[i].order_status;

    this.startDate = this.records[i].order_date;
    return {
      _id: [this.records[i]._id],
      user_id: [this.records[i].user_id, Validators.required],
      orderStatus: [this.records[i].order_status, Validators.required],
      shipping_address: [this.records[i].users[0].shipping_address, Validators.required],
      callback_request: [this.records[i].callback_request, Validators.required]
    }
  }

  // for category form
  getFormFields2(i) {
    console.log("records", this.productRecords[i]);
    if (i != 'null') {
      setTimeout(() => {
        this.forCal("");
      }, 1000)
      this.supplier = this.productRecords[i].product_detail.supplier_id.name;
      this.selectBrandId = this.productRecords[i].product_detail.brand._id;
      this.selectSupplierId = this.productRecords[i].product_detail.supplier_id._id;
      this.productId = this.productRecords[i].product_detail._id;
      this.productinorderId = this.productRecords[i].product_detail.brand._id;
      this.editCategory = 'Edit';
      this.totalQuantity = this.forTotalQuantity(this.selectBrandId);
      this.actualPriceProduct = this.productRecords[i].product_detail.sales_per_case_aed
      this.quantityEdit = this.productRecords[i].quantity, Validators.required;
      console.log("id", this.productId)
      this.price12 = this.productRecords[i].product_detail.sales_per_case_aed;
      this.oldDiscountPrice = this.productRecords[i].product_detail.deal.isActive && this.productRecords[i].product_detail.isAdd ? this.productRecords[i].product_detail.deal.offer_price_aed : (this.productRecords[i].product_detail.discount_price_aed ? this.productRecords[i].product_detail.discount_price_aed : this.productRecords[i].product_detail.sales_per_case_aed);
      this.oldEditPriceAED = this.productRecords[i].product_detail.deal.isActive && this.productRecords[i].product_detail.isAdd ? this.productRecords[i].product_detail.deal.offer_price_aed : (this.productRecords[i].product_detail.discount_price_aed ? this.productRecords[i].product_detail.discount_price_aed : this.productRecords[i].product_detail.sales_per_case_aed);
      this.forCal("");
      console.log('recordsssssssss',this.records[i])
      return {
        _id: [this.records[i]._id],
        product_name: [this.productRecords[i].product_detail.brand.brand_name],
        supplier_id: [this.productRecords[i].product_detail.supplier_id._id],
        product_id: [this.productRecords[i].product_detail.brand._id],
        quantity: [this.productRecords[i].quantity, Validators.required],
        sales_per_case_aed: [this.productRecords[i].product_detail.deal.isActive && this.productRecords[i].product_detail.isAdd ? this.productRecords[i].product_detail.deal.offer_price_aed : (this.productRecords[i].product_detail.discount_price_aed ? this.productRecords[i].product_detail.discount_price_aed : this.productRecords[i].product_detail.sales_per_case_aed), Validators.required],
      }

    } else {
      console.log(this.selectSupplierId)
      this.editCategory = 'Add';
      this.selectSupplierId = 's'
      this.supplier = '';
      this.discountPriceProduct = '';
      this.actualPriceProduct = '';
      this.totaltPriceProduct = '';
      return {
        product_name: ["",],
        supplier_id: [""],
        product_id: ["", Validators.required],
        quantity: ["", Validators.required],
        sales_per_case_aed: ["", Validators.required],
      }
    }
  }
  orderStatus(value) {
    this.orderStatusArr
    return this.orderStatusArr.filter(countryItem => countryItem.order_status === value)[0].orderStatus;
  }
  forTotalQuantity(id) {
    return this.countries.filter(countryItem => countryItem.brand._id === id)[0].quantity;
  }
  forDropdown() {
    this.ds.getBrandList().subscribe(data => {
      this.brandList = data;
      console.log("brand list", this.brandList);
    });
    this.ds.getSuppliers().subscribe(data => {
      this.supplierList = data;
    });
  }
  onItemChange1(value) {
    this.selectSupplierId = value;
  }
  onItemChange3(value) {
    this.selectBrandId = value;
  }

  forDisabled() { return true }
  openModal(template: TemplateRef<any>, i, type) {
    this.editOrder = type;
    this.viewOnly = false;
    this.modalRef = this.modalService.show(template, this.config);
    this.OrderForm = this.formBuilder.group(this.getFormFields(i));
    this.submitted = false;
    console.log(this.OrderForm);

  }

  get g() { return this.categoryForm.controls; }

  openModal2(template: TemplateRef<any>, i, type) {
    setTimeout(() => {
      if (type == 'Add') {
        this.selectSupplierId = 's';
        this.selectBrandId = 'b';
      }
      console.log("id", this.selectBrandId, this.selectSupplierId);
      this.editCategory = type;
      this.viewOnly = false;
      this.modalRef = this.modalService.show(template, this.config);
      this.categoryForm = this.formBuilder.group(this.getFormFields2(i));
      this.submitted = false;
    }, 1000)

  }
  forCal(value) {
    console.log('calculation call');
    if ((document.getElementById('price') as HTMLInputElement).value) {
      let price: any = ((document.getElementById('price') as HTMLInputElement).value);
      console.log('first price', price, 'second price', this.price12)
      this.discountPriceProduct = (this.price12 - price)
    } else {
      this.discountPriceProduct = '';
      this.totaltPriceProduct = '';
    }

    if (((document.getElementById('quantity') as HTMLInputElement).value) && ((document.getElementById('price') as HTMLInputElement).value)) {
      let quantity: any = ((document.getElementById('quantity') as HTMLInputElement).value);
      let price: any = ((document.getElementById('price') as HTMLInputElement).value);
      this.totaltPriceProduct = quantity * price;
    }
  }

  AEDtoUSD(value) {
    value = Number((value / 3.67250).toFixed(2));
    return value;
  }

  // for view order details 
  viewonlyOrder(i) {
    this.details = "Details"
    this.openOrderDetails = true;
    this.selectSupplierId = "s"
    this.OrderForm = this.formBuilder.group(this.getFormFields(i));
    this.order_id = this.OrderForm.value._id;
    this.getOrdersDetails(this.order_id);

  }

  // for order list button 
  orderList() {
    this.details = "";
    this.openOrderDetails = false;
    this.ngOnInit();
  }

  onSubmit() {
    this.submitted = true;
    if (this.OrderForm.valid) {
      let formValue = Object.assign({}, this.OrderForm.value);
      this.editNew = formValue;
      this.submitsection();
    }
  }

  get f() { return this.OrderForm.controls; }

  submitsection() {
    var id = this.editNew._id;
    this.cs.updateOrder(id, this.editNew).subscribe(data => {
      this.closeModal();
      this.reload();
      this.cs.successMessage('Order Status Updated Successfully')
    });
  }
  // for calculation 
  onSubmit1() {

    if (this.flags) {
      this.categoryForm.get('product_name').setErrors({ 'notFound': true })
    }
    this.submitted = true;
    if (this.categoryForm.valid) {
      // for total category
      if (this.totalQuantity >= (this.categoryForm.get('quantity').value)) {
        this.categoryForm.get('quantity').setErrors(null)
      } else {
        this.categoryForm.get('quantity').setErrors({ 'more': true });
        return false;
      }
      // for negative price
      if ((this.categoryForm.get('sales_per_case_aed').value) >= 1) {
        this.categoryForm.get('sales_per_case_aed').setErrors(null)
      } else {
        this.categoryForm.get('sales_per_case_aed').setErrors({ 'notFound': true });
        return false;
      }

      if (this.flags) {
        this.categoryForm.get('product_name').setErrors({ 'notFound': true })
        return false;
      } else {
        let valueMobileDeal;
        let formValue = Object.assign({}, this.categoryForm.value);
        let discountPrice = formValue.sales_per_case_aed;
        console.log('discounttttt', discountPrice, this.oldDiscountPrice);
        if (discountPrice.toString() == this.oldDiscountPrice.toString()) {
          console.log('insdie if')
          valueMobileDeal = true;
        }
        else {
          valueMobileDeal = false;
          this.dealObject = {
            deal_percentage: null,
            isActive: false,
            offer_price_aed: null,
            offer_price_usd: null
          }
          console.log('inside else')
        }
        if (this.editCategory == 'Edit') {
          // let formValue = Object.assign({}, this.categoryForm.value);
          let temp = {};
          temp["user_id"] = this.user_id1;
          temp["flag"] = false;
          temp["isMobile"] = valueMobileDeal;
          temp["isAdd"] = valueMobileDeal;
          temp["product"] = {
            // "actual_price": this.actualPriceProduct,
            "deal": this.dealObject,
            "total_aed": { 'new': (formValue.sales_per_case_aed * formValue.quantity), 'old': (this.oldEditPriceAED * this.quantityEdit) },
            "total_usd": { 'new': this.AEDtoUSD(formValue.sales_per_case_aed * formValue.quantity), 'old': this.AEDtoUSD(this.oldEditPriceAED * this.quantityEdit) },
            "sales_per_case_aed": this.actualPriceProduct,
            "sales_per_case_usd": this.AEDtoUSD(this.actualPriceProduct),
            "discount_price_aed": formValue.sales_per_case_aed,
            "discount_price_usd": this.AEDtoUSD(formValue.sales_per_case_aed),
            "quantity": { new: formValue.quantity, old: this.quantityEdit }, "product_id": {
              "_id": this.productId,
            }
          }
          this.editNew = temp;
        } else {
          // let formValue = Object.assign({}, this.categoryForm.value);
          let temp = {};
          temp["user_id"] = this.user_id1;
          temp["flag"] = true;
          temp["isMobile"] = valueMobileDeal;
          temp["isAdd"] = valueMobileDeal;
          temp["product"] = {
            // "actual_price": this.actualPriceProduct,
            "deal": this.dealObject,
            "total_aed": { 'new': (formValue.sales_per_case_aed * formValue.quantity), 'old': 0 },
            "total_usd": { 'new': this.AEDtoUSD(formValue.sales_per_case_aed * formValue.quantity), 'old': 0 },
            // "sales_per_case_aed": formValue.sales_per_case_aed,
            "sales_per_case_aed": this.actualPriceProduct,
            "sales_per_case_usd": this.AEDtoUSD(this.actualPriceProduct),
            "discount_price_aed": formValue.sales_per_case_aed,
            "discount_price_usd": this.AEDtoUSD(formValue.sales_per_case_aed),
            "quantity": { new: formValue.quantity, old: 0 }, "product_id": {
              "_id": this.productId,
            }
          }
          this.addNew = temp;
        }
      }

      this.confirmForproduct(this.confirmmodal, this.productinorderId);
      // this.submitsection2();
    }
  }
  submitsection2() {
    if (this.editCategory == 'Edit') {
      var id = this.editNew._id;
      this.cs.updateOrder(this.pro_id, this.editNew).subscribe(data => {
        this.closeModal();

        this.reload();
        this.cs.successMessage('Item Updated Successfully')
        setTimeout(() => {
          this.getOrdersDetails(this.pro_id)
        }, 1000)
      });
    } else {
      this.cs.addOrderProduct(this.id2, this.addNew).subscribe(data => {
        this.closeModal();
        // this.closeModal1();
        // this.reload();
        this.cs.successMessage('New Item added Successfully')
        setTimeout(() => {
          this.getOrdersDetails(this.order_id)
        }, 1000)
      });
    }
    setTimeout(() => {
      this.closeModal1();
    }, 1000)
  }


  // for delete record
  DeleteRecord(template: TemplateRef<any>, i, id) {
    this.modalRef3 = this.modalService.show(template);
    this.id = id;
  }
  confirmToDelete() {

    this.cs.deleteOrder(this.id).subscribe(data => {
      this.cs.successMessage("Order Deleted Successfully");
      this.ngOnInit();
    })
    this.modalRef3.hide();
    this.modalRef3 = null
  }
  DeleteRecords(template: TemplateRef<any>) {
    this.modalRef4 = this.modalService.show(template);
  }

  // for delete product from order
  DeleteRecord1(template: TemplateRef<any>, i, id, quantity) {
    let isactivr
    let a = this.productRecords[i].product_detail.deal.isActive && this.productRecords[i].product_detail.isAdd ? this.productRecords[i].product_detail.deal.offer_price_aed * quantity : this.productRecords[i].product_detail.sales_per_case_aed * quantity
    console.log('a', a, 'product', this.productRecords[i].product_detail);

    this.modalRef3 = this.modalService.show(template);
    this.deleteQuantity = quantity;
    // this.deleteAED = this.productRecords[i].product_detail.sales_per_case_aed;
    this.deleteAED = this.productRecords[i].product_detail.deal.isActive && this.productRecords[i].product_detail.isAdd ? this.productRecords[i].product_detail.deal.offer_price_aed : this.productRecords[i].product_detail.sales_per_case_aed;
    // this.deleteUSD = this.productRecords[i].product_detail.sales_per_case_usd;
    this.deleteUSD = this.productRecords[i].product_detail.deal.isActive && this.productRecords[i].product_detail.isAdd ? this.productRecords[i].product_detail.deal.offer_price_usd : this.productRecords[i].product_detail.sales_per_case_usd;
    this.id = id;
  }
  // priceAED,priceUSD
  confirmToDelete1() {
    this.cs.deleteOrder1(this.id2, this.id, this.deleteQuantity, this.deleteAED, this.deleteUSD).subscribe(data => {
      this.cs.successMessage("Product Deleted Successfully");
    })
    this.modalRef3.hide();
    this.modalRef3 = null
    setTimeout(() => {
      this.getOrdersDetails(this.pro_id)
    }, 1000)
  }
  DeleteRecords1(template: TemplateRef<any>) {
    this.modalRef4 = this.modalService.show(template);
  }

  onItemChange(value) {
    this.selectStatusId = value;
  }

  // for modal close 
  closeModal() {
    this.modalRef.hide();
    this.modalRef = null
  }
  closeModal1() {
    console.log('inside close')
    this.modalRef5.hide();
    this.modalRef5 = null
  }
  // for reload skip after changes 
  reload() {
    this.search = "";
    this.skip = 1;
    this.ngOnInit();
  }
  generatePDF() {
    this.cs.generateProductPDF(this.order_id).subscribe((data) => {
      window.open(data.path)
      this.cs.successMessage("Pdf Downloaded Successfully");
    });
  }

  // for load more Record(skip 2)
  loadMore() {
    this.skip++;
    this.cs.getOrder(this.search, this.limit, this.skip).subscribe(data => {
      var product = data.order;
      product.forEach((element) => {
        element.isSelect = false;
      })
      this.records = this.records.concat(product);
      this.remainingRecords = data.totalcount - (this.skip * this.limit);
    });
  }



  print(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          .center{
            text-align: center;
          }
          .btn-green {
            color: #fff;
            background-color: #069627;
            border-color: #069627;
          }
          .btn-red{
            background-color: red;
            border-color: red;
          }
          .invalid-feedback {
            display: block;
            width: 100%;
            margin-top: 0.25rem;
            font-size: 80%;
            color: #e65252;
          }
          .selected-list .c-btn {
            width: 102%;
            padding: 10px;
            cursor: pointer;
            display: flex;
            position: relative;
          }
          
          .description {
            margin-top: 0px;
            margin-bottom: 0px;
            height: 129px;
          }
          .view {
          display: none;
          }
          .disInput {
            pointer-events: none;
          }
          .scrollable {
            // width: 100%;
             height: 30px;
             margin: 0;
             padding: 0;
             overflow: auto;
         }
         .tableDiv {
          overflow: auto;
          width: 100%;
        }
        .openOrder {
          display: none;
        }
        .dateForm {
          display: flex;
          justify-content: center;
          margin: 2% 0%;
          padding: 0px;
        }
        .toolbar-spacer {
          flex: 1 1 auto;
        }
        .orderDetailsHeading {
          color: red;
          margin-bottom: 4%;
          padding-left: 0px;
        }
        .dateForm2 {
          display: flex;
          justify-content: flex-end;
        }
        .table th, .table td {
          padding: 0.7rem 0.5rem;
        }
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }


  // for product name list

  SelectName(data) {
    console.log("in select", data);
    this.flags = false;
    this.flag = true;
    this.namelist = true;
    this.productId = data._id;
    this.productinorderId = data.brand._id;
    this.price12 = data.sales_per_case_aed;
    this.totalQuantity = data.quantity;
    (document.getElementById('suggestName') as HTMLInputElement).value = data.brand.brand_name;
    this.categoryForm.controls['product_id'].setValue(data.brand._id);
    this.categoryForm.controls['product_name'].setValue(data.brand.brand_name);
    this.actualPriceProduct = data.sales_per_case_aed;
    let price = data.deal.isActive ? data.deal.sales_per_case_aed : data.sales_per_case_aed
    this.dealObject = data.deal
    console.log('data123', data.deal.isActive, data.deal, price);
    this.categoryForm.controls['sales_per_case_aed'].setValue(price);
    this.oldDiscountPrice = price;
    this.supplier = data.supplier_id.name;
    console.log(this.supplier, data.supplier_id.name);

    this.categoryForm.get('product_name').setErrors(null);
    this.forCal("");
  }
  suggest() {
    console.log('in input', this.countries);
    this.flag = false;
    this.categoryForm.get('product_name').markAsPristine();
    this.categoryForm.get('product_name').markAsUntouched();
    this.namelist = false;
    console.log(((document.getElementById('suggestName') as HTMLInputElement).value).toLowerCase());
    console.log(this.suggestions);
    console.log('type of', typeof (((document.getElementById('suggestName') as HTMLInputElement).value)));
    // testing start 
    let num1 = ((document.getElementById('suggestName') as HTMLInputElement).value).toLowerCase();

    if (num1 != '' && num1 != undefined) {
      num1 = num1.charAt(0);
      var matches = num1[0].match(/\d+/g);
      if (matches != null) {
        this.suggestions = this.countries
          .filter(c => c.product_code.toLowerCase().match(((document.getElementById('suggestName') as HTMLInputElement).value).toLowerCase()));
      } else {
        this.suggestions = this.countries
          .filter(c => c.brand.brand_name.toLowerCase().match(((document.getElementById('suggestName') as HTMLInputElement).value).toLowerCase()));
      }
    } else {
      this.suggestions = this.countries
        .filter(c => c.brand.brand_name.toLowerCase().match(((document.getElementById('suggestName') as HTMLInputElement).value).toLowerCase()));
    }


    let temp = this.countries.filter(c => c.brand.brand_name.toLowerCase() == (((document.getElementById('suggestName') as HTMLInputElement).value).toLowerCase()));
    if (temp.length > 0) {
      console.log('match in list', temp);
      this.flag = true;
      this.namelist = true;
      this.productId = temp[0]._id;
      this.productinorderId = temp[0].brand._id
      this.totalQuantity = temp[0].quantity;
      (document.getElementById('suggestName') as HTMLInputElement).value = temp[0].brand.brand_name;
      this.price12 = temp[0].sales_per_case_aed;
      this.categoryForm.controls['product_id'].setValue(temp[0].brand._id);
      this.categoryForm.controls['product_name'].setValue(temp[0].brand.brand_name);
      let price = temp[0].deal.isActive ? temp[0].deal.sales_per_case_aed : temp[0].sales_per_case_aed
      console.log('temppppppppp', temp[0], price, temp[0].deal);
      this.dealObject = temp[0].deal
      this.oldDiscountPrice = price;
      this.actualPriceProduct = temp[0].sales_per_case_aed;
      this.categoryForm.controls['sales_per_case_aed'].setValue(price);
      this.supplier = temp[0].supplier_id.name
      this.categoryForm.get('product_name').setErrors(null);
      this.forCal("");
      this.flags = false;
    } else {
      this.supplier = '';
      this.categoryForm.controls['sales_per_case_aed'].setValue("");
      this.flags = true;
      // this.categoryForm.get('product_name').setErrors({ 'notFound': true })
    }
  }
  forclear() {
    this.flag = false;
    this.categoryForm.controls['sales_per_case_aed'].setValue("");
    (document.getElementById('suggestName') as HTMLInputElement).value = '';
    this.categoryForm.get('product_name').setErrors({ 'required': true })
    this.supplier = "";
  }

  // for add/edit confirmation
  confirmForproduct(template: TemplateRef<any>, product_id) {
    // console.log(this.orderProductId.indexOf(product_id), product_id, this.orderProductId)
    if (this.orderProductId.indexOf(product_id) != -1) {
      this.modalRef5 = this.modalService.show(template);
      console.log('inside if');
    } else {
      console.log('inside else');
      this.submitsection2();
    }
  }

}
