import express from "express";
import userRouter from './server/modules/User/user.route';
import userAuthRouter from "./server/modules/UserAuthentication/userAuth.route";
import supplierRouter from "./server/modules/Supplier/Supplier.route";
import productRouter from "./server/modules/Product/Product.route";
import categoryRouter from "./server/modules/Category/category.route";
import adminRouter from "./server/modules/Admin/admin.route";
import settingRouter from "./server/modules/Settings/settings.route";
import customerRouter from "./server/modules/Customer/customer.route";
import dealRouter from "./server/modules/Deal/deal.route";
import orderRouter from "./server/modules/Order/order.route";
import cartRouter from "./server/modules/Cart/cart.route";
import brandRouter from "./server/modules/Brand/brand.route";
import reportRouter from "./server/modules/Reports/report.route";
import dashboardRouter from './server/modules/Dashboard/dashboard.route';
let router = [];

router.push(userRouter);
router.push(userAuthRouter);
router.push(supplierRouter);
router.push(productRouter);
router.push(categoryRouter);
router.push(adminRouter);
router.push(settingRouter);
router.push(customerRouter);
router.push(dealRouter);
router.push(orderRouter);
router.push(cartRouter);
router.push(brandRouter);
router.push(reportRouter);
router.push(dashboardRouter)

export default router;