import { Component, OnInit } from '@angular/core';
import { DataService } from '../../app/data.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormGroup } from '@angular/forms';
import { environment } from '../../environments/environment'
@Component({
  selector: 'app-add-feedback',
  templateUrl: './add-feedback.component.html',
  styleUrls: ['./add-feedback.component.css']
})
export class AddFeedbackComponent implements OnInit {

  constructor(
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    // tslint:disable-next-line:no-shadowed-variable
    private DataService: DataService) { }
  userName: string;
  DashboardData;
  object;
  feed;
  feedback: [];
  userData;
  savebtn = false;
  profileImage: string;
  receiverImage: string;
  saveFeed: FormGroup;
  image;
  public BASE_URL = environment.BASE_URL;
  ngOnInit() {
    this.spinner.show();
    this.DataService.addFeedback()
      .subscribe(
        userData => {
          if (userData.list.length === 0) {
            setTimeout(() => {
              this.spinner.hide();
            }, 1000);
            this.toastr.error('No record found!!');
            document.getElementById('empty').innerHTML = 'You already submit your feedback, thank you !!';
          } else {
            this.userData = userData.list;
            setTimeout(() => {
              this.spinner.hide();
            }, 1000);
          }
        }
      );
  }
  // for add Add Feedback (on save button)
  saveFeedback(i, data): void {
    const text = 'msg' + i;
    const feedbackValue = (document.getElementById(text) as HTMLInputElement).value;
    const file = (document.getElementById('file' + i) as HTMLInputElement).value;
    console.log('filleeeee', file);
    const b = /^\S[a-zA-Z0-9 ]+$/;
    if (feedbackValue.match(b)) {
      const formdata = new FormData();
      formdata.append('receiver_id', data.receiver_id);
      formdata.append('feedback', feedbackValue);
      formdata.append('file', this.image);

      this.object = { receiver_id: data.receiver_id, feedback: feedbackValue };
      this.DataService.saveFeedback(formdata)
        .subscribe(
          userData => {
            if (!!userData.success === true) {
              const c = 'btn' + i;
              document.getElementById(text).setAttribute('disabled', 'disabled');
              document.getElementById(c).setAttribute('disabled', 'disabled');
              document.getElementById('msg' + i).setAttribute('class', 'suc');
              setTimeout(() => {
                this.spinner.hide();
              }, 1000);
              this.toastr.success(userData.message);
            } else {
              this.toastr.error(userData.message);
            }
          }
        );
    } else {
      document.getElementById('msg' + i).setAttribute('class', 'war');
    }
  }

  fileUpload(event) {
    this.image = event[0];
    console.log('kkkkk', this.image)
  }
}
