import { Component, OnInit, AfterViewInit } from '@angular/core';
import { DataService } from '../../app/data.service';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from '../../environments/environment'
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  // tslint:disable-next-line:no-shadowed-variable
  constructor(private spinner: NgxSpinnerService, private toastr: ToastrService, private DataService: DataService, private route: Router) {
    const now = moment();
  }
  userName: string;
  // tslint:disable-next-line:max-line-length
  array = ['#f68a8a', '#f3b36f', '#154360', '#5D6D7E', '#b3d9ff', '#f9f878', '#67d781', '#fffe88' , '#5d8ae9', '#99A3A4', '#D68910', '#1D8348'];
  DashboardData;
  feedback: [];
  profileImage: string;
  button;
  public BASE_URL = environment.BASE_URL;
  ngOnInit() {
    this.spinner.show();
    this.DataService.getDashboard()
      .subscribe(
        userData => {
          if (userData.feedback.length === 0) {
            setTimeout(() => {
              this.spinner.hide();
            }, 1000);
            this.toastr.error('No data found!!');
            document.getElementById('empty').innerHTML = 'No feedback found!!!';
          } else {
            this.userName = userData.data[0].name;
            this.profileImage = this.BASE_URL + userData.data[0].profile_photo;
            this.feedback = userData.feedback;
            window.sessionStorage.setItem('Data', JSON.stringify(userData));
            setTimeout(() => {
              this.spinner.hide();
            }, 1000);
          }
        }
      );
  }
  backColor() {
    const randomColor = this.array[
      Math.floor(Math.random() * this.array.length)
    ];
    return randomColor;
  }
}
