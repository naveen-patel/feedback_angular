import express from "express";
import CategoryController from "./category.controller";
import UserAuthController from "../UserAuthentication/userAuth.controller";


const categoryRouter = express.Router();

categoryRouter.post('/category',UserAuthController.verify,CategoryController.createCategory);
categoryRouter.put('/category/:category_id',UserAuthController.verify,CategoryController.updateCategory);
categoryRouter.get('/category',UserAuthController.verify,CategoryController.getAllCategory);
categoryRouter.get('/category/:category_id',UserAuthController.verify,CategoryController.getCategoryById);
categoryRouter.get('/category/brand_name/:category_id',UserAuthController.verify,CategoryController.getBrandNameById)
categoryRouter.get('/category/product/:category_id',UserAuthController.verify,CategoryController.searchProductCategory)
categoryRouter.delete('/category/:category_id',UserAuthController.verify,CategoryController.deleteCategory);
export default categoryRouter;