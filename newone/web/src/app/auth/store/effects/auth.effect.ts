import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Effect, Actions } from '@ngrx/effects';

import { AuthService } from '../../services/auth.service';
import * as Auth from '../actions/auth.actions';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Store } from "@ngrx/store";
import * as fromAuth from "../../store/reducers";
declare var $: any;

@Injectable()
export class AuthEffects {
  @Effect()
  login$ = this.actions$
    .ofType(Auth.LOGIN)
    .map((action: Auth.Login) => action.payload)
    .exhaustMap(auth =>
      this.authService
        .login(auth)
        .map(user => new Auth.LoginSuccess(user))
        .catch(error => of(new Auth.LoginFailure(error)))
    );

  @Effect()
  register$ = this.actions$
    .ofType(Auth.REGISTER)
    .map((action: Auth.Register) => action.payload)
    .exhaustMap(auth =>
      this.authService
        .register(auth)
        .map(user => new Auth.RegisterSuccess())
        .catch(error => of(new Auth.LoginFailure(error)))
    );

    @Effect({ dispatch: false })
    registerSuccess$ = this.actions$.ofType(Auth.REGISTER_SUCCESS).do(() => {
      this.router.navigate(["/auth/login"]);
    });

    @Effect({ dispatch: false })
  loginSuccess$ = this.actions$.ofType(Auth.LOGIN_SUCCESS).do(() => {
    $("app-root").addClass("app-root-login");
    this.route.queryParams.subscribe(params => {
      if (params && params.returnUrl) {
        this.router.navigate([params.returnUrl]);
      }
    });
  });

  @Effect({ dispatch: false })
  loginFailure$ = this.actions$.ofType(Auth.LOGIN_FAILURE).do(() => {
    $("app-root").addClass("app-root-logout");
    this.store.dispatch(new Auth.HidePreLoader());
  });

  @Effect({ dispatch: false })
  loginRedirect$ = this.actions$
    .ofType(Auth.LOGIN_REDIRECT)
    .map((action: Auth.LoginRedirect) => action.payload)
    .do(params => {
      this.router.navigate(['/auth/login'], {...params});
    });

    @Effect({ dispatch: false })
    logoutRedirect$ = this.actions$.ofType(Auth.LOGOUT).do(() => {
      this.authService.logout();
      $("app-root")
      .removeClass("app-root-login")
      .addClass("app-root-logout");
      this.router.navigate(['/auth/login']);
    });

    @Effect()
    authorized$ = this.actions$
      .ofType(Auth.AUTHORIZED)
      .map(body => {
        this.store.dispatch(new Auth.ShowPreLoader());
        return body;
      })
      .switchMap(() => this.authService.authorized())
      .filter<any>(body => body.code === 200)
      .map(body => new Auth.LoginSuccess(body.user))
      .catch(error => of(new Auth.LoginFailure(error)));

  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private router: Router,
    private store: Store<fromAuth.State>,
    private route: ActivatedRoute
  ) { }
}
