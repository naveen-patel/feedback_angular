import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const SequenceSchema = mongoose.Schema({
    key: {
        type: String
    },
    sequence_no:{
        type:Number
    }
}, {
    collection: 'sequence',
    timestamps: true
});

let SequenceModel = mongoose.model('sequence', SequenceSchema);

export default SequenceModel;