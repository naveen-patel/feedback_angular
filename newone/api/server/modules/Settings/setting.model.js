import mongoose from 'mongoose';
var uuidv1 = require('uuid/v1');

const SettingsSchema = mongoose.Schema({
    host: {
        type: String
    },
    port:{
        type: String
    },
    email: {
        type: String
    },
    password: {
        type: String,
        required: true 
    },
    otp_duration : {
        type:Number
    }
}, { collection: 'Settings', timestamps: true });

let SettingsModel = mongoose.model('Settings', SettingsSchema);

export default SettingsModel