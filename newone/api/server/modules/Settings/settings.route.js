import express from "express";
import settings from "./settings.controller";
import UserAuthController from "./../UserAuthentication/userAuth.controller";

const router = express.Router();

//Get all users data
router.get('/settings', UserAuthController.verifySuperAdmin, settings.getAllsettings);

//Get Settings..
router.post('/settings/:id', UserAuthController.verifySuperAdmin, settings.Updatesettings);
router.post('/settings/otp/:id', UserAuthController.verifySuperAdmin, settings.UpdateOTPSettings);

export default router;