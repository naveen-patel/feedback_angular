import { Component, OnInit } from '@angular/core';
import { DataService } from '../../app/data.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from '../../environments/environment'
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(
    private spinner: NgxSpinnerService,
    // tslint:disable-next-line:no-shadowed-variable
    private DataService: DataService,
    private route: Router,
    private toastr: ToastrService
  ) { }
  userName: string;
  DashboardData;
  feedback: [];
  profileImage: string;
  button;
  public BASE_URL = environment.BASE_URL
  ngOnInit() {
    if (this.route.url === '/dashboard') {
      this.button = 'Add Feedback';
    } else {
      this.button = 'Dashboard';
    }
    this.DataService.getDashboard()
      .subscribe(
        userData => {
          this.userName = userData.data[0].name;
          this.profileImage = this.BASE_URL + userData.data[0].profile_photo;
          this.feedback = userData.feedback;
        }
      );
  }
  addfeedback() {
    if (this.route.url === '/dashboard') {
      this.route.navigate(['addFeedback']);
    } else {
      this.route.navigate(['dashboard']);
    }
  }
  logout() {
    this.spinner.show();
    sessionStorage.clear();
    localStorage.clear();
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);
    this.toastr.success('Successfully logout, thank you for your time!!!');
    this.route.navigate(['login']);
  }
}
