import express from "express";
import UserAuthController from "./userAuth.controller"
import userAuthService from "./userAuth.service";

const router = express.Router();

//Authenticate user and provide token for login
router.post('/users/login', (req, res) => {
    return UserAuthController.login(req, res);
});
//For mobile login
router.post('/customer/login', (req, res) => {
    return UserAuthController.customerLogin(req, res);
});

router.post('/super_admin/login', (req, res) => {
    return UserAuthController.superAdminlogin(req, res);
});

//signup with API
router.post('/users/signup', (req, res) => {
    return UserAuthController.signup(req, res);
});

router.post('/super_admin/signup', (req, res) => {
    return UserAuthController.creatSuperAdmin(req, res);
});
//for mobile signup
router.post('/customer/signup', (req, res) => {
    return UserAuthController.customerSignup(req, res);
});

router.get('/customer/logout/:id',(req, res)=>{
    return UserAuthController.customerLogout(req, res);
})

router.post('/forget_password', (req, res) => {
    return UserAuthController.forgetPassword(req, res);
});
router.post('/change_password', (req, res) => {
    return UserAuthController.changePassword(req, res);
});

export default router;