import mongoose from 'mongoose';
var Schema = mongoose.Schema;

const OrderSchema = mongoose.Schema({
    user_id:{
        type: Schema.Types.ObjectId,
        ref: "user"
    },
    order_number:{
        type:String
    },
    
    product:[{
        quantity:{
        
        type:Number
        },
        deal:{
            isActive:{type:Boolean},
            offer_price_usd:{type:String},
            offer_price_aed:{type:String},
            deal_percentage:{type:String}
        },
        sales_per_case_aed: {
            type: String
        },
        total_price_aed:{ type: String},
        total_price_usd:{ type: String},
        sales_per_case_usd: {
            type: String
        },
        discount_price_aed:{
            type: String
        },
        discount_price_usd:{
            type: String
        },
        isAdd:{
            type:Boolean,
            default:true
        },
        product_detail:{
            type: Schema.Types.ObjectId,
            ref: "product"
        },
     }],
    order_date:{
        type:Date
    },
    total_amount_aed: {
        type: String
    },
    total_amount_usd: {
        type: String
    },
    order_status: {
        type: String,
        enum: ['dispatched', 'order_in_progress', 'delivered','order_placed','cancelled'],
        default:'order_placed'
    },
    callback_request:{
        type:Boolean,
        default:false
    }
}, {
    collection: 'order',
    timestamps: true
});

let OrderModel = mongoose.model('order', OrderSchema);

export default OrderModel