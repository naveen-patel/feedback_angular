import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-view-supplier-details',
  templateUrl: './view-supplier-details.component.html',
  styleUrls: ['./view-supplier-details.component.scss']
})
export class ViewSupplierDetailsComponent implements OnInit {

  supplier: any
  productDetails: any
  currentId: String
  page = 1
  openViewProduct: boolean = false
  parentmessage: any

  constructor(
    private route: ActivatedRoute,
    private ds: DataService
  ) { }

  ngOnInit() {
    this.supplier = this.route.snapshot.data['supplierDetails']
    this.productDetails = this.supplier.product
  }
  productLoadMore() {
    this.page = this.page + 1
    var limit = 10
    this.ds.getAllProduct(this.supplier._id, '', '', '', limit, this.page).subscribe(res => {
      if (res.product.product.length) {
        res.product.product.forEach(element => {
          this.productDetails.product.push(element)
        });
      }
    })
  }
  openViewProductDetails(product, view) {
    product.isViewOnly = view
    this.openViewProduct = true;
    this.parentmessage = product;
  }
  receiveMessage(event) {
    this.openViewProduct = false;
  }
  imageData:any;
  openImagePreview:Boolean=false;
  imagePreview(data){
    if(data){
      this.openImagePreview=true;
      this.imageData = data
    }
  }
  closeImagePreview(){
    this.imageData="";
    this.openImagePreview =false
  }
}
