import { Component, OnInit, OnDestroy, TemplateRef, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { DataService } from '../../services/data.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { CompressorService } from '../../services/compressor.service';
import { map, expand } from 'rxjs/operators';
import { NgxImageCompressService } from 'ngx-image-compress';
import { CustomerService } from '../../services/customer.service';
@Component({
  selector: 'app-manage-product',
  templateUrl: './manage-product.component.html',
  styleUrls: ['./manage-product.component.scss']
})
export class ManageProductComponent implements OnInit {

  modalRef3: BsModalRef;
  modalRef: BsModalRef;
  myform: FormGroup;
  message = false;
  addNew: any;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    keyboard: false,
    class: 'modal-lg'
  }
  bsConfig =
    {
      showWeekNumbers: false,
      containerClass: 'theme-red',
      changeMonth: true,
      dateInputFormat: 'DD/MM/YYYY',
      changeYear: true
    }

  editProduct: boolean = false;
  submitted: boolean = false
  editNew: any;

  product_front_image: any = "";
  product_back_image: any = "";

  formData: any = new FormData();
  couponType: String = "";
  errorImageSize1: Boolean = false;
  errorImageSize2: Boolean = false
  viewOnly: Boolean = false
  categoryDetail;
  expirydate: Date;
  //new 
  selectLocationId = 'l';
  selectCategoryId = 'c';
  selectSupplierId = 's';
  selectBrandId = 'b';
  search = '';
  limit: Number = 10;
  page: Number = 1;
  count: Number;
  deleteImage1: Boolean = false;
  deleteImage2: Boolean = false;

  loader: boolean = false;
  records = [];
  //new 

  pageForm;
  locationList = [];
  categoryList = [];
  supplierList = [];
  brandList = [];
  minDate: Date = new Date();
  constructor(
    private routePath: Router,
    private ds: DataService,
    private cs: CustomerService,
    private route: ActivatedRoute,
    public formBuilder: FormBuilder,
    private modalService: BsModalService,
    private compressor: CompressorService,
    private imageCompress: NgxImageCompressService
  ) {

  }
  @Input() childMessage: any;

  @Output() messageEvent = new EventEmitter<any>();

  @ViewChild("template") addTemplate: TemplateRef<any>;
  @ViewChild("delete") addBrand: TemplateRef<any>;

  getFormFields() {
    if (this.editProduct) {
      this.product_front_image = this.childMessage.product_front_image;
      this.product_back_image = this.childMessage.product_back_image;
      this.selectBrandId = this.childMessage.brand;
      this.selectSupplierId = this.childMessage.supplier_id;
      this.selectCategoryId = this.childMessage.category_id;
      this.selectLocationId = this.childMessage.stock_location;
      this.expirydate = this.childMessage.expiry_date;
      //this.expirydate = moment(this.childMessage.expiry_date).format('yyyy-MM-dd')
      const currentDate = new Date().toISOString().substring(0, 10);
      // document.getElementById('datePicker'). = new Date(this.childMessage.expiry_date);
      this.childMessage.expiry_date
      // let a = this.childMessage.sales_per_case_aed
      // console.log(a.toFixed(2));
      return {
        _id: [this.childMessage._id],
        fmcg_availability: [{ value: this.childMessage.fmcg_availability, disabled: this.viewOnly }],
        stock_location: [{ value: this.childMessage.stock_location, disabled: this.viewOnly }],
        category_id: [{ value: this.childMessage.categorys[0]._id, disabled: this.viewOnly }],
        supplier_id: [{ value: this.childMessage.supplier_id, disabled: true }],
        //product_name: [{ value: this.childMessage.product_name, disabled: this.viewOnly }, <any>,],
        brand_id: [{ value: this.childMessage.brands[0].brand_name, disabled: this.viewOnly }, <any>Validators.required],
        product_type: [{ value: this.childMessage.product_type, disabled: this.viewOnly }],
        expiry_date: [{ value: this.childMessage.expiry_date, disabled: this.viewOnly }],
        ru_per_case: [{ value: this.childMessage.ru_per_case, disabled: this.viewOnly }],
        ean_code_ru: [{ value: this.childMessage.ean_code_ru, disabled: this.viewOnly }],
        ean_code_case: [{ value: this.childMessage.ean_code_case, disabled: this.viewOnly }],
        language: [{ value: this.childMessage.language, disabled: this.viewOnly }],
        origin: [{ value: this.childMessage.origin, disabled: this.viewOnly }],
        quantity: [{ value: this.childMessage.quantity, disabled: this.viewOnly }],
        sales_per_case_aed: [{ value: parseFloat(this.childMessage.sales_per_case_aed).toFixed(2), disabled: this.viewOnly }],
        sales_per_case_usd: [{ value: this.childMessage.sales_per_case_usd, disabled: this.viewOnly }],
        cost_price_per_case: [{ value: this.childMessage.cost_price_per_case, disabled: this.viewOnly }],
        description: [{ value: this.childMessage.description, disabled: this.viewOnly }],
        size: [{ value: this.childMessage.size, disabled: this.viewOnly }]
      }
    } else {
      return {
        fmcg_availability: [null],
        stock_location: [""],
        category_id: [""],
        supplier_id: [""],
        brand_id: ["", Validators.required],
        // brand_id: ["", Validators.required],
        product_type: [""],
        expiry_date: [""],
        ru_per_case: [""],
        ean_code_ru: [""],
        ean_code_case: [""],
        language: [""],
        origin: [""],
        quantity: [""],
        sales_per_case_aed: [""],
        sales_per_case_usd: [""],
        cost_price_per_case: [""],
        description: [""],
        size: [""]
      }
    }
  }

  ngOnInit() {
    if (this.childMessage) {
      this.editProduct = true;
      this.viewOnly = this.childMessage.isViewOnly
    } else {
      this.viewOnly = false
    }

    setTimeout(() => {
      this.openModal(this.addTemplate)
    }, 1000);
    // this.ds.getBrandList().subscribe(data => {
    //   this.brandList = data;
    //   console.log(this.brandList);

    //   this.myform = this.formBuilder.group(this.getFormFields());
    //   setTimeout(() => {
    //     this.selectBrandId = this.childMessage.brand_id;
    //   }, 1000);
    // });
    this.ds.getSuppliers().subscribe(data => {
      this.supplierList = data;
      this.myform = this.formBuilder.group(this.getFormFields());
      setTimeout(() => {
        this.selectSupplierId = this.childMessage.supplier_id;
        this.expirydate = this.childMessage.expiry_date;
      }, 1000);
    });
    this.cs.getAllCategory().subscribe(data => {
      this.categoryList = data.category;

      this.locationList = [{ 'country_name': 'UK' }, { 'country_name': 'UAE' }];
      this.myform = this.formBuilder.group(this.getFormFields());
      setTimeout(() => {
        if (this.editProduct) {
          this.selectCategoryId = this.childMessage.category_id;
        } else {
          this.selectCategoryId = this.childMessage.category_id;
        }
        this.selectLocationId = this.childMessage.stock_location;
      }, 1000);
      let a = [];
      for (let index = 0; index < this.categoryList.length; index++) {
        if (this.categoryList[index]._id == this.childMessage.category_id) {
          let c = this.categoryList[index].brand_id;
          for (let index1 = 0; index1 < c.length; index1++) {
            let temp = {}
            temp['brand_id'] = this.categoryList[index].brand_id[index1];
            temp['brand_name'] = this.categoryList[index].brand_name[index1];
            a.push(temp)
          }
        }
      }
      this.brandList = a
      this.selectBrandId = this.childMessage.brand;
    });
  }


  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.config);
  }
  onSubmit() {
    let value = this.myform.get('sales_per_case_aed').value;
    console.log('valueeee', value);
    if (String(value).match('/^(?=.*[1-9])\d*(?:\.\d{1,2})?$/') || value == null) {
      this.myform.get('sales_per_case_aed').setErrors({ 'notValid': true })
    }
    else {
      this.myform.get('sales_per_case_aed').setErrors(null);
    }
    this.submitted = true;
    if (this.myform.valid) {
      if (this.editProduct) {
        let formValue = Object.assign({}, this.myform.value);
        this.editNew = formValue;
        this.editNew.delete_card_1 = this.deleteImage1;
        this.editNew.delete_card_2 = this.deleteImage2;
        this.formData.set('bodyData', JSON.stringify(this.editNew))
      } else {
        let formValue = Object.assign({}, this.myform.value);
        this.addNew = formValue;
        this.formData.set('bodyData', JSON.stringify(this.addNew))
      }
      this.submitsection();
    }
  }

  get f() { return this.myform.controls; }

  submitsection() {
    if (this.editProduct) {
      this.ds.updateProduct(this.childMessage._id, this.formData).subscribe(data => {
        this.closeModel();
      })
    } else {

      this.ds.addProduct(this.formData, this.selectSupplierId).subscribe(data => {
        this.closeModel();
      });
    }
  }

  // forBrandDropDown() {
  //   console.log("hey");

  //   // this.ds.getBrandList().subscribe(data => {
  //   //  this.brandList = []
  //   //   console.log(this.brandList);

  //   //   this.myform = this.formBuilder.group(this.getFormFields());
  //   //   setTimeout(() => {
  //   //     this.selectBrandId = this.childMessage.brand_id;
  //   //   }, 1000);
  //   // });
  // }

  closeModel1() {
    this.modalRef3.hide();
    this.modalRef3 = null;
  }
  closeModel() {
    this.modalRef.hide();
    this.modalRef = null
    this.messageEvent.emit([this.message, this.addNew, this.editNew])
  }
  // ngOnDestroy(): void {
  // this.closeModel1()
  // }

  onItemChange(value) {
    this.selectSupplierId = value;
  }
  onItemChange1(value, i) {
    this.selectCategoryId = value;
    let b = this.categoryList.filter(countryItem => countryItem._id === value);
    this.categoryDetail = b;
    if (b[0].brand_name.length) {
      let a = []
      for (let index1 = 0; index1 < b[0].brand_name.length; index1++) {
        let temp = {}
        temp['brand_id'] = b[0].brand_id[index1];
        temp['brand_name'] = b[0].brand_name[index1];
        a.push(temp)
      }
      this.brandList = a;
      // setTimeout(() => {
      this.selectBrandId = this.childMessage.brand_id;
      // }, 1000);
    }
    else {
      this.brandList = [];
      this.selectBrandId = "";
      this.modalRef3 = this.modalService.show(this.addBrand, this.config);

    }
  }

  forBrandPage() {
    this.closeModel();
    setTimeout(() => {
      this.closeModel1();
      this.cs.forBrandModal(this.categoryDetail, true);
      this.routePath.navigate(['brand']);
    }, 1000)
  }

  onItemChange2(value) {
    this.selectLocationId = value;
  }
  onItemChange3(value) {
    this.selectBrandId = value;
  }
  // getProducts(){
  //   this.ds.getAllProduct(this.selectSupplierId,this.search, this.limit, this.page).subscribe(data => {
  //     this.loader = false;
  //     this.count = data.product.totalcount;
  //     this.records = data.product.product;
  //   })
  // }
  keypressOnlyAlpha(event) {
    var regex = RegExp("^a-zA-Z");
    var value = event.key.match(/[0-9]/i)
    return value === null ? true : false;
  }
  imageDelete(type) {
    if (type === 'image1') {
      this.deleteImage1 = true;
      this.product_front_image = "";
      this.formData.set("product_front_image", "");

    } else if (type === 'image2') {
      this.deleteImage2 = true;
      this.product_back_image = "";
      this.formData.set("product_back_image", "");

    }

  }


  data: FileList;
  compressedImages = [];
  recursiveCompress = (image: File, index, array) => {


    if (image) {
      return this.compressor.compress(image).pipe(
        map(response => {

          //Code block after completing each compression
          this.compressedImages.push(response);
          return {
            data: response,
            index: index + 1,
            array: array,
          };
        }),
      );
    }

  }

  public process(event, image) {
    this.data = event.target.files;
    var filesize = ((this.data[0].size / 1024) / 1024).toFixed(4);
    // if (Number(filesize) < 1.1) {


    var obj = {};
    const compress = this.recursiveCompress(this.data[0], 0, this.data);
    // .pipe(
    //   expand(res => {
    //     return this.recursiveCompress(this.data[res.index], res.index, this.data);
    //   }),
    // );
    compress.subscribe(res => {
      if (res.index > res.array.length - 1) {
        var reader = new FileReader();
        // reader.onload = (function (aImg) { return function (e) { aImg.src = e.target.result; }; })(img);
        reader.readAsDataURL(this.compressedImages[0]);
        reader.onload = () => {
          // this.filename = file.name;
          if (image === 'product_front_image') {
            this.deleteImage1 = false;
            this.formData.set("product_front_image", this.compressedImages[0], this.compressedImages[0].name);
            this.product_front_image = reader.result;
            this.errorImageSize1 = false;
            this.compressedImages = []
          } else {
            this.deleteImage2 = false;
            this.formData.set("product_back_image", this.compressedImages[0], this.compressedImages[0].name);
            this.product_back_image = reader.result;
            this.errorImageSize2 = false;
            this.compressedImages = [];
          }
        };
      }
    });
    // } else {
    //   this.ds.errorMessage('Plesae Upload Image size lesser than 1 MB')
    //   if (image === 'product_front_image') {
    //     this.errorImageSize1 = true;

    //   } else {
    //     this.errorImageSize2 = true;

    //   }
    // }
  }
  toogle() {
  }
  AEDtoUSD(value) {
    value = Number((value / 3.67250).toFixed(2));

    //this.myform['sales_per_case_usd'].value = value;
    this.myform.controls['sales_per_case_usd'].setValue(value);
  }

  forclear() {
    this.myform.controls['expiry_date'].setValue('');
  }

  compressFile(type) {
    this.imageCompress.uploadFile().then(({ image, orientation }) => {
      if (this.imageCompress.byteCount(image) > 30000) {
        this.imageCompressToMinSize(image, type, orientation)
      } else {
        //this.imgResultAfterCompress = result;
        var block = image.split(";");
        // Get the content type of the image
        var contentType = block[0].split(":")[1];// In this case "image/gif"
        // get the real base64 content of the file
        var realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."

        // Convert it to a blob to upload
        var blob = this.b64toBlob(realData, contentType);
        if (type === 'product_front_image') {
          this.deleteImage1 = false;
          this.product_front_image = image;
          this.formData.set("product_front_image", blob, 'product_front_image');
        } else {
          this.deleteImage2 = false;

          this.product_back_image = image;
          this.formData.set("product_back_image", blob, 'product_back_image');
        }

        console.warn('Size in bytes is now:', this.imageCompress.byteCount(image));
      }
    });
  }

  imageCompressToMinSize(image, type, orientation) {
    this.imageCompress.compressFile(image, orientation, 50, 50).then(
      result => {
        console.warn('Size in bytes was:', this.imageCompress.byteCount(image));
        if (this.imageCompress.byteCount(result) < 40000) {
          var block = result.split(";");
          var contentType = block[0].split(":")[1];
          var realData = block[1].split(",")[1];
          var blob = this.b64toBlob(realData, contentType);
          if (type === 'product_front_image') {
            this.deleteImage1 = false;
            this.product_front_image = image;
            this.formData.set("product_front_image", blob, 'product_front_image');
          } else {
            this.deleteImage2 = false;

            this.product_back_image = image;
            this.formData.set("product_back_image", blob, 'product_back_image');
          }

          console.warn('Size in bytes is now:', this.imageCompress.byteCount(result));
        } else {
          this.imageCompressToMinSize(result, type, orientation);
        }
      }
    );
  }
  b64toBlob(b64Data, contentType) {
    contentType = contentType || '';
    var sliceSize = 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);

      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      var byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }
  imageData: any;
  openImagePreview: Boolean = false;
  imagePreview(data) {
    if (data) {
      this.openImagePreview = true;
      this.imageData = data
    }
  }
  closeImagePreview() {
    this.imageData = "";
    this.openImagePreview = false
  }


}
