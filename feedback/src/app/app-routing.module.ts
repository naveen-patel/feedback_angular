import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AddFeedbackComponent } from './add-feedback/add-feedback.component';
import { RegistrationComponent } from './registration/registration.component';
import { ForgotpassComponent } from './forgotpass/forgotpass.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DemoComponent } from './demo/demo.component';
import { AuthGuard } from './auth.guard';
import { HeaderComponent } from './header/header.component';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// for routing
const routes: Routes = [
  { path: 'addFeedback', component: AddFeedbackComponent },
  { path: 'registration', component: RegistrationComponent },
  { path: 'login', component: LoginComponent, canActivate: [AuthGuard] },
  { path: 'forgot', component: ForgotpassComponent },
  { path: 'navbar', component: HeaderComponent, canActivate: [AuthGuard] },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'demo', component: DemoComponent },
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: '**', redirectTo: 'login', pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes,
      { useHash: true }),
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 2000,
      extendedTimeOut: 1000,
      preventDuplicates: false
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
