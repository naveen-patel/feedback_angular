import mongoose from 'mongoose';
var Schema = mongoose.Schema;

const backupSchema = mongoose.Schema({
    fileId: {
        type: String,
    }  
}, {
    collection: 'backup',
    timestamps: true
});

let backupModel = mongoose.model('backup', backupSchema);

export default backupModel