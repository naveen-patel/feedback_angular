import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { CustomerService } from '../../services/customer.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {

  title = 'Order Reports';
  forTab = 'order_reports';
  forDateform = false;
  dateForm: FormGroup;
  submitted: boolean = false;
  addNew: any;

  minDate1: Date;
  minDate2: Date;
  startDate: any;
  endDate: any;
  bsConfig =
    {
      showWeekNumbers: false,
      containerClass: 'theme-red',
      changeMonth: true,
      dateInputFormat: 'DD/MM/YYYY',
      changeYear: true,
    }


  remainingRecords = 0;
  search: string = '';
  limit = 10;
  skip = 1;
  loader: boolean = false;
  count: Number;
  records = [];
  index;

  constructor(
    private Router: Router,
    public cs: CustomerService,
    private modalService: BsModalService,
    public formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.forPageRefresh();
    this.forNavigate();
    this.dateForm = this.formBuilder.group(this.getFormFields());
  }

  forNavigate() {
    this.Router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        if (e.url == "/reports/order_reports") {
          this.title = "Order Reports";
          this.forTab = 'order_reports';
          this.forDateform = false;
          this.dateForm.setValue({
            start_date: '',
            end_date: ''
          });
          // this.dateForm.get('end_date').setErrors({ 'required': false });
          // this.dateForm.get('start_date').setErrors({ 'required': false });
          // (document.getElementById('start') as HTMLInputElement).value = '';
          // (document.getElementById('end') as HTMLInputElement).value = '';
          this.index = 0;
          this.count = 0;
          this.records = [];
          this.remainingRecords = 0;
        }
        if (e.url == "/reports/sales_reports") {
          this.title = "Sales Reports";
          this.forTab = 'sales_reports';
          this.forDateform = false;
          this.dateForm.setValue({
            start_date: '',
            end_date: ''
          });
          // this.dateForm.get('end_date').setErrors({ 'required': false });
          // this.dateForm.get('start_date').setErrors({ 'required': false });
          // (document.getElementById('start') as HTMLInputElement).value = '';
          // (document.getElementById('end') as HTMLInputElement).value = '';
          this.index = 1;
          this.count = 0;
          this.records = [];
          this.remainingRecords = 0;
        }
        if (e.url == "/reports/stock_reports") {
          this.title = "Low Stock Items";
          this.forTab = 'stock_reports';
          this.forDateform = true;
          this.index = 2;
          this.forLowStock();
          this.count = 0;
          this.records = [];
          this.remainingRecords = 0;
        }
      }
    });
  }
  forPageRefresh() {
    if (this.Router.url == "/reports/order_reports") {
      this.title = "Order Reports";
      this.forTab = 'order_reports';
      this.forDateform = false;
      this.index = 0;
    }
    if (this.Router.url == "/reports/sales_reports") {
      this.title = "Sales Reports";
      this.forTab = 'sales_reports';
      this.forDateform = false;
      this.index = 1;
    }
    if (this.Router.url == "/reports/stock_reports") {
      this.title = "Low Stock Items";
      this.forTab = 'stock_reports';
      this.forDateform = true;
      this.index = 2;
      this.forLowStock();
    }
  }

  // for deals form
  getFormFields() {
    {
      this.startDate = '';
      this.endDate = '';
      return {
        start_date: ["", Validators.required],
        end_date: ["", Validators.required],
      }
    }
  }
  forLowStock() {
    this.remainingRecords = 0;
    this.skip = 1;
    let endDate = "";
    let startDate = "";
    this.cs.getReports(endDate, startDate, this.limit, this.skip, this.index).subscribe(data => {
     
      this.loader = false;
      this.count = data.count;
      this.records = data.data;
      this.remainingRecords = data.count - (this.skip * this.limit);
      this.records.forEach((element) => {
        element.isSelect = false;
      })

    });


  }

  forclear() {
    this.remainingRecords = 0;
    this.records = [];
    this.remainingRecords = 0;
  }

  onSubmit() {
    this.submitted = true;
    if (new Date(this.dateForm.controls['start_date'].value).setHours(0, 0, 0, 0) > new Date(this.dateForm.controls['end_date'].value).setHours(0, 0, 0, 0)) {
      this.dateForm.get('end_date').setErrors({ 'startdate': true });
      return false
    }
    if (this.dateForm.valid) {
      let formValue = Object.assign({}, this.dateForm.value);
      this.addNew = formValue;
      this.submitsection();
    }// this.cs.addDeal(this.addNew, this.productId).subscribe(data => {
    // });
  }

  findInvalidControls() {
    const invalid = [];
    const controls = this.dateForm.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
 

    return invalid;
  }
  get f() { return this.dateForm.controls; }

  submitsection() {
    this.remainingRecords = 0;
    this.skip = 1;
    let endDate = this.dateForm.get('end_date').value;
    let startDate = this.dateForm.get('start_date').value;
   
    this.cs.getReports(endDate, startDate, this.limit, this.skip, this.index).subscribe(data => {
      
      this.loader = false;
      this.count = data.count;
      this.records = data.data;
      this.remainingRecords = data.count - (this.skip * this.limit);
      this.records.forEach((element) => {
        element.isSelect = false;
      })
    });
  }
  // for load more Record(skip 2)
  loadMore() {
    let endDate = this.dateForm.get('end_date').value;
    let startDate = this.dateForm.get('start_date').value;
    this.skip++;
    this.cs.getReports(endDate, startDate, this.limit, this.skip, this.index).subscribe(data => {
      var product = data.data;
      this.loader = false;
      this.remainingRecords = data.count - (this.skip * this.limit);
      this.records = this.records.concat(product);
      this.remainingRecords = data.count - (this.skip * this.limit);
    });
  }


  generatePDF() {
    let endDate = this.dateForm.get('end_date').value;
    let startDate = this.dateForm.get('start_date').value;
    if (this.forTab == "order_reports") {
      if ((this.dateForm.valid) && (startDate < endDate)) {
        this.cs.generateReportPdf(endDate, startDate, 'order').subscribe((data) => {
          window.open(data.path)
          this.cs.successMessage("Pdf Downloaded Successfully");
        });
      } else {
        this.cs.errorMessage("Please select valid date first!!");
      }
    } else if (this.forTab == "sales_reports") {
      if ((this.dateForm.valid) && (startDate < endDate)) {
        this.cs.generateReportPdf(endDate, startDate, 'sales').subscribe((data) => {
          window.open(data.path)
          this.cs.successMessage("Pdf Downloaded Successfully");
        });
      } else {
        this.cs.errorMessage("Please select valid date first!!");
      }
    } else {
      this.cs.generateReportPdf(endDate, startDate, 'low').subscribe((data) => {
        window.open(data.path)
        this.cs.successMessage("Pdf Downloaded Successfully");
      });
    }
  }


}
