const exec = require("child_process").exec;
const zipFolder = require("zip-folder");
import logger from "../../../config/logger.config";
const rimraf = require("rimraf");
import settings from '../../../settings';
import backupService from '../backup/backup.service';
let config = require('./../../../config/' + settings.environment + '.config');
// var cron = require("node-cron");
const fs = require('fs');
const {google} = require('googleapis');
const TOKEN_PATH = '/token.json';
const backupController ={}
backupController.takeMongoBackup = async ()=> {
  //remove directory
  rimraf.sync(__dirname + "/" +config.default.mongo.database_name);
  let cmd;
  if(settings.environment === "production"){
  cmd =
    "mongodump --host "+
    config.default.mongo.host +
    " --port "+
    config.default.mongo.port+
    " -u "+
    config.default.mongo.username +
    " -p "+
    config.default.mongo.password +
    " --authenticationDatabase "+
    config.default.mongo.database_name +
    " --out " +
    __dirname; // Command for mongodb dump process

  }else{
    cmd =
    "mongodump --host " +
    config.default.mongo.host +
    " --db " +
    config.default.mongo.database_name +
    " --out " +
    __dirname; // Command for mongodb dump process
  }
 

  logger.info("DB backup started ... ");
  exec(cmd, function(error, stdout, stderr) {
    // logger.error("Error while backup" +error)
        if (empty(error)) {
          logger.info("DB backup generated ... ");

      //zip backup
      zipFolder(
        __dirname + "/" + config.default.mongo.database_name,
        __dirname + "/" + config.default.mongo.database_name + ".zip",
        function(err) {
          if (err) {
            logger.error("Zip error ... ");
            logger.error("oh no!", err);
          } else {
            logger.info("Backup zipped successful");

            //upload on drive
            uploadFileToDrive(() => {
              //remove directory
              rimraf.sync(__dirname + "/" +config.default.mongo.database_name);
              rimraf.sync(__dirname + "/" +config.default.mongo.database_name + ".zip");
            });
          }
        }
      );
    }
  });
}
backupController.deleteLastBackUp = async ()=>{
try {
  let backUpFiles = await backupService.getFile();
  await deleteFileFromDrive(backUpFiles);
} catch (error) {

}
}

async function uploadFileToDrive(cb) {
await authorize(config.default.drive, storeFiles);
async function authorize(credentials, callback) {
   
    const {client_secret, client_id, redirect_uris} = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(
        client_id, client_secret, redirect_uris[0]);
      oAuth2Client.on('tokens', (tokens) => {
        if (tokens.refresh_token) {
            fs.writeFile(__dirname+TOKEN_PATH, JSON.stringify(tokens), (err) => {
                if (err) return logger.error(err);
                logger.info('Token refreshed');
              });
        }
	  });

    fs.readFile(__dirname+TOKEN_PATH, (err, token) => {
      if(err) throw err 
      oAuth2Client.setCredentials(JSON.parse(token));
      callback(oAuth2Client);
    });
}
  
function storeFiles(auth) {
    const fileName = config.default.mongo.database_name + ".zip";
    const drive = google.drive({version: 'v3', auth});
    var fileMetadata = {
      name: fileName,
      parents: [config.default.driveBackupFileLocation]
    };
    var media = {
      mimeType: "application/zip",
      body: fs.createReadStream(__dirname+'/'+fileName)
    };
    drive.files.create(
      {
        resource: fileMetadata,
        media: media,
        fields: "id"      },
    async  function(err, file) {
        if (err) {
          // Handle error
          logger.error(err);
          logger.info(
            "Make sure you shared your drive folder with service email/user."
          );
        } else {
          logger.info("File Id: ", file.data.id);
          let result = await backupService.add({fileId:file.data.id});
          if (cb) cb(result);
        }
      }
    );
  }
}

async function deleteFileFromDrive(fileId) {
  await authorize(config.default.drive);
async function authorize(credentials) {
    const {client_secret, client_id, redirect_uris} = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(
        client_id, client_secret, redirect_uris[0]);
      oAuth2Client.on('tokens', (tokens) => {
        if (tokens.refresh_token) {
            fs.writeFile(__dirname+TOKEN_PATH, JSON.stringify(tokens), (err) => {
                if (err) return console.log(err);
                console.log('Token refreshed');
              });
        }
	  });
    fs.readFile(__dirname+TOKEN_PATH, (err, token) => {
      if(err) throw err 
      oAuth2Client.setCredentials(JSON.parse(token));
      deleteFiles(oAuth2Client,fileId);
    });
}
}
async function deleteFiles(auth,fileId) {
  const drive = google.drive({version: 'v3', auth});
  await Promise.all(fileId.map(fileId => {
    drive.files.delete(
      {
        fileId:  fileId.fileId     },
    async  function(err, file) {
        if (err) {
          // Handle error
          console.log(
            "Make sure you shared your drive folder with service email/user."+err
          );
        } else {
          console.log("File Id: ", file);
          await backupService.deleteFile(fileId.fileId);
        }
      }
    );
  }));
  

}
/* return if variable is empty or not. */
const empty = function(mixedVar) {
  var undef, key, i, len;
  var emptyValues = [undef, null, false, 0, "", "0"];
  for (i = 0, len = emptyValues.length; i < len; i++) {
    if (mixedVar === emptyValues[i]) {
      return true;
    }
  }
  if (typeof mixedVar === "object") {
    for (key in mixedVar) {
      return false;
    }
    return true;
  }
  return false;
};
// cron.schedule(config.SCHEDULE_TIME, () => {
//   console.log("running a task every minute");
//   takeMongoBackup();
// });

// takeMongoBackup();
export default backupController