import OrderModel from "../Order/order.model";
import ProductModel from "../Product/Product.model";
const fs = require('fs');
var mongoose = require('mongoose');
import settings from '../../../settings';
let config = require('./../../../config/' + settings.environment + '.config').default;
import moment from "moment";
import { stat } from "fs";

const ReportService = {
  getLowStockReport: async (limit,skip) => {
    try {
      let report
      let count = await ProductModel.find({quantity:{$lte:10}})
      .populate({path:'supplier_id',select:'name'})
      .populate({path:'brand',select:'brand_name'})
      .sort({"order_date": 1}).count();
      if(skip != null){
        report = await ProductModel.find({quantity:{$lte:10}})
                                     .populate({path:'supplier_id',select:'name'})
                                     .populate({path:'brand',select:'brand_name'})
                                     .sort({"order_date": 1})
                                     .skip(skip).limit(limit).lean();
      }else{
        report = await ProductModel.find({quantity:{$lt:10}})
                                     .populate({path:'supplier_id',select:'name'})
                                     .populate({path:'brand',select:'brand_name'})
                                     .sort({"order_date": 1}).lean();
      }
      let json ={};
      json.count = count;
      json.report = report;
      return json;
    } catch (error) {
      throw error;
    }
  },
  getOrderReport: async (startDate,endDate,limit,skip)=>{
    try {
      let startDate1 = moment(startDate).startOf('day').format("YYYY-MM-DD")
      let endDate1 = moment(endDate).startOf('day').format("YYYY-MM-DD");
      let data;
      let count = await OrderModel.find({$and:[{order_date:{$gte:startDate1.toString(),$lte:endDate1.toString()}}]})
                                      .populate({path:'user_id',select:'country_name'})
                                      .sort({"order_date": 1}).count();
      if(skip != null){  
      data=  await OrderModel.find({$and:[{order_date:{$gte:startDate1.toString(),$lte:endDate1.toString()}}]})
                                  .populate({path:'user_id',select:'country_name'})
                                  .sort({"order_date": 1})
                                  .skip(skip).limit(limit).lean();
      }else{
        data =  await OrderModel.find({$and:[{order_date:{$gte:startDate1.toString(),$lte:endDate1.toString()}}]})
                                  .populate({path:'user_id',select:'country_name'})
                                  .sort({"order_date": 1})
                                  .lean();
      }
      let json ={};
      json.count = count;
      json.order = data
      return json;
    } catch (error) {
      throw error;
    }
  },
  getSalesReport : async (startDate,endDate,limit,skip)=>{
    try {
      let startDate1 = moment(startDate).startOf('day').format("YYYY-MM-DD")
      let endDate1 = moment(endDate).startOf('day').format("YYYY-MM-DD");
      let data;
      let count = await OrderModel.find({$and:[{order_date:{$gte:startDate1.toString(),$lte:endDate1.toString()}},{order_status:'delivered'}]})
                                          .populate({
                                            path:'product.product_detail',
                                            select:'product_type',
                                            populate:{
                                            path:'supplier_id brand',
                                            select:'name brand_name'
                                          }
                                        })  
                                      .sort({"order_date": 1}).count();
      if(skip != null){  
      data=  await OrderModel.find({$and:[{order_date:{$gte:startDate1.toString(),$lte:endDate1.toString()}},{order_status:'delivered'}]})
                                      .populate({
                                        path:'product.product_detail',
                                        select:'product_type',
                                        populate:{
                                        path:'supplier_id brand',
                                        select:'name brand_name'
                                      }
                                    })  
                                  .sort({"order_date": 1})
                                  .skip(skip).limit(limit).lean();
      }else{
        data =  await OrderModel.find({$and:[{order_date:{$gte:startDate1.toString(),$lte:endDate1.toString()}},{order_status:'delivered'}]})
                                          .populate({
                                            path:'product.product_detail',
                                            select:'product_type',
                                            populate:{
                                            path:'supplier_id brand',
                                            select:'name brand_name'
                                          }
                                        })  
                                  .sort({"order_date": 1})
                                  .lean();
      }
      let json ={};
      json.count = count;
      json.order = data
      return json;
    } catch (error) {
      throw error;
    }

    // try {
    //   let data;
    //   let json ={};
    //   if(limit != null){
    //     data =  await OrderModel.aggregate([
    //       {
    //         $match: {
    //           $and:[{order_date:{$gte:new Date(startDate),$lte:new Date(endDate)}},{order_status:'delivered'}]
    //       }
    //       },
    //       {
    //               '$lookup': {
    //                 from: 'product',
    //                 localField: 'product.product_detail',
    //                 foreignField: '_id',
    //                 as: 'products'
    //               }
    //             }, 
    //             { '$unwind': '$products' },
    //             {
    //               '$lookup': {
    //                 from: 'brand',
    //                 localField: 'products.brand',
    //                 foreignField: '_id',
    //                 as: 'brands'
    //               }
    //             }, 
    //             { '$unwind': '$brands' },
                
    //             {
    //               "$group" : {
    //                 "_id" : '$brands.brand_name',
    //                 "brand_name": { $first: "$brands.brand_name" },
    //                 "total_amount_aed" : {
    //                     "$sum" : {$toDecimal: {$arrayElemAt:["$product.total_price_aed",0]}}
    //                 },
    //                 "total_amount_usd" : {
    //                   "$sum" : {$toDecimal: {$arrayElemAt:["$product.total_price_usd",0]}}
    //               },
    //               "total_quantity" : {
    //                 "$sum" : { $arrayElemAt:[ "$product.quantity", 0 ]}
    //             }
    //             }
                
    //           },
    //           // {
    //           //     "$skip" : skip
    //           // },
    //           // {
    //           //     "$limit" : limit
    //           // },
    //           {$facet: {
    //             paginatedResults: [{ $skip: skip }, { $limit: limit }],
    //             totalCount: [
    //               {
    //                 $count: 'count'
    //               }
    //             ]
    //           }}
             
    //     ])
    //     if(data&&data[0]&&data[0].paginatedResults&& data[0].paginatedResults.length>1){
    //       json.count = data[0].totalCount[0].count;
  
    //     }else{
    //       json.count = data[0].totalCount.length;
  
    //     }
    //     json.data = data[0].paginatedResults;
    //   }else{
    //     data = await OrderModel.aggregate([
    //       {
    //               '$lookup': {
    //                 from: 'product',
    //                 localField: 'product.product_detail',
    //                 foreignField: '_id',
    //                 as: 'products'
    //               }
    //             }, 
    //             { '$unwind': '$products' },
    //             {
    //               '$lookup': {
    //                 from: 'brand',
    //                 localField: 'products.brand',
    //                 foreignField: '_id',
    //                 as: 'brands'
    //               }
    //             }, 
    //             { '$unwind': '$brands' },
    //             { $match: {
    //               $and:[{order_date:{$gte:new Date(startDate),$lte:new Date(endDate)}},{order_status:'delivered'}]
    //             }},
    //             {
    //               "$group" : {
    //                   "_id" : '$brands.brand_name',
    //                   "brand_name": { $first: "$brands.brand_name" },
                      
    //                   "total_amount_aed" : {
    //                       "$sum" : {$toDecimal: {$arrayElemAt:["$product.total_price_aed",0]}}
    //                   },
    //                   "total_amount_usd" : {
    //                     "$sum" : {$toDecimal: {$arrayElemAt:["$product.total_price_usd",0]}}
    //                 },
    //                 "total_quantity" : {
    //                   "$sum" : { $arrayElemAt:[ "$product.quantity", 0 ]}
    //               }
    //               }
                
    //           }
             
    //     ])
    //     json.count = data.length;
    //     json.data = data;
    //   }
      
     
    //   return json
    // } catch (error) {
    //   throw error;
    // }
  },
};
export default ReportService;