import { Component, OnInit, HostBinding } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { fadeInAnimation } from '../../../shared/animations/fade-in.animation';
import { Store } from '@ngrx/store';
import * as fromAuth from '../../store/reducers';
import * as Auth from '../../store/actions/auth.actions';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import {AuthService} from '../../services/auth.service';
import { ToastyService } from 'ng2-toasty';



@Component({
  selector: 'app-forget-password',
  animations: [fadeInAnimation],
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss']
})
export class ForgetPasswordComponent implements OnInit {

  redirectSubs: Subscription;
  returnUrl: string;
  submitted:Boolean=false;

  constructor(public formBuilder: FormBuilder,
    public store: Store<fromAuth.State>,
    private route: ActivatedRoute,
    private router: Router,
    private ds : AuthService,
    private toasty: ToastyService,
  ) { }

  form: FormGroup = this.formBuilder.group({
    email: ['', Validators.required],
  });
  ngOnInit() {
    this.store.dispatch(new Auth.Authorized());
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.redirectIfUserLoggedIn();
  }
  redirectIfUserLoggedIn() {
    this.redirectSubs = this.store
      .select(fromAuth.getLoggedIn)
      .subscribe(authed =>
        authed === true ? this.router.navigate([this.returnUrl]) : null
      );
  }
  get f() { return this.form.controls; }

  @HostBinding('@fadeInAnimation')
  public animateMe = true;
  submit() {
    this.submitted =true;
    let formValue = Object.assign({}, this.form.value);
    if(this.form.valid){
      this.ds.forgetPassword(formValue).subscribe(data => {
        setTimeout(() => {
          this.router.navigate(['/auth/login'])
        }, 5000);
      });
    }
  }

}
