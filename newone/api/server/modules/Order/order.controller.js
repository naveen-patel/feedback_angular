import OrderService from "./order.service";
import SequenceService from "../Sequence/Sequence.service";
import UserService from "../User/user.service";
import CartService from "../Cart/cart.service";
import request from "request";

import logger from "../../../config/logger.config";
var _ = require("lodash");
import settings from "../../../settings";
let config = require("./../../../config/" + settings.environment + ".config")
  .default;
const MEDIA = config.media;
let mkdirp = require("mkdirp");
var uuidv1 = require("uuid/v1");
const fs = require("fs");
const path = require("path");
var pdf = require("html-pdf");
let Promise = require("bluebird");
import moment from "moment";
const nodemailer = require("nodemailer");
const api_end_point = config.api_end_point;
import EmailService from "../CommonService/email.service";
import SettingsService from "../Settings/settings.service";
import { cpus } from "os";
import ProductService from "../Product/Product.service";
import OrderModel from "./order.model";
var pdfCount = 0,
  excelCount = 0;
var imgSrc = path.normalize(
  "file://" + __dirname + "../../../../logo/logo.png"
);
var fmcg_logo = path.normalize(
  "file://" + __dirname + "../../../../logo/FMCG_logo.png"
);

const orderController = {
  getOrderById: async (req, res) => {
    try {
      const order = await OrderService.getOrderById(req.params.order_id);
      logger.info(" Getting order by id = " + req.params.order_id);
      res.send({
        code: 200,
        status: "success",
        message: "List the order",
        data: order
      });
    } catch (error) {
      logger.error("Error in getting order :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting order",
        data: {}
      });
    }
  },
  createOrder: async (req, res) => {
    try {
      if (!req.body) res.semd({ code: 400, message: "Invalid call" });
      // let setting = await SettingsService.getAllsettings();
      const user = await UserService.getUserByUserId(req.params.user_id);
      let lastOrdercode = await SequenceService.getSequenceBykey("order");
      let orderCode = lastOrdercode + 1;
      let order = {},
        aed = 0,
        usd = 0;
      order.product = [];
      order.user_id = req.params.user_id;
      order.order_number = await digitorderCode(orderCode);
      order.order_date = req.body.order_date;
      order.total_amount_aed = req.body.total_amount_aed;
      order.total_amount_usd = req.body.total_amount_usd;

      if (req.body && req.body.cart_id) {
        await CartService.removeProductFromCart(req.body._id);
      }
      let temp_total_amount_aed = 0,
        temp_total_amount_usd = 0;
      Promise.map(req.body.product, async item => {
        let deal = {};
        deal.isActive = item.product_id.deal.isActive;
        deal.offer_price_aed = item.product_id.deal.sales_per_case_aed;
        deal.offer_price_usd = item.product_id.deal.sales_per_case_usd;
        deal.deal_percentage = item.product_id.deal.deal_percentage || 0;
        if (item && item._id) {
          await CartService.removeProductFromCart(item._id);
        }
        let product = await ProductService.getProductById(item.product_id._id);
        let newQuantity = Math.abs(
          Number(product.quantity) - Number(item.quantity)
        );
        await ProductService.updateQuantity(item.product_id._id, newQuantity);
        let temp_total_price_aed = 0;
        let temp_total_price_usd = 0;
        if (item.product_id.deal && item.product_id.deal.isActive) {
          temp_total_amount_aed +=
            Number(item.product_id.deal.sales_per_case_aed) * item.quantity;
          temp_total_amount_usd +=
            Number(item.product_id.deal.sales_per_case_usd) * item.quantity;
          temp_total_price_aed =
            Number(item.product_id.deal.sales_per_case_aed) * item.quantity;
          temp_total_price_usd =
            Number(item.product_id.deal.sales_per_case_usd) * item.quantity;
        } else {
          temp_total_amount_aed +=
            Number(product.sales_per_case_aed) * item.quantity;
          temp_total_amount_usd +=
            Number(product.sales_per_case_usd) * item.quantity;
          temp_total_price_aed =
            Number(product.sales_per_case_aed) * item.quantity;
          temp_total_price_usd =
            Number(product.sales_per_case_usd) * item.quantity;
        }
        order.product.push({
          product_detail: item.product_id._id,
          quantity: item.quantity,
          sales_per_case_aed: product.sales_per_case_aed,
          sales_per_case_usd: product.sales_per_case_usd,
          deal: deal,
          total_price_aed: temp_total_price_aed,
          total_price_usd: temp_total_price_usd
        });
        if (order.product.length === req.body.product.length) return;
      })
        .then(async success => {
          if (
            temp_total_amount_aed.toFixed(2) !=
            Number(req.body.total_amount_aed)
          )
            order.total_amount_aed = temp_total_amount_aed.toFixed(2);
          if (
            temp_total_amount_usd.toFixed(2) !=
            Number(req.body.total_amount_usd)
          )
            order.total_amount_usd = temp_total_amount_usd.toFixed(2);
          let orderData = await OrderService.createOrder(order);
          await SequenceService.updateSequenceBykey("order", orderCode);
         
          let sid = 'FMCGmarkets'
          if(user.country_code=='91'|| user.country_code=='+91'){
            sid ='FMCGmt'
          }else if(user.country_code=='+966'|| user.country_code=='966'){
            sid ='FMCGmrkt-AD'
          }else{
            sid = 'FMCGmarkets';
          }
          let message =
            "Dear Customer,Thank you for using FMCG markets. Your order has been placed successfully and the order number is " +
            orderData.order_number +
            ".You can track your order";
          request(
            {
              uri:
                config.sms.endpoint +
                "mobilenumber=" +
                user.country_code +
                user.mobile_number +
                "&message=" +
                message +
                "&sid=" +
                sid +
                "&mtype=N" +
                "&DR=Y"
            },
            (error, response, body) => {
              console.log(
                "place order ->",
                body,
                user.country_code + user.mobile_number
              );
              logger.info("order placed by" + req.params.user_id);
              res.send({
                code: 200,
                status: "success",
                message: "order placed succesfully",
                data: orderData
              });
            }
          );

          // let imagepath = api_end_point + '/image/logo/';

          // let EMAIL_ORDER_HTML = await fs.readFileSync(
          //   path.join(__dirname, "../../EmailTemplates/customerOrderNotification.html"),
          //   "utf-8"
          // );
          // let EMAIL_SALES_ORDER_HTML = await fs.readFileSync(
          //   path.join(__dirname, "../../EmailTemplates/salesOrderNotification.html"),
          //   "utf-8"
          // );
          // EMAIL_ORDER_HTML = EMAIL_ORDER_HTML.
          // replace('{firstname}',customerResult.first_name)
          // .replace('{otp}',otp)
          // .replace('{time}',duration+" "+m)
          // .replace('{logo}', imagepath + 'maxbrands.png');
          // EmailService.sendEmail(
          //   customerResult.email,
          //   "OTP for forget password",
          //   EMAIL_ORDER_HTML,setting,[]
          // );

          // EMAIL_SALES_ORDER_HTML = EMAIL_SALES_ORDER_HTML.
          // replace('{firstname}','Sales Team')
          // .replace('{otp}',otp)
          // .replace('{time}',duration+" "+m)
          // .replace('{logo}', imagepath + 'maxbrands.png');
        })
        .catch(error => {
          logger.error("Error in placing order " + error);
          res.status(400).send({
            code: 400,
            status: "error",
            message: "Error in placing order",
            data: []
          });
        });
    } catch (error) {
      logger.error("Error in placing order " + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in placing order",
        data: []
      });
    }
  },
  updateOrderStatus: async (req, res) => {
    try {
      let orderId = req.params.order_id;
      let tempObject = {};
      if (req.body.shipping_address && req.body.user_id) {
        await UserService.updateUserById(req.body.user_id, {
          shipping_address: req.body.shipping_address
        });
      }
      if (req.body.callback_request) {
        await OrderService.updateCallbackStatus(
          orderId,
          req.body.callback_request
        );
      }
      if (req.body.orderStatus) {
        tempObject.order_status = req.body.orderStatus.toLowerCase();
      }
      if (req.body.product) {
        let deal = {};
        if (req.body.product.deal && req.body.product.deal.isActive) {
          deal.isActive = req.body.product.deal.isActive;
          deal.offer_price_aed = req.body.product.deal.sales_per_case_aed;
          deal.offer_price_usd = req.body.product.deal.sales_per_case_usd;
          deal.deal_percentage = req.body.product.deal.deal_percentage || 0;
        } else {
          deal.isActive = false;
          deal.offer_price_aed = null;
          deal.offer_price_usd = null;
          deal.deal_percentage = 0;
        }
        let oldOrder = await OrderService.getOrderById(orderId);
        tempObject.product = [];
        let product = await ProductService.getProductById(
          req.body.product.product_id._id
        );
        let tempQuantity = Math.abs(
          Number(req.body.product.quantity.new) -
            Number(req.body.product.quantity.old)
        );
        let newUSDPrice,newAEDPrice;
          newUSDPrice = Math.abs(
            Number(req.body.product.total_usd.new) -
              Number(req.body.product.total_usd.old)
          );
          newAEDPrice = Math.abs(
            Number(req.body.product.total_aed.new) -
              Number(req.body.product.total_aed.old)
          );
        
        let temp_total_price_aed = Number(req.body.product.total_aed.new);
        let temp_total_price_usd = Number(req.body.product.total_usd.new);
        let isPlus = false,
          newQuantity;
        if (Number(req.body.product.quantity.new) -Number(req.body.product.quantity.old) <0) {
          newQuantity = Math.abs(Number(product.quantity) + tempQuantity);
          isPlus = false;
        } else {
          newQuantity = Math.abs(Number(product.quantity) - tempQuantity);
          isPlus = true;
        }
        await ProductService.updateQuantity(
          req.body.product.product_id._id,
          newQuantity
        );
        if (req.body.flag) {
          //add
          tempObject.total_amount_aed =
            Number(oldOrder.total_amount_aed) + newAEDPrice;
          tempObject.total_amount_usd =
            Number(oldOrder.total_amount_usd) + newUSDPrice;
          // tempObject.product.push({product_detail:req.body.product.product_id._id,isAdd:false,quantity:Number(req.body.product.quantity.new),sales_per_case_aed:req.body.product.sales_per_case_aed,sales_per_case_usd:req.body.product.sales_per_case_usd,discount_price_aed:req.body.product.discount_price_aed || "00.00",discount_price_usd:req.body.product.discount_price_usd || "00.00"});
          if (req.body.isMobile) {
            tempObject.product.push({
              product_detail: req.body.product.product_id._id,
              isAdd: true,
              quantity: Number(req.body.product.quantity.new),
              sales_per_case_aed: req.body.product.sales_per_case_aed,
              sales_per_case_usd: req.body.product.sales_per_case_usd,
              discount_price_aed:
                req.body.product.discount_price_aed || "00.00",
              discount_price_usd:
                req.body.product.discount_price_usd || "00.00",
              deal: deal,
              total_price_aed: temp_total_price_aed,
              total_price_usd: temp_total_price_usd
            });

            let is = await OrderService.checkProduct(
              req.body.product.product_id._id,
              orderId
            );
            if (is) return res.status(200).send({ code: 200, status: "Exist" });
          } else {
            tempObject.product.push({
              product_detail: req.body.product.product_id._id,
              isAdd: req.body.isAdd,
              quantity: Number(req.body.product.quantity.new),
              sales_per_case_aed: req.body.product.sales_per_case_aed,
              sales_per_case_usd: req.body.product.sales_per_case_usd,
              discount_price_aed:
                req.body.product.discount_price_aed || "00.00",
              discount_price_usd:
                req.body.product.discount_price_usd || "00.00",
              deal: deal,
              total_price_aed: temp_total_price_aed,
              total_price_usd: temp_total_price_usd
            });
          }
          await OrderService.updateOrderStatus(orderId, tempObject, 1);
        } else {
          //edit
          if (req.body.isMobile) {
            tempObject.product.push({
              product_detail: req.body.product.product_id._id,
              isAdd: true,
              quantity: Number(req.body.product.quantity.new),
              sales_per_case_aed: req.body.product.sales_per_case_aed,
              sales_per_case_usd: req.body.product.sales_per_case_usd,
              discount_price_aed:
                req.body.product.discount_price_aed || "00.00",
              discount_price_usd:
                req.body.product.discount_price_usd || "00.00",
              deal: deal,
              total_price_aed: temp_total_price_aed,
              total_price_usd: temp_total_price_usd
            });
          } else {
            tempObject.product.push({
              product_detail: req.body.product.product_id._id,
              isAdd: req.body.isAdd,
              quantity: Number(req.body.product.quantity.new),
              sales_per_case_aed: req.body.product.sales_per_case_aed,
              sales_per_case_usd: req.body.product.sales_per_case_usd,
              discount_price_aed:
                req.body.product.discount_price_aed || "00.00",
              discount_price_usd:
                req.body.product.discount_price_usd || "00.00",
              deal: deal,
              total_price_aed: temp_total_price_aed,
              total_price_usd: temp_total_price_usd
            });
          }
          if (isPlus) {
            if(req.body.isAdd == false){
              if (Number(req.body.product.total_usd.new) - Number(req.body.product.total_usd.old) >0) {
                tempObject.total_amount_aed =
                Math.abs(Number(oldOrder.total_amount_aed) + newAEDPrice);
              tempObject.total_amount_usd =
              Math.abs(Number(oldOrder.total_amount_usd) + newUSDPrice);
              }else{
                tempObject.total_amount_aed =
                Math.abs(Number(oldOrder.total_amount_aed) - newAEDPrice);
              tempObject.total_amount_usd =
              Math.abs(Number(oldOrder.total_amount_usd) - newUSDPrice);
              }
            }else{
              tempObject.total_amount_aed =
              Number(oldOrder.total_amount_aed) + newAEDPrice;
            tempObject.total_amount_usd =
              Number(oldOrder.total_amount_usd) + newUSDPrice;
            }
           
          } else {
            tempObject.total_amount_aed =
            Math.abs(Number(oldOrder.total_amount_aed) - newAEDPrice);
            tempObject.total_amount_usd =
            Math.abs(Number(oldOrder.total_amount_usd) - newUSDPrice);
          }
          
          await OrderService.updateOrderStatus(
            orderId,
            tempObject,
            3,
            product._id
          );
        }
        logger.info(" Order status updated");
        return res.send({
          code: 200,
          status: "success",
          message: "Status updated",
          data: []
        });
      } 
      
        if (req.body.orderStatus == "cancelled") {
          let Order = await OrderService.getOrderById(orderId);
          Order.product.forEach(async item => {
            let p = await ProductService.getProductById(item.product_detail);
            let nQ = Math.abs(Number(p.quantity) + item.quantity);
            await ProductService.updateQuantity(item.product_detail, nQ);
          });
        }
        let updated = await OrderService.updateOrderStatus(
          orderId,
          tempObject,
          2
        );
        const user = await UserService.getUserByUserId(req.body.user_id);
        if (
          req.body.orderStatus == "dispatched" || 
          req.body.orderStatus == "delivered" ||
          req.body.orderStatus == "cancelled"
        ) {
          let sid = 'FMCGmarkets'
          if(user.country_code=='91'|| user.country_code=='+91'){
            sid ='FMCGmt'
          }else if(user.country_code=='+966'|| user.country_code=='966'){
            sid ='FMCGmrkt-AD'
          }else{
            sid = 'FMCGmarkets';
          }
          let message =
            "Dear Customer, your order has been " +
            req.body.orderStatus +
            ". Thank You for using FMCG markets";
          request(
            {
              uri:
                config.sms.endpoint +
                "mobilenumber=" +
                user.country_code +
                user.mobile_number +
                "&message=" +
                message +
                "&sid=" +
                sid+
                "&mtype=N" +
                "&DR=Y"
            },
            (error, response, body) => {
              logger.info(
                "TCL: updated order ->",
                body,
                user.country_code + user.mobile_number
              );
              logger.info(" Order status updated");
              res.send({
                code: 200,
                status: "success",
                message: "Status updated",
                data: []
              });
            }
          );
        }else{
          logger.info(" Order status updated");
          res.send({
            code: 200,
            status: "success",
            message: "Status updated",
            data: []
          });
        }
      
    } catch (error) {
      logger.error("Error in updating order status :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in updating order status",
        data: []
      });
    }
  },
  updateCallbackStatus: async (req, res) => {
    try {
      let order_id = req.params.order_id;
      let callback_request = req.body.callback_request;
      let user_id = req.params.user_id;
      let updatedStatus = await OrderService.updateCallbackStatus(
        order_id,
        callback_request
      );
      // let setting = await SettingsService.getAllsettings();
      // let imagepath = api_end_point + '/image/logo/';
      // let EMAIL_CALLBACK_REQUEST_HTML = await fs.readFileSync(
      //         path.join(__dirname, "../../EmailTemplates/callbackRequest.html"),
      //         "utf-8"
      //       );
      //       EMAIL_CALLBACK_REQUEST_HTML = EMAIL_CALLBACK_REQUEST_HTML.
      //       replace('{firstname}',customerResult.first_name)
      //       .replace('{otp}',otp)
      //       .replace('{time}',duration+" "+m)
      //       .replace('{logo}', imagepath + 'maxbrands.png');
      //     EmailService.sendEmail(
      //         customerResult.email,
      //         "OTP for forget password",
      //         EMAIL_CALLBACK_REQUEST_HTML,setting,[]
      //       );
      logger.info("request for callback " + order_id);

      res.send({
        code: 200,
        status: "success",
        message: "callback request ",
        data: updatedStatus
      });
    } catch (error) {
      logger.error("Error in requesting callback " + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in requesting callback",
        data: []
      });
    }
  },
  getAllOrder: async (req, res) => {
    try {
      var search, isFilter;
      if (_.isEmpty(req.query.searchkey)) {
        search = "";
      } else {
        search = req.query.searchkey;
      }
      let limit = Number(req.query.limit) || 10;
      let offset = req.query.skip ? parseInt(req.query.skip) : 1;
      let skip = (offset - 1) * limit;
      const categoryData = await OrderService.getAllOrder(search, limit, skip);
      logger.info(" Getting all orders");
      res.send({
        code: 200,
        status: "success",
        message: "Listing All orders",
        data: categoryData
      });
    } catch (error) {
      logger.error("Error in getting orders :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting orders record",
        data: []
      });
    }
  },
  getAllOrderByUser: async (req, res) => {
    try {
      let isFilter;
      if (req.query.filter === "all") {
        isFilter = false;
      } else {
        isFilter = true;
      }
      let limit = Number(req.query.limit) || 10;
      let offset = req.query.skip ? parseInt(req.query.skip) : 1;
      let skip = (offset - 1) * limit;
      const categoryData = await OrderService.getAllOrderByUser(
        req.params.user_id,
        isFilter,
        req.query.filter,
        limit,
        skip
      );
      logger.info(" Getting all orders");
      res.send({
        code: 200,
        status: "success",
        message: "Listing All orders",
        data: categoryData
      });
    } catch (error) {
      logger.error("Error in getting orders :" + error);
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting orders record",
        data: []
      });
    }
  },
  deleteOrder: async (req, res) => {
    try {
      var ids = req.params.order_id.split(",");
      var data = await OrderService.deleteOrderByIds(ids);

      res.send({
        code: 200,
        status: "success",
        message: "order is successfully deleted",
        data: data
      });
    } catch (error) {
      res.status(400).send({
        code: 400,
        status: "error",
        message: "order Does not delete",
        data: {}
      });
    }
  },
  createPDF: async (req, res) => {
    try {
      var html = fs.readFileSync(
        "./server/EmailTemplates/order_template_pdf.html",
        "utf8"
      );
      var options = {
        format: "A4",
        orientation: "portrait",
        Content_Type: "application/pdf",
        border: {
          top: "0.2in",
          right: "0.2in",
          bottom: "0.2in",
          left: "0.2in"
        },
        header: {
          height: "5mm",
          contents:
            "<div> <img style='width:70px;height:45px'src='" +
            fmcg_logo +
            "' /><img style='width:200px;height:35px'src='" +
            imgSrc +
            "' align='right' /></div><hr style='border-color: rgb(255, 0, 0);' size='1'>"
        },
        footer: {
          height: "16mm",
          contents: {
            default:
              '<hr style="border-color:rgb(255, 0, 0);" size="1"><div style="font-size: 7px;color: #949393;">Maxbrands Marketing</div><div style="font-size: 7px;color: #949393;">Hotline M: 0097154 461 4848</div><div style="font-size: 7px;color: #949393;">PO Box 239492, Dubai, UAE. | T: +971 4250 1134 | E:info@maxbrandsintl.com WEB <a style="font-size: 7px;color: #949393;" href="http://www.maxbrandsintl.com">www.maxbrandsintl.com</a></div>'
          }
        },
        timeout: "1200000"
      };

      let count = 0;
      let defaultBreak = 19;
      let strOrderList = "";
      const orders = await OrderService.getOrderById(req.query.order_id);
      const user = await UserService.getUserByUserId(orders.user_id);
      let order_number = orders.order_number;
      let day, month;
      if (new Date(orders.order_date).getDate() > 9) {
        day = new Date(orders.order_date).getDate();
      } else {
        day = "0" + new Date(orders.order_date).getDate();
      }
      if (new Date(orders.order_date).getMonth() > 9) {
        month = new Date(orders.order_date).getMonth();
      } else {
        month = "0" + new Date(orders.order_date).getMonth();
      }
      let order_date =
        day + "-" + month + "-" + new Date(orders.order_date).getFullYear();
      let name = user.first_name + " " + user.last_name + " ";
      let address = name.concat(user.shipping_address);
      let total_AED = !Number.isInteger(Number(orders.total_amount_aed))
        ? Number(orders.total_amount_aed).toFixed(2)
        : orders.total_amount_aed + ".00";
      let total_USD = !Number.isInteger(Number(orders.total_amount_usd))
        ? Number(orders.total_amount_usd).toFixed(2)
        : orders.total_amount_usd + ".00";

      orders.product.forEach((product, i) => {
        let deal = product.product_detail.deal
          ? product.product_detail.deal
          : "";
        let total_amount;
        if (deal && deal.offer_price_aed) {
          total_amount = Number(product.quantity) * Number(deal.offer_price_aed)
            
        } else {
          total_amount = Number(product.product_detail.discount_price_aed) >0? Number(product.quantity) *Number(product.product_detail.discount_price_aed):Number(product.quantity) *Number(product.product_detail.sales_per_case_aed)
        }
        // let netAmount = Number(product.product_detail.sales_per_case_aed).toFixed(2);
        let disAmmount = deal.offer_price_aed
          ? Number(deal.offer_price_aed)
          : Number(product.product_detail.discount_price_aed) >0? Number(product.product_detail.discount_price_aed):Number(product.product_detail.sales_per_case_aed)
        strOrderList += `<tr>
        <td style="font-size: 8px">${i + 1}</td>
        <td style="font-size:  8px">${
          product.product_detail.brand
            ? product.product_detail.brand.brand_name
            : ""
        }</td>
        <td style="font-size:  8px">${
          product.product_detail.supplier_id.name
        }</td>
        <td style="font-size:  8px">${product.quantity}</td>
        <td style="font-size:  8px">${
          deal.deal_percentage ? deal.deal_percentage : "00"
        }%</td>
        <td style="font-size:  8px;text-align:right">${Number(
          disAmmount
        ).toFixed(2)}</td>
        <td style="font-size:  8px;text-align:right">${Number(product.product_detail.sales_per_case_aed).toFixed(2)}</td>
        <td style="font-size:  8px;text-align:right">${Number(
          total_amount.toFixed(2)
        ).toFixed(2)}</td></tr>`;
        count++;
        if (count >= defaultBreak && i + 1 < orders.product.length) {
          count = 0;
          defaultBreak = 27;
          console.log("additional br first" + count);
          strOrderList +=
            "</table><div style='page-break-before: always;break-before: always;'></div><br><br><br><br><br><table style='width:100%'>";
        } else if (count === orders.product.length) {
          strOrderList += "</table>";
        }
      });
      pdfCount++;
      const fileName = "order_pdf(" + pdfCount + ").pdf";
      const filePath = path.join(__dirname + "/pdf", fileName);
      // strOrderList = strOrderList.replace(/{logo}/g, config.api_end_point + '/image/logo/logo.png');
      var pdftemplate = html

        .replace(/{order_number}/g, order_number)
        .replace(/{order_date}/g, order_date)
        .replace(/{address}/g, address)
        .replace(/{strOrderList}/g, strOrderList)
        .replace(/{total_AED}/g, total_AED)
        .replace(/{total_USD}/g, total_USD);
      pdftemplate +=
        "<div id='pageHeader'><div style='display:flex;flex-wrap: wrap; justify-content:space-between'> <img style='width:200px;height:40px'src='" +
        fmcg_logo +
        "' /> <img style='width:200px;height:40px'src='" +
        imgSrc +
        "' /></div><hr style='border-color: rgb(255, 0, 0);' size='1'></div>";

      pdf.create(pdftemplate, options).toFile(filePath, function(err, pdfPath) {
        if (err) {
          return res.status(400).send({
            code: 400,
            status: "error",
            message: "Error in getting order",
            data: {}
          });
        } else {
          setTimeout(function() {
            if (fs.existsSync(filePath)) {
              fs.unlinkSync(filePath);
            }
          }, 300000);
          return res.send({
            code: 200,
            status: "success",
            message: "Creating PDF file for Order",
            data: {
              path: config.api_end_point + "/api/order/report/pdf/" + fileName,
              extension: "pdf"
            }
          });
        }
      });
    } catch (error) {
      return res.status(400).send({
        code: 400,
        status: "error",
        message: "Error in getting Product",
        data: {}
      });
    }
  },
  pdfFileDownload: async (req, res) => {
    try {
      var fileName = req.params.file_name;
      const filePath = path.join(__dirname + "/pdf", fileName);
      res.download(filePath);
    } catch (error) {
      res.status(400).send({
        code: 400,
        status: "error",
        message: "Error occured",
        data: {}
      });
    }
  },
  removeProductFromOrder: async (req, res) => {
    try {
      let order_id = req.params.order_id;
      let product_id = req.params.product_id;
      let quantity = req.query.quantity;
      let product = await ProductService.getProductById(product_id);
      let o = await OrderService.getOrderById(order_id);
      let newQuantity = Number(product.quantity) + Number(quantity);
      let usd =
        Number(o.total_amount_usd) -
        Number(req.query.priceUSD) * Number(quantity);
      let aed =
        Number(o.total_amount_aed) -
        Number(req.query.priceAED) * Number(quantity);
      console.log(
        "TCL: aed",
        aed,
        o.total_amount_aed,
        Number(req.query.priceAED),
        Number(quantity)
      );
      await OrderService.updateOrderStatus(
        order_id,
        { total_amount_aed: aed, total_amount_usd: usd },
        2
      );
      let removedResult = await OrderService.removeProductFromOrder(
        order_id,
        product_id
      );
      await ProductService.updateQuantity(product_id, newQuantity);
      return res.send({
        code: 200,
        status: "success",
        message: "product removed from order successfully",
        data: removedResult
      });
    } catch (error) {
      res.status(400).send({
        code: 400,
        status: "error",
        message: "product  Does not removed from order",
        data: {}
      });
    }
  }
};
let digitorderCode = async digit => {
  return await new Promise((resolve, reject) => {
    var len = digit.toString().length;
    var strDigit = "";
    switch (len) {
      case 0:
        strDigit = "000000";
        break;
      case 1:
        strDigit = "00000" + digit;
        break;
      case 2:
        strDigit = "0000" + digit;
        break;
      case 3:
        strDigit = "000" + digit;
        break;
      case 4:
        strDigit = "00" + digit;
        break;
      case 5:
        strDigit = "0" + digit;
        break;
      case 6:
        strDigit = "" + digit;
        break;
    }
    resolve(strDigit);
  });
};
export default orderController;
