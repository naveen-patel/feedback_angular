var mongoose = require('mongoose');
import settings from '../../../settings';

let config = require('./../../../config/' + settings.environment + '.config').default;
const nodemailer = require('nodemailer');
const s_key = config.s_key;
const SettingsController = {};
const Cryptr = require('cryptr');
const cryptr = new Cryptr(s_key);

const EmailService = {
    sendEmail: async (toMail, subject, body, setting, attachment) => {
        try {
            var password = cryptr.decrypt(setting.password);
            var transporter;
            var  secure =(setting.port==465?true:false)
            transporter = nodemailer.createTransport({
                host: setting.host, // Gmail Host
                port: setting.port, // Port
                secure: secure , // this is true as port is 465
                auth: {
                    user: setting.email, // generated ethereal user
                    pass: password // generated ethereal password
                }
            });
            var attachments = attachment
            let mailOptions = {
                from:  setting.email,
                to: toMail, // Recepient email address. Multiple emails can send separated by commas
                subject: subject,
                //text: body,
                html: body,
                attachments: attachments,

            };
            transporter.sendMail(mailOptions, (error, info) => {
                console.log(info,error)
                if (error) {
                    return console.log(error,'error');
                }
                console.log('Message sent: %s', info.messageId);
            });

        } catch (error) {
            console.log(error,"error")
        }
    }
}
export default EmailService;